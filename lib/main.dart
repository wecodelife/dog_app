import 'package:app_template/src/app.dart';
import 'package:app_template/src/screens/testing_page.dart';
import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/widgets/market_product_template.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:hive/hive.dart';
import 'package:path_provider/path_provider.dart' as path_provider;

void main() async{
  SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
      statusBarColor: Constants.kitGradients[17],
      statusBarIconBrightness: Brightness.dark
  ));
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  final appDocDir = await path_provider.getApplicationDocumentsDirectory();
  Hive.init(appDocDir.path);
  await Hive.openBox(Constants.BOX_NAME);
  runApp(MyApp());

}