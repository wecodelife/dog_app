import 'dart:io';

import 'package:app_template/src/screens/notification_page.dart';
import 'package:app_template/src/screens/splash_screen.dart';
import 'package:app_template/src/screens/testing_page.dart';
import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/object_factory.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:firebase_analytics/observer.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';


Future<dynamic> myBackgroundMessageHandler(Map<String, dynamic> message) {
  print("Background Message Handler");
  if (message.containsKey('data')) {
// Handle data message
    final dynamic data = message['data'];
  }

  if (message.containsKey('notification')) {
// Handle notification message
    final dynamic notification = message['notification'];
  }

// Or do other work.
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  FirebaseAnalytics analytics = FirebaseAnalytics();
  final GlobalKey<NavigatorState> navigatorKey =
  GlobalKey(debugLabel: "MainNavigator");
  final FirebaseMessaging _firebaseMessaging = FirebaseMessaging();
  AndroidNotificationChannel channel = AndroidNotificationChannel(
      'high_importance_channel', // id
      'High Importance Notifications', // title
      'This channel is used for important notifications.', // description
      importance: Importance.high,
      playSound: true);
  FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
  new FlutterLocalNotificationsPlugin();
  Future<void> openHiveBox() async {
   // await Hive.openBox<EventDataModel>(Constants.EVENT_BOX_NAME);
    //await Hive.openBox<PostDataModel>(Constants.POST_BOX_NAME);
     ObjectFactory().appHive.deleteEmail();
    // ObjectFactory().appHive.deleteEventCount();
    // ObjectFactory().appHive.deleteInterestedList();
    // ObjectFactory().appHive.deleteLikeCount();
    //ObjectFactory().appHive.deletePostsList();
    // ObjectFactory().appHive.deleteLikedPosts();
    // ObjectFactory().appHive.deletePostCount();
  }
  // @override
  // void initState() {
  //   init();
  //   openHiveBox();
  //   // final pushNotificationService = PushNotificationService(_firebaseMessaging);
  //   // pushNotificationService.initialise();
  //   // TODO: implement initState
  //   super.initState();
  // }
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'WoBoW',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
      ),
      home: SplashScreen(),
      navigatorKey: navigatorKey,
      navigatorObservers: [
        FirebaseAnalyticsObserver(analytics: analytics),
      ],
      // home: TestingPage(),
    );
  }
  void init() {
    var initializationSettingsAndroid =
    new AndroidInitializationSettings('@mipmap/ic_launcher');

    var initializationSettingsIOS = new IOSInitializationSettings(
        onDidReceiveLocalNotification: onDidRecieveLocalNotification);

    var initializationSettings = new InitializationSettings(
        android: initializationSettingsAndroid, iOS: initializationSettingsIOS);

    flutterLocalNotificationsPlugin.initialize(initializationSettings,
        onSelectNotification: onSelectNotification);
    print("channel Id:" + channel.id.toString());
    // flutterLocalNotificationsPlugin
    //     .resolvePlatformSpecificImplementation<i
    //     AndroidFlutterLocalNotificationsPlugin>()
    //     ?.createNotificationChannel(channel);
    print("1 local notification");

    getMessage();

    _firebaseMessaging.requestNotificationPermissions(
        const IosNotificationSettings(sound: true, badge: true, alert: true));
    _firebaseMessaging.onIosSettingsRegistered
        .listen((IosNotificationSettings settings) {
      print("Settings registered: $settings");
    });
    _firebaseMessaging.getToken().then((String token) {
      print("hvhv" + token);
      ObjectFactory().appHive.putDeviceToken(token: token);
      assert(token != null);
    });
  }
  Future onDidRecieveLocalNotification(
      int id, String title, String body, String payload) async {
    // display a dialog with the notification details, tap ok to go to another page
    showDialog(
      context: context,
      builder: (BuildContext context) => new CupertinoAlertDialog(
        title: new Text(title),
        content: new Text(body),
        actions: [
          CupertinoDialogAction(
            isDefaultAction: true,
            child: new Text('Ok'),
            onPressed: () async {
              Navigator.of(context, rootNavigator: true).pop();
              await showInfoAnimatedToast(
                  msg: "Notification Clicked", context: context);
            },
          ),
        ],
      ),
    );
  }

  Future onSelectNotification(String payload) async {
    if (payload != null) {
      debugPrint('notification payload: ' + payload);
      navigatorKey.currentState
          .push(MaterialPageRoute(builder: (context) => NotificationPage()));
    }
    await showInfoAnimatedToast(msg: "Notification Clicked", context: context);
    /*Navigator.push(
      context,
      new MaterialPageRoute(builder: (context) => new SecondScreen(payload)),
    );*/
  }

  void getMessage() {
    _firebaseMessaging.configure(
        onMessage: (Map<String, dynamic> message) {
          print('on message ${message}');

          return displayNotification(message);
          // initialise the plugin. app_icon needs to be a added as a drawable resource to the Android head project

          // return_showItemDialog(message);
        },
        onResume: (Map<String, dynamic> message) {
          print('on resume $message');
          return displayNotification(message);
        },
        onLaunch: (Map<String, dynamic> message) {
          print('on launch $message');
          return displayNotification(message);
        },
        onBackgroundMessage:
        Platform.isAndroid ? myBackgroundMessageHandler : null);
  }

  Future displayNotification(Map<String, dynamic> message) async {
    print("displayin  notification function");
    var androidPlatformChannelSpecifics = new AndroidNotificationDetails(
        channel.id, channel.name, channel.description,
        importance: Importance.max, priority: Priority.high);
    var iOSPlatformChannelSpecifics = new IOSNotificationDetails();
    var platformChannelSpecifics = new NotificationDetails(
        android: androidPlatformChannelSpecifics,
        iOS: iOSPlatformChannelSpecifics);
    await flutterLocalNotificationsPlugin.show(
      0,
      message['notification']['title'],
      message['notification']['body'],
      platformChannelSpecifics,
      payload: 'hello',
    );
  }

}
