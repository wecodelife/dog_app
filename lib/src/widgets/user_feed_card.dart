import 'dart:async';
import 'dart:ui' as ui;

import 'package:app_template/src/bloc/user_bloc.dart';
import 'package:app_template/src/models/add_post_comment_request.dart';
import 'package:app_template/src/models/get_post_comment_response.dart';
import 'package:app_template/src/screens/comments_detail_page.dart';
import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/object_factory.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:app_template/src/widgets/comment_box.dart';
import 'package:app_template/src/widgets/comment_tile.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:app_template/src/models/get_post_response.dart';

class UserFeedCard extends StatefulWidget {
  final String profilePic;
  final String feedImage;
  final String name;
  final String location;
  final String caption;
  final int likeCount;
  final String postDate;
  final int postId;
  final Function onTapMore;
  final bool isLiked;
  final ValueChanged<bool> onLiked;
  final List<LikedBy> likedList;
  UserFeedCard(
      {this.name,
        this.profilePic,
        this.feedImage,
        this.location,
        this.likeCount,
        this.postDate,
        // this.comment,
        // this.commentBy,
        this.isLiked,
        this.postId,
        this.caption,
        this.onTapMore,
        this.onLiked,
        this.likedList,});
  @override
  _UserFeedCardState createState() => _UserFeedCardState();
}

class _UserFeedCardState extends State<UserFeedCard> {
  int tagcount = 1;
  int favcount = 1;
  int count = 1;
  bool comment = false;
  int likeCount;
  bool isFav = false;
  TextEditingController commentTextEditingController =
  new TextEditingController();
  UserBloc userBloc = UserBloc();
  String lastComment = "";
  String commentBy;
  double height;
  double width;
  bool hasComment;
  Completer<ui.Image> completer = new Completer<ui.Image>();
  Image image;

  Future<Size> calculateImageDimension(String images) {
    Completer<Size> completer = Completer();
    image = Image.network(images);
    image.image.resolve(ImageConfiguration()).addListener(
      ImageStreamListener(
            (ImageInfo image, bool synchronousCall) {
          var myImage = image.image;
          Size size = Size(myImage.width.toDouble(), myImage.height.toDouble());
          completer.complete(size);
          // print("height" + myImage.height.toDouble().toString());
          // print("height" + myImage.width.toDouble().toString());
          // print("heightS" + screenHeight(context).toString());
          // print("heightW" +
          //     (myImage.height.toDouble() / myImage.width.toDouble())
          //         .toString());
          // print("heightK" +
          //     screenWidth(context,
          //         dividedBy: (myImage.height.toDouble() /
          //             myImage.width.toDouble()))
          //         .toString());
          if (myImage.width > myImage.height) {
            setState(() {
              height = screenWidth(context,
                  dividedBy:
                  (myImage.height.toDouble() / myImage.width.toDouble()));
            });
          } else {
            setState(() {
              height = screenHeight(context,
                  dividedBy:
                  (myImage.height.toDouble() / myImage.width.toDouble()));
            });
          }
          setState(() {
            height = myImage.height.toDouble();
            width = myImage.width.toDouble();
          });
        },
      ),
    );

    return completer.future;
  }

  @override
  void initState() {
    print("Liked list length   "+widget.likedList.length.toString());
    likeCount = widget.likedList.length;
    isFav = widget.isLiked != null
        ? widget.isLiked
        : widget.likedList.contains(ObjectFactory().appHive.getUserId());
    userBloc.getPostComments(id: widget.postId.toString());
    print("post id   " + widget.postId.toString());

    userBloc.getPostCommentsResponse.listen((event) async {
      if (event.status == 200 || event.status == 201) {
        print("comments all good");
        print(event.results);
        event.results.length < 0
            ? setState(() {
          hasComment = false;
        })
            : setState(() {
          hasComment = true;
        });
        print("length" + event.results.length.toString());
      } else {
        showErrorAnimatedToast(msg: "Something went wrong!", context: context);
      }
    });
    userBloc.addPostCommentsResponse.listen((event) async {
      print("Add Comments working good");
      if (event.status == 200 || event.status == 201) {
        setState(() {
          // isLoading = false;
          commentTextEditingController.clear();
          // print("Comment Added successfullyfrom home page");
          // userBloc.getPostComments(id: widget.postId.toString());
          // userBloc.getPostCommentsResponse.listen((event) async {
          //   // print("comments all good");
          //   // print(event.results);
          //   // print("length" + event.results.length.toString());
          // });
          userBloc.getPostComments(id: widget.postId.toString());
        });
      } else {
        showErrorAnimatedToast(msg: "Something went wrong!", context: context);
      }
    });
    // userBloc.getPostComments(id: "3");
    super.initState();
  }

  void onAct() {
    // First change [_isTapped] to `true`.
    if (isFav != true) widget.onLiked(isFav);

    setState(() {
      // _isTapped = true;
      if (isFav != true) {
        isFav = !isFav;
        likeCount = likeCount + 1;
      }
    });
  }
  @override
  Widget build(BuildContext context) {
    calculateImageDimension(widget.feedImage);
    // print(widget.caption.length.toString());
    return Container(
      width: screenWidth(context, dividedBy: 1),
      // height: count == 2 && comment == 2
      //     ? screenHeight(context, dividedBy: 1.05)
      //     : count == 2
      //         ? screenHeight(context, dividedBy: 1.05)
      //         : comment == 2
      //             ? screenHeight(context, dividedBy: 1.05)
      //             : screenHeight(context, dividedBy: 1.27),
      // padding: EdgeInsets.symmetric(
      //   horizontal: screenWidth(context, dividedBy: 60),
      // ),
      // margin:
      //     EdgeInsets.symmetric(vertical: screenHeight(context, dividedBy: 50)),
      decoration: BoxDecoration(
        // color:Colors.yellow,
        color: Constants.kitGradients[17],
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            padding: EdgeInsets.symmetric(
              horizontal: screenWidth(context, dividedBy: 30),
            ),
            width: screenWidth(context, dividedBy: 1),
            //color: Colors.red,
            child: Row(
              //mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Container(
                  width: screenWidth(context, dividedBy: 8),
                  height: screenWidth(context, dividedBy: 8),
                  //color: Colors.red,
                  child: CircleAvatar(
                    backgroundColor: Constants.kitGradients[25],
                    radius: screenWidth(context, dividedBy: 1),
                    child: CachedNetworkImage(
                      imageUrl: widget.profilePic == null
                          ? "https://nd.net/wp-content/uploads/2016/04/profile-dummy.png":widget.profilePic,
                      imageBuilder: (context, imageProvider) =>
                          Container(
                            width: screenWidth(context, dividedBy: 8),
                            height: screenWidth(context, dividedBy: 8),
                            decoration: BoxDecoration(
                              //color: Colors.amber,
                              shape: BoxShape.circle,
                              image: DecorationImage(
                                  image: imageProvider, fit: BoxFit.cover),
                            ),
                          ),
                      placeholder: (context, url) =>
                          Center(
                            heightFactor: 1,
                            widthFactor: 1,
                            child: SizedBox(
                              height: 16,
                              width: 16,
                              child: CircularProgressIndicator(
                                valueColor: AlwaysStoppedAnimation(
                                    Constants.kitGradients[19]),
                                strokeWidth: 2,
                              ),
                            ),
                          ),
                    ),
                  ),
                ),
                SizedBox(
                  width: screenWidth(context, dividedBy: 30),
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      widget.name,
                      style: TextStyle(
                        fontSize: 16,
                        fontWeight: FontWeight.w600,
                        fontFamily: 'Montserrat',
                        color: Constants.kitGradients[27],
                      ),
                    ),
                    SizedBox(height: screenHeight(context, dividedBy: 150)),
                    Text(
                      widget.location,
                      style: TextStyle(
                        fontSize: 12,
                        fontWeight: FontWeight.w600,
                        fontFamily: 'Montserrat',
                        color: Constants.kitGradients[27],
                      ),
                    )
                  ],
                ),
                Spacer(),
                GestureDetector(
                    onTap: () {
                      widget.onTapMore();
                    },
                    child: Icon(
                      Icons.more_vert,
                      color: Constants.kitGradients[27],
                    )),
              ],
            ),
          ),

          //SizedBox(height: screenHeight(context, dividedBy: 50)),
          // count == 2
          //     ? Spacer()
          //     : SizedBox(),
          SizedBox(height: screenHeight(context, dividedBy: 70)),
          Container(
            padding: EdgeInsets.symmetric(
              horizontal: screenWidth(context, dividedBy: 30),
            ),
            width: screenWidth(context, dividedBy: 1),
            child: Row(mainAxisAlignment: MainAxisAlignment.end, children: [
              Text(
                "Posted on : " + widget.postDate,
                style: TextStyle(
                  fontSize: 12,
                  fontWeight: FontWeight.w400,
                  fontFamily: 'Montserrat',
                  color: Constants.kitGradients[27],
                ),
              ),
            ]),
          ),
          // count == 2
          //     ? Spacer()
          //     : SizedBox(height:3),
          SizedBox(height: screenHeight(context, dividedBy: 70)),

          GestureDetector(
            onDoubleTap: () {
              onAct();
              // setState(() {
              //   widget.onLiked(isFav);
              //   isFav = !isFav;
              //   if (isFav == true) {
              //     likeCount = likeCount + 1;
              //     print("FirstTap count = " + likeCount.toString());
              //   } else {
              //     likeCount = likeCount - 1;
              //     print("SecondTap count = " + likeCount.toString());
              //   }
              // });
            },
            child: height != null
                ? Container(
              width: screenWidth(context, dividedBy: 1),
              // height: screenHeight(context, dividedBy: 3),
              // decoration: BoxDecoration(
              //   color: Constants.kitGradients[19].withOpacity(0.12),
              //   borderRadius: BorderRadius.circular(4),
              // ),
              child: CachedNetworkImage(
                fit: BoxFit.fitWidth,
                imageUrl: widget.feedImage,
                imageBuilder: (context, imageProvider) =>
                    Container(
                      width: screenWidth(context, dividedBy: 1),
                      height: height,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(4),
                        image: DecorationImage(
                          image: imageProvider,
                          fit: BoxFit.cover,
                        ),
                      ),
                    ),
                placeholder: (context, url) =>
                    Container(
                      width: screenWidth(context, dividedBy: 1),
                      height: height,
                      color: Constants.kitGradients[17].withOpacity(0.5),
                    ),
              ),
            )
                : Container(
              width: screenWidth(context, dividedBy: 1),
              height: screenHeight(context, dividedBy: 2),
              color: Constants.kitGradients[17].withOpacity(0.4),
              child: Center(
                child: SpinKitFadingCircle(
                  color: Constants.kitGradients[19],
                ),
              ),
            ),
          ),
          // count == 2
          //     ? Spacer()
          //     : SizedBox(height:7),
          SizedBox(height: screenHeight(context, dividedBy: 70)),
          //SizedBox(height: screenHeight(context, dividedBy: 30)),
          Container(
            padding: EdgeInsets.symmetric(
              horizontal: screenWidth(context, dividedBy: 30),
            ),
            width: screenWidth(context, dividedBy: 1),
            child: Row(
              children: [
                // SizedBox(
                //   width: screenWidth(context, dividedBy: 40),
                // ),

                GestureDetector(
                    onTap: () {
                      setState(() {
                        widget.onLiked(isFav);
                        isFav = !isFav;
                        if (isFav == true) {
                          likeCount = likeCount + 1;
                          print("FirstTap count = " + likeCount.toString());
                        } else {
                          likeCount = likeCount - 1;
                          print("SecondTap count = " + likeCount.toString());
                        }
                      });
                    },
                    child: isFav != true
                        ? Icon(
                      Icons.favorite_border_outlined,
                      color: Constants.kitGradients[27],
                    )
                        : Icon(
                      Icons.favorite,
                      color: Constants.kitGradients[27],
                    )),
                SizedBox(
                  width: screenWidth(context, dividedBy: 20),
                ),
                GestureDetector(
                  onTap: () {
                    setState(() {
                      comment = !comment;
                    });
                  },
                  child: Icon(
                    Icons.comment,
                    color: Constants.kitGradients[27],
                  ),
                ),
                SizedBox(
                  width: screenWidth(context, dividedBy: 20),
                ),
                GestureDetector(
                    child: Image(
                        image: AssetImage("assets/images/share_icon.png"),
                        width: 25,
                        height: 25)),
                Spacer(),
                GestureDetector(
                  onTap: () {
                    tagcount == 1
                        ? setState(() {
                      tagcount = 2;
                    })
                        : setState(() {
                      tagcount = 1;
                    });
                  },
                  child: tagcount == 1
                      ? Icon(
                    Icons.bookmark_outline_sharp,
                    color: Constants.kitGradients[27],
                  )
                      : Icon(
                    Icons.bookmark_sharp,
                    color: Constants.kitGradients[27],
                  ),
                ),
              ],
            ),
          ),
          //SizedBox(height: screenHeight(context, dividedBy: 50)),
          //Spacer(),
          // count == 2
          //     ? Spacer()
          //     : SizedBox(height: screenHeight(context, dividedBy: 70)),
          SizedBox(height: screenHeight(context, dividedBy: 70)),
          Container(
            padding: EdgeInsets.symmetric(
              horizontal: screenWidth(context, dividedBy: 30),
            ),
            width: screenWidth(context, dividedBy: 1),
            child: likeCount > 0
                ? Row(children: [
              Text(
                " ${likeCount.toString()} Likes ",
                style: TextStyle(
                  fontSize: 16,
                  fontWeight: FontWeight.w600,
                  fontFamily: 'Montserrat',
                  color: Constants.kitGradients[27],
                ),
              ),
            ]): Container(),
          ),
          // count == 2
          //     ? Spacer()
          //     : SizedBox(height: screenHeight(context, dividedBy: 70)),
          SizedBox(height: screenHeight(context, dividedBy: 70)),
          GestureDetector(
            onTap: () {
              count == 1
                  ? setState(() {
                count = 2;
              })
                  : setState(() {
                count = 1;
              });
            },
            child: Container(
              padding: EdgeInsets.symmetric(
                horizontal: screenWidth(context, dividedBy: 30),
              ),
              width: screenWidth(context, dividedBy: 1),
              child: RichText(
                overflow:
                count == 2 ? TextOverflow.visible : TextOverflow.ellipsis,
                maxLines: count == 2 ? 6 : 2,
                text: TextSpan(
                    text: widget.name,
                    style: TextStyle(
                      fontSize: 14,
                      fontWeight: FontWeight.w600,
                      fontFamily: 'Montserrat',
                      color: Constants.kitGradients[27],
                    ),
                    children: <TextSpan>[
                      widget.caption == null
                          ? null
                          : TextSpan(
                        text: "  " + widget.caption,
                        style: TextStyle(
                          fontSize: 13,
                          fontWeight: FontWeight.w400,
                          color: Constants.kitGradients[27],
                        ),
                      )
                    ]),
              ),
            ),
          ),
          SizedBox(height: screenHeight(context, dividedBy: 80)),
          // hasComment == false
          //     ? Container()
          //     :
          Container(
            padding: EdgeInsets.symmetric(
              horizontal: screenWidth(context, dividedBy: 30),
            ),
            width: screenWidth(context, dividedBy: 1),
            child: StreamBuilder<GetPostCommentResponse>(
              stream: userBloc.getPostCommentsResponse,
              builder: (context, snapshot) {
                return Container(
                  width: screenWidth(context, dividedBy: 1),
                  child: snapshot.hasData
                      ? snapshot.data.results.length == 0
                      ? Container(
                    child: GestureDetector(
                      onTap: () {
                        setState(() {
                          comment = !comment;
                        });
                      },
                      child: Text(
                        "Add comment",
                        style: TextStyle(
                          fontSize: 12,
                          fontWeight: FontWeight.w500,
                          fontFamily: 'Montserrat',
                          color: Constants.kitGradients[19],
                        ),
                      ),
                    ),
                  )
                      : Container(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        GestureDetector(
                          onTap: () {
                            push(
                                context,
                                CommentDetailsPage(
                                  // commentBy: widget.commentBy,
                                  // comment: widget.comment,
                                  feedImage: widget.feedImage,
                                  caption: widget.caption,
                                  postId: widget.postId,
                                ));
                          },
                          child: Text(
                            "View all comments",
                            style: TextStyle(
                              fontSize: 12,
                              fontWeight: FontWeight.w500,
                              fontFamily: 'Montserrat',
                              color: Constants.kitGradients[19],
                            ),
                          ),
                        ),
                        SizedBox(
                            height:
                            screenHeight(context, dividedBy: 80)),
                        GestureDetector(
                          onTap: () {
                            setState(() {
                              comment = !comment;
                            });
                          },
                          child: Container(
                            width: screenWidth(context, dividedBy: 1),
                            child: ListView.builder(
                              itemCount: 1,
                              shrinkWrap: true,
                              physics: NeverScrollableScrollPhysics(),
                              itemBuilder:
                                  (BuildContext context, int index) {
                                return Container(
                                  child: CommentTile(
                                    postId: snapshot
                                        .data.results[index].postId
                                        .toString(),
                                    comment: snapshot
                                        .data.results[index].comment,
                                    commentBy: snapshot
                                        .data
                                        .results[index]
                                        .username ==
                                        ""
                                        ? "UserName"
                                        : snapshot
                                        .data
                                        .results[index]
                                        .username,
                                  ),
                                );
                              },
                            ),
                          ),
                        ),
                      ],
                    ),
                  )
                      : Container(
                    // child: Text("No Data"),
                  ),
                );
              },
            ),
          ),
          SizedBox(height: screenHeight(context, dividedBy: 80)),
          comment == true
              ? CommentBox(
            textEditingController: commentTextEditingController,
            autofocus: false,
            onTapPost: () {
              if (commentTextEditingController.text == "") {
                showToast("Please type something");
              } else {
                setState(() {
                  print(commentTextEditingController.text);
                  print("Comment Adding from home page");
                });
                userBloc.addPostComments(
                    addPostCommentRequest: AddPostCommentRequest(
                      post: widget.postId,
                      comment: commentTextEditingController.text,
                      isDeleted: false,
                      createdBy:
                      int.parse(ObjectFactory().appHive.getUserId()),
                    ));
              }
            },
          )
              : Container(),
          //SizedBox(height: screenHeight(context, dividedBy: 50)),
          //SizedBox(height: screenHeight(context, dividedBy: 80)),
          Divider(color: Constants.kitGradients[26]),
          SizedBox(height: screenHeight(context, dividedBy: 80)),
        ],
      ),
    );
  }
}