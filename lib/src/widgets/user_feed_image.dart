import 'package:flutter/material.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/widgets/bottom_bar.dart';
import 'package:app_template/src/bloc/user_bloc.dart';
import 'package:app_template/src/screens/profile_page.dart';
import 'package:app_template/src/screens/market_place.dart';
import 'package:app_template/src/screens/home_page.dart';
import 'package:flutter/services.dart';
import 'package:app_template/src/utils/object_factory.dart';
import 'package:app_template/src/screens/new_post_page.dart';
import 'package:app_template/src/widgets/user_feed_card.dart';
import 'package:app_template/src/widgets/home_page_cards.dart';
import 'package:app_template/src/models/get_post_response.dart';
import 'package:intl/intl.dart';
import 'dart:ui' as ui;
import 'package:app_template/src/services/dynamic_link_service.dart';
import 'package:app_template/src/models/like_post_request.dart';
import 'package:app_template/src/widgets/alert_dialog_box.dart';
import 'package:app_template/src/widgets/bottom_sheet_item.dart';
import 'dart:async';

class UserFeedImage extends StatefulWidget {
  // final List<String> userImage;
  final List<String> userCaption;
  final String userName;
  final String userProfile;
  final List<bool> hasCaption;
  final double currentIndex;
  final GetPostResponse getPostResponse;
  //final List<String> userImage;
  UserFeedImage(
      {
        // this.userImage,
      this.userCaption,
      this.userName,
      this.userProfile,
      this.hasCaption,
        this.getPostResponse,
      this.currentIndex});
  @override
  _UserFeedImageState createState() => _UserFeedImageState();
}

class _UserFeedImageState extends State<UserFeedImage> {
  Completer<ui.Image> completer = new Completer<ui.Image>();
  UserBloc userBloc = new UserBloc();
  Image image;
  double height;
  double width;
  bool isLiked;
  int likeId;
  bool shareLoad = false;
  List<double> heightList = [];

  Future<Size> calculateImageDimension(String images) {
    Completer<Size> completer = Completer();
    image = Image.network(images);
    image.image.resolve(ImageConfiguration()).addListener(
      ImageStreamListener(
            (ImageInfo image, bool synchronousCall) {
          var myImage = image.image;
          Size size = Size(myImage.width.toDouble(), myImage.height.toDouble());
          completer.complete(size);
          if (myImage.width > myImage.height) {
            setState(() {
              height = screenWidth(context,
                  dividedBy:
                  (myImage.height.toDouble() / myImage.width.toDouble()));
            });
          } else {
            setState(() {
              height = screenHeight(context,
                  dividedBy:
                  (myImage.height.toDouble() / myImage.width.toDouble()));
            });
          }
          setState(() {
            height = myImage.height.toDouble();
            heightList.add(height);
            width = myImage.width.toDouble();
          });
        },
      ),
    );

    return completer.future;
  }

@override
  void initState() {
    // TODO: implement initState
  userBloc.getPost();
  userBloc.deletePostResponse.listen((event) {
    setState(() {
      // loading = false;
    });
    showSuccessAnimatedToast(
        context: context, msg: "Post Deleted Successfully");
    userBloc.getPost();
  }).onError((event) {
    setState(() {
      // userBloc = false;
    });
    showErrorAnimatedToast(context: context, msg: event);
  });
    super.initState();
  }

  void showMore(BuildContext context,
      {Function onPressedEdit, Function onPressedDelete, int postId}) {
    showModalBottomSheet(
        context: context,
        //transitionAnimationController: controller,
        // isScrollControlled: earnedPoint.length > 6 ? true : false,
        backgroundColor: Constants.kitGradients[17],
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.only(
                topLeft: Radius.circular(8), topRight: Radius.circular(8))),
        builder: (BuildContext context) {
          return Container(
            width: screenWidth(context, dividedBy: 1),
            //height:screenHeight(context, dividedBy:1.5),
            //height: MediaQuery.of(context).copyWith().size.height * 0.75,
            padding: EdgeInsets.symmetric(
              horizontal: screenWidth(context, dividedBy: 20),
              vertical: screenHeight(context, dividedBy: 20),
            ),
            decoration: BoxDecoration(
                color: Constants.kitGradients[17],
                borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(8), topRight: Radius.circular(8))),
            child: Wrap(children: [
              SizedBox(
                height: screenHeight(context, dividedBy: 30),
              ),
              BottomSheetItem(
                title: "Edit Post",
                leading: Icon(
                  Icons.edit,
                  color: Constants.kitGradients[27],
                ),
                trailing: null,
                onTap: () {
                  //imgFromCamera();
                  onPressedEdit();
                  // Navigator.of(context).pop();
                },
              ),
              Divider(
                color: Constants.kitGradients[27].withOpacity(0.2),
                indent: 10,
                endIndent: 10,
              ),
              BottomSheetItem(
                title: "Delete",
                leading: Icon(
                  Icons.delete,
                  color: Constants.kitGradients[27],
                ),
                trailing: null,
                onTap: () {
                  onPressedDelete();
                  // Navigator.of(context).pop();
                },
              ),
            ]),
          );
        });
  }
  @override
  Widget build(BuildContext context) {
    for(int i=0; i< widget.getPostResponse.results.length; i++){
      calculateImageDimension(widget.getPostResponse.results[i].image1);
      print("images are");
    }
    return AnnotatedRegion<SystemUiOverlayStyle>(
      value: SystemUiOverlayStyle(
        statusBarColor: Constants.kitGradients[17],
      ),
      child: Scaffold(
        resizeToAvoidBottomInset: false,
        appBar: AppBar(
          backgroundColor: Constants.kitGradients[17],
          elevation: 0,
          leading: Icon(
            Icons.lock_outline_rounded,
            color: Constants.kitGradients[27],
          ),
          title: Row(
            children: [
              Text(
                widget.userName,
                style: TextStyle(
                  fontSize: 17,
                  fontWeight: FontWeight.w600,
                  color: Constants.kitGradients[27],
                ),
              ),
              Spacer(),
              Icon(
                Icons.settings_sharp,
                color: Constants.kitGradients[27],
              ),
            ],
          ),
        ),
        body: Container(
          width: screenWidth(context, dividedBy: 1),
          //height: screenWidth(context, dividedBy: 1),
          //color: Colors.cyan,
          child: Stack(
            children: [
              SingleChildScrollView(
                padding: EdgeInsets.only(
                    bottom: MediaQuery.of(context).viewInsets.bottom),
                controller: ScrollController(
                  initialScrollOffset: screenHeight(context, dividedBy: 1.2) *
                      widget.currentIndex,
                  keepScrollOffset: true,
                ),
                child: Container(
                  padding: EdgeInsets.symmetric(
                    horizontal: screenWidth(context, dividedBy: 60),
                  ),
                  color: Constants.kitGradients[17],
                  child: Column(
                    children: [
                      // SizedBox(
                      //   height: screenHeight(context, dividedBy: 40),
                      // ),
                      Container(
                        child: ListView.builder(
                          shrinkWrap: true,
                          physics: NeverScrollableScrollPhysics(),
                          itemCount:  widget.getPostResponse.results.length,
                          itemBuilder: (BuildContext context, int index) {
                            return widget.getPostResponse.results.length > 0 ?
                            Column(children: [
                              Container(
                                  child: HomePageCard(
                                      profilePic: widget.userProfile,
                                      name: widget.userName,
                                      feedImage: widget.getPostResponse.results[index].image1,
                                      caption: widget.getPostResponse.results[index].description,
                                      location: widget.getPostResponse.results[index].lon == null ?
                                      "Not Available" : widget.getPostResponse.results[index].lon,
                                      isLiked: isLiked,
                                      postId: widget.getPostResponse.results[index].id,
                                      //likeCount:widget.getPostResponse.results[index].likeCount.toString(),
                                      postDate: widget.getPostResponse.results[index].createdAt,
                                      //comment: null,
                                      likedList: widget.getPostResponse.results[index].likedBy,
                                      onTapShare: (){
                                        setState(() {
                                          shareLoad = true;
                                        });
                                        DynamicLinkService()
                                            .createFirstPostLink(
                                            widget.getPostResponse.results[index].id.toString(),
                                            context,
                                            description: widget.getPostResponse.results[index].description,
                                            imgUrl: widget.getPostResponse.results[index].image1,
                                            loadMore: (val) {
                                              setState(() {
                                                shareLoad = val;
                                              });
                                            }

                                        );
                                      },
                                      onTapMore: (){
                                        showMore(context,
                                            postId: widget
                                                .getPostResponse
                                                .results[index]
                                                .id,
                                            onPressedDelete: () {
                                          print("delete tapped");
                                          pop(context);
                                          showDialog(
                                              context: context,
                                              builder: (BuildContext context) {
                                                return AlertDialogBox(
                                                  title: "Delete Post",
                                                  descriptions:
                                                      "Are you sure do you want to delete this post?",
                                                  onPressedYes: () async {
                                                    await userBloc.deletePost(
                                                      id: widget.getPostResponse
                                                          .results[index].id
                                                          .toString(),
                                                    );
                                                    // showSuccessAnimatedToast(
                                                    //     context: context, msg: "Post deleted successfully");
                                                    pushAndRemoveUntil(context,
                                                        HomePage(), false);
                                                  },
                                                );
                                              });
                                        },
                                        onPressedEdit: (){
                                          print(
                                              "Edit tapped");
                                          pop(context);
                                          push(
                                              context,
                                              NewPostPage(
                                                isEdit: true,
                                                addPostData: widget.getPostResponse.results[index],
                                                postId: widget.getPostResponse.results[index].id,
                                                profileImage: ObjectFactory().appHive.getProfilePic(),
                                                userName: ObjectFactory().appHive.getName(),
                                                editingImage: widget.getPostResponse.results[index].image1,
                                                // postDate: widget.getPostResponse.results[index].createdAt,

                                              ));
                                        }

                                        );
                                      },

                                      onLiked: (val) async {
                                       if (val ==
                                            false) {
                                          setState((){
                                            isLiked = true;
                                          });
                                          print("postid  " +
                                              widget.getPostResponse.results[index].id.toString());
                                          print("Liked list length after like " +
                                              widget.getPostResponse.results[index].likedBy.length.toString());
                                          print("likecount after like  " +
                                              widget.getPostResponse.results[index].likeCount.toString());
                                          userBloc.addPostLike(
                                              likePostRequest: LikePostRequest(
                                                  createdBy:
                                                  ObjectFactory().appHive.getUserId(),
                                                  post:widget.getPostResponse.results[index].id.toString(),
                                                  isDeleted: false));
                                        } else {
                                          print("postid  " +
                                              widget.getPostResponse.results[index].id.toString());
                                          print("Liked list length after dislike " +
                                              widget.getPostResponse.results[index].likedBy.length.toString());
                                          print("likecount after dislike  " +
                                              widget.getPostResponse.results[index].likeCount.toString());
                                          userBloc.disLikePost(
                                              id: likeId.toString(),
                                              likePostRequest: LikePostRequest(
                                                  createdBy:
                                                  ObjectFactory().appHive.getUserId(),
                                                  post: widget.getPostResponse.results[index].id.toString(),
                                                  isDeleted: true));}

                                      },
                                    /*  comment:
                                          "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore.",
                                      commentBy: "ABBd"*/
                                  )),
                              // SizedBox(
                              //   height: screenHeight(context, dividedBy: 40),
                              // ),
                            ]):Container();
                          },
                        ),
                      ),
                      SizedBox(
                        height: screenHeight(context, dividedBy: 10),
                      ),
                    ],
                  ),
                ),
              ),
              Positioned(
                bottom: 0,
                child: CustomBottomBar(
                  currentIndex: 2,
                  onTapSearch: () {
                    push(context, MarketPlace());
                  },
                  onTapProfile: () {
                    push(
                        context,
                        ProfilePage(

                          // name: "Bi Kines",
                          // profilePic:
                              // 'https://images.unsplash.com/photo-1520342868574-5fa3804e551c?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=6ff92caffcdd63681a35134a6770ed3b&auto=format&fit=crop&w=1951&q=80',
                        )
                    );
                  },
                  onTapHome: () {
                    push(
                        context,
                        HomePage(
                          // name: "Bi",
                          // profilePic:
                          //     "https://img.17qq.com/images/fjjhjggbebz.jpeg",
                        ));
                  },
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
