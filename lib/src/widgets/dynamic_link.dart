import 'package:firebase_dynamic_links/firebase_dynamic_links.dart';
import 'package:flutter/material.dart';

class DynamicLink {
  void initDynamicLinks(context) async {
    final PendingDynamicLinkData data =
        await FirebaseDynamicLinks.instance.getInitialLink();
    final Uri deepLink = data?.link;
    print("workhgh");
    if (deepLink != null) {
      print("workhgh" + deepLink.path);

      // msg = deepLink.path;
      Navigator.pushNamed(context, deepLink.path);
    } else {
      print("not working");
    }

    FirebaseDynamicLinks.instance.onLink(
        onSuccess: (PendingDynamicLinkData dynamicLink) async {
      final Uri deepLink = dynamicLink?.link;

      if (deepLink != null) {
        Navigator.pushNamed(context, deepLink.path);
      }
    }, onError: (OnLinkErrorException e) async {
      print('onLinkError');
      print(e.message);
    });
  }

  //
  void fetchLinkData() async {
    // FirebaseDynamicLinks.getInitialLInk does a call to firebase to get us the real link because we have shortened it.
    var link = await FirebaseDynamicLinks.instance.getInitialLink();
    // This link may exist if the app was opened fresh so we'll want to handle it the same way onLink will.
    handleLinkData(link);

    // This will handle incoming links if the application is already opened
    FirebaseDynamicLinks.instance.onLink(
        onSuccess: (PendingDynamicLinkData dynamicLink) async {
      print("all good");
      handleLinkData(dynamicLink);
    });
  }

  void handleLinkData(PendingDynamicLinkData data) {
    final Uri uri = data?.link;
    if (uri != null) {
      final queryParams = uri.queryParameters;
      if (queryParams.length > 0) {
        String userName = queryParams["user"];
        // verify the username is parsed correctly
        print("My users username is: $userName");
      }
    }
  }

  Future<void> createDynamicLink() async {
    bool _isCreatingLink = true;

    final DynamicLinkParameters parameters = DynamicLinkParameters(
      uriPrefix: 'https://wowbow.page.link',
      link: Uri.parse('https://wecodelife/welcome/helloworld?user=1'),
      androidParameters: AndroidParameters(
        packageName: 'com.wecodelife.dog',
        minimumVersion: 0,
      ),
      dynamicLinkParametersOptions: DynamicLinkParametersOptions(
        shortDynamicLinkPathLength: ShortDynamicLinkPathLength.short,
      ),
      iosParameters: IosParameters(
        bundleId: 'com.google.FirebaseCppDynamicLinksTestApp.dev',
        minimumVersion: '0',
      ),
    );

    Uri url;
    // if (short) {
    final ShortDynamicLink shortLink = await parameters.buildShortLink();
    url = shortLink.shortUrl;
    print(url.toString());
    print(url.queryParameters.toString());
    // } else {
    //   url = await parameters.buildUrl();
    // }

    // setState(() {
    String _linkMessage = url.toString();
    _isCreatingLink = false;
    // });
  }
}
