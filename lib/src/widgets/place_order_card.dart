import 'package:flutter/material.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:app_template/src/utils/constants.dart';
import 'package:cached_network_image/cached_network_image.dart';


class PlaceOrderCard extends StatefulWidget {
  // const PlaceOrderCard({Key? key}) : super(key: key);
  final String itemName;
  final String itemImage;
  final String itemPrice;
  final int itemCount;
  PlaceOrderCard({this.itemName, this.itemImage, this.itemPrice, this.itemCount});

  @override
  _PlaceOrderCardState createState() => _PlaceOrderCardState();
}

class _PlaceOrderCardState extends State<PlaceOrderCard> {
  int count = 0;
  int itemCount = 1;
  @override
  Widget build(BuildContext context) {
    return Container(
      width: screenWidth(context, dividedBy: 1),
      padding: EdgeInsets.symmetric(
        horizontal: screenWidth(context, dividedBy: 30),
        vertical: screenHeight(context, dividedBy: 60),
      ),
      decoration: BoxDecoration(
        color:  Constants.kitGradients[17],
        borderRadius: BorderRadius.circular(8),
        border: Border.all(color: Colors.transparent, width: 0.0),
      ),
      child: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Expanded(
                child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        widget.itemName,
                        style: TextStyle(
                          fontSize: 18,
                          fontFamily: 'Prompt-Light',
                          fontWeight: FontWeight.w700,
                          color: Constants.kitGradients[27],
                        ),
                      ),
                      SizedBox(height: screenHeight(context, dividedBy: 120)),
                      Text(
                        "Lorem ipsum dolor sit amet, consectetur adipiscing",
                        style: TextStyle(
                          fontSize: 16,
                          fontFamily: 'Prompt-Light',
                          fontWeight: FontWeight.w700,
                          color: Constants.kitGradients[19],
                        ),
                      ),
                      SizedBox(height: screenHeight(context, dividedBy: 90)),
                      Text(
                        "₹ " + widget.itemPrice,
                        // textAlign: TextAlign.left,
                        style: TextStyle(
                          color: Constants.kitGradients[27],
                          fontWeight: FontWeight.bold,
                          fontFamily: 'Prompt-Light',
                          fontSize: 20,
                        ),
                      ),
                      SizedBox(height: screenHeight(context, dividedBy: 90)),
                      Text(
                        "Number of item "+widget.itemCount.toString(),
                        // textAlign: TextAlign.left,
                        style: TextStyle(
                          color: Constants.kitGradients[27],
                          fontWeight: FontWeight.bold,
                          fontFamily: 'Prompt-Light',
                          fontSize: 20,
                        ),
                      ),
                      SizedBox(height: screenHeight(context, dividedBy: 90)),
                    ]),
              ),
              Column(
                children: [
                  ClipRRect(
                    borderRadius: BorderRadius.circular(10),
                    child: CachedNetworkImage(
                      width: screenWidth(context, dividedBy: 4.5),
                      //height: screenHeight(context, dividedBy:3),
                      fit: BoxFit.cover,
                      placeholder: (context, url) => Center(
                        heightFactor: 1,
                        widthFactor: 1,
                        child: SizedBox(
                          height: 16,
                          width: 16,
                          child: CircularProgressIndicator(
                            valueColor: AlwaysStoppedAnimation(
                                Constants.kitGradients[19]),
                            strokeWidth: 2,
                          ),
                        ),
                      ),
                      imageUrl: widget.itemImage,
                      imageBuilder: (context, imageProvider) {
                        return Container(
                          width: screenWidth(context, dividedBy: 4.5),
                          height: screenWidth(context, dividedBy: 4.5),
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(10),
                            //shape: BoxShape.circle,
                            image: DecorationImage(
                                image: imageProvider, fit: BoxFit.cover),
                          ),
                        );
                      },
                    ),
                  ),
                  SizedBox(height: screenHeight(context, dividedBy: 60)),
                ],
              ),
            ],
          ),
          SizedBox(height: screenHeight(context, dividedBy: 30)),
          //SizedBox(height: screenHeight(context, dividedBy: 30)),
        ],
      ),
    );
  }
}
