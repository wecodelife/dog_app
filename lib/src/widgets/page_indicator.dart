import 'package:flutter/material.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:app_template/src/utils/constants.dart';

class PageIndicator extends StatefulWidget {
  final int currentIndex;
  PageIndicator({this.currentIndex});
  @override
  _PageIndicatorState createState() => _PageIndicatorState();
}

class _PageIndicatorState extends State<PageIndicator> {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: screenWidth(context, dividedBy: 1),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Container(
            width: screenWidth(context, dividedBy: 17 ),
            height: screenWidth(context, dividedBy: 80),
            decoration: BoxDecoration(
                borderRadius:  BorderRadius.circular(6) ,
                //border: Border.all(color:Constants.kitGradients[27]),
                color: widget.currentIndex == 0 ? Constants.kitGradients[27] : Constants.kitGradients[27].withOpacity(0.2)
            ),
          ),
          SizedBox(
            width: screenWidth(context, dividedBy: 30),
          ),
          Container(
            width: screenWidth(context, dividedBy: 17 ),
            height: screenWidth(context, dividedBy: 80),
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(6) ,
                //border: Border.all(color:Constants.kitGradients[27]),
                color: widget.currentIndex == 1 ? Constants.kitGradients[27] : Constants.kitGradients[27].withOpacity(0.2)
            ),
          ),
          SizedBox(
            width: screenWidth(context, dividedBy: 30),
          ),
          Container(
            width: screenWidth(context, dividedBy: 17 ),
            height: screenWidth(context, dividedBy: 80),
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(6) ,
                //border: Border.all(color:Constants.kitGradients[27]),
                color: widget.currentIndex == 2 ? Constants.kitGradients[27] : Constants.kitGradients[27].withOpacity(0.2)
            ),
          ),
          SizedBox(
            width: screenWidth(context, dividedBy: 30),
          ),
          Container(
            width: screenWidth(context, dividedBy: 17 ),
            height: screenWidth(context, dividedBy: 80),
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(6) ,
                //border: Border.all(color:Constants.kitGradients[27]),
                color: widget.currentIndex == 3 ? Constants.kitGradients[27] : Constants.kitGradients[27].withOpacity(0.2)
            ),
          ),
          SizedBox(
            width: screenWidth(context, dividedBy: 30),
          ),
          Container(
            width: screenWidth(context, dividedBy: 17 ),
            height: screenWidth(context, dividedBy: 80),
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(6) ,
                //border: Border.all(color:Constants.kitGradients[27]),
                color: widget.currentIndex == 4 ? Constants.kitGradients[27] : Constants.kitGradients[27].withOpacity(0.2)
            ),
          ),
        ],
      ),
    );
  }
}
