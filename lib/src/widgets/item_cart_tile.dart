import 'package:flutter/material.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:app_template/src/utils/constants.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:app_template/src/bloc/user_bloc.dart';



class ItemCartTile extends StatefulWidget {
  final String itemName;
  final String itemImage;
  final String itemPrice;
  final String totalPrice;
  final Function onTapDelete;
  final ValueChanged<int> onItemCountChange;
  final Function onTapWishlist;
  final Function onSelected;
  final bool selected;
  ItemCartTile({this.itemPrice,this.totalPrice, this.itemName, this.itemImage,this.onTapDelete, this.onTapWishlist,this.onSelected,this.onItemCountChange, this.selected = false });
  @override
  _ItemCartTileState createState() => _ItemCartTileState();
}

class _ItemCartTileState extends State<ItemCartTile> {
  UserBloc userBloc=UserBloc();
  int count = 0;
  int itemCount = 1;
  void initState() {
    // TODO: implement initState
    userBloc.getCartDetails();
    userBloc.deletePostResponse.listen((event) {
      setState(() {
        // loading = false;
      });
      showSuccessAnimatedToast(
          context: context, msg: "Item Deleted Successfully");
      userBloc.getCartDetails();
    });
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap:widget.onSelected,
      child: Container(
            width: screenWidth(context, dividedBy: 1),
            padding: EdgeInsets.symmetric(
              horizontal: screenWidth(context, dividedBy: 30),
              vertical: screenHeight(context, dividedBy: 60),
            ),
            decoration: BoxDecoration(
              color:
              // (widget.selectedItems).contains(widget.indexValues )
              Constants.kitGradients[17],
              borderRadius: BorderRadius.circular(8),
            //  border: widget.selected == true
              // (widget.selectedItems).contains(widget.indexValues )
              //    ? Border.all(color: Constants.kitGradients[19].withOpacity(0.4), width: 0.0)
               //   : Border.all(color: Colors.transparent, width: 0.0),
            ),
            child: Column(
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Expanded(
                      child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              widget.itemName,
                              style: TextStyle(
                                fontSize: 18,
                                fontFamily: 'Prompt-Light',
                                fontWeight: FontWeight.w700,
                                color: Constants.kitGradients[27],
                              ),
                            ),
                            SizedBox(height: screenHeight(context, dividedBy: 120)),
                            Text(
                              "Lorem ipsum dolor sit amet, consectetur adipiscing",
                              style: TextStyle(
                                fontSize: 16,
                                fontFamily: 'Prompt-Light',
                                fontWeight: FontWeight.w700,
                                color: Constants.kitGradients[19],
                              ),
                            ),
                            SizedBox(height: screenHeight(context, dividedBy: 90)),
                            Text(
                              //"₹"+widget.itemPrice,
                             "₹ " +(int.parse( widget.itemPrice) * (itemCount)).toString(),
                              // textAlign: TextAlign.left,
                              style: TextStyle(
                                color: Constants.kitGradients[27],
                                fontWeight: FontWeight.bold,
                                fontFamily: 'Prompt-Light',
                                fontSize: 20,
                              ),
                            ),
                            SizedBox(height: screenHeight(context, dividedBy: 90)),
                          ]),
                    ),
                    Column(
                      children: [
                        ClipRRect(
                          borderRadius: BorderRadius.circular(10),
                          child: CachedNetworkImage(
                            width: screenWidth(context, dividedBy: 4.5),
                            //height: screenHeight(context, dividedBy:3),
                            fit: BoxFit.cover,
                            placeholder: (context, url) => Center(
                              heightFactor: 1,
                              widthFactor: 1,
                              child: SizedBox(
                                height: 16,
                                width: 16,
                                child: CircularProgressIndicator(
                                  valueColor: AlwaysStoppedAnimation(
                                      Constants.kitGradients[19]),
                                  strokeWidth: 2,
                                ),
                              ),
                            ),
                            imageUrl: widget.itemImage,
                            imageBuilder: (context, imageProvider) {
                              return Container(
                                width: screenWidth(context, dividedBy: 4.5),
                                height: screenWidth(context, dividedBy: 4.5),
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(10),
                                  //shape: BoxShape.circle,
                                  image: DecorationImage(
                                      image: imageProvider, fit: BoxFit.cover),
                                ),
                              );
                            },
                          ),
                        ),
                        SizedBox(height: screenHeight(context, dividedBy: 60)),
                        Container(
                          width: screenWidth(context, dividedBy: 4),
                          height: screenWidth(context, dividedBy: 10),
                          alignment: Alignment.center,
                          decoration: BoxDecoration(
                            color: Constants.kitGradients[19].withOpacity(0.12),
                            borderRadius: BorderRadius.circular(7),
                          ),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceAround,
                            children: [
                              GestureDetector(
                                  child: Icon(
                                    Icons.remove,
                                    size: 16,
                                    color: Constants.kitGradients[27],
                                  ),
                                  onTap: () {
                                    setState(() {
                                      if(itemCount>0)
                                      itemCount--;
                                    });
                                    widget.onItemCountChange(itemCount);
                                  }),
                              Text(
                                itemCount.toString(),
                                style: TextStyle(
                                  //decoration: TextDecoration.underline,
                                  fontSize: 20,
                                  fontWeight: FontWeight.w400,
                                  color: Constants.kitGradients[27],
                                ),
                              ),
                              GestureDetector(
                                child: Icon(
                                  Icons.add,
                                  size: 16,
                                  color: Constants.kitGradients[27],
                                ),
                                onTap: () {
                                  setState(
                                    () {
                                      itemCount++;
                                    },
                                  );
                                  widget.onItemCountChange(itemCount);
                                },
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
                SizedBox(height: screenHeight(context, dividedBy: 30)),
                Container(
                  width: screenWidth(context, dividedBy: 1),
                  child: Row(
                    children: [
                      // Spacer(),
                              Container(
                            width: screenWidth(context, dividedBy: 2.35),
                            height: screenWidth(context, dividedBy: 10),
                            alignment: Alignment.center,
                            decoration: BoxDecoration(
                              color: Constants.kitGradients[19].withOpacity(0.12),
                              borderRadius: BorderRadius.circular(7),
                            ),
                            child: GestureDetector(
                              onTap: () {
                                widget.onTapWishlist();
                              },
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  Icon(
                                    Icons.save_alt_rounded,
                                    color: Constants.kitGradients[27],
                                  ),
                                  SizedBox(width: screenWidth(context, dividedBy: 30)),
                                  Text(
                                    "Save for later",
                                    style: TextStyle(
                                      color: Constants.kitGradients[27],
                                      fontWeight: FontWeight.w500,
                                      fontFamily: 'Prompt-Light',
                                      fontSize: 18,
                                    ),
                                  )
                                ],
                              ),
                            ),
                          ),
                      //SizedBox(width: screenWidth(context, dividedBy: 30)),
                      Spacer(),
                       Container(
                            width: screenWidth(context, dividedBy: 2.35),
                            height: screenWidth(context, dividedBy: 10),
                            alignment: Alignment.center,
                            decoration: BoxDecoration(
                              color: Constants.kitGradients[19].withOpacity(0.12),
                              borderRadius: BorderRadius.circular(7),
                            ),
                            child: GestureDetector(
                              onTap: () {
                                widget.onTapDelete();
                              },
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  Icon(
                                    Icons.delete,
                                    color: Constants.kitGradients[27],
                                  ),
                                  SizedBox(width: screenWidth(context, dividedBy: 30)),
                                  Text(
                                    "Remove",
                                    style: TextStyle(
                                      color: Constants.kitGradients[27],
                                      fontWeight: FontWeight.w500,
                                      fontFamily: 'Prompt-Light',
                                      fontSize: 18,
                                    ),
                                  )
                                ],
                              ),
                            ),
                          ),
                      // Spacer(),
                    ],
                  ),
                ),
                //SizedBox(height: screenHeight(context, dividedBy: 30)),
              ],
            ),
          ),
    );
    // GestureDetector(
    //   onTap: () {},
    //   child: Column(
    //     crossAxisAlignment:CrossAxisAlignment.start,
    //     children: [
    //       Container(
    //         width:screenWidth(context, dividedBy:1),
    //         margin: EdgeInsets.symmetric(
    //           horizontal: screenWidth(context, dividedBy: 40),
    //         ),
    //         alignment: Alignment.center,
    //         padding: EdgeInsets.symmetric(
    //           vertical: screenHeight(context, dividedBy: 50),
    //           horizontal: screenWidth(context, dividedBy: 60),
    //         ),
    //         decoration: BoxDecoration(
    //           borderRadius: BorderRadius.circular(8),
    //           color: Constants.kitGradients[25],
    //         ),
    //         child: Row(
    //           mainAxisAlignment: MainAxisAlignment.start,
    //           crossAxisAlignment:CrossAxisAlignment.start,
    //           children: [
    //             CachedNetworkImage(
    //               fit: BoxFit.fill,
    //               placeholder: (context, url) => Center(
    //                 heightFactor: 1,
    //                 widthFactor: 1,
    //                 child: SizedBox(
    //                   height: 16,
    //                   width: 16,
    //                   child: CircularProgressIndicator(
    //                     valueColor: AlwaysStoppedAnimation(
    //                         Constants.kitGradients[29]),
    //                     strokeWidth: 2,
    //                   ),
    //                 ),
    //               ),
    //               imageUrl: widget.itemImage,
    //               imageBuilder: (context, imageProvider) => Container(
    //                 width:screenWidth(context, dividedBy:1),
    //                 child: Column(
    //                   children: [
    //                     Container(
    //                       width: screenWidth(context, dividedBy: 4.5),
    //                       height: screenWidth(context, dividedBy: 4.5),
    //                       decoration: BoxDecoration(
    //                         color: Constants.kitGradients[29],
    //                         borderRadius: BorderRadius.circular(7),
    //                         image: DecorationImage(
    //                             image: imageProvider, fit: BoxFit.cover),
    //                       ),
    //                     ),
    //                     // SizedBox(
    //                     //     height: screenHeight(context,
    //                     //         dividedBy: 80)),
    //                   ],
    //                 ),
    //               ),
    //             ),
    //             SizedBox(
    //               width: screenWidth(context, dividedBy: 30),
    //             ),
    //             Column(
    //                 crossAxisAlignment: CrossAxisAlignment.start,
    //                 children: [
    //                   Text(
    //                     widget.itemName,
    //                     style: TextStyle(
    //                       fontSize: 16,
    //                       fontWeight: FontWeight.w700,
    //                       color:  Constants.kitGradients[27],
    //                     ),
    //                   ),
    //                   SizedBox(
    //                     height: screenHeight(context, dividedBy: 200),
    //                   ),
    //                   Text(
    //                     "₹ " + widget.itemPrice,
    //                     style: TextStyle(
    //                       fontSize: 16,
    //                       fontWeight: FontWeight.w700,
    //                       color:  Constants.kitGradients[27]
    //                          ,
    //                     ),
    //                   )
    //                 ]),
    //             Spacer(),
    //             Column(
    //               children: [
    //                 Text(
    //                   "No of items: ",
    //                   style: TextStyle(
    //                     fontSize: 16,
    //                     fontWeight: FontWeight.w700,
    //                     color:  Constants.kitGradients[27]
    //                        ,
    //                   ),
    //                 ),
    //                 SizedBox(height: screenHeight(context, dividedBy: 80)),
    //                 Container(
    //                     width: screenWidth(context, dividedBy: 4),
    //                     height: screenWidth(context, dividedBy: 10),
    //                     alignment: Alignment.center,
    //                     decoration: BoxDecoration(
    //                       color:Constants.kitGradients[19].withOpacity(0.12),
    //                       borderRadius: BorderRadius.circular(7),
    //                     ),
    //                     child: Row(
    //                       mainAxisAlignment: MainAxisAlignment.spaceAround,
    //                       children: [
    //                         GestureDetector(
    //                             child: Icon(
    //                               Icons.remove,
    //                               color:  Constants.kitGradients[27],
    //                             ),
    //                             onTap: () {
    //                               setState(() {
    //                                 itemCount--;
    //                               });
    //                             }),
    //                         Text(
    //                           itemCount.toString(),
    //                           style: TextStyle(
    //                             //decoration: TextDecoration.underline,
    //                             fontSize: 20,
    //                             fontWeight: FontWeight.w400,
    //                             color:  Constants.kitGradients[27],
    //                           ),
    //                         ),
    //                         GestureDetector(
    //                             child: Icon(
    //                               Icons.add,
    //                               color:  Constants.kitGradients[27],
    //                             ),
    //                             onTap: () {
    //                               setState(() {
    //                                 itemCount++;
    //                               });
    //                             }),
    //                       ],
    //                     ))
    //               ],
    //             )
    //           ],
    //         ),
    //       ),
    //       SizedBox(height: screenHeight(context, dividedBy: 80)),
    //     ],
    //   ));
  }
}
