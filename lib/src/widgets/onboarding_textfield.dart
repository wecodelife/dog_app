import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class OnBoardingTextField extends StatefulWidget {
  final String labelText;
  final Widget suffixIcon;
  final bool hasSuffix;
  final Function onTapIcon;
  final Function onTap;
  final bool textArea;
  final bool readOnly;
  final Function onChanged;
  final bool isNumber;
  final bool userExists;
  final bool minLine;
  final TextEditingController textEditingController;
  OnBoardingTextField({
    this.labelText,
    this.userExists,
    this.textEditingController,
    this.hasSuffix,
    this.onTapIcon,
    this.suffixIcon,
    this.readOnly,
    this.onTap,
    this.textArea,
    this.isNumber,
    this.onChanged,
    this.minLine,
  });

  @override
  _OnBoardingTextFieldState createState() => _OnBoardingTextFieldState();
}

class _OnBoardingTextFieldState extends State<OnBoardingTextField> {
  @override
  Widget build(BuildContext context) {
    return Container(
     // height: widget.textArea == true ? screenHeight(context, dividedBy: 16) : screenHeight(context),

      decoration: BoxDecoration(
        border: widget.textArea ==false  ? Border.all(color: Constants.kitGradients[27].withOpacity(0.7),width: 0.0):Border.all(color: Constants.kitGradients[27].withOpacity(0.7)),
        borderRadius: BorderRadius.circular(20),
      ),
      child:  TextField(
          controller: widget.textEditingController,
          cursorColor: Constants.kitGradients[19],
          onChanged: widget.onChanged,
          style: TextStyle(
            color: Constants.kitGradients[27],
            fontFamily: 'Montserrat',
            fontSize: 16,
          ),
          autofocus: false,
          readOnly: widget.readOnly,
          onTap: widget.onTap,
          keyboardType:
              widget.textArea ==true ? TextInputType.multiline : widget.isNumber == true ?
              TextInputType.number : TextInputType.text ,
          maxLines: widget.textArea == true ? 5: 1,
        // minLines: widget.minLine == false? 5:2,
        // decoration: InputDecoration(
        //     filled: true,
            // fillColor: Colors.white10,
            // border: OutlineInputBorder(
            //   borderRadius: BorderRadius.circular(10),
            //
            //   borderSide: BorderSide.none,
            // )
        // ),
      // ),
      //       );
      //     ),
      //   ),
          decoration: InputDecoration(
            floatingLabelBehavior: FloatingLabelBehavior.never,
              // suffixIcon: widget.userExists == false
              //     ? Icon(Icons.check_circle_rounded,
              //   color: Constants.kitGradients[27],
              // ): Container(
              //   height: 1,
              //   width: 1,
              // )
            suffixIcon: widget.userExists == false
          ? Icon(Icons.check_circle_rounded ,
            color: Constants.kitGradients[27],
          ) : widget.hasSuffix
                ? GestureDetector(
                    onTap: widget.onTapIcon, child: widget.suffixIcon)
                : Container(width: screenWidth(context, dividedBy: 20),height: screenHeight(context, dividedBy: 120),),
            contentPadding: EdgeInsets.symmetric(
              horizontal: screenWidth(context, dividedBy: 30),
              vertical: screenHeight(context, dividedBy: 120),
            ),
            hintText: widget.labelText,
            hintStyle: TextStyle(
              color: Constants.kitGradients[27],
              fontFamily: 'Montserrat',
              fontSize: 16,
            ),
            // labelText: widget.labelText,
            // labelStyle: TextStyle(
            //   color: Constants.kitGradients[19],
            //   fontFamily: 'OpenSansRegular',
            //   fontSize: 16,
            // ),
            filled: false,
            fillColor: Constants.kitGradients[27],
            focusedBorder: OutlineInputBorder(
              borderSide:widget.textArea ==false ? BorderSide(color: Colors.transparent, width: 0.0):BorderSide(color: Colors.transparent, width: 0.0),
              borderRadius:widget.textArea ==false  ?  BorderRadius.circular(40.0):BorderRadius.circular(40),
            ),
            enabledBorder: OutlineInputBorder(
              borderSide:widget.textArea ==false  ? BorderSide(color: Colors.transparent, width: 0.0):BorderSide(color: Colors.transparent, width: 0.0),
              borderRadius:widget.textArea ==false ?  BorderRadius.circular(40.0):BorderRadius.circular(40),
            ),
          ),
    ),
      // )
    );
  }
}
