import 'package:flutter/material.dart';
import 'package:flutter/material.dart';
import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:app_template/src/widgets/heading_tile.dart';
import 'package:app_template/src/widgets/market_place_card.dart';

class CategoryListTile extends StatefulWidget {
  final List<String> itemtype;
  final List<String> images;
  final String categoryType;
  final List<String> productRating;
  final Function seeMore;
  final Function itemPressed;
  final int currentIndex;
  CategoryListTile(
      {this.itemtype,
      this.images,
      this.categoryType,
      this.seeMore,
      this.itemPressed,
        this.productRating,
      this.currentIndex, });

  @override
  _CategoryListTileState createState() => _CategoryListTileState();
}

class _CategoryListTileState extends State<CategoryListTile> {
  @override
  Widget build(BuildContext context) {
    return Container(
      //width: screenWidth(context, dividedBy: 1),
      // margin: EdgeInsets.symmetric(
      //   horizontal: screenWidth(context, dividedBy: 40),
      //   vertical: screenHeight(context, dividedBy: 50),
      // ),
      padding: EdgeInsets.only(
        top: screenHeight(context, dividedBy: 80),
        //left: screenWidth(context, dividedBy: 25),
      ),
      child: Column(
        children: [
          Row(
            children: [
              SizedBox(
                width: screenWidth(context, dividedBy: 30),
              ),
              HeadingTile(
                  heading: widget.categoryType, seeMore: widget.seeMore),
            ],
          ),
          SizedBox(height: screenHeight(context, dividedBy: 100)),
          Container(
            alignment: Alignment.center,
            child: Column(
              children: [
                //SizedBox(height: screenHeight(context, dividedBy: 50)),
                Container(
                  width: screenWidth(context, dividedBy: 1),
                  height: screenHeight(context, dividedBy: 3.6),
                  // color:Colors.red,
                  child: ListView.builder(
                    //shrinkWrap: true,
                    scrollDirection: Axis.horizontal,
                    itemCount: widget.images.length,
                    itemBuilder: (BuildContext context, int index) {
                      return Row(children: [
                        SizedBox(
                          width: screenWidth(context, dividedBy: 30),
                        ),
                        MarketPlaceCard(
                          imgUrl: widget.images[index],
                          catType: widget.itemtype[index],
                          circle: false,
                          itemPressed: widget.itemPressed,
                          rating: widget.productRating[index],
                          price: "\u0024 20",

                        ),
                        SizedBox(
                          width: screenWidth(context, dividedBy: 40),
                        )
                      ]);
                    },
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
