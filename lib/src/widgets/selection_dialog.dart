import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:app_template/src/widgets/country_code.dart';
import 'package:flutter/material.dart';

/// selection dialog used for selection of the country code
class SelectionDialog extends StatefulWidget {
  final List<CountryCode> elements;
  final bool showCountryOnly;
  final InputDecoration searchDecoration;
  final TextStyle searchStyle;
  final TextStyle textStyle;
  final BoxDecoration boxDecoration;
  final WidgetBuilder emptySearchBuilder;
  final bool showFlag;
  final double flagWidth;
  final Size size;
  final bool hideSearch;
  final Icon closeIcon;

  /// Background color of SelectionDialog
  final Color backgroundColor;

  /// Boxshaow color of SelectionDialog that matches CountryCodePicker barrier color
  final Color barrierColor;

  /// elements passed as favorite
  final List<CountryCode> favoriteElements;

  SelectionDialog(
      this.elements,
      this.favoriteElements, {
        Key key,
        this.showCountryOnly,
        this.emptySearchBuilder,
        InputDecoration searchDecoration = const InputDecoration(),
        this.searchStyle,
        this.textStyle,
        this.boxDecoration,
        this.showFlag,
        this.flagWidth = 32,
        this.size,
        this.backgroundColor,
        this.barrierColor,
        this.hideSearch = false,
        this.closeIcon,
      })  : assert(searchDecoration != null, 'searchDecoration must not be null!'),
        this.searchDecoration = searchDecoration.prefixIcon == null
            ? searchDecoration.copyWith(prefixIcon: Icon(Icons.search))
            : searchDecoration,
        super(key: key);

  @override
  State<StatefulWidget> createState() => _SelectionDialogState();
}

class _SelectionDialogState extends State<SelectionDialog> {
  /// this is useful for filtering purpose
  List<CountryCode> filteredElements;

  @override
  Widget build(BuildContext context) => Padding(
    padding: const EdgeInsets.all(16.0),
    child: Container(
      clipBehavior: Clip.hardEdge,
      width: widget.size?.width ?? MediaQuery.of(context).size.width,
      height:screenHeight(context,dividedBy: 3),
      decoration: widget.boxDecoration ??
          BoxDecoration(
            color: widget.backgroundColor ?? Colors.white,
            borderRadius: BorderRadius.all(Radius.circular(10.0)),
            boxShadow: [
              BoxShadow(
                color: widget.barrierColor ?? Colors.grey.withOpacity(1),
                // spreadRadius: 5,
                // blurRadius: 7,
                // offset: Offset(0, 3), // changes position of shadow
              ),
            ],
          ),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.end,
        children: [
          SizedBox(height: screenHeight(context,dividedBy: 30),),
          Row(
            children: [
              !widget.hideSearch?
                Container(
                  height: screenHeight(context,dividedBy: 16),
                  width: screenWidth(context,dividedBy: 1.3),
                  padding: const EdgeInsets.only(left: 20),
                  child: TextField(
                    cursorColor: Colors.grey,
                    cursorWidth: 1,
                    style: TextStyle(color: Constants.kitGradients[27],
                        fontFamily: 'Montserrat',
                        fontSize: 16),
                    decoration: InputDecoration(
                      labelText: "Search",
                      labelStyle: TextStyle(
                        color: Colors.grey,
                        fontFamily: 'Montserrat',
                        fontSize: 16,
                      ),
                      border: InputBorder.none,
                      filled: true,
                      fillColor: Colors.transparent,
                      focusedBorder: OutlineInputBorder(
                        borderSide:
                        BorderSide(color: Constants.kitGradients[27], width: 0.0),
                        borderRadius: BorderRadius.circular(20.0),
                      ),
                      enabledBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: Colors.grey, width: 0.0),
                        borderRadius: BorderRadius.circular(20.0),
                      ),),
                    onChanged: _filterElements,
                  ),
                ):Container(),
              IconButton(
                iconSize: 30,
                color: Colors.grey,
                icon: widget.closeIcon,
                onPressed: () => Navigator.pop(context),
              ),
            ],
          ),
          SizedBox(height: screenHeight(context,dividedBy: 50),),
          const Divider(),
          Expanded(
            child: ListView(
              children: [
                widget.favoriteElements.isEmpty
                    ? const DecoratedBox(decoration: BoxDecoration())
                    : Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    ...widget.favoriteElements.map(
                          (f) => SimpleDialogOption(
                        child: _buildOption(f),
                        onPressed: () {
                          _selectItem(f);
                        },
                      ),
                    ),
                    const Divider(),
                  ],
                ),
                if (filteredElements.isEmpty)
                  _buildEmptySearchWidget(context)
                else
                  ...filteredElements.map(
                        (e) => SimpleDialogOption(
                      child: _buildOption(e),
                      onPressed: () {
                        _selectItem(e);
                      },
                    ),
                  ),
              ],
            ),
          ),
        ],
      ),
    ),
  );

  Widget _buildOption(CountryCode e) {
    return Container(
      width: 400,
      child: Flex(
        direction: Axis.horizontal,
        children: <Widget>[
          if (widget.showFlag)
            Flexible(
              child: Padding(
                padding: const EdgeInsets.only(right: 16.0),
                child: Image.asset(
                  e.flagUri,
                  package: 'country_code_picker',
                  width: widget.flagWidth,
                ),
              ),
            ),
          Expanded(
            flex: 4,
            child: Text(
              widget.showCountryOnly
                  ? e.toCountryStringOnly()
                  : e.toLongString(),
              overflow: TextOverflow.fade,
              style: TextStyle(
                color: Colors.grey,
                fontFamily: 'Montserrat',
                fontSize: 16,
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildEmptySearchWidget(BuildContext context) {
    if (widget.emptySearchBuilder != null) {
      return widget.emptySearchBuilder(context);
    }

    return Center(
      child: Text('No country found',style: TextStyle(color: Colors.grey,
        fontFamily: 'Montserrat',
        fontSize: 18,),),
    );
  }

  @override
  void initState() {
    filteredElements = widget.elements;
    super.initState();
  }

  void _filterElements(String s) {
    s = s.toUpperCase();
    setState(() {
      filteredElements = widget.elements
          .where((e) =>
      e.code.contains(s) ||
          e.dialCode.contains(s) ||
          e.name.toUpperCase().contains(s))
          .toList();
    });
  }

  void _selectItem(CountryCode e) {
    Navigator.pop(context, e);
  }
}
