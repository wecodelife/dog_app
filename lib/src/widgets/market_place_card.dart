import 'package:flutter/material.dart';
import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:cached_network_image/cached_network_image.dart';

class MarketPlaceCard extends StatefulWidget {
  final String imgUrl;
  final Function itemPressed;
  final String catType;
  final String price;
  final String rating;
  final bool circle;
  MarketPlaceCard({this.imgUrl, this.itemPressed, this.catType, this.circle,this.rating, this.price});
  @override
  _MarketPlaceCardState createState() => _MarketPlaceCardState();
}

class _MarketPlaceCardState extends State<MarketPlaceCard> {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
        onTap: widget.itemPressed,
        child: Container(
          // color:Colors.blue,
         // height: screenWidth(context, dividedBy: 2.1),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              CachedNetworkImage(
                width: screenWidth(context, dividedBy: 1.6),
                height: screenWidth(context, dividedBy: 2.5),
                fit: BoxFit.fill,
                placeholder: (context, url) => Center(
                  heightFactor: 3,
                  widthFactor: 1,
                  child: SizedBox(
                    height: 16,
                    width: 16,
                    child: CircularProgressIndicator(
                      valueColor: AlwaysStoppedAnimation(Constants.kitGradients[19]),
                      strokeWidth: 2,
                    ),
                  ),
                ),
                imageUrl: widget.imgUrl,
                imageBuilder: (context, imageProvider) => Container(
                  width: screenWidth(context, dividedBy: 2.5),
                  height: screenWidth(context, dividedBy: 3.2),
                  decoration: BoxDecoration(
                    //shape: widget.circle ? BoxShape.circle : BoxShape.rectangle,
                    color: Constants.kitGradients[28],
                    borderRadius: widget.circle
                        ? BorderRadius.circular(
                        screenWidth(context, dividedBy: 4.5) / 2)
                        : BorderRadius.circular(7),
                    //shape: BoxShape.circle,
                    image: DecorationImage(
                        image: imageProvider, fit: BoxFit.cover),
                  ),
                ),
              ),
              SizedBox(height: screenHeight(context, dividedBy: 50)),
              Text(
                widget.catType,
                style: TextStyle(
                  color: Constants.kitGradients[27],
                  fontFamily: 'Prompt-Light',
                  fontWeight: FontWeight.w600,
                  fontSize: 16,
                ),
              ),
              // SizedBox(height: screenHeight(context, dividedBy: 200)),
              Container(
                height: screenHeight(context, dividedBy: 50) ,
                width:screenWidth(context, dividedBy: 1.65),
                child: Row(
                 // mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      widget.rating,
                      style: TextStyle(
                        color: Constants.kitGradients[27],
                        fontFamily: 'Prompt-Light',
                        fontWeight: FontWeight.w400,
                        fontSize: 14,
                      ),

                    ),
                    Icon(Icons.star,
                        color: Constants.kitGradients[19],
                        size: 15),
                    Spacer(),
                    Text("₹"+
                    widget.price,
                    style: TextStyle(
                      color: Constants.kitGradients[27],
                      fontFamily: 'Prompt-Light',
                      fontWeight: FontWeight.w400,
                      fontSize: 14
                    ),
                  ),
                  ],
                ),
              )

            ],
          ),
        ));
  }
}
