import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:app_template/src/widgets/profile_icon.dart';
import 'package:flutter/material.dart';

class CommentTile extends StatefulWidget {
  //const CommentTile({Key? key}) : super(key: key);
  final String commentBy;
  final String comment;
  final String postId;
  CommentTile({this.commentBy, this.comment, this.postId});

  @override
  _CommentTileState createState() => _CommentTileState();
}

class _CommentTileState extends State<CommentTile> {
  int favcount = 1;
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          width: screenWidth(context, dividedBy: 1),
          padding: EdgeInsets.symmetric(
            horizontal: screenWidth(context, dividedBy: 30),
            //vertical: screenHeight(context, dividedBy: 6),
          ),
          child: Row(
            children: [
              RichText(
                // overflow: count == 2 ? TextOverflow.visible : TextOverflow.ellipsis,
                // maxLines:count == 2 ?  6 :2,
                text: TextSpan(
                    text: widget.commentBy,
                    style: TextStyle(
                      fontSize: 14,
                      fontWeight: FontWeight.w600,
                      fontFamily: 'Prompt-Light',
                      color: Constants.kitGradients[27],
                    ),
                    children: <TextSpan>[
                      TextSpan(
                        text: "  " + widget.comment,
                        style: TextStyle(
                          fontSize: 14,
                          fontWeight: FontWeight.w400,
                          color: Constants.kitGradients[27],
                        ),
                      )
                    ]),
              ),
              Spacer(),
              GestureDetector(
                  onTap: () {
                    favcount == 1
                        ? setState(() {
                      favcount = 2;
                    })
                        : setState(() {
                      favcount = 1;
                    });
                  },
                  child: favcount == 1
                      ? Icon(
                    Icons.favorite_border_outlined,
                    size: 15,
                    color: Constants.kitGradients[27],
                  )
                      : Icon(
                    Icons.favorite,
                    size: 15,
                    color: Constants.kitGradients[27],
                  )),
              /*SizedBox(
                width: screenWidth(context, dividedBy: 10),
              ),*/
            ],
          ),
        ),
      ],
    );
  }
}
