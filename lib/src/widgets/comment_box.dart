import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:flutter/material.dart';

class CommentBox extends StatefulWidget {
  final TextEditingController textEditingController;
  final bool autofocus;
  final Function onTapPost;
  CommentBox({this.textEditingController, this.autofocus, this.onTapPost});

  @override
  _CommentBoxState createState() => _CommentBoxState();
}

class _CommentBoxState extends State<CommentBox> {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(
        horizontal: screenWidth(context, dividedBy: 30),
      ),
      // width: screenWidth(context, dividedBy: 1),
      height: screenHeight(context, dividedBy: 17),
      child: Row(
        children: [
          Container(
            width: screenWidth(context, dividedBy: 1.3),
            child: TextFormField(
              controller: widget.textEditingController,
              cursorColor: Constants.kitGradients[27],
              keyboardType: TextInputType.text,
              autofocus: widget.autofocus,
              style: TextStyle(
                color: Constants.kitGradients[27],
                fontFamily: 'Montserrat',
                fontSize: 16,
              ),
              autocorrect: false,
              decoration: InputDecoration(
                // labelText: widget.labelText,
                // labelStyle: TextStyle(
                //   color: Colors.grey,
                //   fontFamily: 'Montserrat',
                //   fontSize: 16,
                // ),
                contentPadding: EdgeInsets.symmetric(
                    horizontal: screenWidth(context, dividedBy: 30),
                    vertical: screenHeight(context, dividedBy: 90)),
                hintText: "Add a comment",
                hintStyle: TextStyle(
                  color: Colors.grey,
                  fontFamily: 'Montserrat',
                  fontSize: 15,
                ),
                border: InputBorder.none,
                filled: true,
                fillColor: Colors.transparent,
                focusedBorder: OutlineInputBorder(
                  borderSide: BorderSide(
                      color: Constants.kitGradients[27].withOpacity(0.5) ,
                      width: 0.0),
                  borderRadius: BorderRadius.circular(15.0),
                ),
                enabledBorder: OutlineInputBorder(
                  borderSide: BorderSide(
                      color: Constants.kitGradients[27].withOpacity(0.5),
                      width: 0.0),
                  borderRadius: BorderRadius.circular(15.0),
                ),
              ),
            ),
          ),
          SizedBox(
            width: screenWidth(context, dividedBy: 25),
          ),
          Container(
            // color: Colors.blueAccent,
            width: screenWidth(context,
                dividedBy: 8.1),
            height: screenHeight(context,
                dividedBy: 35),
            child: GestureDetector(
                onTap: widget.onTapPost,
                child: Text(
                  "Post",
                  style: TextStyle(
                    color: Constants.kitGradients[27],
                    fontFamily: 'Montserrat-bold',
                    fontSize: 16,
                  ),
                )),
          )
        ],
      ),
    );
  }
}
