import 'package:flutter/material.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:app_template/src/utils/constants.dart';

class FullScreenImage extends StatelessWidget {
  final String imageUrl;
  final String tag;

  const FullScreenImage({Key key, this.imageUrl, this.tag}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      elevation: 50,
      contentPadding: EdgeInsets.symmetric(horizontal: 0, vertical: 0),
      backgroundColor: Colors.transparent,
      content:  Container(
       // width: screenWidth(context,dividedBy: 1),
        child: GestureDetector(
            child: CachedNetworkImage(
             width: MediaQuery.of(context).size.width,
             height: screenHeight(context, dividedBy: 2),
             // width: screenWidth(context,dividedBy: 1),
              fit: BoxFit.contain,
              placeholder: (context, url) => Center(
                heightFactor: 1,
                widthFactor: 1,
                child: SizedBox(
                  height: 16,
                  width: 16,
                  child: CircularProgressIndicator(
                    valueColor: AlwaysStoppedAnimation(Constants.kitGradients[29]),
                    strokeWidth: 2,
                  ),
                ),
              ),
              imageUrl: imageUrl,
              imageBuilder: (context, imageProvider) => Container(
                width: screenWidth(context, dividedBy: 2),
                height: screenWidth(context, dividedBy: 3.2),
                decoration: BoxDecoration(
                  //shape: widget.circle ? BoxShape.circle : BoxShape.rectangle,
                  color: Constants.kitGradients[28],
                  borderRadius: BorderRadius.circular(7),
                  //shape: BoxShape.circle,
                  image: DecorationImage(
                      image: imageProvider, fit: BoxFit.fill),
                ),
              ),
            ),
            onTap: () {
              Navigator.pop(context);
            },
          ),
      ),

    );
  }
}