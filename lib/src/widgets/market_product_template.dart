import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:shimmer/shimmer.dart';

class ProductTemplate extends StatefulWidget {
  // const ProductTemplate({Key? key}) : super(key: key);

  @override
  _ProductTemplateState createState() => _ProductTemplateState();
}

class _ProductTemplateState extends State<ProductTemplate> {
  @override
  Widget build(BuildContext context) {
    return Column(crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: const EdgeInsets.all(10.0),
          child:
          Shimmer.fromColors(baseColor: Constants.kitGradients[26],
          highlightColor: Constants.kitGradients[12],
            child:
              Container(
                // alignment:Alignment.topLeft,
    width: screenWidth(context,
                  dividedBy: 1.6),
                height: screenHeight(context,
                    dividedBy: 4.8),
                decoration: BoxDecoration(
                  color: Constants.kitGradients[26],

                  // shape: BoxShape.rectangle,
                   borderRadius:  BorderRadius.circular(7),
                  // top: Radius.circular(20),
                  // ),
              ),
              ),

          ),
        ),

        Padding(
          padding: const EdgeInsets.only(left: 20,top: 0,right: 20,bottom: 00),
          child:Shimmer.fromColors(baseColor: Constants.kitGradients[26],
    highlightColor: Constants.kitGradients[12],
            child: Container(
              alignment:Alignment.topLeft,
              width: screenWidth(context,
                dividedBy: 6.6),
              height: screenHeight(context,
                  dividedBy: 55),
              color: Constants.kitGradients[28],



            ),
          ),
        ),

   Row(children: [
       Padding(
         padding: const EdgeInsets.only(left: 20,top: 10,right: 20,bottom: 0),
         child: Shimmer.fromColors(baseColor: Constants.kitGradients[26],
    highlightColor: Constants.kitGradients[12],
           child: Container(
           width: screenWidth(context,
           dividedBy: 15.6),
            height: screenHeight(context,
                dividedBy: 55),
            color: Constants.kitGradients[21],),
         ),
       ),
     // Spacer(),
     Padding(
       padding: const EdgeInsets.only(left: 180,top: 10,right: 20,bottom: 0),
       child: Shimmer.fromColors(baseColor: Constants.kitGradients[26],
    highlightColor: Constants.kitGradients[12],
         child: Container(
              width: screenWidth(context,
                  dividedBy: 15.6),
              height: screenHeight(context,
                  dividedBy: 55),
              color: Constants.kitGradients[17],),
       ),
     ),
    ],

  ),

      ],
    );
  }
}
