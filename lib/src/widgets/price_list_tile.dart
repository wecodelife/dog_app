import 'package:flutter/material.dart';
import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';

class PriceListTile extends StatefulWidget {
  //const BulletedListTile({Key? key}) : super(key: key);
  final String text;
  final String price;

  PriceListTile({this.text,this.price});

  @override
  _PriceListTileState createState() => _PriceListTileState();
}

class _PriceListTileState extends State<PriceListTile> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top:8.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            widget.text,
            style: TextStyle(
              fontSize: 16,
              fontWeight: FontWeight.w400,
              fontFamily: "Prompt-Light",
              color: Constants.kitGradients[27].withOpacity(0.7),
            ),
          ),
          Spacer(),
          Text(widget.price,
            style: TextStyle(
              color: Constants.kitGradients[19],
              fontWeight: FontWeight.bold,
              fontFamily: 'Prompt-Light',
              fontSize: 15,
            ),)
        ],
      ),
    );
  }
}
