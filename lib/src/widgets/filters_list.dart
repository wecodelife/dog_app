import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:app_template/src/widgets/filters.dart';

class FiltersListView extends StatefulWidget {
  // const FiltersListView({Key? key}) : super(key: key);
  final List<List<double>> filters;
  final File image;
  final Function onValueChangedFilter;
  const FiltersListView(
      {Key key, this.filters, this.image, this.onValueChangedFilter});

  @override
  _FiltersListViewState createState() => _FiltersListViewState();
}

class _FiltersListViewState extends State<FiltersListView> {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: screenWidth(context, dividedBy: 1),
      height: screenHeight(context, dividedBy: 3),
      color: Colors.red,
      child: ListView.builder(
        scrollDirection: Axis.horizontal,
        itemCount: widget.filters.length,
        shrinkWrap: true,
        itemBuilder: (BuildContext context, int index) {
          return GestureDetector(
            onTap: widget.onValueChangedFilter,
            child: Container(
              width: screenWidth(context, dividedBy: 4),
              height: screenWidth(context, dividedBy: 4),
              decoration: BoxDecoration(
                color: Constants.kitGradients[19],
                shape: BoxShape.circle,
              ),
              child: ClipRRect(
                child: ColorFiltered(
                  colorFilter: ColorFilter.matrix(filters[index]),
                  child: Image.file(
                    widget.image,
                    fit: BoxFit.cover,
                    width: screenWidth(context, dividedBy: 4),
                    height: screenWidth(context, dividedBy: 4),
                  ),
                ),
              ),
            ),
          );
        },
      ),
    );
  }
}
