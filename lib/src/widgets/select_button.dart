import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class SelectButton extends StatefulWidget {
  final Function onPressed;
  final String title;
  final bool isLoading;
  final double buttonWidth;
  final double buttonHeight;
  final bool isDisabled;
  SelectButton({this.onPressed, this.title, this.isLoading, this.buttonHeight,this.buttonWidth,this.isDisabled});
  @override
  _SelectButtonState createState() => _SelectButtonState();
}

class _SelectButtonState extends State<SelectButton> {
  bool valueSet = false;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        widget.onPressed();
        setState(() {
          valueSet = !valueSet;
        });
      },
      child: Container(
        width: screenWidth(context, dividedBy: widget.buttonWidth),
        height: widget.buttonHeight != null
            ? screenHeight(context, dividedBy: widget.buttonHeight)
            : screenHeight(context, dividedBy: 18),
        alignment: Alignment.center,
        decoration: BoxDecoration(
          border: Border.all(
              color: valueSet? Constants.kitGradients[27].withOpacity(0.3)
                   : Constants.kitGradients[27],
              width: 0.0),
          borderRadius: BorderRadius.circular(25),
          color: widget.isDisabled == true
              ? Colors.transparent
              : Constants.kitGradients[27].withOpacity(0.2),
        ),
        child: widget.isLoading != true
            ? Text(
          widget.title,
          style: TextStyle(
              color: valueSet
                  ? Constants.kitGradients[27].withOpacity(0.3)
                  : Constants.kitGradients[27],
              fontFamily: 'Montserrat',
              fontSize: 13,
              fontWeight: FontWeight.bold),
        )
            : Container(
          // height: screenWidth(context, dividedBy: 40),
          // width:screenWidth(context, dividedBy: 40),
          child: Center(
            child: SizedBox(
              height: 16,
              width:16,
              child: CircularProgressIndicator(
                strokeWidth: 2,
                valueColor: new AlwaysStoppedAnimation<Color>(
                    Constants.kitGradients[0]),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
