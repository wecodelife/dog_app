import 'package:flutter/material.dart';
import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';

class HeadingTile extends StatefulWidget {
  final String heading;
  final Function seeMore;
  HeadingTile({this.heading, this.seeMore});
  @override
  _HeadingTileState createState() => _HeadingTileState();
}

class _HeadingTileState extends State<HeadingTile> {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: widget.seeMore,
      child: Container(
        // padding:
        //     //EdgeInsets.symmetric(horizontal: screenWidth(context, dividedBy: 30)),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Text(widget.heading,
                style: TextStyle(
                  fontSize: 18,
                  fontWeight: FontWeight.w600,
                  fontFamily: 'Prompt-Light',
                  color: Constants.kitGradients[27],
                )),
            Padding(
              padding: const EdgeInsets.only(top:3,),
              child: Icon(Icons.keyboard_arrow_right_rounded,color: Constants.kitGradients[27],),
            ),
          ],
        ),
      ),
    );
  }
}
