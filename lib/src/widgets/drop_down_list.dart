import 'package:flutter/material.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:app_template/src/utils/constants.dart';

class DropDownList extends StatefulWidget {
  List<String> dropDownList;
  String title;
  String dropDownValue;
  ValueChanged<String> onClicked;
  DropDownList(
      {this.title, this.dropDownList, this.dropDownValue, this.onClicked});
  @override
  _DropDownListState createState() => _DropDownListState();
}


String dropdownValue;

class _DropDownListState extends State<DropDownList> {

  @override
  Widget build(BuildContext context) {
    return Container(
      height: screenHeight(context, dividedBy: 15),
     // width: screenWidth(context, dividedBy: 1.2),
      decoration: BoxDecoration(
          color: Colors.transparent,
        // borderRadius: BorderRadius.circular(20),
        // border: Border.all(color: Constants.kitGradients[26],)
      ),
      padding: EdgeInsets.symmetric(horizontal: screenWidth(context, dividedBy: 30),),
      child: DropdownButton<String>(
        underline: Divider(
          thickness: 1.0,
          color: Colors.transparent,
        ),
        icon: Row(
          //mainAxisAlignment: MainAxisAlignment.end,
          crossAxisAlignment: CrossAxisAlignment.end,
          children: [
            Icon(
              Icons.arrow_drop_down,
              size: 30,
              color: Constants.kitGradients[19],
            ),
          ],
        ),
        iconSize: 20,
        dropdownColor: Constants.kitGradients[17],
        value:
        widget.dropDownValue == "" ? dropdownValue : widget.dropDownValue,
        style: TextStyle(fontSize: 16, color: Constants.kitGradients[7],fontWeight: FontWeight.w400,fontFamily: 'Montserrat',),
        items: widget.dropDownList
            .map<DropdownMenuItem<String>>(
              (String value) => DropdownMenuItem<String>(
            value: value,
            child: Container(
              //width: screenWidth(context, dividedBy: 1.5),
              child: Text(
                value,
                style: TextStyle(
                  fontSize: 18,
                  fontWeight:FontWeight.w400,
                  fontFamily: 'Montserrat',
                  color: Constants.kitGradients[27],
                ),
              ),
            ),
          ),
        )
            .toList(),
        onChanged: (selectedValue) {
          setState(() {
            widget.dropDownValue = selectedValue;
            print(widget.dropDownValue);
          });
          widget.onClicked(widget.dropDownValue);
        },
        hint: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.end,
          children: [
            Text(
              widget.title,
              style: TextStyle(fontSize: 18, color: Constants.kitGradients[27]),
            ),
          ],
        ),
      ),
    );
  }
}
