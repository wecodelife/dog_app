
import 'package:app_template/src/widgets/profile_grid_shimmer_loading.dart';
import 'package:flutter/material.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:app_template/src/widgets/user_feed_image.dart';
import 'package:app_template/src/utils/constants.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:app_template/src/models/get_post_response.dart';

class ProfileTabBar extends StatefulWidget {
  final GetPostResponse postResponse;
  final List<bool> hasCaption;
  final List<String> captions;
  final String userName;
  final String userProfile;
  ProfileTabBar(
      {this.postResponse, this.hasCaption, this.captions, this.userProfile, this.userName});
  @override
  _ProfileTabBarState createState() => _ProfileTabBarState();
}

class _ProfileTabBarState extends State<ProfileTabBar> {
  int count = 1;
  //int tIndex = 1;
  double currentIndex=0;
  bool loading = true;

  @override
  Widget build(BuildContext context) {
    return
      Container(
      // height: screenHeight(context, dividedBy: 1),
      width: screenWidth(context, dividedBy: 1),
      child: Column(
        children: [
          Container(
            padding: EdgeInsets.symmetric(
              //horizontal: screenWidth(context, dividedBy: 30),
              //vertical: screenHeight(context, dividedBy: 50),
            ),
            width: screenWidth(context, dividedBy: 1),
            color: Constants.kitGradients[17],
            child: Row(
              children: [
                GestureDetector(
                  onTap: () {
                    count == 1
                        ? setState(() {
                            count = 1;
                          })
                        : setState(() {
                            count = 1;
                          });
                  },
                  child:
                  Container(
                    width: screenWidth(context, dividedBy: 2.3),
                    padding: EdgeInsets.symmetric(
                      vertical: screenHeight(context, dividedBy: 150),
                    ),
                    decoration: BoxDecoration(
                      //color: Colors.blue,
                      border: Border(
                          bottom: count == 1
                              ? BorderSide(
                                  color: Constants.kitGradients[27],
                                )
                              : BorderSide(
                                  color: Constants.kitGradients[17],
                                )),
                    ),
                    child: Icon(
                      Icons.grid_on_sharp,
                      color: Constants.kitGradients[27],
                    ),
                  ),
                ),
                Spacer(
                  flex: 5,
                ),
                GestureDetector(
                  onTap: () {
                    count == 1
                        ? setState(() {
                            count = 2;
                          })
                        : setState(() {});
                  },
                  child: Container(
                    width: screenWidth(context, dividedBy: 2.3),
                    padding: EdgeInsets.symmetric(
                      vertical: screenHeight(context, dividedBy: 150),
                    ),
                    decoration: BoxDecoration(
                      border: Border(
                          bottom: count == 2
                              ? BorderSide(
                                  color: Constants.kitGradients[27],
                                )
                              : BorderSide(
                                  color: Constants.kitGradients[17],
                                )),
                    ),
                    child: Icon(
                      Icons.pets_rounded,
                      color: Constants.kitGradients[27],
                    ),
                  ),
                ),
              ],
            ),
          ),
          SizedBox(
            height: screenHeight(context, dividedBy: 40),
          ),
          widget.postResponse.results !=null?
          count == 1
              ? Container(
                  padding: EdgeInsets.symmetric(
                    horizontal: screenWidth(context, dividedBy: 40),
                    //vertical: screenHeight(context, dividedBy: 50),
                  ),
                  child: GridView.builder(
                    shrinkWrap: true,
                    physics: NeverScrollableScrollPhysics(),
                    itemCount: widget.postResponse.results.length,
                    gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                      crossAxisCount: 3,
                      crossAxisSpacing: 5,
                      mainAxisSpacing: 5,
                    ),
                    itemBuilder: (BuildContext context, int index) {
                      return loading == true ?
                        GestureDetector(
                          onTap: () {
                            setState((){
                              currentIndex= index.toDouble();
                            });
                            push(
                              context,
                              UserFeedImage(
                                getPostResponse: widget.postResponse,
                                // userImage: widget.imgCollection,
                                hasCaption: widget.hasCaption,
                                userCaption: widget.captions,
                                userName: widget.userName,
                                userProfile: widget.userProfile,
                                currentIndex: currentIndex,
                              ),
                            );
                          },
                          child: Container(
                            height: screenWidth(context, dividedBy: 15),
                            width: screenWidth(context, dividedBy: 15),
                            decoration: BoxDecoration(
                              border:
                                  Border.all(color: Constants.kitGradients[27]),
                            ),
                            child: CachedNetworkImage(
                              height: screenWidth(context, dividedBy: 15),
                              width: screenWidth(context, dividedBy: 15),
                              imageUrl: widget.postResponse.results[index].image1,
                              fit: BoxFit.cover,
                              placeholder: (context, url) => Center(
                                heightFactor: 1,
                                widthFactor: 1,
                                child: SizedBox(
                                  height: 16,
                                  width: 16,
                                  child: CircularProgressIndicator(
                                    valueColor: AlwaysStoppedAnimation(
                                        Constants.kitGradients[8]),
                                    strokeWidth: 2,
                                  ),
                                ),
                              ),
                            ),
                          )):
                      Container();
                    },
                  ),
                )
              : Container(
            padding: EdgeInsets.symmetric(
              horizontal: screenWidth(context, dividedBy: 40),
              //vertical: screenHeight(context, dividedBy: 50),
            ),
            child: GridView.builder(
              shrinkWrap: true,
              physics: NeverScrollableScrollPhysics(),
              itemCount: widget.postResponse.results.length,
              gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                crossAxisCount: 3,
                crossAxisSpacing: 5,
                mainAxisSpacing: 5,
              ),
              itemBuilder: (BuildContext context, int index) {
                return GestureDetector(
                    onTap: () {
                      setState((){
                        currentIndex= index.toDouble();
                      });
                      push(
                        context,
                        UserFeedImage(
                          getPostResponse: widget.postResponse,
                          // userImage: widget.imgCollection,
                          hasCaption: widget.hasCaption,
                          userCaption: widget.captions,
                          userName: widget.userName,
                          userProfile: widget.userProfile,
                          currentIndex: currentIndex,
                        ),
                      );
                    },
                    child: Container(
                      height: screenWidth(context, dividedBy: 15),
                      width: screenWidth(context, dividedBy: 15),
                      decoration: BoxDecoration(
                        border:
                        Border.all(color: Constants.kitGradients[27]),
                      ),
                      child: CachedNetworkImage(
                        height: screenWidth(context, dividedBy: 15),
                        width: screenWidth(context, dividedBy: 15),
                        imageUrl: widget.postResponse.results[index].image1,
                        fit: BoxFit.cover,
                        placeholder: (context, url) => Center(
                          heightFactor: 1,
                          widthFactor: 1,
                          child: SizedBox(
                            height: 16,
                            width: 16,
                            child: CircularProgressIndicator(
                              valueColor: AlwaysStoppedAnimation(
                                  Constants.kitGradients[8]),
                              strokeWidth: 2,
                            ),
                          ),
                        ),
                      ),
                    )
                      );
              },
            ),
          )
         : ProfileGridShimmerLoading(),
          SizedBox(
            height: screenHeight(context, dividedBy: 10),
          ),
        ],
      ),
    );
  }
}

