import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:flutter/material.dart';

class FormFeildUserDetails extends StatefulWidget {
  final String labelText;
  // final bool isPassword;
  final ValueChanged onValueChanged;
  final TextEditingController textEditingController;
  final bool textArea;
  final bool userExists;
  // final bool isnumber;
  // final Function getotp;
  final bool autoFocus;
  final Function onChanged;

  FormFeildUserDetails(
      {this.labelText,
        this.textArea,
      this.textEditingController,
      this.onValueChanged,
      // this.isPassword,
        this.autoFocus,
      // this.isnumber,
        this.onChanged,
        this.userExists
      // this.getotp
  });
  @override
  _FormFeildUserDetailsState createState() => _FormFeildUserDetailsState();
}

class _FormFeildUserDetailsState extends State<FormFeildUserDetails> {
  bool visibility = true;
  @override
  Widget build(BuildContext context) {
    return Container(
          child: TextField(
          controller: widget.textEditingController,
          cursorColor: Constants.kitGradients[27],
          onChanged: widget.onChanged,
          keyboardType: widget.textArea ? TextInputType.multiline : TextInputType.text,
          maxLines: widget.textArea ? 6 : 1 ,
          style: TextStyle(
            color: Constants.kitGradients[27],
            fontFamily: 'Montserrat',
            fontSize: 16,
          ),
          autocorrect: false,
          autofocus: widget.autoFocus,
          decoration: InputDecoration(
            contentPadding: EdgeInsets.symmetric(
              horizontal: screenWidth(context, dividedBy: 30),
              vertical: screenHeight(context, dividedBy: 120),
            ),
              labelText: widget.labelText,
              labelStyle: TextStyle(
                color: Colors.grey,
                fontFamily: 'Montserrat',
                fontSize: 16,
              ),
              border: InputBorder.none,
              filled: true,
              fillColor: Colors.transparent,
              focusedBorder: OutlineInputBorder(
                borderSide:
                    BorderSide(color: Constants.kitGradients[27].withOpacity(0.3), width: 0.0),
                borderRadius: BorderRadius.circular(6.0),
              ),
              enabledBorder: OutlineInputBorder(
                borderSide: BorderSide(color: Constants.kitGradients[27].withOpacity(0.3), width: 0.0),
                borderRadius: BorderRadius.circular(6.0),
              ),
              suffixIcon: widget.userExists == false
                  ? Icon(Icons.check_circle_rounded,
                color: Constants.kitGradients[27],
              ): Container(
                height: 1,
                width: 1,
              ), floatingLabelBehavior: FloatingLabelBehavior.never

              // enabledBorder: InputBorder(
              //    BorderSide(color: Constants.kitGradients[29], width: 1.0),
              //   borderRadius: BorderRadius.circular(10.0),
              // )
              // UnderlineInputBorder(
              //   borderSide: BorderSide(color: Constants.kitGradients[29]),
              // ),
              ),
        ));
  }
}
