import 'package:flutter/material.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:app_template/src/utils/constants.dart';

class SwitchAccountDropdown extends StatefulWidget {
  List<String> dropDownList;
  String title;
  String dropDownValue;
  ValueChanged<String> onClicked;
  SwitchAccountDropdown(
      {this.title, this.dropDownList, this.dropDownValue, this.onClicked});

  @override
  _SwitchAccountDropdownState createState() => _SwitchAccountDropdownState();
}

class _SwitchAccountDropdownState extends State<SwitchAccountDropdown> {
  String dropdownValue;
  @override
  Widget build(BuildContext context) {
    return
      Container(
      child: DropdownButton<String>(
        underline: Divider(
          thickness: 1.0,
          color: Colors.transparent,
        ),
        icon: Icon(
          Icons.arrow_drop_down,
         // size: 30,
          color: Constants.kitGradients[27],
        ),
        iconSize: 26,
        dropdownColor: Constants.kitGradients[17],
        value:
        widget.dropDownValue == "" ? dropdownValue : widget.dropDownValue,
        style: TextStyle(fontSize: 16, color: Constants.kitGradients[7],fontWeight: FontWeight.w400,fontFamily: 'Montserrat',),
        items: widget.dropDownList
            .map<DropdownMenuItem<String>>(
              (String value) => DropdownMenuItem<String>(
            value: value,
            child: Container(
              child: Text(
                value,
                style: TextStyle(
                  fontSize: 18,
                  fontWeight:FontWeight.w400,
                  fontFamily: 'Prompt-Ligh',
                  color: Constants.kitGradients[27],
                ),
              ),
            ),
          ),
        )
            .toList(),
        onChanged: (selectedValue) {
          setState(() {
            widget.dropDownValue = selectedValue;
            print(widget.dropDownValue);
          });
          widget.onClicked(widget.dropDownValue);
        },
        hint: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.end,
          children: [
            Text(
              widget.title,
              style: TextStyle(fontSize: 18, color: Constants.kitGradients[27]),
            ),
          ],
        ),
      ),
    );
  }
}
