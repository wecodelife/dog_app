import 'package:flutter/material.dart';
import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';

class BulletedListTile extends StatefulWidget {
  //const BulletedListTile({Key? key}) : super(key: key);
  final String text;
  BulletedListTile({this.text,
  });

  @override
  _BulletedListTileState createState() => _BulletedListTileState();
}

class _BulletedListTileState extends State<BulletedListTile> {
  @override
  Widget build(BuildContext context) {
    return Container(

      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: EdgeInsets.only(
              top: screenHeight(context, dividedBy: 80),
            ),
            child: Container(
              width: 6,
              height: 6,
              decoration: BoxDecoration(
                color: Constants.kitGradients[27].withOpacity(0.7),
                shape: BoxShape.circle,
              ),
            ),
          ),
          SizedBox(
            width: screenWidth(context, dividedBy: 80),
          ),
          Expanded(
              child: Column(
                children: [
                  Text(
                    widget.text,
                    style: TextStyle(
                      fontSize: 15,
                      fontWeight: FontWeight.w400,
                      fontFamily: "Prompt-Light",
                      color: Constants.kitGradients[27].withOpacity(0.7),
                    ),
                  ),
                  // SizedBox(
                  //   height: screenHeight(context, dividedBy: 80),
                  // ),
                  // Text(
                  //   widget.data,
                  //   textAlign: TextAlign.left,
                  //   style: TextStyle(
                  //     fontSize: 15,
                  //     fontWeight: FontWeight.w400,
                  //     fontFamily: "Prompt-Light",
                  //     color: Constants.kitGradients[19],
                  //   ),
                  // ),
                ],
              ))
        ],
      ),
    );
  }
}
