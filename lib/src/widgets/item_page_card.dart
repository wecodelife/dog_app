import 'package:flutter/material.dart';
import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:cached_network_image/cached_network_image.dart';

class ItemPageCard extends StatefulWidget {
  final String imgUrl;
  final Function itemPressed;
  final String catType;
  final String rating;
  final bool circle;
  ItemPageCard({this.imgUrl, this.itemPressed, this.catType, this.circle,this.rating});
  @override
  _ItemPageCardState createState() => _ItemPageCardState();
}

class _ItemPageCardState extends State<ItemPageCard> {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
          onTap: widget.itemPressed,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(height: screenHeight(context, dividedBy: 80)),

              Expanded(
                child: Container(
                  child: CachedNetworkImage(
                    width: screenWidth(context, dividedBy: 2.3),
                    height: screenWidth(context, dividedBy: 3.6),
                    fit: BoxFit.fill,
                    placeholder: (context, url) => Center(
                      heightFactor: 1,
                      widthFactor: 1,
                      child: SizedBox(
                        height: 16,
                        width: 16,
                        child: CircularProgressIndicator(
                          valueColor: AlwaysStoppedAnimation(Constants.kitGradients[19]),
                          strokeWidth: 2,
                        ),
                      ),
                    ),
                    imageUrl: widget.imgUrl,
                    imageBuilder: (context, imageProvider) => Container(
                      width: screenWidth(context, dividedBy: 1.5),
                      height: screenWidth(context, dividedBy: 3.6),
                      decoration: BoxDecoration(
                        //shape: widget.circle ? BoxShape.circle : BoxShape.rectangle,
                        color: Constants.kitGradients[28],
                        borderRadius: widget.circle
                            ? BorderRadius.circular(
                            screenWidth(context, dividedBy: 4.5) / 2)
                            : BorderRadius.circular(7),
                        //shape: BoxShape.circle,
                        image: DecorationImage(
                            image: imageProvider, fit: BoxFit.cover),
                      ),
                    ),
                  ),
                ),
              ),
              SizedBox(height: screenHeight(context, dividedBy: 80)),
              Text(
                widget.catType,
                style: TextStyle(
                  color: Constants.kitGradients[27],
                  fontFamily: 'Prompt-Light',
                  fontWeight: FontWeight.w600,
                  fontSize: 16,
                ),
              ),
              // SizedBox(height: screenHeight(context, dividedBy: 200)),
              Text(
                widget.rating,
                style: TextStyle(
                  color: Constants.kitGradients[27],
                  fontFamily: 'Prompt-Light',
                  fontWeight: FontWeight.w400,
                  fontSize: 14,
                ),
              ),
              SizedBox(height: screenHeight(context, dividedBy: 80)),

            ],
          ));
  }
}
