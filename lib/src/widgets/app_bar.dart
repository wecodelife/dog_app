import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class AppBarDogApp extends StatefulWidget {

   final Widget leftIcon;
   final Widget tickIcon;
   final Function onTapTickIcon;
   final Function onTapLeftIcon;
   final Widget rightIcon;
   final Function onTapRightIcon;
   final Widget title;
   final Widget plusrightIcon;
   final Function onTapplusRightIcon;

  AppBarDogApp({this.leftIcon, this.onTapLeftIcon, this.title, this.onTapRightIcon,this.rightIcon, this.onTapplusRightIcon,  this.plusrightIcon, this.tickIcon, this.onTapTickIcon});
  @override
  _AppBarDogAppState createState() => _AppBarDogAppState();
}

class _AppBarDogAppState extends State<AppBarDogApp> {
  List<String> appbarImage =["assets/images/WOBOW_logo.svg"];

  @override
  Widget build(BuildContext context) {
    return Container(

    // decoration: BoxDecoration(
    // borderRadius: BorderRadius.only(bottomLeft: Radius.circular(20),bottomRight: Radius.circular(20))),
        //height: screenHeight(context, dividedBy: 1),
        width: screenWidth(context, dividedBy: 1),
        padding: EdgeInsets.symmetric(
          horizontal: screenWidth(context, dividedBy:200),
          //vertical: screenHeight(context, dividedBy: 6),
        ),
        child:Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children:[
          SizedBox(
            width: screenWidth(context, dividedBy: 150),
          ),
          if(widget.leftIcon!=null)
            GestureDetector(
          child: Icon(Icons.arrow_back,color: Constants.kitGradients[27],),
          onTap: widget.onTapplusRightIcon != null ? widget.onTapplusRightIcon : (){pop(context);},
        ),
          // Spacer(),
          SizedBox(
            width: screenWidth(context, dividedBy: 30),
          ),
          // GestureDetector(
          //   child:widget.leftIcon != null ? widget.leftIcon : Icon(Icons.arrow_back,color: Constants.kitGradients[27],),
          //   onTap: widget.onTapLeftIcon != null ? widget.onTapLeftIcon : (){pop(context);},
          // ),
          Container(
            child: widget.title,
          ),
          // SizedBox(
          //   width: screenWidth(context, dividedBy: 30),
          // ),
          // SizedBox(
          //   width: screenWidth(context, dividedBy: 200),
          // ),

          Spacer(),
          if(widget.rightIcon!=null)
          GestureDetector(
            child:widget.rightIcon != null ? widget.rightIcon : Icon(Icons.arrow_back,color: Constants.kitGradients[27],),
            onTap: widget.onTapRightIcon != null ? widget.onTapRightIcon : (){pop(context);},
          ),
          if (widget.plusrightIcon!=null)
          GestureDetector(
            child:widget.plusrightIcon != null ? widget.plusrightIcon : Container(width: screenWidth(context, dividedBy: 50),),
            onTap: widget.onTapplusRightIcon != null ? widget.onTapplusRightIcon : (){},
          ),
if( widget.tickIcon!=null)
    GestureDetector(
    child:widget.tickIcon != null ? widget.tickIcon : Container(width: screenWidth(context, dividedBy: 50),),
    onTap: widget.onTapTickIcon != null ? widget.onTapTickIcon : (){},
    ) ,
          SizedBox(
            width: screenWidth(context, dividedBy:20),
          ),

        ]
      )

    );
  }
}
