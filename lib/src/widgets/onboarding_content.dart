import 'package:app_template/src/widgets/app_bar.dart';
import 'package:app_template/src/widgets/build_button.dart';
import 'package:app_template/src/widgets/drop_down_list.dart';
import 'package:app_template/src/widgets/onboarding_button.dart';
import 'package:app_template/src/widgets/onboarding_textfield.dart';
import 'package:app_template/src/widgets/page_indicator.dart';
import 'package:app_template/src/widgets/user_text_field.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:app_template/src/utils/constants.dart';

class OnBoardingContent extends StatefulWidget {
  //const OnBoardingContent({Key? key}) : super(key: key);
  final String caption;
  final String image;
  final String buttonTitle;
  final String labelText;
  final Widget suffixIcon;
  final bool hasSuffix;
  final bool isGender;
  Function onTapField;
  bool readOnly;
  final TextEditingController textEditingController;
  Function onTapIcon;
  final int currentIndex;
  final Function onButtonPressed;
  List<String> dropDownList;
  String dropDownTitle;
  String dropDownValue;
  ValueChanged<String> onClicked;
  OnBoardingContent(
      {this.caption,
      this.image,
      this.buttonTitle,
      this.labelText,
      this.isGender,
      this.onButtonPressed,
      this.currentIndex,
      this.textEditingController,
      this.suffixIcon,
      this.onTapIcon,
      this.hasSuffix,
      this.dropDownList,
      this.dropDownTitle,
      this.dropDownValue,
      this.onTapField,
      this.readOnly,
      this.onClicked});

  @override
  _OnBoardingContentState createState() => _OnBoardingContentState();
}

class _OnBoardingContentState extends State<OnBoardingContent> {
  TextEditingController nameTextEditingController = new TextEditingController();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Constants.kitGradients[17],
      body: Container(
        height: screenHeight(context, dividedBy: 1),
        width: screenWidth(context, dividedBy: 1),
        decoration: BoxDecoration(
          color: Constants.kitGradients[17],
          image: DecorationImage(
            fit: BoxFit.cover,
            image: AssetImage(Constants.BACKGROUND_IMAGE),
            colorFilter: new ColorFilter.mode(
                Colors.black.withOpacity(0.6), BlendMode.darken),
          ),
        ),
        child: SingleChildScrollView(
          child: Container(
            width: screenWidth(context, dividedBy: 1),
            padding: EdgeInsets.symmetric(
              horizontal: screenWidth(context, dividedBy: 30),
// vertical: screenHeight(context, dividedBy: 5),
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(
                  height: screenHeight(context, dividedBy: 3.2),
                ),
                Text(
                  widget.caption,
                  style: TextStyle(
                    fontSize: 26,
                    fontWeight: FontWeight.bold,
                    fontFamily: "Montserrat-ExtraBold",
                    color: Constants.kitGradients[27],
                  ),
                ),
                SizedBox(
                  height: screenHeight(context, dividedBy: 90),
                ),
                Text(
                  "Add some more details about your puppy to complete the registration             ",
                  textAlign: TextAlign.left,
                  style: TextStyle(
                      color: Constants.kitGradients[27],
                      fontFamily: "Montserrat",
                      fontSize: 16,
                      fontWeight: FontWeight.w400),
                ),
                SizedBox(
                  height: screenHeight(context, dividedBy: 30),
                ),
                Column(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    SizedBox(
                      height: screenHeight(context, dividedBy: 30),
                    ),
                    widget.isGender == true
                        ? DropDownList(
                            dropDownList: widget.dropDownList,
                            dropDownValue: widget.dropDownValue,
                            onClicked: widget.onClicked,
                            title: widget.dropDownTitle,
                          )
                        : OnBoardingTextField(
                            labelText: widget.labelText,
                            textEditingController: widget.textEditingController,
                            hasSuffix: widget.hasSuffix,
                            suffixIcon: widget.suffixIcon,
                            onTapIcon: widget.onTapIcon,
                            textArea: false,
                            onTap: widget.onTapField,
                            readOnly: widget.readOnly,
                          ),

                    SizedBox(
                      height: screenHeight(context, dividedBy: 8),
                    ),
                    PageIndicator(
                      currentIndex: widget.currentIndex,
                    ),
                    SizedBox(
                      height: screenHeight(context, dividedBy: 15),
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: [
                        OnBoardingButton(
                          title: "Next",
                          onPressed: (){
                            widget.textEditingController.text.isEmpty ? showToast("Enter valid details")
                            :widget.onButtonPressed();
                          },
                          isLoading: false,
                          width: 3,
                        ),
                        SizedBox(width: screenWidth(context, dividedBy: 20)),
                      ],
                    ),
                    SizedBox(
                      height: screenHeight(context, dividedBy: 30),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
//
// DropDownList(
// title: "Select",
// dropDownList: size,
// dropDownValue: sizeType,
// onClicked: (value) {
// setState(() {
// sizeType = value;
// });
// },
// ),
