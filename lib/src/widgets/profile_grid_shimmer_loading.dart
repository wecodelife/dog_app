import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:shimmer/shimmer.dart';

class ProfileGridShimmerLoading extends StatefulWidget {
  // const ProfileGridShimmerLoading({Key? key}) : super(key: key);

  @override
  _ProfileGridShimmerLoadingState createState() => _ProfileGridShimmerLoadingState();
}

class _ProfileGridShimmerLoadingState extends State<ProfileGridShimmerLoading> {
  @override
  Widget build(BuildContext context) {
    return Column(mainAxisAlignment: MainAxisAlignment.end,
      children: [
        Padding(
          padding: const EdgeInsets.all(18.0),
          child: Container(
            height: screenHeight(context,
                dividedBy: 2),
            child: GridView.builder(
            itemCount: 9,
            gridDelegate:
            SliverGridDelegateWithFixedCrossAxisCount(
                crossAxisCount: 3),
            itemBuilder: (BuildContext context, int index) {
              return Shimmer.fromColors(baseColor: Constants.kitGradients[26],
                highlightColor: Constants.kitGradients[12],
                child:
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Container(
                    width: screenWidth(context,
                        dividedBy: 1.5),
                    height: screenHeight(context,
                        dividedBy: 4.6),
                    decoration: BoxDecoration(
                      color: Constants.kitGradients[18],
                      shape: BoxShape.rectangle,
                      borderRadius: BorderRadius.vertical(bottom: Radius.circular(15),
                      top: Radius.circular(15)),
                    ),
                  ),
                ),
              );

            }),
          ),
        ),



    ]


    );
  }
}
