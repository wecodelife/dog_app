import 'package:flutter/material.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:app_template/src/utils/constants.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:app_template/src/bloc/user_bloc.dart';



class EstimateProductDelivery extends StatefulWidget {
  final String itemImage;
  EstimateProductDelivery({
    this.itemImage,
  });
  @override
  _EstimateProductDeliveryState createState() => _EstimateProductDeliveryState();
}

class _EstimateProductDeliveryState extends State<EstimateProductDelivery> {
  UserBloc userBloc=UserBloc();
  int count = 0;
  int itemCount = 1;
  void initState() {
    // TODO: implement initState
    userBloc.getCartDetails();
    userBloc.deletePostResponse.listen((event) {
      setState(() {
        // loading = false;
      });
      showSuccessAnimatedToast(
          context: context, msg: "Item Deleted Successfully");
      userBloc.getCartDetails();
    });
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return Container(
      width: screenWidth(context, dividedBy: 1),
      padding: EdgeInsets.symmetric(
        horizontal: screenWidth(context, dividedBy: 30),
        vertical: screenHeight(context, dividedBy: 60),
      ),
      decoration: BoxDecoration(
        color:
        // (widget.selectedItems).contains(widget.indexValues )
        Constants.kitGradients[19].withOpacity(0.12),
        borderRadius: BorderRadius.circular(8),
        //  border: widget.selected == true
        // (widget.selectedItems).contains(widget.indexValues )
        //    ? Border.all(color: Constants.kitGradients[19].withOpacity(0.4), width: 0.0)
        //   : Border.all(color: Colors.transparent, width: 0.0),
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          ClipRRect(
            borderRadius: BorderRadius.circular(10),
            child: CachedNetworkImage(
              width: screenWidth(context, dividedBy: 6.3),
              height: screenHeight(context, dividedBy:15),
              fit: BoxFit.cover,
              placeholder: (context, url) => Center(
                heightFactor: 1,
                widthFactor: 1,
                child: SizedBox(
                  height: 16,
                  width: 16,
                  child: CircularProgressIndicator(
                    valueColor: AlwaysStoppedAnimation(
                        Constants.kitGradients[19]),
                    strokeWidth: 2,
                  ),
                ),
              ),
              imageUrl:widget.itemImage,
              imageBuilder: (context, imageProvider) {
                return Container(
                  width: screenWidth(context, dividedBy: 4.5),
                  height: screenWidth(context, dividedBy: 4.5),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10),
                    //shape: BoxShape.circle,
                    image: DecorationImage(
                        image: imageProvider, fit: BoxFit.cover),
                  ),
                );
              },
            ),
          ),
          SizedBox(width: screenWidth(context,
              dividedBy: 70),),
          Text(
            "Estimated Delivery by",
            style: TextStyle(
              fontSize: 15,
              fontFamily: 'Prompt-Light',
              //fontWeight: FontWeight.w700,
              color: Constants.kitGradients[27],
            ),
          ),

        ],
      ),
    );

  }
}
