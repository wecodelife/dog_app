import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:flutter/material.dart';
import 'package:app_template/src/widgets/build_button.dart';

class AlertDialogBox extends StatefulWidget {
  // const AlertDialogBox({Key? key}) : super(key: key);
  final String title, descriptions;
  // final Image img;
  final Function onPressedYes;
  // final TextStyle titleStyle, descriptionStyle;
  const AlertDialogBox(
      {Key key,
      this.title,
      this.descriptions,
      // this.text,
      // this.img,
      // this.leftButtonLabel,
      // this.rightButtonLabel,
      // this.titleStyle,
      // this.descriptionStyle,
      this.onPressedYes})
      : super(key: key);

  @override
  _AlertDialogBoxState createState() => _AlertDialogBoxState();
}

class _AlertDialogBoxState extends State<AlertDialogBox> {
  @override
  Widget build(BuildContext context) {
    return Dialog(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(7),
        ),
        elevation: 0,
        backgroundColor: Colors.transparent,
        child:  contentBox(context)
    );
  }

  contentBox(context) {
    return Container(

      padding: EdgeInsets.all(20),
      margin: EdgeInsets.only(top: 45),
      decoration: BoxDecoration(
          shape: BoxShape.rectangle,
          color: Constants.kitGradients[17],
          borderRadius: BorderRadius.circular(7),
          boxShadow: [
            BoxShadow(
                color: Constants.kitGradients[7],
                offset: Offset(0, 10),
                blurRadius: 10),
          ]),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          // SizedBox(
          //   height: screenHeight(context, dividedBy: 50),
          // ),
          Text(widget.title,
            style: TextStyle(
              fontSize: 17,
              fontWeight: FontWeight.bold,
              fontFamily: "Montserrat",
              color: Constants.kitGradients[27],
            ),),
          SizedBox(
            height: screenHeight(context, dividedBy: 50),
          ),
          Text(widget.descriptions,
            style: TextStyle(
              fontSize: 14,
              fontWeight: FontWeight.w600,
              fontFamily: "Montserrat",
              color: Constants.kitGradients[27],
            ),),
          SizedBox(
            height: screenHeight(context, dividedBy: 30),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              GestureDetector(
                onTap: (){
                  pop(context);
                },
                child: Text(
                  "Cancel",
                  style: TextStyle(
                    fontSize: 18,
                    fontWeight: FontWeight.bold,
                    fontFamily: "Montserrat",
                    color: Constants.kitGradients[27],
                  ),
                ),
              ),
              SizedBox(width: screenWidth(context, dividedBy: 20)),
              GestureDetector(
                onTap: (){
                  widget.onPressedYes();
                },
                child: Text(
                  "Ok",
                  style: TextStyle(
                    fontSize: 18,
                    fontWeight: FontWeight.bold,
                    fontFamily: "Montserrat",
                    color: Constants.kitGradients[27],
                  ),
                ),
              ),
              // BuildButton(
              //   buttonWidth: 4,
              //   buttonHeight: 30,
              //   isLoading:false,
              //   title: "Cancel",
              //   isDisabled:false,
              //   onPressed: (){
              //     pop(context);
              //   },
              // ),
              // BuildButton(
              //   buttonWidth: 4,
              //   buttonHeight: 30,
              //   isLoading:false,
              //   title: "Ok",
              //   isDisabled:false,
              //   onPressed: (){
              //     widget.onPressedYes();
              //   },
              // ),
            ],
          )
        ],
      ),
    );
  }
}
