import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class OnBoardingButton extends StatefulWidget {
  final Function onPressed;
  final String title;
  final bool isLoading;
  final double width;
  OnBoardingButton({this.onPressed, this.title, this.isLoading, this.width});

  @override
  _OnBoardingButtonState createState() => _OnBoardingButtonState();
}

class _OnBoardingButtonState extends State<OnBoardingButton> {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        widget.onPressed();
      },
      child: Container(
        width: screenWidth(context, dividedBy: widget.width),
        height: screenHeight(context, dividedBy: 17),
        alignment: Alignment.center,
        decoration: BoxDecoration(
          border: Border.all(color: Colors.grey, width: 0.0),
          borderRadius: BorderRadius.circular(20),
          color: Colors.transparent,
        ),
        // padding: EdgeInsets.all(screenWidth(context, dividedBy: 40),),
        child: widget.isLoading != true
            ? Text(
          widget.title,
          style:  TextStyle(
              fontSize: 16,
              fontWeight: FontWeight.bold,
              fontFamily: "Montserrat",
              color: Constants.kitGradients[27],),
        )
            : Container(
          // height: screenWidth(context, dividedBy: 40),
          // width:screenWidth(context, dividedBy: 40),
          child: Center(
            child: SizedBox(
              height: 16,
              width:16,
              child: CircularProgressIndicator(
                strokeWidth: 2,
                valueColor: new AlwaysStoppedAnimation<Color>(
                    Constants.kitGradients[0]),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
