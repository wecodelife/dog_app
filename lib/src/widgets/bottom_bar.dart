import 'package:app_template/src/screens/item_details_page.dart';
import 'package:flutter/material.dart';
import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';


class CustomBottomBar extends StatefulWidget {
  final Function onTapProfile;
  final Function onTapHome;
  final double currentIndex;
  final Function onTapSearch;
  final Widget rightIcon;
  final Function onTapplusRightIcon;
  // final Function onTapRightIcon;

  CustomBottomBar(
      {this.onTapHome,
        this.onTapSearch,
      this.onTapProfile,
      this.currentIndex,
        this.rightIcon,
        // this.onTapRightIcon
         this.onTapplusRightIcon,});
  @override
  _CustomBottomBarState createState() => _CustomBottomBarState();
}

class _CustomBottomBarState extends State<CustomBottomBar> {
  bool Size = false;
  // Map<int, bool> home= {0:true,1:false,2:false,3:false};
  // Map<int, bool> shop= {0:false,1:true,2:false,3:false};
  // Map<int, bool> notification= {0:false,1:false,2:true,3:false};
  // Map<int, bool> profile= {0:false,1:false,2:false,3:true};


  // double _size = 50;
  int count;
  int _currentIndex = 0;
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Container(
        width: screenWidth(context, dividedBy: 1),
        height: screenHeight(context, dividedBy: 13),
        alignment: Alignment.center,
        decoration: BoxDecoration(

          color: Constants.kitGradients[27]
          ,
          border: Border(
            top: BorderSide(color: Constants.kitGradients[27]),
            // bottom: BorderSide(color: Constants.kitGradients[27])
          ),
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            SizedBox(
              width: screenWidth(context, dividedBy: 17),
            ),
            GestureDetector(
              onTap: () {setState(() {
                Size =!Size;


                // _size = _size == 25 ? 50 : 75;
              });
                widget.onTapHome();
              },
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Container(
                    width: screenWidth(context,
                        dividedBy: 6.6),
                    height: screenHeight(context,
                        dividedBy: 15),
                    // color: Constants.kitGradients[28],
                    child: Icon(Icons.home,
                        color: widget.currentIndex == 0
                            ? Constants.kitGradients[3]
                            : Colors.black,
                      size:widget.currentIndex== 0 ? 35 : 25,
                        ),
                  ),
                  // Text(
                  //   "Home",
                  //   style: TextStyle(
                  //     fontSize: 16,
                  //
                  //     fontWeight: FontWeight.w400,
                  //     color: widget.currentIndex == 0
                  //         ? Constants.kitGradients[31]
                  //         : Colors.black,
                  //   ),
                  // ),
                ],
              ),
            ),
            Spacer(),
            SizedBox(
              width: screenWidth(context, dividedBy: 65),
            ),


            GestureDetector(
              onTap: () {
                widget.onTapSearch();
              },
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Container( width: screenWidth(context,
                      dividedBy: 6.6),
                    height: screenHeight(context,
                        dividedBy: 15),
                    // color: Constants.kitGradients[28],
                    child: Icon(Icons.shopping_cart,
                        color: widget.currentIndex == 1
                            ? Constants.kitGradients[3]
                            : Colors.black,
                      size:widget.currentIndex== 1 ? 35 : 25,

                    ),
                  ),
                  // Text(
                  //   "Shop",
                  //   style: TextStyle(
                  //     fontSize: 14,
                  //     fontWeight: FontWeight.w400,
                  //     color: widget.currentIndex == 1
                  //         ? Constants.kitGradients[29]
                  //         : Colors.black,
                  //   ),
                  // ),
                ],
              ),
            ),
            Spacer(),
            // GestureDetector(
            //   child:widget.rightIcon != null ? widget.rightIcon : Container(width: screenWidth(context, dividedBy: 50),),
            //   onTap: widget.onTapRightIcon != null ? widget.onTapRightIcon : (){},
            //   )
            // ),
            SizedBox(
              width: screenWidth(context, dividedBy: 35),
            ),
            GestureDetector(
                onTap: () {
                  widget.onTapplusRightIcon(
                  );
                },
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Container( width: screenWidth(context,
                        dividedBy: 6.6),
                      height: screenHeight(context,
                          dividedBy: 15),
                      // color: Constants.kitGradients[28],
                      child: Icon(Icons.add,
                        color: widget.onTapplusRightIcon == 2
                            ? Constants.kitGradients[3]
                            : Colors.black,
                        size:35
                        // widget.currentIndex== 2? 30 : 25,
                      ),
                    ),
                    // GestureDetector(
                    //   onTap: () {
                    //     widget.onTapRightIcon ();
                    //   },
                    //   child: Column(
                    //     mainAxisAlignment: MainAxisAlignment.center,
                    //     children: [
                    //       Container( width: screenWidth(context,
                    //           dividedBy: 6.6),
                    //         height: screenHeight(context,
                    //             dividedBy: 15),
                    //         // color: Constants.kitGradients[28],
                    //         child: Icon(Icons.favorite,
                    //             color: widget.onTapRightIcon == 3
                    //                 ? Constants.kitGradients[3]
                    //                 : Colors.black,
                    //           size:widget.currentIndex== 3 ? 30 : 25,
                    //         ),
                    //       ),
                    //
                    //       // Text(
                    //       //   'Notification',
                    //       //   style: TextStyle(
                    //       //     fontSize: 14,
                    //       //     fontWeight: FontWeight.w400,
                    //       //     color: widget.onTapRightIcon == 2
                    //       //         ? Constants.kitGradients[27]
                    //       //         : Colors.black,
                    //       //   ),
                    //       // ),
                    //     ],
                    //   ),
                    // ),
                    // SizedBox(
                    //   width: screenWidth(context, dividedBy: 15),
                    // ),

                  ],
                )
            ),
            Spacer(),
            SizedBox(
              width: screenWidth(context, dividedBy: 35),
            ),
            GestureDetector(
              onTap: () {
                widget.onTapProfile();
              },
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Container( width: screenWidth(context,
                      dividedBy: 6.6),
                    height: screenHeight(context,
                        dividedBy: 15),
                    // color: Constants.kitGradients[28],
                    child: Icon(Icons.pets_rounded,
                      color: widget.currentIndex == 2
                          ? Constants.kitGradients[3]
                          : Colors.black,
                      size:widget.currentIndex== 2 ? 35 : 25,
                    ),
                  ),


                  // Text(
                  //   "Profile",
                  //   style: TextStyle(
                  //     fontSize: 14,
                  //     fontWeight: FontWeight.w400,
                  //     color: widget.currentIndex == 2
                  //         ? Constants.kitGradients[27]
                  //         : Colors.black,
                  //   ),
                  // ),
                ],
              ),
            ),
            Spacer(),

            SizedBox(
              width: screenWidth(context, dividedBy: 55),
            ),

      ]
    )
    )

    );

  }
}
