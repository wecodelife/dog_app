import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class BuildButton extends StatefulWidget {
  final Function onPressed;
  final String title;
  final bool isLoading;
  final double buttonWidth;
  final double buttonHeight;
  final bool isDisabled;
  BuildButton({this.onPressed, this.title, this.isLoading, this.buttonHeight,this.buttonWidth,this.isDisabled});
  @override
  _BuildButtonState createState() => _BuildButtonState();
}

class _BuildButtonState extends State<BuildButton> {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        widget.onPressed();
      },
      child: Container(
        width: screenWidth(context, dividedBy: widget.buttonWidth),
        height: widget.buttonHeight != null
            ? screenHeight(context, dividedBy: widget.buttonHeight)
            : screenHeight(context, dividedBy: 18),
        alignment: Alignment.center,
        decoration: BoxDecoration(
          border: Border.all(color: Colors.grey, width: 0.0),
          borderRadius: BorderRadius.circular(25),
          color: widget.isDisabled == true
              ? Colors.transparent
              : Constants.kitGradients[27].withOpacity(0.2),
        ),
        child: widget.isLoading != true
            ? Text(
                widget.title,
                style: TextStyle(
                    color: widget.isDisabled == true
                        ? Constants.kitGradients[27]
                        : Constants.kitGradients[27],
                    fontFamily: 'OpenSansRegular',
                    fontSize: 15,
                    fontWeight: FontWeight.bold),
              )
            : Container(
          // height: screenWidth(context, dividedBy: 40),
          // width:screenWidth(context, dividedBy: 40),
          child: Center(
            child: SizedBox(
              height: 16,
              width:16,
              child: CircularProgressIndicator(
                strokeWidth: 2,
                valueColor: new AlwaysStoppedAnimation<Color>(
                    Constants.kitGradients[0]),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
