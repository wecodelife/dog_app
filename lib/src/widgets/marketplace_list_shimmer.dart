

import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:app_template/src/widgets/market_product_template.dart';
import 'package:flutter/material.dart';
import 'package:shimmer/shimmer.dart';

class MarketPlaceList extends StatefulWidget {
  // const MarketPlaceList({Key? key}) : super(key: key);

  @override
  _MarketPlaceListState createState() => _MarketPlaceListState();
}

class _MarketPlaceListState extends State<MarketPlaceList> {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: screenHeight(context,dividedBy: 2.70
      ),
      width: screenWidth(context,dividedBy: 1),

      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          // Spacer(),
          Padding(
            padding: const EdgeInsets.only(left: 5,top: 0,right: 275,bottom: 00),
            child: Shimmer.fromColors(
              baseColor: Constants.kitGradients[26],
              highlightColor: Constants.kitGradients[12],
              child:Container(
                alignment:Alignment.topLeft,
                width: screenWidth(context,
                    dividedBy: 4.0),
                height: screenHeight(context,
                    dividedBy: 30),
                color: Constants.kitGradients[28],

              ),
            ),
          ),
          Container( width: screenWidth(context,
              dividedBy: 1),
            height: screenHeight(context,
                dividedBy: 3.27
            ),
            child: ListView.builder(
                shrinkWrap: true,
                scrollDirection: Axis
                    .horizontal,
                itemCount: 5,
                // itemCount: snapshot.data.results.length,
                itemBuilder: (
                    BuildContext context,
                    int index,
                    ) {
                  return ProductTemplate();

                }

            ),
          ),


        ],
      ),
    );
  }
}
