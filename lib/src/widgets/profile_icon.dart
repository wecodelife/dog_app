import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:flutter/material.dart';
import 'package:cached_network_image/cached_network_image.dart';

class ProfileIcon extends StatefulWidget {
  final String profileImage;
  final double circleRadius;
  ProfileIcon({this.profileImage, this.circleRadius});
  @override
  _ProfileIconState createState() => _ProfileIconState();
}

class _ProfileIconState extends State<ProfileIcon> {
  @override
  Widget build(BuildContext context) {
    return CachedNetworkImage(
      width: screenWidth(context, dividedBy: widget.circleRadius),
      height: screenWidth(context, dividedBy: widget.circleRadius),
      imageUrl: widget.profileImage == null ? "https://www.cornwallbusinessawards.co.uk/wp-content/uploads/2017/11/dummy450x450.jpg" :widget.profileImage,
      fit: BoxFit.cover,
      placeholder: (context, url) => Center(
        heightFactor: 1,
        widthFactor: 1,
        child: SizedBox(
          height: 16,
          width: 16,
          child: CircularProgressIndicator(
            valueColor: AlwaysStoppedAnimation(Constants.kitGradients[27]),
            strokeWidth: 2,
          ),
        ),
      ),
      imageBuilder: (context, imageProvider) {
        return Container(
          width: screenWidth(context, dividedBy: widget.circleRadius),
          height: screenWidth(context, dividedBy: widget.circleRadius),
          alignment: Alignment.center,
          decoration: BoxDecoration(
              color: Constants.kitGradients[17].withOpacity(0.5),
              shape: BoxShape.circle,
             // border: Border.all(color: Constants.kitGradients[0], width: 2),
              image: DecorationImage(
                image: imageProvider,
                fit: BoxFit.cover,
              ),
              // boxShadow: [
              //   BoxShadow(
              //       blurRadius: 2.0,
              //       spreadRadius: 2.0,
              //       offset: Offset(3.0, 2.0),
              //       color: Constants.kitGradients[19].withOpacity(0.5)),
              // ]
          ),
          // child: Icon(Icons.person_rounded,
          //     color: Constants.kitGradients[19],
          //     size: widget.circleRadius < 5 ? 60 : 24),
        );
      },
    );
  }
}
