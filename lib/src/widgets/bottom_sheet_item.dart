import 'package:flutter/material.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:app_template/src/utils/constants.dart';

class BottomSheetItem extends StatefulWidget {
  //const BottomSheetItem({Key? key}) : super(key: key);
  final String title;
  final Widget leading;
  final Function onTap;
  final Widget trailing;
  BottomSheetItem({this.title,this.leading,this.onTap,this.trailing});

  @override
  _BottomSheetItemState createState() => _BottomSheetItemState();
}

class _BottomSheetItemState extends State<BottomSheetItem> {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: widget.onTap,
      child: Container(
        //height:screenHeight(context, dividedBy:14),
        width: screenWidth(context, dividedBy: 1),
        padding: EdgeInsets.symmetric(
          horizontal: screenWidth(context, dividedBy: 30),
           //vertical: screenHeight(context, dividedBy: 60),
        ),
        decoration: BoxDecoration(
          color: Constants.kitGradients[17],
        ),
          child:Row(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Container(
                width: screenWidth(context, dividedBy: 10),
                child: widget.leading == null ? Container() : widget.leading,
              ),
              SizedBox(width: screenWidth(context, dividedBy: 20)),
              // Icon( Icons.done,
              //   color: Constants.kitGradients[27],),

              Text(widget.title,style: TextStyle(
                fontSize: 16,
                fontWeight: FontWeight.w600,
                fontFamily: 'Montserrat',
                color: Constants.kitGradients[27],
              ),),
              Spacer(),
              Container(
                width: screenWidth(context, dividedBy: 20),
                child: widget.trailing == null ? Container() : widget.trailing,
              ),

            ],
          ),

      ),
    );
  }
}
