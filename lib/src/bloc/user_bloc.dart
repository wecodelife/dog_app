import 'dart:async';

import 'package:app_template/src/models/add_post_request.dart';
import 'package:app_template/src/models/add_post_response.dart';
import 'package:app_template/src/models/get_breed_response_model.dart';
import 'package:app_template/src/models/get_post_comment_response.dart';
import 'package:app_template/src/models/add_post_comment_response.dart';
import 'package:app_template/src/models/add_post_comment_request.dart';
import 'package:app_template/src/models/get_post_response.dart';
import 'package:app_template/src/models/get_user_profile_response.dart';
import 'package:app_template/src/models/login_request_model.dart';
import 'package:app_template/src/models/login_response_model.dart';
import 'package:app_template/src/models/state.dart';
import 'package:app_template/src/models/user_sign_up_request_model.dart';
import 'package:app_template/src/models/user_sign_up_response_model.dart';
import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/object_factory.dart';
import 'package:app_template/src/utils/validators.dart';
import 'package:app_template/src/models/user_update_response_model.dart';
import 'package:app_template/src/models/user_update_request_model.dart';
import 'package:app_template/src/models/post_update_response_model.dart';
import 'package:app_template/src/models/post_update_request_model.dart';
import 'package:app_template/src/models/get_logout_response.dart';
import 'package:app_template/src/models/username_request_model.dart';
import 'package:app_template/src/models/username_response_model.dart';
import 'package:app_template/src/models/like_post_request.dart';
import 'package:app_template/src/models/like_post_response.dart';
import 'package:app_template/src/models/get_image_slider.dart';
import 'package:app_template/src/models/get_product_category.dart';
import 'package:app_template/src/models/get_product_details.dart';
import 'package:app_template/src/models/get_cart_details.dart';
import 'package:app_template/src/models/get_wishlist_details.dart';
import 'package:app_template/src/models/add_cart_details_request.dart';
import 'package:app_template/src/models/add_cart_details_response.dart';
import 'package:app_template/src/models/add_wishlist_request.dart';
import 'package:app_template/src/models/add_wishlist_response.dart';
import 'package:app_template/src/models/get_product_review.dart';
import 'package:app_template/src/models/update_cart_count_response.dart';
import 'package:app_template/src/models/update_cart_count_request.dart';



import 'base_bloc.dart';

/// api response of login is managed by AuthBloc
/// stream data is handled by StreamControllers

class UserBloc extends Object with Validators implements BaseBloc {
  StreamController<bool> _loading = new StreamController<bool>.broadcast();

  StreamController<LoginResponse> _login =
      new StreamController<LoginResponse>.broadcast();

  StreamController<UserSignUpResponseModel> _userSignUp =
      new StreamController<UserSignUpResponseModel>.broadcast();

  StreamController<GetBreedResponseModel> _getBreed =
  new StreamController<GetBreedResponseModel>.broadcast();


  StreamController<GetPostResponse> _getPost = new StreamController<GetPostResponse>.broadcast();

  StreamController<UserUpdateResponseModel> _userUpdate =
  new StreamController<UserUpdateResponseModel>.broadcast();

  StreamController<AddPostResponse> _addPost =
  new StreamController<AddPostResponse>.broadcast();

  StreamController<GetPostCommentResponse> _getPostComments = new StreamController<GetPostCommentResponse>.broadcast();

  StreamController<AddPostCommentResponse> _addPostComment =
  new StreamController<AddPostCommentResponse>.broadcast();

  StreamController<GetLogoutResponse> _logout = new StreamController<GetLogoutResponse>.broadcast();

  StreamController<UpdatePostResponse> _postUpdate =
  new StreamController<UpdatePostResponse>.broadcast();

  StreamController<String> _deletePost =
  new StreamController<String>.broadcast();

  StreamController<GetUserProfileResponse> _userProfile = new StreamController<GetUserProfileResponse>.broadcast();

  StreamController<UserNameResponse> _userCheck =
  new StreamController<UserNameResponse>.broadcast();

  StreamController<LikePostResponse> _addPostlike =
  new StreamController<LikePostResponse>.broadcast();

  StreamController<String> _disLikePost =
  new StreamController<String>.broadcast();


  StreamController<GetImageSlider> _getImageSlider =
  new StreamController<GetImageSlider>.broadcast();


  StreamController<GetProductCategory> _getProductCategory =
  new StreamController<GetProductCategory>.broadcast();

  StreamController<GetProductDetails> _getProductDetails =
  new StreamController<GetProductDetails>.broadcast();

  StreamController<GetCartDetails> _getCartDetails =
  new StreamController<GetCartDetails>.broadcast();

  StreamController<GetWishlistDetails> _getWishlistDetails =
  new StreamController<GetWishlistDetails>.broadcast();

  StreamController<AddCartDetailsResponse> _addCartDetails =
  new StreamController<AddCartDetailsResponse>.broadcast();

  StreamController<String> _deleteCartItem =
  new StreamController<String>.broadcast();

  StreamController<AddWishlistResponse> _addWishlist =
  new StreamController<AddWishlistResponse>.broadcast();

  StreamController<String> _deleteWishlistItem =
  new StreamController<String>.broadcast();

  StreamController<GetProductReview> _getProductReview =
  new StreamController<GetProductReview>.broadcast();

  StreamController<UpdateCartCountResponse> _updateCartCount =
  new StreamController<UpdateCartCountResponse>.broadcast();

  // StreamController<DisLikePostResponse> _dislikePost =
  // new StreamController<DisLikePostResponse>.broadcast();

  // ignore: close_sinks

  // stream controller is broadcasting the  details

  /// stream for progress bar
  Stream<bool> get loadingListener => _loading.stream;

  Stream<LoginResponse> get loginResponse => _login.stream;

  Stream<UserSignUpResponseModel> get userSignUpResponse => _userSignUp.stream;

  StreamSink<bool> get loadingSink => _loading.sink;

  Stream<GetBreedResponseModel> get getBreedResponse => _getBreed.stream;

  Stream<UserUpdateResponseModel> get userUpdateResponseModel => _userUpdate.stream;


  Stream<GetPostResponse> get getPostResponse => _getPost.stream;
  

  Stream<AddPostResponse> get addPostResponse => _addPost.stream;

  Stream<GetPostCommentResponse> get getPostCommentsResponse => _getPostComments.stream;

  Stream<AddPostCommentResponse> get addPostCommentsResponse => _addPostComment.stream;

  Stream<GetLogoutResponse> get logout => _logout.stream;

  Stream<UpdatePostResponse> get postUpdate => _postUpdate.stream;

  Stream<String> get deletePostResponse => _deletePost.stream;

  Stream<GetUserProfileResponse> get getUserProfileResponse => _userProfile.stream;

  Stream<UserNameResponse> get userNameResponse => _userCheck.stream;

  Stream<LikePostResponse> get addPostLikeResponse => _addPostlike.stream;

  Stream<String> get disLikePostResponse => _disLikePost.stream;

  Stream<GetImageSlider> get getImageSliderResponse => _getImageSlider.stream;

  Stream<GetProductCategory> get getProductCategoryResponse => _getProductCategory.stream;

  Stream<GetProductDetails> get getProductDetailsResponse => _getProductDetails.stream;

  Stream<GetCartDetails> get getCartDetailsResponse => _getCartDetails.stream;

  Stream<GetWishlistDetails> get getWishlistDetailsResponse => _getWishlistDetails.stream;

  Stream<AddCartDetailsResponse> get addCartDetailsResponse => _addCartDetails.stream;

  Stream<String> get deleteCartItemResponse => _deleteCartItem.stream;

  Stream<AddWishlistResponse> get addWishlistResponse => _addWishlist.stream;


  Stream<String> get deleteWishlistItemResponse => _deleteWishlistItem.stream;

  Stream<GetProductReview> get getProductReviewResponse => _getProductReview.stream;

  Stream<UpdateCartCountResponse> get updateCartCountResponse => _updateCartCount.stream;





  login({LoginRequest loginRequest}) async {
    loadingSink.add(true);
    print("User bloc OK");
    State state =
        await ObjectFactory().repository.login(loginRequest: loginRequest);

    if (state is SuccessState) {
      print("User bloc OK");
      loadingSink.add(false);
      _login.sink.add(state.value);
    } else if (state is ErrorState) {
      print("User bloc not OK");
      loadingSink.add(false);
      _login.sink.addError(Constants.SOME_ERROR_OCCURRED);
    }
  }

  userSignUp({UserSignUpRequest userSignUpRequest}) async {
    loadingSink.add(true);

    State state = await ObjectFactory().repository.userSignUp(userSignUpRequest: userSignUpRequest);

    if (state is SuccessState) {
      loadingSink.add(false);
      _userSignUp.sink.add(state.value);
    } else if (state is ErrorState) {
      loadingSink.add(false);
      _userSignUp.sink.addError(Constants.SOME_ERROR_OCCURRED);
    }
  }
  getBreed() async {
    loadingSink.add(true);

    State state = await ObjectFactory().repository.getBreed();

    if (state is SuccessState) {
      loadingSink.add(false);
      _getBreed.sink.add(state.value);
    } else if (state is ErrorState) {
      loadingSink.add(false);
      _getBreed.sink.addError(Constants.SOME_ERROR_OCCURRED);
    }
  }

  userUpdate({UserUpdateRequestModel userUpdateRequest, String id}) async {
    loadingSink.add(true);
    print("profile");
    print(userUpdateRequest.profileImage);
    State state = await ObjectFactory().repository.userUpdate(userUpdateRequest: userUpdateRequest, id: id);

    if (state is SuccessState) {
      loadingSink.add(false);
      _userUpdate.sink.add(state.value);
    } else if (state is ErrorState) {
      loadingSink.add(false);
      _userUpdate.sink.addError(Constants.SOME_ERROR_OCCURRED);
    }
  }

  getPost({String filter}) async {
    loadingSink.add(true);
    print("api  bloc ok");
    State state = await ObjectFactory().repository.getPosts(filter: filter);

    if (state is SuccessState) {
      loadingSink.add(false);
      _getPost.sink.add(state.value);
    } else if (state is ErrorState) {
      loadingSink.add(false);
      _getPost.sink.addError(Constants.SOME_ERROR_OCCURRED);
    }
  }

  /// add posts
  addPost({AddPostRequest addPostRequest}) async {
    loadingSink.add(true);

    State state = await ObjectFactory().repository.addPost(addPostRequest);

    if (state is SuccessState) {
      loadingSink.add(false);
      _addPost.sink.add(state.value);
    } else if (state is ErrorState) {
      loadingSink.add(false);
      _addPost.sink.addError(Constants.SOME_ERROR_OCCURRED);
    }
  }

  /// get post comments
  getPostComments({String id}) async {
    loadingSink.add(true);
    print("api  bloc ok");
    State state = await ObjectFactory().repository.getPostComments(id:id);

    if (state is SuccessState) {
      loadingSink.add(false);
      _getPostComments.sink.add(state.value);
    } else if (state is ErrorState) {
      loadingSink.add(false);
      _getPostComments.sink.addError(Constants.SOME_ERROR_OCCURRED);
    }
  }

  /// add posts comments
  addPostComments({AddPostCommentRequest addPostCommentRequest}) async {
    loadingSink.add(true);
    print(" post comments user bloc ok");
    State state = await ObjectFactory().repository.addPostComments(addPostCommentRequest);

    if (state is SuccessState) {
      loadingSink.add(false);
      _addPostComment.sink.add(state.value);
    } else if (state is ErrorState) {
      loadingSink.add(false);
      _addPostComment.sink.addError(Constants.SOME_ERROR_OCCURRED);
    }
  }

  ///logout
  userLogout() async {
    loadingSink.add(true);
    print("api  bloc ok");
    State state = await ObjectFactory().repository.logout();

    if (state is SuccessState) {
      loadingSink.add(false);
      _logout.sink.add(state.value);
    } else if (state is ErrorState) {
      loadingSink.add(false);
      _logout.sink.addError(Constants.SOME_ERROR_OCCURRED);
    }
  }

  /// post update
  updatePost({UpdatePostRequest updatePostRequest, String id}) async {
    loadingSink.add(true);
    print("post update user bloc");
    print(updatePostRequest.image1);
    State state = await ObjectFactory().repository.postUpdate(updatePostRequest: updatePostRequest, id: id);

    if (state is SuccessState) {
      loadingSink.add(false);
      _postUpdate.sink.add(state.value);
    } else if (state is ErrorState) {
      loadingSink.add(false);
      _postUpdate.sink.addError(Constants.SOME_ERROR_OCCURRED);
    }
  }

  /// post delete
  deletePost({String id}) async {
    print("delete post user bloc");
    loadingSink.add(true);

    State state = await ObjectFactory().repository.deletePost(id);

    if (state is SuccessState) {
      loadingSink.add(false);
      _deletePost.sink.add(state.value);
    } else if (state is ErrorState) {
      loadingSink.add(false);
      _deletePost.sink.addError(Constants.SOME_ERROR_OCCURRED);
    }
  }
  /// get user profile
  getUserProfile({String id}) async {
    loadingSink.add(true);
    print("profile api  bloc ok");
    State state = await ObjectFactory().repository.getUserProfile(id: id);

    if (state is SuccessState) {
      loadingSink.add(false);
      _userProfile.sink.add(state.value);
    } else if (state is ErrorState) {
      loadingSink.add(false);
      _userProfile.sink.addError(Constants.SOME_ERROR_OCCURRED);
    }
  }

  /// username check
  userCheck({UserNameRequest userNameRequest}) async {
    loadingSink.add(true);
    print("username-check user block ok");
    State state = await ObjectFactory().repository.userCheck(userNameRequest);

    if (state is SuccessState) {
      loadingSink.add(false);
      _userCheck.sink.add(state.value);
    } else if (state is ErrorState) {
      loadingSink.add(false);
      _userCheck.sink.addError(Constants.SOME_ERROR_OCCURRED);
    }
  }

  /// add posts like
  addPostLike({LikePostRequest likePostRequest}) async {
    loadingSink.add(true);
    print(" post like user bloc ok");
    State state = await ObjectFactory().repository.addPostLike(likePostRequest);

    if (state is SuccessState) {
      loadingSink.add(false);
      _addPostlike.sink.add(state.value);
    } else if (state is ErrorState) {
      loadingSink.add(false);
      _addPostlike.sink.addError(Constants.SOME_ERROR_OCCURRED);
    }
  }

  /// post dislike
  disLikePost({LikePostRequest likePostRequest, String id}) async {
    print("dislike post user bloc");
    loadingSink.add(true);

    State state = await ObjectFactory().repository.dislikePost(likePostRequest:likePostRequest, id:id);

    if (state is SuccessState) {
      loadingSink.add(false);
      _disLikePost.sink.add(state.value);
    } else if (state is ErrorState) {
      loadingSink.add(false);
      _disLikePost.sink.addError(Constants.SOME_ERROR_OCCURRED);
    }
  }

  /// get Image slider
  getImageSlider() async {
    loadingSink.add(true);
    print("User bloc OK");
    State state =
    await ObjectFactory().repository.getImageSlider();

    if (state is SuccessState) {
      print("User bloc OK");
      loadingSink.add(false);
      _getImageSlider.sink.add(state.value);
    } else if (state is ErrorState) {
      print("User bloc not OK");
      loadingSink.add(false);
      _getImageSlider.sink.addError(Constants.SOME_ERROR_OCCURRED);
    }
  }

  /// get product category
  getProductCategory() async {
    loadingSink.add(true);
    print("User bloc OK");
    State state =
    await ObjectFactory().repository.getProductCategory();

    if (state is SuccessState) {
      print("User bloc OK");
      loadingSink.add(false);
      _getProductCategory.sink.add(state.value);
    } else if (state is ErrorState) {
      print("User bloc not OK");
      loadingSink.add(false);
      _getProductCategory.sink.addError(Constants.SOME_ERROR_OCCURRED);
    }
  }

  /// get product details
  getProductDetails() async {
    loadingSink.add(true);
    print("User bloc OK");
    State state =
    await ObjectFactory().repository.getProductDetails();

    if (state is SuccessState) {
      print("User bloc OK");
      loadingSink.add(false);
      _getProductDetails.sink.add(state.value);
    } else if (state is ErrorState) {
      print("User bloc not OK");
      loadingSink.add(false);
      _getProductDetails.sink.addError(Constants.SOME_ERROR_OCCURRED);
    }
  }

  /// get cart details
  getCartDetails({String productId}) async {
    loadingSink.add(true);
    print("User bloc OK");
    State state =
    await ObjectFactory().repository.getCartDetails();

    if (state is SuccessState) {
      print("User bloc OK");
      loadingSink.add(false);
      _getCartDetails.sink.add(state.value);
    } else if (state is ErrorState) {
      print("User bloc not OK");
      loadingSink.add(false);
      _getCartDetails.sink.addError(Constants.SOME_ERROR_OCCURRED);
    }
  }

  /// get wishlist details
  getWishlistDetails({String productId}) async {
    loadingSink.add(true);
    print("User bloc OK");
    State state =
    await ObjectFactory().repository.getWishlistDetails();

    if (state is SuccessState) {
      print("User bloc OK");
      loadingSink.add(false);
      _getWishlistDetails.sink.add(state.value);
    } else if (state is ErrorState) {
      print("User bloc not OK");
      loadingSink.add(false);
      _getWishlistDetails.sink.addError(Constants.SOME_ERROR_OCCURRED);
    }
  }

  /// add cart details
  addCartDetails({AddCartDetailsRequest addCartDetailsRequest}) async {
    loadingSink.add(true);
    print(" add cart user bloc ok");
    State state = await ObjectFactory().repository.addCartDetails(addCartDetailsRequest);

    if (state is SuccessState) {
      loadingSink.add(false);
      _addCartDetails.sink.add(state.value);
    } else if (state is ErrorState) {
      loadingSink.add(false);
      _addCartDetails.sink.addError(Constants.SOME_ERROR_OCCURRED);
    }
  }


  /// cart item delete
  deleteCartItem({String id}) async {
    print("delete post user bloc");
    loadingSink.add(true);

    State state = await ObjectFactory().repository.deleteCartItem(id);

    if (state is SuccessState) {
      loadingSink.add(false);
      _deleteCartItem.sink.add(state.value);
    } else if (state is ErrorState) {
      loadingSink.add(false);
      _deleteCartItem.sink.addError(Constants.SOME_ERROR_OCCURRED);
    }
  }

  /// add wishlist details
  addWishlistDetails({AddWishlistRequest addWishlistRequest}) async {
    loadingSink.add(true);
    print(" add wishlist user bloc ok");
    State state = await ObjectFactory().repository.addWishlistDetails(addWishlistRequest);

    if (state is SuccessState) {
      loadingSink.add(false);
      _addWishlist.sink.add(state.value);
    } else if (state is ErrorState) {
      loadingSink.add(false);
      _addWishlist.sink.addError(Constants.SOME_ERROR_OCCURRED);
    }
  }

  /// wishlist item delete
  deleteWishlistItem({String id}) async {
    print("delete wishlist item user bloc");
    loadingSink.add(true);

    State state = await ObjectFactory().repository.deleteWishlistItem(id);

    if (state is SuccessState) {
      loadingSink.add(false);
      _deleteWishlistItem.sink.add(state.value);
    } else if (state is ErrorState) {
      loadingSink.add(false);
      _deleteWishlistItem.sink.addError(Constants.SOME_ERROR_OCCURRED);
    }
  }

  /// get cart details
  getProductReview() async {
    loadingSink.add(true);
    print("User bloc OK");
    State state =
    await ObjectFactory().repository.getProductReview();

    if (state is SuccessState) {
      print("User bloc OK");
      loadingSink.add(false);
      _getProductReview.sink.add(state.value);
    } else if (state is ErrorState) {
      print("User bloc not OK");
      loadingSink.add(false);
      _getProductReview.sink.addError(Constants.SOME_ERROR_OCCURRED);
    }
  }

  /// update cart count
  updateCartCount(String id,UpdateCartCountRequest updateCartCountRequest) async {
    loadingSink.add(true);
    print("User bloc OK");
    State state =
    await ObjectFactory().repository.updateCartCount(id,updateCartCountRequest);

    if (state is SuccessState) {
      print("User bloc OK");
      loadingSink.add(false);
      _updateCartCount.sink.add(state.value);
    } else if (state is ErrorState) {
      print("User bloc not OK");
      loadingSink.add(false);
      _updateCartCount.sink.addError(Constants.SOME_ERROR_OCCURRED);
    }
  }

  ///disposing the stream if it is not using
  @override
  void dispose() {
    _loading?.close();
    _login?.close();
    _userSignUp.close();
    _getBreed.close();
    _userUpdate?.close();
    _getBreed?.close();
    _getPost?.close();
    _addPost?.close();
    _getPostComments?.close();
    _addPostComment?.close();
    _logout?.close();
    _postUpdate?.close();
    _deletePost?.close();
    _userProfile?.close();
    _userCheck?.close();
    _addPostlike?.close();
    _disLikePost?.close();
    _getImageSlider?.close();
    _getProductCategory?.close();
    _getProductDetails?.close();
    _getCartDetails?.close();
    _getWishlistDetails?.close();
    _addCartDetails?.close();
    _deleteCartItem?.close();
    _addWishlist.close();
    _deleteWishlistItem?.close();
    _getProductReview?.close();
    _updateCartCount?.close();
  }
}
