import 'package:hive/hive.dart';
import 'constants.dart';

class AppHive {
  // void main (){}

  // void openBox()async{
  //   await Hive.openBox(Constants.BOX_NAME);
  //
  // }
  static const String _USER_ID = "user_id";
  static const String _NAME = "name";
  static const String _TOKEN = "token";
  static const String _EMAIL = "email";
  static const String _PREFERENCE = "preferenceid";
  static const String _LATITUDE = "longitude";
  static const String _XUSER = "newuserid";
  static const String _PASSWORD = "userpassword";
  static const String _PROFILE_PIC = "profile pic";
  static const String _DEVICE_TOKEN = "device_token";



  void hivePut({String key, String value}) async {
    await Hive.box(Constants.BOX_NAME).put(key, value);
  }

  String hiveGet({String key}) {
    // openBox();
    return Hive.box(Constants.BOX_NAME).get(key);
  }

  void clearHive({String key}) async {
    await Hive.box(Constants.BOX_NAME).delete(key);
  }


  putUserId({String userId}) {
    hivePut(key: _USER_ID, value: userId);
  }

  String getUserId() {
    return hiveGet(key: _USER_ID);
  }


  putUserPassword({String email}) {
    hivePut(key: _PASSWORD, value: email);
  }

  String getUserPassword() {
    return hiveGet(key: _PASSWORD);
  }

  putName({String name}) {
    hivePut(key: _NAME, value: name);
  }

  putProfilePic(String value) {
    return hivePut(key: _PROFILE_PIC, value: value);
  }

  String getProfilePic() {
    return hiveGet(key: _PROFILE_PIC);
  }
  String getName() {
    return hiveGet(key: _NAME);
  }

  putToken({String token}) {
    hivePut(key: _TOKEN, value: token);
  }

  String getToken() {
    return hiveGet(key: _TOKEN);
  }

  putDeviceToken({String token}) {
    hivePut(key: _DEVICE_TOKEN, value: token);
  }

  String getDeviceToken() {
    return hiveGet(key: _DEVICE_TOKEN);
  }

  void deleteToken() {
    return clearHive(key: _TOKEN);
  }

  String getEmail() {
    return hiveGet(key: _EMAIL);
  }

  putEmail(String value) {
    return hivePut(key: _EMAIL, value: value);
  }

  void deleteEmail() {
    return clearHive(key: _EMAIL);
  }

  void deleteImage() {
    return clearHive(key: _PROFILE_PIC);
  }

  void deleteName() {
    return clearHive(key: _NAME);
  }

  putPreference(String value) {
    return hivePut(key: _PREFERENCE, value: value);
  }

  String getPreferenceId() {
    return hiveGet(key: _PREFERENCE);
  }

  putXUser(String value) {
    return hivePut(key: _XUSER, value: value);
  }

  getXUser() {
    return hiveGet(key: _XUSER,);
  }


  AppHive();


}