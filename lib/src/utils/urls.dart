class Urls {
  static final baseUrl = "http://194.195.209.226:8005/api/v1/";
  static final userSignUp = "users/";
  static final loginUrl = baseUrl + "login/";
  static final breedUrl=baseUrl+"breed/";
  static final postUrl = baseUrl +"posts/";
  static final addPostUrl = baseUrl +"posts/";
  static final userUpdate=baseUrl+"user/";
  static final postComments="http://194.195.209.226:8005/api/v1/post-comment/?post_id=";
  static final addPostComments=baseUrl+"post-comment/";
  static final imageBaseUrl = "http://194.195.209.226:8005";
  static final logout=baseUrl+"logout/";
  static final postUpdate=baseUrl+"posts/";
  static final userProfile = baseUrl+"user/";
  static final userCheckUrl = "http://194.195.209.226:8005/api/v1/user/check/";
  static final likePost = baseUrl + "post-like/";
  static final dislikePost = "http://194.195.209.226:8005/api/v1/post-like/";
  static final userPosts = "http://194.195.209.226:8005/api/v1/posts/?created_by=";
  static final imageSlider = baseUrl+"image-slider/";
  static final productCategory = baseUrl+"product-category/";
  static final productDetails= baseUrl+"product/";
  static final cartDetails= baseUrl+"cart/";
  static final wishlistDetails= baseUrl+"wishlist/";
  static final addCartDetails= baseUrl+"cart/";
  static final addWishlistDetails= baseUrl+"wishlist/";
  static final productReview= baseUrl+"product-review/";


}
