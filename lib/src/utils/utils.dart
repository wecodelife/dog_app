import 'dart:io';
import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/widgets/alert_dialog_box.dart';
import 'package:app_template/src/widgets/cancel_alert_box.dart';
import 'package:app_template/src/widgets/otp_message.dart';
import 'package:flutter/material.dart';
import 'package:top_snackbar_flutter/custom_snack_bar.dart';
import 'package:top_snackbar_flutter/top_snack_bar.dart';
import 'api_client.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:page_transition/page_transition.dart';
import 'package:app_template/src/widgets/bulleted_list_items.dart';

///it contain common functions
class Utils {}

Size screenSize(BuildContext context) {
  return MediaQuery.of(context).size;
}

double screenHeight(BuildContext context, {double dividedBy = 1}) {
  return screenSize(context).height / dividedBy;
}

double screenWidth(BuildContext context, {double dividedBy = 1}) {
  return screenSize(context).width / dividedBy;
}

Future<dynamic> push(BuildContext context, Widget route) {
  return Navigator.push(
      context,
      PageTransition(
        child: route,
        type: PageTransitionType.fade,
      ));
}

void pop(BuildContext context) {
  return Navigator.pop(context);
}

Future<dynamic> pushAndRemoveUntil(
    BuildContext context, Widget route, bool goBack) {
  return Navigator.pushAndRemoveUntil(context,
      MaterialPageRoute(builder: (context) => route), (route) => goBack);
}

Future<dynamic> pushAndReplacement(BuildContext context, Widget route) {
  return Navigator.pushReplacement(
      context, MaterialPageRoute(builder: (context) => route));
}

void showSuccessAnimatedToast({String msg, context}) {
  showTopSnackBar(
    context,
    CustomSnackBar.success(
      message: msg,
    ),
  );
}

void showErrorAnimatedToast({String msg, context}) {
  showTopSnackBar(
    context,
    CustomSnackBar.error(
      message: msg,
    ),
  );
}

void showInfoAnimatedToast({String msg, context}) {
  showTopSnackBar(
    context,
    CustomSnackBar.info(
      message: msg,
    ),
  );
}

///common toast
void showToast(String msg) {
  Fluttertoast.showToast(
    msg: msg,
    toastLength: Toast.LENGTH_SHORT,
    gravity: ToastGravity.BOTTOM,
  );
}

void cancelAlertBox(
    {context,
    msg,
    text1,
    text2,
    route,
    double insetPadding,
    double contentPadding,
    double titlePadding}) {
  showGeneralDialog(
      context: context,
      barrierDismissible: false,
      barrierLabel: MaterialLocalizations.of(context).modalBarrierDismissLabel,
      barrierColor: Colors.black.withOpacity(0.8),
      transitionDuration: const Duration(milliseconds: 200),
      pageBuilder: (BuildContext buildContext, Animation animation,
          Animation secondaryAnimation) {
        return CancelAlertBox(
          title: msg,
          text1: text1,
          text2: text2,
          route: route,
          contentPadding: contentPadding,
          titlePadding: titlePadding,
          insetPadding: insetPadding,
        );
      });
}

void showAlert(context, String msg) {
  // flutter defined function
  showDialog(
    context: context,
    builder: (BuildContext context) {
      // return object of type Dialog
      return AlertDialog(
        title: new Text(msg),
//        content: new Text("Alert Dialog body"),
        actions: <Widget>[
          // usually buttons at the bottom of the dialog
          new FlatButton(
            child: new Text("OK"),
            onPressed: () {
              Navigator.of(context).pop();
            },
          ),
        ],
      );
    },
  );
}

void otpAlertBox({context, title, route, stayOnPage}) {
  showGeneralDialog(
      context: context,
      barrierDismissible: false,
      barrierLabel: MaterialLocalizations.of(context).modalBarrierDismissLabel,
      barrierColor: Colors.transparent,
      transitionDuration: const Duration(milliseconds: 200),
      pageBuilder: (BuildContext buildContext, Animation animation,
          Animation secondaryAnimation) {
        return OtpMessage(
          title: title,
          route: route,
          stayOnPage: stayOnPage,
        );
      });
}

///common toast
void showToastLong(String msg) {
  Fluttertoast.showToast(
    msg: msg,
    toastLength: Toast.LENGTH_LONG,
    gravity: ToastGravity.BOTTOM,
  );
}

void showToastConnection(String msg) {
  Fluttertoast.showToast(
    msg: msg,
    toastLength: Toast.LENGTH_LONG,
    gravity: ToastGravity.BOTTOM,
  );
}
//
// /// alert box
// void showAlertDialog(
//     {context,String title,
//       String description,
//       Function onPressed,
//       // TextStyle descriptionStyle,
//       // TextStyle titleStyle,
//       // String leftButtonLabel,
//       // String rightButtonLabel
//     }) {
//   showDialog(
//       context: context,
//       builder: (BuildContext context) {
//         return Center(
//           child: AlertDialogBox(
//             title: title,
//             descriptions: description,
//             onPressedYes: () {
//               onPressed();
//             },),);
//       });
// }
// showPicker(context,String title,List<BottomSheetItem> items) {
//   showModalBottomSheet(
//       context: context,
//       backgroundColor: Constants.kitGradients[17],
//       shape: RoundedRectangleBorder(
//           borderRadius: BorderRadius.only(
//               topLeft: Radius.circular(8), topRight: Radius.circular(8))),
//       builder: (BuildContext context) {
//         return
//           Container(
//             // height:screenHeight(context, dividedBy:4),
//             padding: EdgeInsets.symmetric(
//               horizontal: screenWidth(context, dividedBy: 30),
//               vertical: screenHeight(context, dividedBy: 20),
//             ),
//             decoration:BoxDecoration(
//                 color:Constants.kitGradients[17],
//                 borderRadius: BorderRadius.only(
//                     topLeft: Radius.circular(8), topRight: Radius.circular(8))
//             ),
//             child: Wrap (
//               //crossAxisAlignment:CrossAxisAlignment.start,
//               children: <Widget>[
//                 Text(title,style: TextStyle(
//                   fontSize: 20,
//                   fontWeight: FontWeight.bold,
//                   fontFamily: "Montserrat",
//                   color: Constants.kitGradients[27],
//                 ),),
//                 Column(
//                   children: items,
//                 )
//               ],
//             ),
//           );
//       });
// }

// // Report Modal
// TextEditingController reportTextEditingController = new TextEditingController();
// bool flag = false;
// List<String> reportList = [
//   "It's spam",
//   "Nudity or sexual activity",
//   "Hate speech or symbols",
//   "I don't like this post",
//   "False information",
//   "Bullying or harassment",
//   "Scam or fraud",
//   "Violence or dangerous organisations",
//   "intellectual property violation",
//   "Sale of illegal or regulated goods",
//   "Suicide or self-injury",
//   "Eating disorders",
// ];
// showReportModal(context) {
//   showModalBottomSheet(
//       context: context,
//       //transitionAnimationController: controller,
//       isScrollControlled: reportList.length > 6 ? true : false,
//       backgroundColor: Constants.kitGradients[29],
//       shape: RoundedRectangleBorder(
//           borderRadius: BorderRadius.only(
//               topLeft: Radius.circular(10), topRight: Radius.circular(10))),
//       builder: (BuildContext context) {
//         return ReportPostPage(reportList: reportList);
//       });
// }
