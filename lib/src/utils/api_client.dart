import 'dart:async';

import 'package:app_template/src/models/add_post_request.dart';
import 'package:app_template/src/models/header_model.dart';
import 'package:app_template/src/models/login_request_model.dart';
import 'package:app_template/src/models/user_sign_up_request_model.dart';
import 'package:app_template/src/models/user_update_request_model.dart';
import 'package:app_template/src/models/add_post_comment_request.dart';
import 'package:app_template/src/models/add_cart_details_request.dart';
import 'package:app_template/src/models/add_wishlist_request.dart';
import 'package:app_template/src/models/post_update_request_model.dart';
import 'package:app_template/src/models/username_request_model.dart';
import 'package:app_template/src/models/like_post_request.dart';
import 'package:app_template/src/models/update_cart_count_response.dart';
import 'package:app_template/src/models/update_cart_count_request.dart';
import 'package:app_template/src/utils/urls.dart';
import 'package:dio/dio.dart';

import 'object_factory.dart';

enum AccessMode { READ, WRITE }
HeaderModel authHeaderModel = new HeaderModel();

void setAuthHeaderModel({var accessMode, String xReferenceId}) {
  if (ObjectFactory().appHive != null &&
      ObjectFactory().appHive.getToken() != null &&
      ObjectFactory().appHive.getToken().trim().length > 0)
    authHeaderModel.authorization =
        "Token ${ObjectFactory().appHive.getToken()}";
  print(ObjectFactory().appHive.getToken());
  // authHeaderModel.xAccessMode = accessMode.toString();
  // authHeaderModel.xUdid = "";
  // authHeaderModel.xRequestTime = "";
  // authHeaderModel.xReferenceId = xReferenceId;
  // authHeaderModel.xSession = "";
  // if (ObjectFactory().appHive != null &&
  //     ObjectFactory().appHive.getXUser() != null &&
  //     ObjectFactory().appHive.getXUser().trim().length > 0)
  //   authHeaderModel.xUser = ObjectFactory().appHive.getXUser();
  // if (ObjectFactory().appHive != null &&
  //     ObjectFactory().appHive.getUserId() != null &&
  //     ObjectFactory().appHive.getUserId().trim().length > 0)
  //   authHeaderModel.xUserId = ObjectFactory().appHive.getUserId();
  // print("Token "+authHeaderModel.xTenantId);
}

class ApiClient {
  HeaderModel loginModel = new HeaderModel();

  ///  user login
  Future<Response> loginRequest(LoginRequest loginRequest) {
    print("API client OK");
    print(loginRequest.uid.toString());
    return ObjectFactory()
        .appDio
        .loginPost(url: Urls.loginUrl, data: loginRequest, header: loginModel);
  }

  /// get posts
  Future<Response> getPosts({String filter}) {
    print(" post api client ok");
    print(" fliter "+filter.toString());
    setAuthHeaderModel();
    return ObjectFactory()
        .appDio
        .get(url: Urls.postUrl + (filter!=null ? filter :''), header: authHeaderModel);
  }
  /// get posts
  Future<Response> getUserPosts({String id}) {
    print("user's post api client ok");
    setAuthHeaderModel();
    return ObjectFactory()
        .appDio
        .get(url: Urls.userPosts, header: authHeaderModel);
  }

  Future<Response> userSignUp({UserSignUpRequest userSignUpRequest}) {
    print(userSignUpRequest.firstName);
    return ObjectFactory().appDio.loginPost(
        url: Urls.baseUrl + Urls.userSignUp, data: userSignUpRequest);
  }

  Future<Response> getBreed() {
    return ObjectFactory().appDio.get(url: Urls.breedUrl, header: loginModel);
  }

  Future<Response> userUpdate(
      {UserUpdateRequestModel userUpdateRequest, String id}) async {
    // String pid = ObjectFactory().appHive.getUserId();
    // print("id   "+ObjectFactory().appHive.getUserId());
    // print("UserName   " + userUpdateRequest.username);
    print("Name  " + userUpdateRequest.name);
    print("firstName  " + userUpdateRequest.firstName);
    print("description  " + userUpdateRequest.description);
    print("phoneNumber  " + userUpdateRequest.phoneNumber.toString());
    print("dateOfBirth  " + userUpdateRequest.dateOfBirth.toString());
    print("breedType  " + userUpdateRequest.breedType.toString());
    print("gender  " + userUpdateRequest.gender.toString());
    if (userUpdateRequest.profileImage != null)
    print("profileImage  " + userUpdateRequest.profileImage.path);
    print("is_user_has_dog  " + userUpdateRequest.isUserHasDog.toString());
    print("uid  " + userUpdateRequest.uid);
    // print("description  " + userUpdateRequest.description);
    print("ID     "+ObjectFactory()
        .appHive
        .getUserId());
    FormData formData = FormData.fromMap({
      if (userUpdateRequest.profileImage != null)
        "profile_image": await MultipartFile.fromFile(
            userUpdateRequest.profileImage.path,
            filename: userUpdateRequest.profileImage.path.split('/').last),
      if (userUpdateRequest.username != null)
        "username": userUpdateRequest.username,
      if (userUpdateRequest.name != null) "name": userUpdateRequest.name,
      if (userUpdateRequest.firstName != null)
        "first_name": userUpdateRequest.firstName,
      if (userUpdateRequest.phoneNumber != null)
        "phone_number": userUpdateRequest.phoneNumber.toString(),
      if (userUpdateRequest.lastName != null)
        "last_name": userUpdateRequest.lastName,
      if (userUpdateRequest.description != null)
        "last_name": userUpdateRequest.description,
      if (userUpdateRequest.gender != null) "gender": userUpdateRequest.gender,
      if (userUpdateRequest.dateOfBirth != null)
        "date_of_birth": userUpdateRequest.dateOfBirth,
      if (userUpdateRequest.breedType != null)
        "breed_type": userUpdateRequest.breedType,
      if (userUpdateRequest.isUserHasDog != null)
        "is_user_has_dog": userUpdateRequest.isUserHasDog,
      if (userUpdateRequest.uid != null)
        "uid": userUpdateRequest.uid,
      if (userUpdateRequest.description != null)
        "description": userUpdateRequest.description,
    });
    print(userUpdateRequest.firstName);
    setAuthHeaderModel();
    return ObjectFactory().appDio.patch(
        url: Urls.userUpdate + "$id/", header: authHeaderModel, data: formData);
  }

  /// add posts
  Future<Response> addPost(AddPostRequest addPostRequest) async {
    print("image  " + addPostRequest.image1.path);
    print("description  " + addPostRequest.description.toString());
    print("lat  " + addPostRequest.lat.toString());
    print("lon  " + addPostRequest.lon.toString());
    print("createdby  " + addPostRequest.createdBy.toString());
    print("likedby  " + addPostRequest.likedBy.toString());

    FormData formData = FormData.fromMap({
      // if (addPostRequest.image1 != null)
      "image1": await MultipartFile.fromFile(addPostRequest.image1.path,
          filename: addPostRequest.image1.path.split('/').last),
      "description": addPostRequest.description,
      "lat": addPostRequest.lat,
      "lon": addPostRequest.lon,
      "created_by": addPostRequest.createdBy,
      "liked_by": addPostRequest.likedBy,
    });
    setAuthHeaderModel();
    print(authHeaderModel.authorization.toString());
    return ObjectFactory()
        .appDio
        .post(url: Urls.addPostUrl, data: formData, header: authHeaderModel);
  }

  /// get post Comments
  Future<Response> getPostComments({String id}) {
    print(" post comments api client ok");
    print("id"+id.toString());
    setAuthHeaderModel();
    return ObjectFactory()
        .appDio
        .get(url: Urls.postComments+"$id", header: authHeaderModel);
  }

  /// add post Comments
  Future<Response> addPostComments(
      AddPostCommentRequest addPostCommentRequest) {
    print("add comments api client ok");
    setAuthHeaderModel();
    print(authHeaderModel.authorization.toString());
    return ObjectFactory().appDio.post(
        url: Urls.addPostComments,
        data: addPostCommentRequest,
        header: authHeaderModel);
  }

  /// logout
  Future<Response> logout() {
    print(" logout api client ok");
    setAuthHeaderModel();
    return ObjectFactory()
        .appDio
        .get(url: Urls.logout, header: authHeaderModel);
  }

  Future<Response> postUpdate(
      {UpdatePostRequest updatePostRequest, String id}) async {
    // print("title  " + updatePostRequest.title);
    print("description  " + updatePostRequest.description);
    print("lat  " + updatePostRequest.lat.toString());
    print("lon  " + updatePostRequest.lon.toString());
    // print("hash_tags  " + updatePostRequest.hashTags.toString());
    print("created_by  " + updatePostRequest.createdBy.toString());
    print("liked_by  " + updatePostRequest.likedBy.toString());
    // print("is_deleted  " + updatePostRequest.isDeleted.toString());
    // print("image1  " + updatePostRequest.image1.path);
    FormData formData = FormData.fromMap({
      // if (updatePostRequest.image1 != null)
      //   "image1": await MultipartFile.fromFile(updatePostRequest.image1.path,
      //       filename: updatePostRequest.image1.path.split('/').last),
        "description": updatePostRequest.description,
      "lat": updatePostRequest.lat.toString(),
      "lon": updatePostRequest.lon.toString(),
      // "hash_tags": updatePostRequest.hashTags,
      "created_by": updatePostRequest.createdBy,
      "liked_by": updatePostRequest.likedBy,
      // if (userUpdateRequest.description != null)
      //   "description": userUpdateRequest.description,
    });
    print(updatePostRequest.title);
    setAuthHeaderModel();
    return ObjectFactory().appDio.patch(
        url: Urls.postUpdate + "$id/", header: authHeaderModel, data: formData);
  }

  /// delete post
  Future<Response> deletePosts(String id) async {
    print("delete api client");
    // print("api");
    setAuthHeaderModel();
    print(authHeaderModel.authorization.toString());
    return ObjectFactory()
        .appDio
        .delete(url: Urls.postUrl + "$id/", header: authHeaderModel);
  }

  /// get userProfile
  Future<Response> getUserProfile({String id}) {
    print("IDDDD   " + id);
    print(" user api client ok");
    setAuthHeaderModel();
    return ObjectFactory()
        .appDio
        .get(url: Urls.userProfile+"$id/", header: authHeaderModel);
  }

  /// username check
  Future<Response> userCheck(UserNameRequest userNameRequest) async {
    // print(updateProfileRequest.profileImage.path.toString());
    print(" username check api client ok");
    setAuthHeaderModel();
    print(authHeaderModel.authorization.toString());
    return ObjectFactory().appDio.post(
        url: Urls.userCheckUrl, data: userNameRequest, header: authHeaderModel);
  }

  /// add post like
  Future<Response> addPostLike(
      LikePostRequest likePostRequest) {
    print("like post api client ok");
    setAuthHeaderModel();
    print(authHeaderModel.authorization.toString());
    return ObjectFactory().appDio.post(
        url: Urls.likePost,
        data: likePostRequest,
        header: authHeaderModel);
  }

  /// post dislike
  Future<Response> disLikePost({LikePostRequest likePostRequest, String id}) async {
    setAuthHeaderModel();
    print("like ID " + id );
    print(authHeaderModel.authorization.toString());
    return ObjectFactory().appDio.delete(
        url: Urls.dislikePost+"$id/", header: authHeaderModel);
  }

  ///get image slider
  Future<Response> getImageSlider() {
   // print("IDDDD   " + id);
    print(" user api client ok");
    setAuthHeaderModel();
    return ObjectFactory()
        .appDio
        .get(url: Urls.imageSlider, header: authHeaderModel);
  }

  //get product category
  Future<Response> getProductCategory() {
    // print("IDDDD   " + id);
    print(" user api client ok");
    setAuthHeaderModel();
    return ObjectFactory()
        .appDio
        .get(url: Urls.productCategory, header: authHeaderModel);
  }

  ///get product details
  Future<Response> getProductDetails() {
    // print("IDDDD   " + id);
    print(" user api client ok");
    setAuthHeaderModel();
    return ObjectFactory()
        .appDio
        .get(url: Urls.productDetails, header: authHeaderModel);
  }

  ///get cart details
  Future<Response> getCartDetails() {
    /// print("IDDDD   " + id);
    print(" user api client ok");
    setAuthHeaderModel();
    return ObjectFactory()
        .appDio
        .get(url: Urls.cartDetails, header: authHeaderModel);
  }

  ///get wishlist details
  Future<Response> getWishlistDetails() {
    // print("IDDDD   " + id);
    print(" user api client ok");
    setAuthHeaderModel();
    return ObjectFactory()
        .appDio
        .get(url: Urls.wishlistDetails, header: authHeaderModel);
  }

/// add cart details
  Future<Response> addCartDetails(
      AddCartDetailsRequest addCartDetailsRequest) {
    print("add cart api client ok");
    setAuthHeaderModel();
    print(authHeaderModel.authorization.toString());
    print("product  " + addCartDetailsRequest.product.toString());
    print("id  " + addCartDetailsRequest.user.toString());

    return ObjectFactory().appDio.post(
        url: Urls.addCartDetails,
        data: addCartDetailsRequest,
        header: authHeaderModel);
  }

  /// delete Cart item
  Future<Response> deleteCartItem(String id) async {
    print("delete api client");
    // print("api");
    setAuthHeaderModel();
    print(authHeaderModel.authorization.toString());
    return ObjectFactory()
        .appDio
        .delete(url: Urls.addCartDetails + "$id/", header: authHeaderModel);
  }


///add wishlist details
  Future<Response> addWishlistDetails(
      AddWishlistRequest addWishlistRequest) {
    print("add wishlist api client ok");
    setAuthHeaderModel();
    print(authHeaderModel.authorization.toString());
    print("product  " + addWishlistRequest.product.toString());
    print("id  " + addWishlistRequest.user.toString());

    return ObjectFactory().appDio.post(
        url: Urls.addWishlistDetails,
        data: addWishlistRequest,
        header: authHeaderModel);
  }

  /// delete wishlist item
  Future<Response> deleteWishlistItem(String id) async {
    print("delete api client");
    // print("api");
    setAuthHeaderModel();
    print(authHeaderModel.authorization.toString());
    return ObjectFactory()
        .appDio
        .delete(url: Urls.wishlistDetails + "$id/", header: authHeaderModel);
  }

  ///get product review
  Future<Response> getProductReview() {
    // print("IDDDD   " + id);
    print(" user api client ok");
    setAuthHeaderModel();
    return ObjectFactory()
        .appDio
        .get(url: Urls.productReview, header: authHeaderModel);
  }

  ///update cart details
  Future<Response> updateCartCount(String id,UpdateCartCountRequest updateCartCountRequest) {
    /// print("IDDDD   " + id);
    print(" user api client ok");
    setAuthHeaderModel();
    return ObjectFactory()
        .appDio
        .patch(url: Urls.cartDetails + "$id/",data: updateCartCountRequest, header: authHeaderModel);
  }

}
