import 'package:app_template/src/bloc/user_bloc.dart';
import 'package:app_template/src/models/user_update_request_model.dart';
import 'package:app_template/src/screens/have_a_dog_page.dart';
import 'package:app_template/src/screens/onboarding_name.dart';
import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:app_template/src/widgets/onboarding_button.dart';
import 'package:app_template/src/widgets/slide_transition.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:sms_autofill/sms_autofill.dart';

class OTP_Page extends StatefulWidget {
  final String vId;
  final String mobileNumber;
  final bool newUser;
  final UserUpdateRequestModel updateRequestModel;
  final Function onTapVerification;
  OTP_Page(
      {this.vId,
      this.mobileNumber,
      this.newUser,
      this.updateRequestModel,
      this.onTapVerification});

  @override
  _OTP_PageState createState() => _OTP_PageState();
}

class _OTP_PageState extends State<OTP_Page> {
  String appSignature;
  String _code = "";
  UserUpdateRequestModel updateRequestModel;
  TextEditingController otpTextEditingController = TextEditingController();
  bool isLoading = false;
  UserBloc userBloc = UserBloc();
  String id;

  @override
  void initState() {
    super.initState();
    print("fdfgfd" + widget.updateRequestModel.toString());
    // listenForCode();
    //
    // SmsAutoFill().getAppSignature.then((signature) {
    //   setState(() {
    //     appSignature = signature;
    //   });
    // }
    // );
  }

  @override
  void dispose() {
    super.dispose();
  }

  // void listenForCode() async {
  //   await SmsAutoFill().listenForCode;
  // }

  void verifyOtp(String otp, String verificationId, String phoneNumber) {
    setState(() {
      isLoading = true;
    });
    // emit(AuthLoading());
    AuthCredential authCredential = PhoneAuthProvider.credential(
      verificationId: verificationId,
      smsCode: otp,
    );
    FirebaseAuth.instance
        .signInWithCredential(authCredential)
        .then((value) async {
      // userModel.copyWith(phone: phoneNumber);
      // final req = GWhaUsersByPhoneReq((r) => r.vars.phone = phoneNumber);
      // final response = client.request(req);
      // response.listen((event) {
      //   if (event.hasErrors) {
      //     emit(AuthError());
      //   }
      //   if (event.data != null) {
      //     if (event.data.whaUsers != null &&
      //         event.data.whaUsers.isNotEmpty &&
      //         event.data.whaUsers.first.id != null) {
      //       emit(ExistingUser(event.data.whaUsers.first.id));
      //     } else {
      //       emit(NewUser(value));
      //     }
      //   }
      // push(context, RegistrationDetailsPage());
      // });
      setState(() {
        isLoading = false;
      });
      print("success");
      Navigator.of(context).push(SlideRightRoute(page: OnBoardingName()));
    }).catchError((onError) {
      print("Error" + onError.toString());
      showToast("Incorrect OTP");
      setState(() {
        isLoading = false;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return AnnotatedRegion<SystemUiOverlayStyle>(
      value: SystemUiOverlayStyle(
        statusBarColor: Colors.transparent,
      ),
      child: SafeArea(
        child: Scaffold(
          resizeToAvoidBottomInset: true,
          backgroundColor: Colors.transparent,
          body: Container(
            height: screenHeight(context, dividedBy: 1),
            width: screenWidth(context, dividedBy: 1),
            decoration: BoxDecoration(
              image: DecorationImage(
                fit: BoxFit.cover,
                image: AssetImage(Constants.BACKGROUND_IMAGE),
                colorFilter: new ColorFilter.mode(
                    Colors.black.withOpacity(0.6), BlendMode.darken),
              ),
            ),
            child: ListView(
              children: [
                Container(
                    padding: EdgeInsets.symmetric(
                        horizontal: screenWidth(context, dividedBy: 20)),
                    child: Center(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          SizedBox(
                            height: screenHeight(context, dividedBy: 3.3),
                          ),

                          // FormFeildUserDetails(
                          //   labelText: "Enter OTP",
                          //   textEditingController:
                          //       OTPTextEditingController,
                          //   isPassword: false,
                          //   isnumber: true,
                          // ),
                          Text(
                            "Find Your Perfect Puppy,            \nWith Us !          ",
                            //textAlign: TextAlign.left,
                            style: TextStyle(
                                color: Constants.kitGradients[27],
                                fontFamily: "Montserrat-ExtraBold",
                                fontSize: 24,
                                fontWeight: FontWeight.bold),
                          ),
                          SizedBox(
                            height: screenHeight(context, dividedBy: 90),
                          ),
                          Text(
                            "Join us and Discover the world of puppies ..                                          We are here to find your dream Pet            ",
                            style: TextStyle(
                                color: Constants.kitGradients[27],
                                fontFamily: "Montserrat",
                                fontSize: 16,
                                fontWeight: FontWeight.w400),
                          ),
                          SizedBox(
                            height: screenHeight(context, dividedBy: 30),
                          ),

                          Text(
                            "Please Enter the OTP send to your Number",
                            textAlign: TextAlign.left,
                            style: TextStyle(
                                color: Constants.kitGradients[27],
                                fontFamily: "Montserrat",
                                fontSize: 16,
                                fontWeight: FontWeight.w400),
                          ),
                          SizedBox(
                            height: screenHeight(context, dividedBy: 40),
                          ),
                          Container(
                            padding: EdgeInsets.symmetric(horizontal: 10),
                            width: screenWidth(context, dividedBy: 1),
                            child: PinFieldAutoFill(
                              controller: otpTextEditingController,
                              decoration: UnderlineDecoration(
                                textStyle: TextStyle(
                                  color: Constants.kitGradients[27],
                                  fontFamily: 'Montserrat',
                                  fontSize: 16,
                                ),
                                colorBuilder: FixedColorBuilder(
                                    Colors.grey.withOpacity(0.5)),
                              ),
                              // currentCode: otpTextEditingController.text,
                              onCodeSubmitted: (code) {
                                setState(() {
                                  otpTextEditingController.text = code;
                                });
                              },
                              onCodeChanged: (code) {
                                print("hjhjj" + code);
                                // if (code.length == 6) {
                                //   // FocusScope.of(context).requestFocus(FocusNode());
                                // }
                              },
                            ),
                          ),
                          SizedBox(
                            height: screenHeight(context, dividedBy: 20),
                          ),
                          Row(
                              mainAxisAlignment: MainAxisAlignment.end,
                              children: [
                                OnBoardingButton(
                                  title: widget.newUser ? "Next" : "Login",
                                  onPressed: () async {
                                    // if(OTPTextEditingController.text=="")
                                    //   showToast("Please enter the OTP number");
                                    // else
                                    updateRequestModel = UserUpdateRequestModel(
                                        firstName:
                                            widget.updateRequestModel.firstName,
                                        lastName:
                                            widget.updateRequestModel.lastName,
                                        // uid: "bOSPE5aIyRMNQ1BeCLkr1ajsKch2",
                                        phoneNumber: widget.mobileNumber);
                                    push(
                                        context,
                                        HaveADogPage(
                                          updateRequestModel:
                                              updateRequestModel,
                                        ));
                                  },
                                  isLoading: isLoading,
                                  width: 3,
                                ),
                                SizedBox(
                                    width: screenWidth(context, dividedBy: 20)),
                              ]),
                          // BuildButton(
                          //   onPressed: () {
                          //     verifyOtp(OTPTextEditingController.text,vId, mobileTextEditingController.text);
                          //   },
                          //   title: "Submit",
                          // ),
                        ],
                      ),
                    )),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
