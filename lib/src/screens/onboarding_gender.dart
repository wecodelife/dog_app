import 'package:app_template/src/screens/home_page.dart';
import 'package:app_template/src/screens/onboarding_breeds.dart';
import 'package:app_template/src/screens/onboarding_image.dart';
import 'package:app_template/src/screens/onboarding_name.dart';
import 'package:app_template/src/widgets/build_button.dart';
import 'package:app_template/src/widgets/drop_down_list.dart';
import 'package:app_template/src/widgets/onboarding_content.dart';
import 'package:app_template/src/widgets/slide_transition.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/models/user_update_request_model.dart';
import 'package:app_template/src/bloc/user_bloc.dart';

class OnBoardingGender extends StatefulWidget {
  //const OnBoardingGender({Key? key}) : super(key: key);
  final UserUpdateRequestModel updateRequestModel;
  OnBoardingGender({this.updateRequestModel});

  @override
  _OnBoardingGenderState createState() => _OnBoardingGenderState();
}

class _OnBoardingGenderState extends State<OnBoardingGender> {
  UserUpdateRequestModel updateRequestModel;
  TextEditingController genderTextEditingController =
      new TextEditingController();
  bool select = false;
  UserBloc userBloc = UserBloc();
  List<String> gender = [
    "Male",
    "Female",
  ];
  int selectedValue;
  String genderSelected = "Select";
  showPicker() {
    showModalBottomSheet(
        backgroundColor: Constants.kitGradients[17],
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.only(
                topLeft: Radius.circular(8), topRight: Radius.circular(8))),
        context: context,
        builder: (BuildContext context) {
          return Container(
            height: screenHeight(context, dividedBy: 4),
            padding: EdgeInsets.symmetric(
              horizontal: screenWidth(context, dividedBy: 30),
              vertical: screenHeight(context, dividedBy: 30),
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  children: [
                    GestureDetector(
                      onTap: () {
                        pop(context);
                        genderTextEditingController.clear();
                      },
                      child: Text(
                        "Cancel",
                        style: TextStyle(
                          fontSize: 18,
                          fontWeight: FontWeight.w500,
                          fontFamily: 'Prompt-Ligh',
                          color: Constants.kitGradients[27],
                        ),
                      ),
                    ),
                    Spacer(),
                    GestureDetector(
                      onTap: () {
                        pop(context);
                      },
                      child: Text(
                        "Select",
                        style: TextStyle(
                          fontSize: 18,
                          fontWeight: FontWeight.w500,
                          fontFamily: 'Prompt-Ligh',
                          color: Constants.kitGradients[27],
                        ),
                      ),
                    ),
                  ],
                ),
                //SizedBox( height: screenHeight(context, dividedBy: 30),),
                Container(
                  height: screenHeight(context, dividedBy: 7),
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(8),
                          topRight: Radius.circular(8))),
                  child: ClipRRect(
                    borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(8),
                        topRight: Radius.circular(8)),
                    child: CupertinoPicker(
                      backgroundColor: Constants.kitGradients[17],
                      diameterRatio: 10,
                      //looping: true,
                      onSelectedItemChanged: (value) {
                        setState(() {
                          selectedValue = value;
                          genderTextEditingController.text = gender[value];
                          print(selectedValue);
                        });
                      },
                      itemExtent: screenHeight(context, dividedBy: 16),
                      children: gender.map((item) {
                        return Container(
                            alignment: Alignment.centerLeft,
                            padding: EdgeInsets.symmetric(
                              horizontal: screenWidth(context, dividedBy: 30),
                            ),
                            child: Text(
                              item,
                              style: TextStyle(
                                fontSize: 18,
                                fontWeight: FontWeight.w400,
                                fontFamily: 'Prompt-Ligh',
                                color: Constants.kitGradients[27],
                              ),
                            ));
                      }).toList(),
                    ),
                  ),
                ),
              ],
            ),
          );
        });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Constants.kitGradients[0],
      body: Container(
        height: screenHeight(context, dividedBy: 1),
        width: screenWidth(context, dividedBy: 1),
        decoration: BoxDecoration(
          color: Constants.kitGradients[17],
          image: DecorationImage(
            fit: BoxFit.cover,
            image: AssetImage(Constants.BACKGROUND_IMAGE),
            colorFilter: new ColorFilter.mode(
                Colors.black.withOpacity(0.6), BlendMode.darken),
          ),
        ),
        child: SingleChildScrollView(
          child: Container(
            width: screenWidth(context, dividedBy: 1),
            padding: EdgeInsets.symmetric(
              horizontal: screenWidth(context, dividedBy: 30),
              // vertical: screenHeight(context, dividedBy: 5),
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(
                  height: screenHeight(context, dividedBy: 3),
                ),
                Text(
                  "What is your Dog's Gender ?",
                  style: TextStyle(
                    fontSize: 26,
                    fontWeight: FontWeight.bold,
                    fontFamily: "Montserrat-ExtraBold",
                    color: Constants.kitGradients[27],
                  ),
                ),
                SizedBox(
                  height: screenHeight(context, dividedBy: 90),
                ),
                Text(
                  "Add some more details about your puppy to complete the registration             ",
                  textAlign: TextAlign.left,
                  style: TextStyle(
                      color: Constants.kitGradients[27],
                      fontFamily: "Montserrat",
                      fontSize: 16,
                      fontWeight: FontWeight.w400),
                ),
                SizedBox(
                  height: screenHeight(context, dividedBy: 25),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    BuildButton(
                      buttonWidth: 3,
                      isDisabled: true,
                      title: "Female",
                      onPressed: () async {
                        print("Selected female ");
                        updateRequestModel = UserUpdateRequestModel(
                            firstName: widget.updateRequestModel.firstName,
                            // lastName: widget.updateRequestModel.lastName,
                            name: widget.updateRequestModel.name,
                            dateOfBirth: widget.updateRequestModel.dateOfBirth,
                            breedType: widget.updateRequestModel.breedType,
                            gender: 1,
                            uid: widget.updateRequestModel.uid,
                            description: widget.updateRequestModel.description,
                            username: widget.updateRequestModel.username,
                            phoneNumber: widget.updateRequestModel.phoneNumber,
                            isUserHasDog:
                                widget.updateRequestModel.isUserHasDog);
                        push(
                            context,
                            OnBoardingImage(
                              updateRequestModel: updateRequestModel,
                            ));
                      },
                    ),
                    SizedBox(
                      width: screenWidth(context, dividedBy: 15),
                    ),
                    BuildButton(
                      buttonWidth: 3,
                      isDisabled: true,
                      title: "Male",
                      onPressed: () async {
                        print("Selected male  ");
                        updateRequestModel = UserUpdateRequestModel(
                            firstName: widget.updateRequestModel.firstName,
                            // lastName: widget.updateRequestModel.lastName,
                            name: widget.updateRequestModel.name,
                            dateOfBirth: widget.updateRequestModel.dateOfBirth,
                            breedType: widget.updateRequestModel.breedType,
                            gender: 0,
                            uid: widget.updateRequestModel.uid,
                            description: widget.updateRequestModel.description,
                            username: widget.updateRequestModel.username,
                            phoneNumber: widget.updateRequestModel.phoneNumber,
                            isUserHasDog:
                                widget.updateRequestModel.isUserHasDog);
                        push(
                          context,
                          OnBoardingImage(
                            updateRequestModel: updateRequestModel,
                          ),
                        );
                      },
                    ),
                  ],
                ),
                // OnBoardingContent(
                //   caption: "What is your Dog's Gender ?",
                //   image:
                //       "https://images.unsplash.com/photo-1510771463146-e89e6e86560e?ixid=MnwxMjA3fDB8MHxzZWFyY2h8MTN8fGRvZ3MlMjBjYXJ0b29uc3xlbnwwfHwwfHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=700&q=60",
                //   buttonTitle: "Next",
                //   currentIndex: 3,
                //   labelText: "Select",
                //   isGender: false,
                //   hasSuffix: true,
                //   readOnly: true,
                //   textEditingController: genderTextEditingController,
                //   suffixIcon: Icon(
                //     Icons.arrow_drop_down,
                //     color: Constants.kitGradients[19],
                //   ),
                //   onTapField: () {
                //     showPicker();
                //     print(selectedValue);
                //     setState(() {
                //       selectedValue = 0;
                //       genderTextEditingController.text = gender[0];
                //     });
                //   },
                //   onTapIcon: () {
                //     showPicker();
                //     print(selectedValue);
                //     setState(() {
                //       selectedValue = 0;
                //       genderTextEditingController.text = gender[0];
                //     });
                //   },
                //   onButtonPressed: () {
                //     // Navigator.of(context)
                //     //     .push(SlideRightRoute(page: OnBoardingImage()));
                //     updateRequestModel = UserUpdateRequestModel(
                //         firstName: widget.updateRequestModel.firstName,
                //         lastName: widget.updateRequestModel.lastName,
                //         name: widget.updateRequestModel.name,
                //         dateOfBirth: widget.updateRequestModel.dateOfBirth,
                //         breedType: widget.updateRequestModel.breedType,
                //         gender: selectedValue,
                //         uid: widget.updateRequestModel.uid,
                //         phoneNumber: widget.updateRequestModel.phoneNumber,
                //         isUserHasDog: widget.updateRequestModel.isUserHasDog);
                //     push(
                //         context,
                //         OnBoardingImage(
                //           updateRequestModel: updateRequestModel,
                //         ));
                //   },
                // ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
