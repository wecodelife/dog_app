import 'package:app_template/src/models/user_update_request_model.dart';
import 'package:app_template/src/screens/onboarding_dob.dart';
import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:app_template/src/widgets/onboarding_content.dart';
import 'package:flutter/material.dart';

class OnBoardingName extends StatefulWidget {
  //const OnBoardingName({Key? key}) : super(key: key);
  final UserUpdateRequestModel updateRequestModel;
  OnBoardingName({this.updateRequestModel});

  @override
  _OnBoardingNameState createState() => _OnBoardingNameState();
}

class _OnBoardingNameState extends State<OnBoardingName> {
  TextEditingController nameTextEditingController = new TextEditingController();

  UserUpdateRequestModel updateRequestModel;

  @override
  void initState() {
    print("firstName  " + widget.updateRequestModel.firstName);
    // print("lastName  " + widget.updateRequestModel.lastName);
    print("Phone Number  " + widget.updateRequestModel.phoneNumber);
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Constants.kitGradients[0],
      body: Container(
        child: OnBoardingContent(
          caption: "What is your Dog's Name ?",
          image:
              "https://images.unsplash.com/photo-1510771463146-e89e6e86560e?ixid=MnwxMjA3fDB8MHxzZWFyY2h8MTN8fGRvZ3MlMjBjYXJ0b29uc3xlbnwwfHwwfHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=700&q=60",
          labelText: "Name",
          buttonTitle: "Next",
          currentIndex: 0,
          hasSuffix: false,
          isGender: false,
          readOnly: false,
          textEditingController: nameTextEditingController,
          onButtonPressed: () {
            if (nameTextEditingController.text == null)
              showToast("Enter a name");
            else {
              print("Name page  ");
              // print("firstName  " + widget.updateRequestModel.firstName);
              // print("lastName  " + widget.updateRequestModel.lastName);
              // print("Phone Number  " + widget.updateRequestModel.phoneNumber);
              updateRequestModel = UserUpdateRequestModel(
                  firstName: widget.updateRequestModel.firstName,
                  // lastName: widget.updateRequestModel.lastName,
                  name: nameTextEditingController.text,
                  uid: widget.updateRequestModel.uid,
                  username: widget.updateRequestModel.username,
                  description: widget.updateRequestModel.description,
                  phoneNumber: widget.updateRequestModel.phoneNumber,
                  isUserHasDog: widget.updateRequestModel.isUserHasDog);
              push(context,
                  OnBoardingDOB(updateRequestModel: updateRequestModel));
              //push(context, OnBoardingDOB());
            }
          },
        ),
      ),
    );
  }
}
