import 'package:app_template/src/utils/urls.dart';
import 'package:app_template/src/widgets/build_button.dart';
import 'package:app_template/src/widgets/bulleted_list_tile.dart';
import 'package:app_template/src/widgets/category_tile.dart';
import 'package:flutter/material.dart';
import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:app_template/src/screens/item_cart_page.dart';
import 'package:app_template/src/screens/place_order_page.dart';
import 'package:app_template/src/bloc/user_bloc.dart';
import 'package:app_template/src/models/add_cart_details_request.dart';
import 'package:app_template/src/utils/object_factory.dart';
import 'package:app_template/src/models/get_cart_details.dart';
import 'package:flutter/services.dart';




class ItemDetailsPage extends StatefulWidget {
  final String itemName;
  final String itemImage;
  final String itemPrice;
  final String itemRating;
  final List<String> features;
  final int productId;
  ItemDetailsPage(
      {this.itemName,
      this.itemImage,
      this.itemPrice,
      this.itemRating,
      this.features,
      this.productId, String price});
  @override
  _ItemDetailsPageState createState() => _ItemDetailsPageState();
}

List<String> size = ["S", "M", "L", "XL", "XXL"];
int itemCount = 1;
String sizeType;
bool disabled = true;
List<Result> selectedItems = [];
List<String> features = [
  "Lorem ipsum dolor sit amet, consectetur adipiscing",
  "Lorem ipsum dolor sit amet, consectetur adipiscing ",
  "Lorem ipsum dolor sit amet, consectetur adipiscing "
];

class _ItemDetailsPageState extends State<ItemDetailsPage> {
  UserBloc userBloc=UserBloc();
  int count = 1;
  int bcount = 1;
  bool isLoading = false;

  @override
  void initState() {
    print("product"+widget.productId.toString());
    userBloc.getCartDetails(productId: widget.productId.toString());
    userBloc.getCartDetailsResponse.listen((event) async {
      print("cart");
      print(event.results);
      print("length" + event.results.length.toString());
    });
    userBloc.addCartDetailsResponse.listen((event) async {
      print("Add to cart working good");
      if (event.status == 200 || event.status == 201) {
        setState(() {
          isLoading = false;
          print("Cart Added successfully");
          userBloc.getCartDetailsResponse.listen((event) async {
            print("cart all good");
            print(event.results);
            print("length" + event.results.length.toString());
            print("price of item "+widget.itemPrice.toString());

          });
          showSuccessAnimatedToast(msg: "Added to cart Successfully", context: context);

        });
      } else {
        //showSuccessAnimatedToast(msg: "Added Successfully", context: context);
      }
    });
    // TODO: implement initState
    super.initState();

  }
  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.dark.copyWith(
        statusBarColor: Constants.kitGradients[17],
        //or set color with: Color(0xFF0000FF)
        statusBarIconBrightness: Brightness.dark));
    return Scaffold(
        backgroundColor: Constants.kitGradients[17],
        appBar: AppBar(
          backgroundColor: Constants.kitGradients[17],
          centerTitle: false,
          leading: GestureDetector(
            onTap: () {
              pop(context);
            },
            child: Icon(
              Icons.arrow_back_ios_rounded,
              color: Constants.kitGradients[27],
            ),
          ),
          title: Container(
            child: Row(
              children: [
                Text(
                  widget.itemName,
                  style: TextStyle(
                    //fontSize: 16,
                    fontWeight: FontWeight.w700,
                    fontFamily: 'Prompt-Light',
                    color: Constants.kitGradients[27],
                  ),
                ),
                Spacer(),
                GestureDetector(
                    onTap: () {
                      push(
                          context,
                          ItemCartPage(
                             itemPrice: widget.itemPrice,
                              itemImage:  widget.itemImage,
                              itemName: widget.itemName,
                          ));
                    },
                    child: Icon(
                      Icons.shopping_cart_outlined,
                      color: Constants.kitGradients[27],
                    ))
              ],
            ),
          ),
        ),
        body: Stack(
          children: [
            SingleChildScrollView(
              child: Container(

                child: Column(
                  children: [
                    SizedBox(
                      height: screenHeight(context, dividedBy: 40),
                    ),
                    CachedNetworkImage(
                      width: screenWidth(context, dividedBy: 1),
                      // height: screenHeight(context, dividedBy:1.27),
                      fit: BoxFit.cover,
                      placeholder: (context, url) => Center(
                        heightFactor: 1,
                        widthFactor: 1,
                        child: SizedBox(
                          height: 16,
                          width: 16,
                          child: CircularProgressIndicator(
                            valueColor: AlwaysStoppedAnimation(
                                Constants.kitGradients[19]),
                            strokeWidth: 2,
                          ),
                        ),
                      ),
                      imageUrl: widget.itemImage,
                      imageBuilder: (context, imageProvider) => Container(
                        padding: EdgeInsets.symmetric(
                          horizontal: screenWidth(context, dividedBy: 30)
                        ),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Container(
                              width: screenWidth(context, dividedBy: 1),
                              height: screenHeight(context, dividedBy: 3),
                              // margin: EdgeInsets.symmetric(horizontal: screenWidth(context, dividedBy: 30),),
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(10),
                                //shape: BoxShape.circle,
                                image: DecorationImage(
                                    image: imageProvider, fit: BoxFit.fill),
                              ),
                            ),
                            SizedBox(
                                height: screenHeight(context, dividedBy: 30)),
                            Text(
                              widget.itemName,
                              // textAlign: TextAlign.left,
                              style: TextStyle(
                                color: Constants.kitGradients[27],
                                fontWeight: FontWeight.w200,
                                fontFamily: 'Prompt-Light',
                                fontSize: 19,
                              ),
                            ),
                               SizedBox(
                                height: screenHeight(context, dividedBy: 70)),
                            Row(
                              children: [
                                Text(
                                  //" " ,
                                     "₹" + widget.itemPrice,
                                  textAlign: TextAlign.right,
                                  style: TextStyle(
                                    color: Constants.kitGradients[27],
                                    fontWeight: FontWeight.bold,
                                    fontFamily: 'Prompt-Light',
                                    fontSize: 20,
                                  ),
                                ),
                              ],
                            ),

                            SizedBox(
                                height: screenHeight(context, dividedBy: 90)),
                            Row(
                              children: [
                                Container(
                                  width: screenWidth(context, dividedBy: 6.6),
                                  height: screenHeight(context, dividedBy: 22),
                                  decoration: BoxDecoration(
                                    color: Constants.kitGradients[27],
                                    borderRadius: BorderRadius.circular(8),
                                  ),
                                  padding: EdgeInsets.symmetric(
                                      horizontal:
                                          screenWidth(context, dividedBy: 40)),
                                  //alignment: Alignment.center,
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    children: [
                                      Text(
                                        widget.itemRating,
                                        // textAlign: TextAlign.left,
                                        style: TextStyle(
                                          color: Constants.kitGradients[19],
                                          fontWeight: FontWeight.bold,
                                          fontFamily: 'Prompt-Light',
                                          fontSize: 15,
                                        ),
                                      ),
                                      Spacer(),
                                      Icon(Icons.star,
                                          color: Constants.kitGradients[19],
                                          size: 17)
                                    ],
                                  ),
                                ),
                                SizedBox(
                                    width: screenWidth(context, dividedBy: 40)),
                                Text(
                                  "2300 Ratings",
                                  // textAlign: TextAlign.left,
                                  style: TextStyle(
                                    color: Constants.kitGradients[19],
                                    fontWeight: FontWeight.w500,
                                    fontFamily: 'Prompt-Light',
                                    fontSize: 17,
                                  ),
                                ),
                              ],
                            ),
                            SizedBox(
                              height: screenHeight(context, dividedBy: 88),
                            ),
                            Divider(
                              color:
                                  Constants.kitGradients[19].withOpacity(0.2),
                              // indent: 10,
                              // endIndent: 10,
                            ),
                            // SizedBox(
                            //   height: screenHeight(context, dividedBy: 90),
                            // ),
                            /*Text(
                              "",
                              // textAlign: TextAlign.left,
                              style: TextStyle(
                                color: Constants.kitGradients[19],
                                fontWeight: FontWeight.w500,
                                fontFamily: 'Prompt-Light',
                                fontSize: 17,
                              ),
                            ),*/
                            SizedBox(
                              height: screenHeight(context, dividedBy: 90),
                            ),
                            Text(
                              "Highlights",
                              // textAlign: TextAlign.left,
                              style: TextStyle(
                                color: Constants.kitGradients[27],
                                fontWeight: FontWeight.bold,
                                fontFamily: 'Prompt-Light',
                                fontSize: 17,
                              ),
                            ),
                            ListView.builder(
                              itemCount: 3,
                              shrinkWrap: true,
                              physics: NeverScrollableScrollPhysics(),
                              itemBuilder: (BuildContext context, int index) {
                                return Container(
                                  width: screenWidth(context, dividedBy: 1),
                                  child: Column(children: [
                                    BulletedListTile(
                                      text: widget.features[index],
                                    ),
                                    SizedBox(
                                      height:
                                          screenHeight(context, dividedBy: 90),
                                    ),
                                  ]),
                                );
                              },
                            ),

                            // Container(
                            //   width: screenWidth(context, dividedBy: 1),
                            //   child: Column(
                            //     crossAxisAlignment: CrossAxisAlignment.start,
                            //     children: [
                            //       SizedBox(
                            //         height:
                            //             screenHeight(context, dividedBy: 50),
                            //       ),
                            //       Row(
                            //         children: [
                            //           Text(
                            //             "AVAILABLE SIZES:",
                            //             style: TextStyle(
                            //               color: Constants.kitGradients[27],
                            //               fontFamily: 'Prompt-Light',
                            //               fontWeight: FontWeight.w500,
                            //               fontSize: 18,
                            //             ),
                            //           ),
                            //           SizedBox(
                            //               width: screenWidth(context,
                            //                   dividedBy: 30)),
                            //           DropDownList(
                            //             title: "Select",
                            //             dropDownList: size,
                            //             dropDownValue: sizeType,
                            //             onClicked: (value) {
                            //               setState(() {
                            //                 sizeType = value;
                            //               });
                            //             },
                            //           ),
                            //         ],
                            //       ),
                            //       SizedBox(
                            //         height:
                            //             screenHeight(context, dividedBy: 30),
                            //       ),
                            //       Container(
                            //           child: Row(
                            //         children: [
                            //           Text(
                            //             "QUANTITY YOU NEED:",
                            //             style: TextStyle(
                            //               color: Constants.kitGradients[27],
                            //               fontFamily: 'Prompt-Light',
                            //               fontWeight: FontWeight.w500,
                            //               fontSize: 16,
                            //             ),
                            //           ),
                            //           SizedBox(
                            //             width:
                            //                 screenWidth(context, dividedBy: 30),
                            //           ),
                            //           Container(
                            //               // height: screenHeight(context,
                            //               //     dividedBy: 10),
                            //               child: Column(
                            //             children: [
                            //               GestureDetector(
                            //                 onTap: () {
                            //                   setState(() {
                            //                     itemCount++;
                            //                   });
                            //                 },
                            //                 child: Icon(
                            //                   Icons.arrow_drop_up,
                            //                   color: Constants.kitGradients[27],
                            //                 ),
                            //               ),
                            //               Text(
                            //                 itemCount.toString(),
                            //                 style: TextStyle(
                            //                   color: Constants.kitGradients[29],
                            //                   fontFamily: 'Prompt-Light',
                            //                   fontWeight: FontWeight.bold,
                            //                   fontSize: 15,
                            //                 ),
                            //               ),
                            //               GestureDetector(
                            //                 onTap: () {
                            //                   setState(() {
                            //                     itemCount--;
                            //                   });
                            //                 },
                            //                 child: Icon(
                            //                   Icons.arrow_drop_down,
                            //                   color: Constants.kitGradients[27],
                            //                 ),
                            //               ),
                            //             ],
                            //           ))
                            //         ],
                            //       )),
                            //     ],
                            //   ),
                            // ),
                          ],
                        ),
                      ),
                    ),
                    CategoryListTile(
                      itemtype: [
                        "Pedigree",
                        "Paw & Bone Bowl",
                        "Drools",
                      ],
                      images: [
                        "https://images.unsplash.com/photo-1589924691995-400dc9ecc119?ixid=MnwxMjA3fDB8MHxzZWFyY2h8NHx8ZG9nJTIwZm9vZHxlbnwwfHwwfHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=700&q=60",
                        "https://images.unsplash.com/photo-1589924691995-400dc9ecc119?ixid=MnwxMjA3fDB8MHxzZWFyY2h8NHx8ZG9nJTIwZm9vZHxlbnwwfHwwfHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=700&q=60",
                        "https://images.unsplash.com/photo-1589924691995-400dc9ecc119?ixid=MnwxMjA3fDB8MHxzZWFyY2h8NHx8ZG9nJTIwZm9vZHxlbnwwfHwwfHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=700&q=60"
                      ],
                      productRating: ["4.5","5.0","4.3"],
                      categoryType: "Suggested for you",
                      seeMore: () {},
                      itemPressed: () {
                        push(
                          context,
                          ItemDetailsPage(
                            itemName: "Cookies, Biscuits & Snacks",
                            itemImage:
                                "https://images.unsplash.com/photo-1597603413826-cd1c06b05222?ixid=MnwxMjA3fDB8MHxzZWFyY2h8NHx8ZG9nJTIwYmF0aHxlbnwwfHwwfHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=700&q=60",

                            itemRating: "8.1",
                            price: "\u0024 20",

                            features: features,
                          ),
                        );
                      },
                    ),
                    SizedBox(
                      height: screenHeight(context, dividedBy: 10),
                    ),
                  ],
                ),
              ),
            ),
            Align(
              alignment: Alignment.bottomCenter,
              child: Container(
                width: screenWidth(context, dividedBy: 1),
                padding: EdgeInsets.symmetric(
                  horizontal: screenWidth(context, dividedBy: 30),
                  vertical: screenHeight(context, dividedBy: 40),
                ),
                color: Constants.kitGradients[17],
                child: Row(children: [
                  BuildButton(
                      title: "Add to Cart",
                      buttonWidth: 2.3,
                      isDisabled: disabled,
                      onPressed: () {
                        userBloc.addCartDetails(addCartDetailsRequest: AddCartDetailsRequest(
                          product: widget.productId,
                          quantity: 1,
                          isDeleted: false,
                          user: int.parse(ObjectFactory().appHive.getUserId()),
                        ));
                      }),
                  Spacer(),
                  BuildButton(
                      title: "Buy",
                      buttonWidth: 2.3,
                      isDisabled: true,
                      onPressed: () {
                        push(context, PlaceOrderPage(
                           //items: selectedItems
                        ));

                      }),
                ]),
              ),
              // Container(
              //   height: screenHeight(context, dividedBy: 15),
              //   color: Constants.kitGradients[24],
              //   padding: EdgeInsets.symmetric(
              //     horizontal: screenWidth(context, dividedBy: 30),
              //   ),
              //   margin: EdgeInsets.symmetric(
              //     //horizontal: screenWidth(context, dividedBy: 30),
              //     vertical: screenHeight(context, dividedBy: 80),
              //   ),
              //   child: Row(
              //       crossAxisAlignment: CrossAxisAlignment.center,
              //       children: [
              //         GestureDetector(
              //           onTap: () {
              //             count == 1
              //                 ? setState(() {
              //                     count = 2;
              //                   })
              //                 : setState(() {
              //                     count = 1;
              //                   });
              //
              //             push(
              //                 context,
              //                 ItemCartPage(
              //                     itemPrice: widget.itemPrice,
              //                     itemImage: widget.itemImage,
              //                     itemName: widget.itemName));
              //           },
              //           child: Container(
              //             width: screenWidth(context, dividedBy: 2.2),
              //             height: screenHeight(context, dividedBy: 15),
              //             decoration: BoxDecoration(
              //               color: count == 2
              //                   ? Constants.kitGradients[29]
              //                   : Colors.transparent,
              //               border: Border.all(
              //                 color: Constants.kitGradients[29],
              //               ),
              //             ),
              //             alignment: Alignment.center,
              //             child: Text(
              //               "ADD TO CART",
              //               style: TextStyle(
              //                 color: Constants.kitGradients[27],
              //                 fontWeight: FontWeight.w500,
              //                 fontSize: 16,
              //               ),
              //             ),
              //           ),
              //         ),
              //         Spacer(),
              //         GestureDetector(
              //           onTap: () {
              //             bcount == 1
              //                 ? setState(() {
              //                     bcount = 2;
              //                   })
              //                 : setState(() {
              //                     bcount = 1;
              //                   });
              //           },
              //           child: Container(
              //             width: screenWidth(context, dividedBy: 2.2),
              //             height: screenHeight(context, dividedBy: 15),
              //             decoration: BoxDecoration(
              //               color: bcount == 2
              //                   ? Constants.kitGradients[29]
              //                   : Colors.transparent,
              //               border: Border.all(
              //                 color: Constants.kitGradients[29],
              //               ),
              //             ),
              //             alignment: Alignment.center,
              //             child: Text(
              //               "BUY",
              //               style: TextStyle(
              //                 color: Constants.kitGradients[27],
              //                 fontWeight: FontWeight.w500,
              //                 fontSize: 16,
              //               ),
              //             ),
              //           ),
              //         )
              //       ]),
              // ),
            )
          ],
        ));
  }
}
