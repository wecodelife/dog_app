import 'package:app_template/src/bloc/user_bloc.dart';
import 'package:app_template/src/models/get_post_comment_response.dart';
import 'package:app_template/src/models/add_post_comment_response.dart';
import 'package:app_template/src/models/add_post_comment_request.dart';
import 'package:app_template/src/utils/object_factory.dart';
import 'package:app_template/src/widgets/app_bar.dart';
import 'package:app_template/src/widgets/comment_box.dart';
import 'package:app_template/src/widgets/comment_tile.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:app_template/src/utils/constants.dart';

class CommentDetailsPage extends StatefulWidget {
  // const CommentDetailsPage({Key? key}) : super(key: key);
  // final String comment;
  // final String commentBy;
  final String feedImage;
  final String caption;
  final int postId;
  CommentDetailsPage(
      {
      //   this.comment,
      // this.commentBy,
      this.caption,
      this.feedImage,
      this.postId});

  @override
  _CommentDetailsPageState createState() => _CommentDetailsPageState();
}

class _CommentDetailsPageState extends State<CommentDetailsPage> {
  int favcount = 1;
  TextEditingController commentTextEditingController =
      new TextEditingController();
  UserBloc userBloc = UserBloc();
  List<String> comments = [];
  bool isLoading = false;

  @override
  void initState() {
    userBloc.getPostComments(id: widget.postId.toString());
    userBloc.getPostCommentsResponse.listen((event) async {
      print("comments all good");
      print(event.results);
      print("length" + event.results.length.toString());
    });
    userBloc.addPostCommentsResponse.listen((event) async {
      print("Add Comments working good");
      if (event.status == 200 || event.status == 201) {
        setState(() {
          isLoading = false;
          commentTextEditingController.clear();
          print("Comment Added successfully");
          userBloc.getPostComments(id: widget.postId.toString());
          userBloc.getPostCommentsResponse.listen((event) async {
            print("comments all good");
            print(event.results);
            print("length" + event.results.length.toString());
          });
        });
      } else {
        showErrorAnimatedToast(msg: "Something went wrong!", context: context);
      }
    });
    // userBloc.getPostComments(id: "3");
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Constants.kitGradients[17],
        appBar: AppBar(
            backgroundColor: Constants.kitGradients[17],
            elevation: 0,
            leading: Container(),
            actions: [
              AppBarDogApp(
                  leftIcon: Icon(
                    Icons.arrow_back,
                    color: Constants.kitGradients[27],
                  ),
                  onTapLeftIcon: () {
                    pop(context);
                  },
                  rightIcon: Container(),
                  onTapRightIcon: () {},
                  title: Row(
                    children: [
                      Text(
                        "Comments",
                        style: TextStyle(
                          fontSize: 16,
                          fontWeight: FontWeight.w700,
                          fontFamily: 'Montserrat',
                          color: Constants.kitGradients[27],
                        ),
                      ),
                    ],
                  )),
            ]),
        body: Container(
          height: screenHeight(context, dividedBy: 1),
          width: screenWidth(context, dividedBy: 1),
          child: Stack(
            children: [
              SingleChildScrollView(
                child: Container(
                  padding: EdgeInsets.symmetric(
                    horizontal: screenWidth(context, dividedBy: 30),
                  ),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      GestureDetector(
                        child: Container(
                          width: screenWidth(context, dividedBy: 1),
                          height: screenHeight(context, dividedBy: 3),
                          decoration: BoxDecoration(
                            color: Constants.kitGradients[19].withOpacity(0.12),
                            borderRadius: BorderRadius.circular(4),
                          ),
                          child: ClipRRect(
                            borderRadius: BorderRadius.circular(4),
                            child: CachedNetworkImage(
                              fit: BoxFit.scaleDown,
                              imageUrl: widget.feedImage,
                              imageBuilder: (context, imageProvider) =>
                                  Container(
                                width: screenWidth(context, dividedBy: 1),
                                height: screenHeight(context, dividedBy: 3),
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(4),
                                  image: DecorationImage(
                                    image: imageProvider,
                                    fit: BoxFit.scaleDown,
                                  ),
                                ),
                              ),
                              placeholder: (context, url) => Center(
                                heightFactor: 1,
                                widthFactor: 1,
                                child: SizedBox(
                                  height: 16,
                                  width: 16,
                                  child: CircularProgressIndicator(
                                    valueColor: AlwaysStoppedAnimation(
                                        Constants.kitGradients[8]),
                                    strokeWidth: 2,
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ),
                        // Container(
                        //   width: screenWidth(context, dividedBy: 1),
                        //   height: screenHeight(context, dividedBy: 3),
                        //   decoration: BoxDecoration(
                        //     borderRadius: BorderRadius.circular(8),
                        //   ),
                        //   child: ClipRRect(
                        //     borderRadius: BorderRadius.circular(8),
                        //     child: CachedNetworkImage(
                        //       fit: BoxFit.cover,
                        //       imageUrl: widget.feedImage,
                        //       imageBuilder: (context, imageProvider) =>
                        //           Container(
                        //         width: screenWidth(context, dividedBy: 1),
                        //         // height: screenHeight(context, dividedBy: 3),
                        //         decoration: BoxDecoration(
                        //           image: DecorationImage(
                        //             image: imageProvider,
                        //             fit: BoxFit.cover,
                        //           ),
                        //         ),
                        //       ),
                        //       placeholder: (context, url) => Center(
                        //         heightFactor: 1,
                        //         widthFactor: 1,
                        //         child: SizedBox(
                        //           height: 16,
                        //           width: 16,
                        //           child: CircularProgressIndicator(
                        //             valueColor: AlwaysStoppedAnimation(
                        //                 Constants.kitGradients[8]),
                        //             strokeWidth: 2,
                        //           ),
                        //         ),
                        //       ),
                        //     ),
                        //   ),
                        // ),
                      ),
                      SizedBox(height: screenHeight(context, dividedBy: 20)),
                      Container(
                          child: StreamBuilder<GetPostCommentResponse>(
                              stream: userBloc.getPostCommentsResponse,
                              builder: (context, snapshot) {
                                return Container(
                                  child: snapshot.hasData
                                      ? SingleChildScrollView(
                                          child: Container(
                                            width: screenWidth(context,
                                                dividedBy: 1),
                                            child: snapshot
                                                        .data.results.length <
                                                    0
                                                ? Center(
                                                    child: Container(
                                                        width: screenWidth(
                                                            context,
                                                            dividedBy: 1),
                                                        height: screenHeight(
                                                            context,
                                                            dividedBy: 2),
                                                        // color: Colors.blue,
                                                        child: Center(
                                                            child: Text(
                                                          "No Comments",
                                                          style: TextStyle(
                                                            fontSize: 12,
                                                            fontWeight:
                                                                FontWeight.w600,
                                                            fontFamily:
                                                                'Montserrat',
                                                            color: Constants
                                                                .kitGradients[27],
                                                          ),
                                                        ))),
                                                  )
                                                : ListView.builder(
                                                    itemCount: snapshot
                                                        .data.results.length,
                                                    shrinkWrap: true,
                                                    physics:
                                                        NeverScrollableScrollPhysics(),
                                                    itemBuilder:
                                                        (BuildContext context,
                                                            int index) {
                                                      return Container(
                                                        child: Column(
                                                          children: [
                                                            CommentTile(
                                                              postId: snapshot
                                                                  .data
                                                                  .results[
                                                                      index]
                                                                  .postId
                                                                  .toString(),
                                                              comment: snapshot
                                                                  .data
                                                                  .results[
                                                                      index]
                                                                  .comment,
                                                              commentBy: snapshot
                                                                          .data
                                                                          .results[
                                                                              index]
                                                                          .username ==
                                                                      ""
                                                                  ? "UserName"
                                                                  : snapshot
                                                                      .data
                                                                      .results[
                                                                          index]
                                                                      .username,
                                                            ),
                                                            SizedBox(
                                                                height:
                                                                    screenHeight(
                                                                        context,
                                                                        dividedBy:
                                                                            40))
                                                          ],
                                                        ),
                                                      );
                                                    },
                                                  ),
                                          ),
                                        )
                                      : Container(
                                          width: screenWidth(context,
                                              dividedBy: 1),
                                          height: screenHeight(context,
                                              dividedBy: 1),
                                          color: Constants.kitGradients[17],
                                          child: Center(
                                            heightFactor: 1,
                                            widthFactor: 1,
                                            child: SizedBox(
                                              height: 16,
                                              width: 16,
                                              child: CircularProgressIndicator(
                                                valueColor:
                                                    AlwaysStoppedAnimation(
                                                        Constants
                                                            .kitGradients[19]),
                                                strokeWidth: 2,
                                              ),
                                            ),
                                          ),
                                        ),
                                );
                              })),

                      // Container(
                      //   child: ListView.builder(
                      //     itemCount: comments.length,
                      //     shrinkWrap: true,
                      //     physics: NeverScrollableScrollPhysics(),
                      //     itemBuilder: (BuildContext context, int index) {
                      //       return Container(
                      //         child: Column(
                      //           children: [
                      //             CommentTile(
                      //               comment: comments[index],
                      //               commentBy: "dsnmm",
                      //             ),
                      //             SizedBox(
                      //                 height:
                      //                     screenHeight(context, dividedBy: 40))
                      //           ],
                      //         ),
                      //       );
                      //     },
                      //   ),
                      // ),
                      SizedBox(height: screenHeight(context, dividedBy: 10))
                    ],
                  ),
                ),
              ),
              new Positioned(
                bottom: 0,
                child: Container(
                  color: Constants.kitGradients[17],
                  padding: EdgeInsets.symmetric(
                    horizontal: screenWidth(context, dividedBy: 25),
                    vertical: screenHeight(context, dividedBy: 50),
                  ),
                  child: CommentBox(
                    textEditingController: commentTextEditingController,
                    autofocus: false,
                    onTapPost: () {
                      if (commentTextEditingController.text == "") {
                        showToast("Please type something");
                      } else {
                        setState(() {
                          print(commentTextEditingController.text);
                          print("Comment Adding from home page");
                        });
                        userBloc.addPostComments(
                            addPostCommentRequest: AddPostCommentRequest(
                              post: widget.postId,
                              comment: commentTextEditingController.text,
                              isDeleted: false,
                              createdBy:
                              int.parse(ObjectFactory().appHive.getUserId()),
                            ));
                      }
                    },
                  ),
                ),
              )
            ],
          ),
        ));
  }
}
