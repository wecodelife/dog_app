import 'package:app_template/src/screens/home_page.dart';
import 'package:app_template/src/screens/sign_up_page.dart';
import 'package:app_template/src/utils/object_factory.dart';
import 'package:app_template/src/utils/urls.dart';
import 'package:app_template/src/widgets/bottom_sheet_item.dart';
import 'package:app_template/src/widgets/onboarding_button.dart';
import 'package:app_template/src/widgets/page_indicator.dart';
import 'package:flutter/material.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:app_template/src/utils/constants.dart';
import 'package:image_picker/image_picker.dart';
import 'package:flutter/cupertino.dart';
import 'package:app_template/src/models/user_update_request_model.dart';
import 'package:app_template/src/bloc/user_bloc.dart';

import 'dart:io';

class OnBoardingImage extends StatefulWidget {
  //const OnBoardingImage({Key? key}) : super(key: key);
  UserUpdateRequestModel updateRequestModel;
  OnBoardingImage({this.updateRequestModel});

  @override
  _OnBoardingImageState createState() => _OnBoardingImageState();
}

class _OnBoardingImageState extends State<OnBoardingImage> {
  //image picker
  UserUpdateRequestModel updateRequestModel;
  UserBloc userBloc = UserBloc();
  File _image;
  final picker = ImagePicker();
  bool imageSelected = false;
  String id;
  imgFromCamera() async {
    final pickedFile = await ImagePicker.pickImage(
        source: ImageSource.camera, imageQuality: 50);
    setState(() {
      if (pickedFile != null) {
        _image = File(pickedFile.path);
        setState(() {
          imageSelected = true;
        });
      } else {
        print('No image selected.');
      }
    });
  }

  imgFromGallery() async {
    final pickedFile = await ImagePicker.pickImage(
        source: ImageSource.gallery, imageQuality: 50);

    setState(() {
      if (pickedFile != null) {
        _image = File(pickedFile.path);
      } else {
        print('No image selected.');
      }
    });
  }

  int selected;
  showPicker() {
    showModalBottomSheet(
        context: context,
        backgroundColor: Constants.kitGradients[17],
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.only(
                topLeft: Radius.circular(8), topRight: Radius.circular(8))),
        builder: (BuildContext context) {
          return Container(
            // height:screenHeight(context, dividedBy:4),
            padding: EdgeInsets.symmetric(
              horizontal: screenWidth(context, dividedBy: 30),
              vertical: screenHeight(context, dividedBy: 30),
            ),
            decoration: BoxDecoration(
                color: Constants.kitGradients[17],
                borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(8), topRight: Radius.circular(8))),
            child: Wrap(
              //crossAxisAlignment:CrossAxisAlignment.start,
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.symmetric(
                    horizontal: screenWidth(context, dividedBy: 20),
                    //vertical: screenHeight(context, dividedBy: 20),
                  ),
                  child: Text(
                    "Select Picture from",
                    style: TextStyle(
                      fontSize: 20,
                      fontWeight: FontWeight.bold,
                      fontFamily: "Montserrat",
                      color: Constants.kitGradients[27],
                    ),
                  ),
                ),
                SizedBox(
                  height: screenHeight(context, dividedBy: 15),
                ),
                BottomSheetItem(
                  title: "Photo Library",
                  leading: new Icon(
                    Icons.photo_library,
                    color: Constants.kitGradients[27],
                  ),
                  trailing: null,
                  onTap: () {
                    imgFromGallery();
                    //showToast("msg");
                    Navigator.of(context).pop();
                  },
                ),
                Divider(
                  color: Constants.kitGradients[27].withOpacity(0.2),
                  indent: 10,
                  endIndent: 10,
                ),
                BottomSheetItem(
                  title: "Camera",
                  leading: new Icon(
                    Icons.photo_camera,
                    color: Constants.kitGradients[27],
                  ),
                  trailing: null,
                  onTap: () {
                    imgFromCamera();
                    Navigator.of(context).pop();
                  },
                ),
                Divider(
                  color: Constants.kitGradients[27].withOpacity(0.2),
                  indent: 10,
                  endIndent: 10,
                ),
              ],
            ),
          );
        });
  }

  @override
  void initState() {
    userBloc.userUpdateResponseModel.listen((event) {
      if (event.status == 200 || event.status == 201) {
        ObjectFactory().appHive.putUserId(userId: event.id.toString());
        ObjectFactory().appHive.putName(name: event.firstName);
        ObjectFactory().appHive.putProfilePic(event.profileImage);
        print("Registration userid   " + event.id.toString());
        print("Registration UserName   " + event.name);
        print(event.id);
        print("Registration Profilepic  " +
            ObjectFactory().appHive.getProfilePic());
        print(event.profileImage.toString() + "gghghg");
        push(context, HomePage());
      }
    });
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Constants.kitGradients[30],
      body: Container(
        height: screenHeight(context, dividedBy: 1),
        width: screenWidth(context, dividedBy: 1),
        decoration: BoxDecoration(
          color: Constants.kitGradients[17],
          image: DecorationImage(
            fit: BoxFit.cover,
            image: AssetImage(Constants.BACKGROUND_IMAGE),
            colorFilter: new ColorFilter.mode(
                Colors.black.withOpacity(0.6), BlendMode.darken),
          ),
        ),
        child: SingleChildScrollView(
          child: Container(
            width: screenWidth(context, dividedBy: 1),
            padding: EdgeInsets.symmetric(
              horizontal: screenWidth(context, dividedBy: 30),
              // vertical: screenHeight(context, dividedBy: 5),
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(
                  height: screenHeight(context, dividedBy: 4),
                ),
                Text(
                  "Upload your Dog's Picture",
                  style: TextStyle(
                    fontSize: 26,
                    fontWeight: FontWeight.bold,
                    fontFamily: "Montserrat-ExtraBold",
                    color: Constants.kitGradients[27],
                  ),
                ),
                SizedBox(
                  height: screenHeight(context, dividedBy: 90),
                ),
                Text(
                  "Upload a picture of your puppy to complete the registration             ",
                  textAlign: TextAlign.left,
                  style: TextStyle(
                      color: Constants.kitGradients[27],
                      fontFamily: "Montserrat",
                      fontSize: 16,
                      fontWeight: FontWeight.w400),
                ),
                SizedBox(
                  height: screenHeight(context, dividedBy: 30),
                ),
                Center(
                  child: Stack(
                    children: [
                      Container(
                        width: screenWidth(context, dividedBy: 2.5),
                        height: screenWidth(context, dividedBy: 2.5),
                        decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            color: Constants.kitGradients[27].withOpacity(0.3),
                            border: Border.all(
                                color:
                                    Constants.kitGradients[27].withOpacity(0.3),
                                width: 2.0),
                            image: DecorationImage(
                                fit: BoxFit.cover,
                                image: _image == null
                                    ? AssetImage(Constants.BACKGROUND_IMAGE)
                                    : FileImage(_image))),
                        // child:_image == null
                        //     ? Text("")
                        //     : Image.file(_image),
                      ),
                      Positioned(
                        top: screenHeight(context, dividedBy: 7),
                        right: 0,
                        child: Container(
                          width: screenWidth(context, dividedBy: 10),
                          height: screenWidth(context, dividedBy: 10),
                          decoration: BoxDecoration(
                              color: Constants.kitGradients[19],
                              shape: BoxShape.circle),
                          alignment: Alignment.center,
                          child: GestureDetector(
                            onTap: () {
                              showPicker();
                            },
                            child: Icon(
                              Icons.add_a_photo,
                              color: Constants.kitGradients[7],
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                SizedBox(
                  height: screenHeight(context, dividedBy: 30),
                ),
                Column(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    SizedBox(
                      height: screenHeight(context, dividedBy: 30),
                    ),
                    SizedBox(
                      height: screenHeight(context, dividedBy: 30),
                    ),
                    PageIndicator(
                      currentIndex: 4,
                    ),
                    SizedBox(
                      height: screenHeight(context, dividedBy: 15),
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: [
                        OnBoardingButton(
                          title: "Finish",
                          onPressed: () async {
                            print("Finish Pressed");
                            // print(widget.updateRequestModel.firstName);
                            // print("Name  " + updateRequestModel.name);
                            // print("firstName  " + updateRequestModel.firstName);
                            // print("lastName  " + updateRequestModel.lastName);
                            // print("phoneNumber  " +
                            //     updateRequestModel.phoneNumber.toString());
                            // print("dateOfBirth  " +
                            //     updateRequestModel.dateOfBirth.toString());
                            // print("breedType  " +
                            //     updateRequestModel.breedType.toString());
                            // print("gender  " +
                            //     updateRequestModel.gender.toString());
                            // print("profileImage  " +
                            //     updateRequestModel.profileImage.path);
                            // print("is_user_has_dog  " +
                            //     updateRequestModel.isUserHasDog.toString());
                            updateRequestModel = UserUpdateRequestModel(
                                firstName: widget.updateRequestModel.firstName,
                                // lastName: widget.updateRequestModel.lastName,
                                username: widget.updateRequestModel.username,
                                name: widget.updateRequestModel.name,
                                dateOfBirth:
                                    widget.updateRequestModel.dateOfBirth,
                                breedType: widget.updateRequestModel.breedType,
                                gender: widget.updateRequestModel.gender,
                                profileImage: _image,
                                uid: widget.updateRequestModel.uid,
                              description: widget.updateRequestModel.description,
                                phoneNumber:
                                    widget.updateRequestModel.phoneNumber,
                                isUserHasDog:
                                    widget.updateRequestModel.isUserHasDog,
                                // description:
                                //     widget.updateRequestModel.description
                            );
                            print(
                              "nmhhjhjjg   " + updateRequestModel.firstName,
                            );
                            userBloc.userUpdate(
                              id: ObjectFactory().appHive.getUserId(),
                              userUpdateRequest: updateRequestModel,
                            );
                          },
                          isLoading: false,
                          width: 3,
                        ),
                        SizedBox(width: screenWidth(context, dividedBy: 20)),
                      ],
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
