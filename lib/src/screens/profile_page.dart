import 'package:app_template/src/bloc/user_bloc.dart';
import 'package:app_template/src/models/get_user_profile_response.dart';
import 'package:app_template/src/screens/edit_profile_page.dart';
import 'package:app_template/src/screens/home_page.dart';
import 'package:app_template/src/screens/login_page.dart';
import 'package:app_template/src/screens/market_place.dart';
import 'package:app_template/src/screens/new_post_page.dart';
import 'package:app_template/src/screens/notification_page.dart';
import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/object_factory.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:app_template/src/widgets/app_bar.dart';
import 'package:app_template/src/widgets/bottom_bar.dart';
import 'package:app_template/src/widgets/bottom_sheet_item.dart';
import 'package:app_template/src/widgets/profile_icon.dart';
import 'package:app_template/src/widgets/profile_tab.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:app_template/src/models/get_post_response.dart';
import 'package:app_template/src/widgets/fullscreen_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class ProfilePage extends StatefulWidget {
 ProfilePage({String name, String profilePic, String s});

  // final String profilepic1;
  // final String name;
  // ProfilePage({this.name, this.profilepic1});
   @override

  _ProfilePageState createState() => _ProfilePageState();
}

//ProfileTabBar
class _ProfilePageState extends State<ProfilePage> {
  UserBloc userBloc = UserBloc();
  bool loading = true;
  GetUserProfileResponse profileData = GetUserProfileResponse();
  GetPostResponse postData = GetPostResponse();

  get imageUrlHere => null;
  Widget example() {
    return new DropdownButton(
        isExpanded: true,
        items: [
          new DropdownMenuItem(child: new Text("Abc")),
          new DropdownMenuItem(child: new Text("Xyz")),
        ],
        hint: new Text("Select City"),
        onChanged: null);
  }

  List<String> image = [
    'https://images.unsplash.com/photo-1520342868574-5fa3804e551c?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=6ff92caffcdd63681a35134a6770ed3b&auto=format&fit=crop&w=1951&q=80',
    'https://images.unsplash.com/photo-1522205408450-add114ad53fe?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=368f45b0888aeb0b7b08e3a1084d3ede&auto=format&fit=crop&w=1950&q=80',
    'https://images.unsplash.com/photo-1519125323398-675f0ddb6308?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=94a1e718d89ca60a6337a6008341ca50&auto=format&fit=crop&w=1950&q=80',
    'https://images.unsplash.com/photo-1523205771623-e0faa4d2813d?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=89719a0d55dd05e2deae4120227e6efc&auto=format&fit=crop&w=1953&q=80',
    'https://images.unsplash.com/photo-1508704019882-f9cf40e475b4?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=8c6e5e3aba713b17aa1fe71ab4f0ae5b&auto=format&fit=crop&w=1352&q=80',
    'https://images.unsplash.com/photo-1519985176271-adb1088fa94c?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=a0c8d632e977f94e5d312d9893258f59&auto=format&fit=crop&w=1355&q=80',
    'https://images.unsplash.com/photo-1619009642046-c1961c2a73e7?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxleHBsb3JlLWZlZWR8Mzd8fHxlbnwwfHx8fA%3D%3D&auto=format&fit=crop&w=600&q=60',
    // 'https://images.unsplash.com/photo-1619092738159-9801f5f07cbe?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxleHBsb3JlLWZlZWR8Mzh8fHxlbnwwfHx8fA%3D%3D&auto=format&fit=crop&w=600&q=60',
    // 'https://images.unsplash.com/photo-1619105587572-e2c62edf1db2?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxleHBsb3JlLWZlZWR8Mzl8fHxlbnwwfHx8fA%3D%3D&auto=format&fit=crop&w=600&q=60',
    // 'https://images.unsplash.com/photo-1619100244920-b417eff59772?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxleHBsb3JlLWZlZWR8NDB8fHxlbnwwfHx8fA%3D%3D&auto=format&fit=crop&w=600&q=60',
    // 'https://images.unsplash.com/photo-1619098501887-a70a4b628f9f?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxleHBsb3JlLWZlZWR8NDF8fHxlbnwwfHx8fA%3D%3D&auto=format&fit=crop&w=600&q=60'
  ];

  List<String> tagged = [
    'https://images.unsplash.com/photo-1520342868574-5fa3804e551c?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=6ff92caffcdd63681a35134a6770ed3b&auto=format&fit=crop&w=1951&q=80',
    'https://images.unsplash.com/photo-1522205408450-add114ad53fe?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=368f45b0888aeb0b7b08e3a1084d3ede&auto=format&fit=crop&w=1950&q=80',
    'https://images.unsplash.com/photo-1519125323398-675f0ddb6308?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=94a1e718d89ca60a6337a6008341ca50&auto=format&fit=crop&w=1950&q=80',
    'https://images.unsplash.com/photo-1523205771623-e0faa4d2813d?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=89719a0d55dd05e2deae4120227e6efc&auto=format&fit=crop&w=1953&q=80',
    'https://images.unsplash.com/photo-1508704019882-f9cf40e475b4?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=8c6e5e3aba713b17aa1fe71ab4f0ae5b&auto=format&fit=crop&w=1352&q=80',
    'https://images.unsplash.com/photo-1519985176271-adb1088fa94c?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=a0c8d632e977f94e5d312d9893258f59&auto=format&fit=crop&w=1355&q=80'
  ];

  List<bool> captionStatus = [true, true, true, true, true, true, true];

  List<String> caption = [
    "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.",
    "",
    "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.",
    "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.",
    "",
    "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.",
    "",
  ];
  showPicker() {
    showModalBottomSheet(
        context: context,
        backgroundColor: Constants.kitGradients[17],
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.only(
                topLeft: Radius.circular(8), topRight: Radius.circular(8))),
        builder: (BuildContext context) {
          SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.dark.copyWith(
              statusBarColor: Constants.kitGradients[17],
              //or set color with: Color(0xFF0000FF)
              statusBarIconBrightness: Brightness.dark));
          return Container(
            // height:screenHeight(context, dividedBy:4),
            padding: EdgeInsets.symmetric(
              horizontal: screenWidth(context, dividedBy: 30),
              vertical: screenHeight(context, dividedBy: 30),
            ),
            decoration: BoxDecoration(
                color: Constants.kitGradients[17],
                borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(8), topRight: Radius.circular(8))),
            child: Wrap(
              //crossAxisAlignment:CrossAxisAlignment.start,
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.symmetric(
                    horizontal: screenWidth(context, dividedBy: 30),
                    //vertical: screenHeight(context, dividedBy: 20),
                  ),
                  child: Text(
                    "Choose account",
                    style: TextStyle(
                      fontSize: 20,
                      fontWeight: FontWeight.bold,
                      fontFamily: "Montserrat",
                      color: Constants.kitGradients[27],
                    ),
                  ),
                ),
                SizedBox(
                  height: screenHeight(context, dividedBy: 20),
                ),
                BottomSheetItem(
                  title: profileData.firstName,
                  leading: ProfileIcon(
                      profileImage: profileData.profileImage, circleRadius: 8),
                  trailing: Icon(Icons.pets_rounded,
                      color: Constants.kitGradients[27], size: 20),
                  onTap: () {
                    // imgFromGallery();
                    // showToast("msg");
                    Navigator.of(context).pop();
                  },
                ),
                Divider(
                  color: Constants.kitGradients[27].withOpacity(0.2),
                  indent: 10,
                  endIndent: 10,
                ),
                BottomSheetItem(
                  title: "Kines Bi",
                  leading: ProfileIcon(
                      profileImage: profileData.profileImage, circleRadius: 8),
                  trailing: null,
                  onTap: () {
                    // imgFromCamera();
                    Navigator.of(context).pop();
                  },
                ),
                Divider(
                  color: Constants.kitGradients[27].withOpacity(0.2),
                  indent: 10,
                  endIndent: 10,
                ),
                BottomSheetItem(
                  title: "Add a paw",
                  leading: Icon(Icons.add, color: Constants.kitGradients[27]),
                  trailing: null,
                  onTap: () {
                    //imgFromCamera();
                    Navigator.of(context).pop();
                  },
                ),
              ],
            ),
          );
        });
  }

  showSettings() {
    showModalBottomSheet(
        context: context,
        //transitionAnimationController: controller,
        // isScrollControlled: earnedPoint.length > 6 ? true : false,
        backgroundColor: Constants.kitGradients[17],
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.only(
                topLeft: Radius.circular(8), topRight: Radius.circular(8))),
        builder: (BuildContext context) {
          return Container(
            width: screenWidth(context, dividedBy: 1),
            //height:screenHeight(context, dividedBy:1.5),
            //height: MediaQuery.of(context).copyWith().size.height * 0.75,
            padding: EdgeInsets.symmetric(
              horizontal: screenWidth(context, dividedBy: 20),
              vertical: screenHeight(context, dividedBy: 20),
            ),
            decoration: BoxDecoration(
                color: Constants.kitGradients[17],
                borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(8), topRight: Radius.circular(8))),
            child: Wrap(children: [
              SizedBox(
                height: screenHeight(context, dividedBy: 30),
              ),
              BottomSheetItem(
                title: "Edit Profile",
                leading: Icon(
                  Icons.edit,
                  color: Constants.kitGradients[27],
                ),
                trailing: null,
                onTap: () {
                  //imgFromCamera();
                  push(context, EditProfilePage(
                    profileImage: profileData.profileImage,
                    profileName: profileData.firstName,
                    description: profileData.description,
                    userName: profileData.username,

                  ));
                  // Navigator.of(context).pop();
                },
              ),
              Divider(
                color: Constants.kitGradients[27].withOpacity(0.2),
                indent: 10,
                endIndent: 10,
              ),
              BottomSheetItem(
                title: "Log Out",
                leading: Icon(
                  Icons.logout,
                  color: Constants.kitGradients[27],
                ),
                trailing: null,
                onTap: () {
                  userBloc.userLogout();
                  Navigator.of(context).pop();
                },
              ),
            ]),
          );
        });
  }

  String profileImage;
  String bio;
  @override
  void initState() {
    //addLocation();
    userBloc.logout.listen((event) async {
      if (event.status == 200 || event.status == 201) {
        setState(() {
          ObjectFactory().appHive.deleteToken();
          ObjectFactory().appHive.deleteEmail();
          ObjectFactory().appHive.deleteImage();
          ObjectFactory().appHive.deleteName();
          showSuccessAnimatedToast(
              context: context, msg: "Signed out Successfully");
          pushAndRemoveUntil(context, LoginPage(), false);
          print("Signed out Successfully");
        });
      } else {
        showErrorAnimatedToast(msg: "Something went wrong!", context: context);
      }
    });
    userBloc.getUserProfile(id: ObjectFactory().appHive.getUserId());
    userBloc.getUserProfileResponse.listen((event) {
      print("Profile data loaded Successfully");
      print("Profile description" + event.description.toString());
      setState(() {
        profileData = event;
        loading = false;
      });
    }).onError((event) {
      setState(() {
        // loading = false;
      });
      showErrorAnimatedToast(context: context, msg: event);
    });
    userBloc.getPostResponse.listen((event) async {
      print("posts loadedd from profile page");
      // print("result"+);
      setState(() {
        postData = event;
        loading = false;
      });
    });
    userBloc.getPost(filter: "?user=${ObjectFactory().appHive.getUserId()}");
    super.initState();
  }

  String accountName;
  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: (){
        Navigator.of(context).pushAndRemoveUntil(MaterialPageRoute(builder: (context) =>
            HomePage()), (Route<dynamic> route) => false);
      },
      child: Scaffold(
        backgroundColor: Constants.kitGradients[17],
        appBar: AppBar(
            backgroundColor: Constants.kitGradients[17],
            elevation: 0,
            leading: Container(),
            actions: [
              AppBarDogApp(
                  leftIcon: Icon(
                    Icons.lock_outline_rounded,
                    color: Constants.kitGradients[27],
                  ),
                  onTapLeftIcon: () {},
                  rightIcon: Icon(
                    Icons.settings_sharp,
                    color: Constants.kitGradients[27],
                  ),
                  onTapRightIcon: () {
                    showSettings();
                  },
                  title:
                  // GestureDetector(
                  //     onTap: () {
                  //       showPicker();
                  //     },
                  //     child:
          Row(
                        children: [
                          Text(
                            ObjectFactory().appHive.getName() == ""
                                ? "username"
                                : ObjectFactory().appHive.getName(),
                            style: TextStyle(
                              fontSize: 14,
                              fontWeight: FontWeight.w900,
                              fontFamily: 'Montserrat',
                              color: Constants.kitGradients[24],
                            ),
                          ),
                          // Icon(
                          //   Icons.arrow_drop_down,
                          //   color: Constants.kitGradients[27],
                          // )
                        ],
                      ))
                  // SwitchAccountDropdown(
                  //   title: widget.name,
                  //   dropDownList: [
                  //     widget.name,
                  //     "Bi Kii",
                  //     "Viness",
                  //   ],
                  //   dropDownValue: accountName,
                  //   onClicked: (value) {
                  //     setState(() {
                  //       accountName = value;
                  //     });
                  //   },
                  // ),
                  // ),
            ]),
        body: Stack(
          children: [
            Container(
              height: screenHeight(context, dividedBy: 1),
              width: screenWidth(context, dividedBy: 1),
              padding: EdgeInsets.symmetric(
                horizontal: screenWidth(context, dividedBy: 30),
              ),
              child: loading == true
                  ? Container(
                      width: screenWidth(context, dividedBy: 1),
                      height: screenWidth(context, dividedBy: 1),
                      color: Constants.kitGradients[17],
                      child: Center(
                        heightFactor: 1,
                        widthFactor: 1,
                        child: SizedBox(
                          height: 16,
                          width: 16,
                          child: CircularProgressIndicator(
                            valueColor: AlwaysStoppedAnimation(
                                Constants.kitGradients[19]),
                            strokeWidth: 2,
                          ),
                        ),
                      ),
                    )
                  : SingleChildScrollView(
                      child: Column(
                        //crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          SizedBox(height: screenHeight(context, dividedBy: 40)),
                          Center(
                            child: GestureDetector(
                              onTap: () async{
                               await showDialog(
                                    context: context,
                                    builder: (_) =>
                            FullScreenImage(
                                   imageUrl:
                                        profileData.profileImage ,)
                                  // tag: "profile_picture",
                                  );
                              },
                              child: Hero(
                                tag: "profile_picture",
                                child: Container(
                                    height: screenWidth(context, dividedBy: 3.7),
                                    width: screenWidth(context, dividedBy: 3.7),
                                    decoration: BoxDecoration(
                                      shape: BoxShape.circle,
                                      color: Constants.kitGradients[17].withOpacity(0.5),
                                    ),
                                    child: CachedNetworkImage(
                                      // height: screenWidth(context, dividedBy: 8),
                                      // width: screenWidth(context, dividedBy: 8),
                                      imageUrl: profileData.profileImage,
                                      fit: BoxFit.cover,
                                      imageBuilder: (context, imageProvider) {
                                        return Container(
                                          height:
                                              screenWidth(context, dividedBy: 3.7),
                                          width: screenWidth(context, dividedBy: 3.7),
                                          decoration: BoxDecoration(
                                              shape: BoxShape.circle,
                                              image: DecorationImage(
                                                  image: imageProvider,
                                                  fit: BoxFit.cover)),
                                        );
                                      },
                                      placeholder: (context, url) => Center(
                                        heightFactor: 1,
                                        widthFactor: 1,
                                        child: SizedBox(
                                          height: 16,
                                          width: 16,
                                          child: CircularProgressIndicator(
                                            valueColor: AlwaysStoppedAnimation(
                                                Constants.kitGradients[27]),
                                            strokeWidth: 2,
                                          ),
                                        ),
                                      ),
                                    )),
                              ),
                            ),
                          ),
                          SizedBox(height: screenHeight(context, dividedBy: 70)),
                          Text(
                            // ObjectFactory().appHive.getName(),
                            ObjectFactory().appHive.getName(),
                            textAlign: TextAlign.center,
                            style: TextStyle(
                              fontSize: 16.5,
                              fontWeight: FontWeight.bold,
                              fontFamily: 'Prompt-Light',
                              color: Constants.kitGradients[27],
                            ),
                          ),
                          Text(
                             profileData.description,
                            textAlign: TextAlign.justify,
                            style: TextStyle(
                              fontSize: 14,
                              fontWeight: FontWeight.w200,
                              fontFamily: 'Prompt-Light',
                              color: Constants.kitGradients[27],
                            ),
                          ),
                          SizedBox(height: screenHeight(context, dividedBy: 50)),

                          ProfileTabBar(
                            postResponse: postData,
                            hasCaption: captionStatus,
                            captions: caption,
                            userName: ObjectFactory().appHive.getName(),
                            userProfile: profileData.profileImage,
                          ),
                        ],
                      ),
                    ),
            ),
            Positioned(
              bottom: 0,
              child: CustomBottomBar(
                currentIndex: 2,
                onTapSearch: () {
                  push(context, MarketPlace());
                },
                onTapProfile: () {},
                onTapHome: () {
                  push(
                      context,
                      HomePage(
                          // name: "Bi",
                          // profilePic:
                          //     "https://img.17qq.com/images/fjjhjggbebz.jpeg",
                          ));
                },
                onTapplusRightIcon: (){
                  push(context, NewPostPage(
                    profileImage: ObjectFactory().appHive.getProfilePic(),
                       userName: ObjectFactory().appHive.getName(),
                      isEdit: false,
                  ));
                },
              ),
            )
          ],
        ),
      ),
    );
  }
}
// SwitchAccountDropdown(
// title: widget.name,
// dropDownList: ["Bi Kii", "Viness"],
// dropDownValue: accountName,
// onClicked: (value) {
// setState(() {
// accountName = value;
// });
// },
// )