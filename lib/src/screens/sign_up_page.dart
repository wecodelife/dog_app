import 'package:app_template/src/bloc/user_bloc.dart';
import 'package:app_template/src/models/username_request_model.dart';
import 'package:app_template/src/screens/enter_password_page.dart';
import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:app_template/src/widgets/onboarding_button.dart';
import 'package:app_template/src/widgets/onboarding_textfield.dart';
import 'package:app_template/src/widgets/user_text_field.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class SignUpPage extends StatefulWidget {
  @override
  _SignUpPageState createState() => _SignUpPageState();
}

class _SignUpPageState extends State<SignUpPage> {
  TextEditingController firstNameTextEditingController =
      new TextEditingController();
  TextEditingController descriptionTextEditingController =
      new TextEditingController();
  TextEditingController userNameTextEditingController =
      new TextEditingController();
  UserBloc userBloc =  UserBloc();
  bool usernameNotAvailable = true;
  detailsCheck() {
    if (firstNameTextEditingController.text != "" &&
        descriptionTextEditingController.text != "" && userNameTextEditingController.text != "") {
      // final bool isValid =
      //     EmailValidator.validate(mobileTextEditingController.text);
      // print(isValid);
      // if (isValid == true) {

      push(
          context,
          EnterPasswordPage(
            firstName: firstNameTextEditingController.text,
            description : descriptionTextEditingController.text,
            userName:userNameTextEditingController.text,
          ));
    } else {
      showToast("Please fill al the details");
    }
  }

  @override
  void initState() {
    userBloc.userNameResponse.listen((event) {
      if (event.status == 200 || event.status == 201) {
        setState(() {
          print("Username Response successful");
          print(event.message);
          print("userexists    "+event.userExists.toString());
          usernameNotAvailable = event.userExists;
        });
      }
    }).onError((event) {
      setState(() {
        print("Username Response error");
        // loading = false;
      });
      showErrorAnimatedToast(context: context, msg: event);
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return AnnotatedRegion<SystemUiOverlayStyle>(
      value: SystemUiOverlayStyle(
        statusBarColor: Colors.transparent,
      ),
      child: SafeArea(
        child: Scaffold(
          backgroundColor: Constants.kitGradients[17],
          //resizeToAvoidBottomPadding: false,
          body: Container(
            height: screenHeight(context, dividedBy: 1),
            width: screenWidth(context, dividedBy: 1),
            decoration: BoxDecoration(
              color: Constants.kitGradients[17],
              image: DecorationImage(
                fit: BoxFit.cover,
                image: AssetImage(Constants.BACKGROUND_IMAGE),
                colorFilter: new ColorFilter.mode(
                    Colors.black.withOpacity(0.6), BlendMode.darken),
              ),
            ),
            child: SingleChildScrollView(
              child: Container(
                width: screenWidth(context, dividedBy: 1),
                padding: EdgeInsets.symmetric(
                  horizontal: screenWidth(context, dividedBy: 20),
                  vertical: screenHeight(context, dividedBy: 6),
                ),
                child: Center(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      SizedBox(
                        height: screenHeight(context, dividedBy: 15),
                      ),
                      // SizedBox(
                      //   height: screenHeight(context, dividedBy: 10),
                      // ),
                      Text(
                        "Find Your Perfect Puppy,            \nWith Us !          ",
                        //textAlign: TextAlign.left,
                        style: TextStyle(
                            color: Constants.kitGradients[27],
                            fontFamily: "Montserrat-ExtraBold",
                            fontSize: 24,
                            fontWeight: FontWeight.bold),
                      ),
                      SizedBox(
                        height: screenHeight(context, dividedBy: 90),
                      ),
                      Text(
                        "Join us and Discover the world of puppies ..                                          We are here to find your dream Pet            ",
                        style: TextStyle(
                            color: Constants.kitGradients[27],
                            fontFamily: "Montserrat",
                            fontSize: 16,
                            fontWeight: FontWeight.w400),
                      ),
                      SizedBox(
                        height: screenHeight(context, dividedBy: 30),
                      ),
                      OnBoardingTextField(
                        labelText: "FirstName",
                        textEditingController: firstNameTextEditingController,
                        hasSuffix: false,
                        suffixIcon: Container(),
                        onTapIcon: () {},
                        textArea: false,
                        onTap: () {},
                        readOnly: false,
                          userExists: true,
                          isNumber: false
                      ),
                      // FormFeildUserDetails(
                      //   labelText: "FirstName",
                      //   textEditingController:
                      //       firstNameTextEditingController,
                      //   isPassword: false,
                      //   isnumber: false,
                      //   autoFocus: false,
                      // ),
                      SizedBox(
                        height: screenHeight(context, dividedBy: 20),
                      ),
                      OnBoardingTextField(
                        labelText: "UserName",
                        textEditingController: userNameTextEditingController,
                        hasSuffix: usernameNotAvailable == true  ? false : true ,
                        suffixIcon: usernameNotAvailable == true  ? Container() : Icon(Icons.check_circle_rounded, color: Constants.kitGradients[27],),
                        onTapIcon: () {},
                        textArea: false,
                        onTap: () {},
                        readOnly: false,
                        isNumber: false,
                        userExists: usernameNotAvailable,
                        onChanged: (val) {
                          print("username checking");
                          if (val.length > 3)
                            userBloc.userCheck(
                                userNameRequest: UserNameRequest(
                                    username: userNameTextEditingController
                                        .text));
                          if (val.length < 4 || val.length == 0)
                            setState(() {
                              usernameNotAvailable = true;
                            });
                          // if(userNameTextEditingController.text == "")
                          //   setState(() {
                          //     usernameNotAvailable = true;
                          //   });
                        },
                      ),

                      // FormFeildUserDetails(
                      //   labelText: "LastName",
                      //   textEditingController:
                      //       lastNameTextEditingController,
                      //   isPassword: false,
                      //   isnumber: false,
                      //   autoFocus: false,
                      // ),
                      SizedBox(
                        height: screenHeight(context, dividedBy: 20),
                      ),
                      OnBoardingTextField(
                        labelText: "Description",
                        textEditingController: descriptionTextEditingController,
                        hasSuffix: false,
                        suffixIcon: Container(),
                        onTapIcon: () {},
                        textArea: true,
                        onTap: () {},
                        readOnly: false,
                        isNumber: false,
                        userExists: true,
                      ),

                      // FormFeildUserDetails(
                      //   labelText: "UserName",
                      //   textEditingController:
                      //       userNameTextEditingController,
                      //   isPassword: false,
                      //   isnumber: false,
                      //   autoFocus: false,
                      // ),
                      SizedBox(
                        height: screenHeight(context, dividedBy: 20),
                      ),
                      SizedBox(
                        height: screenHeight(context, dividedBy: 50),
                      ),
                      Row(mainAxisAlignment: MainAxisAlignment.end, children: [
                        OnBoardingButton(
                          title: "Next",
                          onPressed: () {
                            detailsCheck();
                          },
                          isLoading: false,
                          width: 3,
                        ),
                        SizedBox(width: screenWidth(context, dividedBy: 20)),
                      ]),
                      // BuildButton(
                      //   title: "Next",
                      //   onPressed: () {
                      //     print(lastNameTextEditingController.text);
                      //     detailsCheck();
                      //   },
                      // ),
                    ],
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
