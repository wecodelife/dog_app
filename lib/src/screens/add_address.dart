import 'package:flutter/material.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:app_template/src/utils/constants.dart';
import 'package:flutter/services.dart';
import 'package:app_template/src/widgets/user_text_field.dart';
import 'package:app_template/src/widgets/select_button.dart';
import 'package:app_template/src/widgets/build_button.dart';
import 'package:app_template/src/screens/place_order_page.dart';


class AddAddress extends StatefulWidget {
  // const AddAdress({Key? key}) : super(key: key);

  @override
  _AddAddressState createState() => _AddAddressState();
}

class _AddAddressState extends State<AddAddress> {
  bool checkedValue= true;
  bool newValue= false;
  TextEditingController nameTextEditingController =
  new TextEditingController();
  TextEditingController numberTextEditingController =
  new TextEditingController();
  TextEditingController pinTextEditingController =
  new TextEditingController();
  TextEditingController addressTextEditingController =
  new TextEditingController();
  TextEditingController localityTextEditingController =
  new TextEditingController();
  TextEditingController cityTextEditingController =
  new TextEditingController();
  TextEditingController stateTextEditingController =
  new TextEditingController();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Constants.kitGradients[17],
      appBar: AppBar(
        //elevation: 0,
        backgroundColor: Constants.kitGradients[17],
        leading: GestureDetector(
          onTap: () {
            pop(context);
          },
          child: Icon(
            Icons.arrow_back,
            color: Constants.kitGradients[27],
          ),
        ),
        title: Container(
          child: Row(
            children: [
              Text(
                "Add New Address",
                style: TextStyle(
                  //fontSize: 16,
                  fontWeight: FontWeight.w700,
                  color: Constants.kitGradients[27],
                ),
              ),
            ],
          ),
        ),
      ),
      body:Stack(
          children:[
      SingleChildScrollView(
          child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
            SizedBox(
              height: screenHeight(context, dividedBy: 70),
            ),
            Container(
              padding: EdgeInsets.symmetric(
                horizontal: screenWidth(context, dividedBy: 30),
                vertical: screenHeight(context, dividedBy: 60),
              ),
              color: Constants.kitGradients[19].withOpacity(0.12),
              width: screenWidth(context, dividedBy: 1),
              height: screenHeight(context, dividedBy: 4.6),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text("Contact Info",
                    style: TextStyle(
                      fontSize: 16,
                      fontFamily: 'Prompt-Light',
                      //fontWeight: FontWeight.w700,
                      color: Constants.kitGradients[27],
                    ),),
                  SizedBox(
                    height: screenHeight(context, dividedBy: 70),
                  ),
                  FormFeildUserDetails(
                    labelText: "Name",
                    textEditingController:
                    nameTextEditingController,
                    // isPassword: false,
                    // isnumber: false,
                    autoFocus: false,
                    textArea: false,
                    userExists: true,
                  ),
                  SizedBox(
                    height: screenHeight(context, dividedBy: 60),
                  ),
                  FormFeildUserDetails(
                    labelText: "Phone Number",
                    textEditingController:
                    nameTextEditingController,
                    // isPassword: false,
                    // isnumber: false,
                    autoFocus: false,
                    textArea: false,
                    userExists: true,
                  ),
              ],),
            ),
                SizedBox(
                  height: screenHeight(context, dividedBy: 70),
                ),
                Container(
                  padding: EdgeInsets.symmetric(
                    horizontal: screenWidth(context, dividedBy: 30),
                    vertical: screenHeight(context, dividedBy: 60),
                  ),
                  color: Constants.kitGradients[19].withOpacity(0.12),
                  width: screenWidth(context, dividedBy: 1),
                  height: screenHeight(context, dividedBy: 2.9),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text("Address Info",
                        style: TextStyle(
                          fontSize: 16,
                          fontFamily: 'Prompt-Light',
                          //fontWeight: FontWeight.w700,
                          color: Constants.kitGradients[27],
                        ),),
                      SizedBox(
                        height: screenHeight(context, dividedBy: 70),
                      ),
                      FormFeildUserDetails(
                        labelText: "Pin Code",
                        textEditingController:
                        nameTextEditingController,
                        // isPassword: false,
                        // isnumber: false,
                        autoFocus: false,
                        textArea: false,
                        userExists: true,
                      ),
                      SizedBox(
                        height: screenHeight(context, dividedBy: 60),
                      ),
                      FormFeildUserDetails(
                        labelText: "Address (House No, Building,Street, Area)",
                        textEditingController:
                        numberTextEditingController,
                        // isPassword: false,
                        // isnumber: false,
                        autoFocus: false,
                        textArea: false,
                        userExists: true,
                      ),
                      SizedBox(
                        height: screenHeight(context, dividedBy: 60),
                      ),
                      FormFeildUserDetails(
                        labelText: "Locality/Town",
                        textEditingController:
                        nameTextEditingController,
                        // isPassword: false,
                        // isnumber: false,
                        autoFocus: false,
                        textArea: false,
                        userExists: true,
                      ),
                      SizedBox(
                        height: screenHeight(context, dividedBy: 60),
                      ),
                      // Row(
                      //   mainAxisAlignment: MainAxisAlignment.start,
                      //   crossAxisAlignment: CrossAxisAlignment.start,
                      //   children: [
                      //     FormFeildUserDetails(
                      //       labelText: "City/District",
                      //       textEditingController:
                      //       nameTextEditingController,
                      //       // isPassword: false,
                      //       // isnumber: false,
                      //       autoFocus: false,
                      //       textArea: false,
                      //       userExists: true,
                      //     ),
                      //     // FormFeildUserDetails(
                      //     //   labelText: "Locality/Town",
                      //     //   textEditingController:
                      //     //   nameTextEditingController,
                      //     //   // isPassword: false,
                      //     //   // isnumber: false,
                      //     //   autoFocus: false,
                      //     //   textArea: false,
                      //     //   userExists: true,
                      //     // ),
                      //   ],
                      // )R
                      Row(
                        children: [
                          Container(
                            width: screenWidth(context, dividedBy: 2.2),
                            height: screenHeight(context, dividedBy:18),
                            child: FormFeildUserDetails(
                              labelText: "City/District",
                              textEditingController:
                              nameTextEditingController,
                              // isPassword: false,
                              // isnumber: false,
                              autoFocus: false,
                              textArea: false,
                              userExists: true,
                            ),
                          ),
                          SizedBox(
                            width: screenWidth(context, dividedBy: 28),
                          ),
                          Container(
                            width: screenWidth(context, dividedBy: 2.3),
                            height: screenHeight(context, dividedBy: 18),
                            child: FormFeildUserDetails(
                              labelText: "State",
                              textEditingController:
                              nameTextEditingController,
                              // isPassword: false,
                              // isnumber: false,
                              autoFocus: false,
                              textArea: false,
                              userExists: true,
                            ),
                          ),
                        ],
                      ),
                    ],),

                ),
                SizedBox(
                  height: screenHeight(context, dividedBy: 70),
                ),
                Container(
                  padding: EdgeInsets.symmetric(
                    horizontal: screenWidth(context, dividedBy: 30),
                    vertical: screenHeight(context, dividedBy: 60),
                  ),
                  color: Constants.kitGradients[19].withOpacity(0.12),
                  width: screenWidth(context, dividedBy: 1),
                  height: screenHeight(context, dividedBy: 7.5),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                    Text("Save Address As",
                    style: TextStyle(
                      fontSize: 16,
                      fontFamily: 'Prompt-Light',
                      //fontWeight: FontWeight.w700,
                      color: Constants.kitGradients[27],
                    ),),
                  SizedBox(
                    height: screenHeight(context, dividedBy: 70),
                  ),
                      Row(
                        children: [
                          SelectButton(
                            buttonWidth: 4.9,
                            buttonHeight: 22,
                            isDisabled: true,
                            title: "Home",
                            onPressed: () {
                              //  push(context, PlaceOrderPage(itemImage: widget.itemImage,));
                            },
                          ),
                          SizedBox(
                            width: screenWidth(context, dividedBy: 30),
                          ),
                          SelectButton(
                            buttonWidth: 4.9,
                            buttonHeight: 22,
                            isDisabled: true,
                            title: "Work",
                            onPressed: () {
                              //  push(context, PlaceOrderPage(itemImage: widget.itemImage,));
                            },
                          ),
                        ],
                      ),


                    ])
                ),
                CheckboxListTile(
                  activeColor: Constants.kitGradients[27].withOpacity(0.4) ,
                  checkColor:  Constants.kitGradients[27],
                  title: Text("Make this my default address",
                    style: TextStyle(
                      fontSize: 16,
                      fontFamily: 'Prompt-Light',
                      //fontWeight: FontWeight.w700,
                      color: Constants.kitGradients[27].withOpacity(0.4),
                    ),),
                  value: checkedValue,
                  onChanged: (newValue) {
                    setState(() {
                      checkedValue = newValue;
                    });
                  },
                  controlAffinity: ListTileControlAffinity.leading,  //  <-- leading Checkbox
                )
          ]
          )),
        Align(
            alignment: Alignment.bottomCenter,
            child: Container(
              width: screenWidth(context, dividedBy: 1),
              decoration: BoxDecoration(
                boxShadow: [
                  BoxShadow(
                    color: Constants.kitGradients[17].withOpacity(0.5),
                    offset: Offset(1.0, 2.0), //(x,y)
                    //blurRadius: 6.0,
                  ),
                ],
              ),
              child:  Padding(
                padding: const EdgeInsets.all(18.0),
                child: BuildButton(
                  buttonWidth: 2.6,
                  buttonHeight: 16,
                  isDisabled: true,
                  title: "ADD ADDRESS",
                  onPressed: () {
                   push(context, PlaceOrderPage());
                  },

                ),
              ),
            ))
      ]),
    );
  }
}
