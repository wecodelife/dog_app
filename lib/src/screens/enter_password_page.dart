import 'dart:ui';

import 'package:app_template/src/bloc/user_bloc.dart';
import 'package:app_template/src/models/login_request_model.dart';
import 'package:app_template/src/models/user_update_request_model.dart';
import 'package:app_template/src/screens/have_a_dog_page.dart';
import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/object_factory.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:app_template/src/widgets/onboarding_button.dart';
import 'package:app_template/src/widgets/user_text_field.dart';
import 'package:country_code_picker/country_code_picker.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:mobile_number/mobile_number.dart';
import 'package:sms_autofill/sms_autofill.dart';
import 'package:app_template/src/widgets/onboarding_textfield.dart';

class EnterPasswordPage extends StatefulWidget {
  final String firstName;
  final String description;
  final String userName;
  final UserUpdateRequestModel updateRequestModel;
  EnterPasswordPage({ this.firstName, this.updateRequestModel, this.userName, this.description, });

  @override
  _EnterPasswordPageState createState() => _EnterPasswordPageState();
}

class _EnterPasswordPageState extends State<EnterPasswordPage> {
  String appSignature;
  TextEditingController mobileTextEditingController =
      new TextEditingController();
  UserUpdateRequestModel userUpdateModel;
  String code = "+91";
  bool isLoading = false;
  String _mobileNumber = "";
  List<SimCard> _simCard = <SimCard>[];
  UserBloc userBloc = UserBloc();
  String _verificationId;
  final SmsAutoFill _autoFill = SmsAutoFill();
  bool otpSend = false;
  UserUpdateRequestModel updateRequestModel;
  TextEditingController otpTextEditingController = TextEditingController();
  String _code = "";

  // firebase phone number verifications
  autoFillNumber() async {
    mobileTextEditingController.text = await _autoFill.hint;
  }

  FirebaseAuth _auth = FirebaseAuth.instance;

  String verificationId;

  bool showLoading = false;

  void signInWithPhoneAuthCredential(
      PhoneAuthCredential phoneAuthCredential) async {
    setState(() {
      showLoading = true;
    });

    try {
      final authCredential =
          await _auth.signInWithCredential(phoneAuthCredential);

      setState(() {
        showLoading = false;
      });

      if (authCredential?.user != null) {
        // ObjectFactory().appHive.putName(name: widget.firstName);
        // print("username added  " + ObjectFactory().appHive.getName());
        // await userBloc.login(
        //     loginRequest: LoginRequest(uid: _auth.currentUser.uid));
        // Navigator.push(context, MaterialPageRoute(builder: (context)=> HomeScreen()));
        ObjectFactory().appHive.putName(name: widget.firstName);
        print("username added  " + ObjectFactory().appHive.getName());
        await userBloc.login(
            loginRequest: LoginRequest(uid: _auth.currentUser.uid));
        updateRequestModel = UserUpdateRequestModel(
            firstName: widget.firstName,
            description: widget.description,
            uid: _auth.currentUser.uid,
            username: widget.userName,
            phoneNumber: mobileTextEditingController.text);
      }
    } on FirebaseAuthException catch (e) {
      setState(() {
        showLoading = false;
      });
    }
  }

  void _listenOTP() async {
    await SmsAutoFill().listenForCode;
  }

  @override
  void dispose() {
    SmsAutoFill().unregisterListener();
    super.dispose();
  }

  @override
  void initState() {
    super.initState();
    // autoFillNumber();
    _listenOTP();
    // getNumber();
    // userBloc.loginResponse.listen((event) async {
    //   if (event.status == 200 || event.status == 201)  {
    //     ObjectFactory().appHive.putToken(token: event.data.token);
    //     ObjectFactory().appHive.putUserId(userId: event.data.userData.id.toString());
    //     print("Firebase Registration userid   " + event.data.userData.id.toString());
    //     print("Firebase Registration userid fromHive   " + ObjectFactory().appHive.getUserId());
    //     print("Firebase Registration UserName   "+ ObjectFactory().appHive.getName());
    //     print("Firebase Login Success");
    //    updateRequestModel = UserUpdateRequestModel(
    //         firstName:
    //         widget.firstName,
    //         lastName:
    //         widget.lastName,
    //         phoneNumber:mobileTextEditingController.text );
    //     push(
    //         context,
    //         HaveADogPage(
    //           updateRequestModel:
    //           updateRequestModel,
    //         ));
    //     setState(() {
    //       isLoading = false;
    //     });
    //   } else {
    //     showErrorAnimatedToast(msg: "Something went wrong!", context: context);
    //   }
    // });
    userBloc.loginResponse.listen((event) {
      if (event.status == 200 || event.status == 201) {
        ObjectFactory().appHive.putToken(token: event.data.token);
        ObjectFactory()
            .appHive
            .putUserId(userId: event.data.userData.id.toString());
        ObjectFactory().appHive.putName(name: event.data.userData.username);
        // ObjectFactory().appHive.putUserId(userId: event.id.toString());
        // print("userid added  " + event.id.toString());
        // print("username added  " + event.firstName);
        print("Firebase Login Success");
        push(
            context,
            HaveADogPage(
              updateRequestModel: updateRequestModel,
            ));
        setState(() {
          isLoading = false;
        });
      } else {
        showErrorAnimatedToast(msg: "Something went wrong!", context: context);
      }
    });
  }

  // void getNumber() async {
  //   MobileNumber.listenPhonePermission((isPermissionGranted) async {
  //     if (isPermissionGranted) {
  //       await initMobileNumberState();
  //     } else {}
  //   });
  //   await initMobileNumberState();
  //   mobileTextEditingController.text = _simCard[0].number;
  // }
  //
  // Future<void> initMobileNumberState() async {
  //   if (!await MobileNumber.hasPhonePermission) {
  //     await MobileNumber.requestPhonePermission;
  //     return;
  //   }
  //   String mobileNumber = '';
  //   // Platform messages may fail, so we use a try/catch PlatformException.
  //   try {
  //     mobileNumber = await MobileNumber.mobileNumber;
  //     _simCard = await MobileNumber.getSimCards;
  //   } on PlatformException catch (e) {
  //     debugPrint("Failed to get mobile number because of '${e.message}'");
  //   }
  //
  //   // If the widget was removed from the tree while the asynchronous platform
  //   // message was in flight, we want to discard the reply rather than calling
  //   // setState to update our non-existent appearance.
  //   if (!mounted) return;
  //   initMobileNumberState();
  // }

  // final AuthCredential authCredential = AuthCredential();
  void detailsCheck() {
    final FirebaseAuth auth = FirebaseAuth.instance;
    if (mobileTextEditingController.text != "") {
      print(code);
      print(mobileTextEditingController.text);
      setState(() {
        isLoading = true;
      });
      userUpdateModel = UserUpdateRequestModel(
          firstName: widget.firstName, lastName: widget.description);
      User user = FirebaseAuth.instance.currentUser;
      FirebaseAuth.instance.verifyPhoneNumber(
        phoneNumber: mobileTextEditingController.text,
        timeout: Duration(seconds: 60),
        verificationCompleted: (PhoneAuthCredential authCredential) {
          print("verified successfully");
          FirebaseAuth.instance
              .signInWithCredential(authCredential)
              .then((value) async {
            showSuccessAnimatedToast(
                msg:
                    "Phone number automatically verified and user signed in: ${value.user.uid}",
                context: context);
            print(
                "Phone number automatically verified and user signed in: ${value.user.uid}");
            await userBloc.login(
                loginRequest: LoginRequest(uid: auth.currentUser.uid));
            // emit(PhoneVerified(phone: "+91" + phoneNumber));
            print("firebase token" + authCredential.token.toString());
          });
        },
        verificationFailed: (FirebaseAuthException authException) {
          print("error not verified");
          setState(() {
            isLoading = false;
          });
          print(authException.toString());
        },
        codeAutoRetrievalTimeout: (value) {
          print("error code not found");
          print("code auto retrieval timeout  :  " + value);
          setState(() {
            isLoading = false;
          });
        },
        codeSent: (value, [data]) async {
          print(value + "ftuu");
          print([data]);
          setState(() {
            isLoading = false;
            otpSend = true;
          });
          // push(
          //     context,
          //     OTP_Page(
          //       newUser: true,
          //       mobileNumber: mobileTextEditingController.text,
          //       vId: "sdf",
          //       updateRequestModel: userUpdateModel,
          //     ));
          // print("code sent" + auth.currentUser.uid);
          // isLoading = true;
          showSuccessAnimatedToast(
              msg: "OTP generated sucessfully", context: context);
        },
      );
    } else {
      showToast("Please enter the mobile number");
    }
  }

  // UserBloc userBloc = new UserBloc();
  // bool isLoading = false;
  // passwordCheck() {
  //   if (passwordTextEditingController.text != null &&
  //       newPasswordTextEditingController != null) {
  //     if (passwordTextEditingController.text ==
  //         newPasswordTextEditingController.text) {
  //       userBloc.userSignUp(
  //           userSignUpRequest: UserSignUpRequest(
  //               email: widget.emailAddress,
  //               firstName: widget.firstName,
  //               isVerified: true,
  //               lastName: widget.lastName,
  //               password: passwordTextEditingController.text));
  //     } else {
  //       showToast("Passwords does not Match");
  //     }
  //   } else {
  //     showToast("Please fill all the details");
  //   }
  // }

  void _onCountryChange(CountryCode countryCode) {
    //TODO : manipulate the selected country code here
    print("New Country selected: " + countryCode.toString());
    code = countryCode.toString();
  }

  @override
  Widget build(BuildContext context) {
    return AnnotatedRegion<SystemUiOverlayStyle>(
        value: SystemUiOverlayStyle(
          statusBarColor: Colors.transparent,
        ),
        child: SafeArea(
          child: Scaffold(
            // resizeToAvoidBottomPadding: false,
            backgroundColor: Constants.kitGradients[17],
            body: otpSend == false
                ? Container(
                    height: screenHeight(context, dividedBy: 1),
                    width: screenWidth(context, dividedBy: 1),
                    decoration: BoxDecoration(
                      color: Constants.kitGradients[17],
                      image: DecorationImage(
                        fit: BoxFit.cover,
                        image: AssetImage(Constants.BACKGROUND_IMAGE),
                        colorFilter: new ColorFilter.mode(
                            Colors.black.withOpacity(0.6), BlendMode.darken),
                      ),
                    ),
                    child: ListView(
                      children: [
                        Container(
                            padding: EdgeInsets.symmetric(
                                horizontal:
                                    screenWidth(context, dividedBy: 20)),
                            child: Center(
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  SizedBox(
                                    height:
                                        screenHeight(context, dividedBy: 3.3),
                                  ),
                                  Text(
                                    "Find Your Perfect Puppy,            \nWith Us !          ",
                                    //textAlign: TextAlign.left,
                                    style: TextStyle(
                                        color: Constants.kitGradients[27],
                                        fontFamily: "Montserrat-ExtraBold",
                                        fontSize: 24,
                                        fontWeight: FontWeight.bold),
                                  ),
                                  SizedBox(
                                    height:
                                        screenHeight(context, dividedBy: 90),
                                  ),
                                  Text(
                                    "Join us and Discover the world of puppies ..                                          We are here to find your dream Pet            ",
                                    style: TextStyle(
                                        color: Constants.kitGradients[27],
                                        fontFamily: "Montserrat",
                                        fontSize: 16,
                                        fontWeight: FontWeight.w400),
                                  ),
                                  SizedBox(
                                    height:
                                        screenHeight(context, dividedBy: 30),
                                  ),

                                  Text(
                                    "Enter your Mobile Number                 ",
                                    textAlign: TextAlign.left,
                                    style: TextStyle(
                                        color: Constants.kitGradients[27],
                                        fontFamily: "Montserrat",
                                        fontSize: 16,
                                        fontWeight: FontWeight.w400),
                                  ),
                                  SizedBox(
                                    height:
                                        screenHeight(context, dividedBy: 40),
                                  ),
                                  Row(
                                    children: [
                                      Container(
                                        padding: EdgeInsets.all(1),
                                        height: screenHeight(context,
                                            dividedBy: 16),
                                        decoration: BoxDecoration(
                                            borderRadius:
                                                BorderRadius.circular(20),
                                            border: Border.all(
                                                color:
                                                    Constants.kitGradients[27],
                                                width: 0.0)),
                                        child: Center(
                                          child: CountryCodePicker(
                                            backgroundColor:
                                                Constants.kitGradients[17],
                                            showFlag: false,
                                            initialSelection: "+91",
                                            onChanged: _onCountryChange,
                                            textStyle: TextStyle(
                                              color: Constants.kitGradients[27],
                                              fontFamily: 'Montserrat',
                                              fontSize: 16,
                                            ),
                                          ),
                                        ),
                                      ),
                                      SizedBox(
                                        width: screenHeight(context,
                                            dividedBy: 100),
                                      ),
                                      GestureDetector(
                                        onTap: () {},
                                        child: Container(
                                          width: screenWidth(context,
                                              dividedBy: 1.6),
                                          child: OnBoardingTextField(
                                              labelText: "Mobile Number",
                                              textEditingController: mobileTextEditingController,
                                              hasSuffix: false,
                                              suffixIcon: Container(),
                                              onTapIcon: () {},
                                              textArea: false,
                                              onTap: () {},
                                              readOnly: false,
                                              userExists: true,
                                              isNumber: true
                                          ),
                                          // FormFeildUserDetails(
                                          //   labelText: "Mobile Number",
                                          //   textEditingController:
                                          //       mobileTextEditingController,
                                          //   isPassword: false,
                                          //   isnumber: true,
                                          //   autoFocus: false,
                                          // ),
                                        ),
                                      ),
                                    ],
                                  ),
                                  SizedBox(
                                    height:
                                        screenHeight(context, dividedBy: 20),
                                  ),
                                  Row(
                                      mainAxisAlignment: MainAxisAlignment.end,
                                      children: [
                                        OnBoardingButton(
                                          title: "Next",
                                          onPressed: () async {
                                            setState(() {
                                              showLoading = true;
                                            });

                                            await _auth.verifyPhoneNumber(
                                              phoneNumber: code +
                                                  mobileTextEditingController
                                                      .text,
                                              verificationCompleted:
                                                  (phoneAuthCredential) async {
                                                setState(() {
                                                  showLoading = false;
                                                });
                                                //signInWithPhoneAuthCredential(phoneAuthCredential);
                                              },
                                              verificationFailed:
                                                  (verificationFailed) async {
                                                setState(() {
                                                  showLoading = false;
                                                });
                                                print(
                                                    verificationFailed.message);
                                                showToast(
                                                    "verification failed " +
                                                        verificationFailed
                                                            .message);
                                              },
                                              codeSent: (verificationId,
                                                  resendingToken) async {
                                                setState(() {
                                                  showLoading = false;
                                                  otpSend = true;
                                                  this.verificationId =
                                                      verificationId;
                                                });
                                              },
                                              codeAutoRetrievalTimeout:
                                                  (verificationId) async {},
                                            );

                                            // detailsCheck();
                                            // verifyPhoneNumber();
                                            // userUpdateModel = UserUpdateRequestModel(
                                            //     firstName: widget.firstName,
                                            //     lastName: widget.lastName,
                                            //     uid: "bOSPE5aIyRMNQ1BeCLkr1ajsKch2");
                                            // push(
                                            //     context,
                                            //     OTP_Page(
                                            //       newUser: true,
                                            //       mobileNumber:
                                            //           mobileTextEditingController.text,
                                            //       vId: "sdf",
                                            //       updateRequestModel: userUpdateModel,
                                            //     ));
                                            // userBloc.userUpdate(
                                            //   userUpdateRequest: UserUpdateRequestModel()
                                            // )
                                          },
                                          isLoading: showLoading,
                                          width: 3,
                                        ),
                                        SizedBox(
                                            width: screenWidth(context,
                                                dividedBy: 20)),
                                      ]),
                                  // BuildButton(
                                  //   onPressed: () {
                                  //     verifyOtp(OTPTextEditingController.text,vId, mobileTextEditingController.text);
                                  //   },
                                  //   title: "Submit",
                                  // ),
                                ],
                              ),
                            )),
                      ],
                    ),
                  )
                // : OTP_Page(
                //     newUser: true,
                //     mobileNumber: mobileTextEditingController.text,
                //     vId: "sdf",
                //     updateRequestModel: userUpdateModel,
                //     onTapVerification: () async {
                //       updateRequestModel = UserUpdateRequestModel(
                //           firstName: widget.updateRequestModel.firstName,
                //           lastName: widget.updateRequestModel.lastName,
                //           phoneNumber: mobileTextEditingController.text);
                //       push(
                //           context,
                //           HaveADogPage(
                //             updateRequestModel: updateRequestModel,
                //           ));
                //     },
                //   ),
                : Container(
                    height: screenHeight(context, dividedBy: 1),
                    width: screenWidth(context, dividedBy: 1),
                    decoration: BoxDecoration(
                      image: DecorationImage(
                        fit: BoxFit.cover,
                        image: AssetImage(Constants.BACKGROUND_IMAGE),
                        colorFilter: new ColorFilter.mode(
                            Colors.black.withOpacity(0.6), BlendMode.darken),
                      ),
                    ),
                    child: ListView(
                      children: [
                        Container(
                            padding: EdgeInsets.symmetric(
                                horizontal:
                                    screenWidth(context, dividedBy: 20)),
                            child: Center(
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  SizedBox(
                                    height:
                                        screenHeight(context, dividedBy: 3.3),
                                  ),

                                  // FormFeildUserDetails(
                                  //   labelText: "Enter OTP",
                                  //   textEditingController:
                                  //       OTPTextEditingController,
                                  //   isPassword: false,
                                  //   isnumber: true,
                                  // ),
                                  Text(
                                    "Find Your Perfect Puppy,            \nWith Us !          ",
                                    //textAlign: TextAlign.left,
                                    style: TextStyle(
                                        color: Constants.kitGradients[27],
                                        fontFamily: "Montserrat-ExtraBold",
                                        fontSize: 24,
                                        fontWeight: FontWeight.bold),
                                  ),
                                  SizedBox(
                                    height:
                                        screenHeight(context, dividedBy: 90),
                                  ),
                                  Text(
                                    "Join us and Discover the world of puppies ..                                          We are here to find your dream Pet            ",
                                    style: TextStyle(
                                        color: Constants.kitGradients[27],
                                        fontFamily: "Montserrat",
                                        fontSize: 16,
                                        fontWeight: FontWeight.w400),
                                  ),
                                  SizedBox(
                                    height:
                                        screenHeight(context, dividedBy: 30),
                                  ),

                                  Text(
                                    "Please Enter the OTP send to your Number",
                                    textAlign: TextAlign.left,
                                    style: TextStyle(
                                        color: Constants.kitGradients[27],
                                        fontFamily: "Montserrat",
                                        fontSize: 16,
                                        fontWeight: FontWeight.w400),
                                  ),
                                  SizedBox(
                                    height:
                                        screenHeight(context, dividedBy: 40),
                                  ),
                                  Container(
                                    padding:
                                        EdgeInsets.symmetric(horizontal: 10),
                                    width: screenWidth(context, dividedBy: 1),
                                    child: PinFieldAutoFill(
                                      controller: otpTextEditingController,
                                      decoration: UnderlineDecoration(
                                        textStyle: TextStyle(
                                          color: Constants.kitGradients[27],
                                          fontFamily: 'Montserrat',
                                          fontSize: 16,
                                        ),
                                        colorBuilder: FixedColorBuilder(
                                            Colors.grey.withOpacity(0.5)),
                                      ),
                                      currentCode:
                                          otpTextEditingController.text,
                                      onCodeSubmitted: (code) {},
                                      onCodeChanged: (code) {
                                        if (code.length == 6) {
                                          // FocusScope.of(context).requestFocus(FocusNode());
                                        }
                                      },
                                    ),
                                  ),
                                  SizedBox(
                                    height:
                                        screenHeight(context, dividedBy: 20),
                                  ),
                                  Row(
                                      mainAxisAlignment: MainAxisAlignment.end,
                                      children: [
                                        OnBoardingButton(
                                          title: "Next",
                                          // widget.newUser ? "Next" : "Login",
                                          onPressed: () async {
                                            PhoneAuthCredential
                                                phoneAuthCredential =
                                                PhoneAuthProvider.credential(
                                                    verificationId:
                                                        verificationId,
                                                    smsCode:
                                                        otpTextEditingController
                                                            .text);

                                            signInWithPhoneAuthCredential(
                                                phoneAuthCredential);
                                          },
                                          isLoading: showLoading,
                                          width: 3,
                                        ),
                                        SizedBox(
                                            width: screenWidth(context,
                                                dividedBy: 20)),
                                      ]),
                                  // BuildButton(
                                  //   onPressed: () {
                                  //     verifyOtp(OTPTextEditingController.text,vId, mobileTextEditingController.text);
                                  //   },
                                  //   title: "Submit",
                                  // ),
                                ],
                              ),
                            )),
                      ],
                    ),
                  ),
          ),
        ));
  }
}
