import 'package:flutter/material.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:app_template/src/widgets/user_text_field.dart';
import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/screens/new_password_page.dart';
import 'package:flutter/services.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:app_template/src/widgets/build_button.dart';
import 'package:app_template/src/widgets/app_bar.dart';

class ResetPasswordPage extends StatefulWidget {
  @override
  _ResetPasswordPageState createState() => _ResetPasswordPageState();
}

class _ResetPasswordPageState extends State<ResetPasswordPage> {
  @override
  Widget build(BuildContext context) {
    return AnnotatedRegion<SystemUiOverlayStyle>(
      value: SystemUiOverlayStyle(
        statusBarColor: Colors.transparent,
      ),
      child: Scaffold(
        backgroundColor: Constants.kitGradients[17],
        body: Container(
          height: screenHeight(context, dividedBy: 1),
          width: screenWidth(context, dividedBy: 1),
          decoration: BoxDecoration(
            image: DecorationImage(
              fit: BoxFit.cover,
              image: AssetImage(Constants.BACKGROUND_IMAGE),
              colorFilter: new ColorFilter.mode(
                  Colors.black.withOpacity(0.6), BlendMode.darken),
            ),
          ),
          child: Stack(
            children: [
              Positioned(
                top: screenHeight(context, dividedBy: 40),
                child: Container(
                  height: screenHeight(context, dividedBy: 10),
                  width: screenWidth(context, dividedBy: 1),

                  child: AppBarDogApp(
                    leftIcon: Icon(
                      Icons.arrow_back_ios,
                      color: Constants.kitGradients[27],
                    ),
                    onTapLeftIcon: () {},
                    rightIcon: Icon(
                      Icons.info_outline_rounded,
                      color: Constants.kitGradients[27],
                    ),
                    onTapRightIcon: () {},
                    title: Text(
                      "Reset Password",
                      style: TextStyle(
                        fontSize: 17,
                        fontWeight: FontWeight.w600,
                        color: Constants.kitGradients[27],
                      ),
                    ),
                  ),
                  //color: Constants.kitGradients[17],
                ),
              ),
              Positioned(
                top: screenHeight(context, dividedBy: 20),
                child: SingleChildScrollView(
                  child: Container(
                    width: screenWidth(context, dividedBy: 1),
                    padding: EdgeInsets.symmetric(
                      horizontal: screenWidth(context, dividedBy: 20),
                      // vertical: screenHeight(context, dividedBy: 5),
                    ),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      //mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        SizedBox(
                          height: screenHeight(context, dividedBy: 20),
                        ),
                        SizedBox(
                          height: screenHeight(context, dividedBy: 10),
                        ),
                        Text(
                          "Enter the email address associated with your account, and we'll send an email with instructions to reset your password",
                          textAlign: TextAlign.left,
                          style: TextStyle(
                              color: Constants.kitGradients[27],
                              fontFamily: 'OpenSansRegular',
                              fontSize: 16,
                              fontWeight: FontWeight.w400),
                        ),
                        SizedBox(
                          height: screenHeight(context, dividedBy: 30),
                        ),
                        Text(
                          "Email Address:",
                          //textAlign: TextAlign.left,
                          style: TextStyle(
                              color: Colors.grey,
                              fontFamily: 'OpenSansRegular',
                              fontSize: 16,
                              fontWeight: FontWeight.w400),
                        ),
                        SizedBox(
                          height: screenHeight(context, dividedBy: 60),
                        ),
                        FormFeildUserDetails(
                          labelText: "johnas@abc.com",
                          // isPassword: false,
                        ),
                        SizedBox(
                          height: screenHeight(context, dividedBy: 20),
                        ),
                        Center(
                          child:
                          BuildButton(
                            buttonWidth: 2.5,
                            isDisabled: true,
                            title: "Send Verification",
                            onPressed: () {
                              push(context, CreateNewPassword());
                            },
                          ),
                        )
                      ],
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
