import 'dart:io';
import 'dart:typed_data';
import 'dart:ui' as ui;

import 'package:app_template/src/screens/new_post_page.dart';
import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/object_factory.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:app_template/src/widgets/app_bar.dart';
import 'package:app_template/src/widgets/filtered_image_list.dart';
import 'package:app_template/src/widgets/filtered_image_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:image/image.dart' as img;
import 'package:photofilters/filters/filters.dart';
import 'package:photofilters/filters/preset_filters.dart';

class FilterImagePage extends StatefulWidget {
  //const FilterImagePage({Key? key}) : super(key: key);
  final File selectedImage;
  final img.Image image;
  final bool loading;
  FilterImagePage({this.selectedImage, this.image, this.loading});

  @override
  _FilterImagePageState createState() => _FilterImagePageState();
}

class _FilterImagePageState extends State<FilterImagePage> {
  img.Image image;
  Filter filter = presetFiltersList.first;
  final GlobalKey _globalKey = GlobalKey();
  Uint8List uint8list;

  void convertWidgetToImage() async {
    RenderRepaintBoundary repaintBoundary =
        _globalKey.currentContext.findRenderObject();
    ui.Image boxImage = await repaintBoundary.toImage(pixelRatio: 3.0);
    ByteData byteData =
        await boxImage.toByteData(format: ui.ImageByteFormat.png);
    uint8list = byteData.buffer.asUint8List();
    push(
        context,
        NewPostPage(
          // profileIcon:
          //     'https://images.unsplash.com/photo-1619092502089-76f475610e1d?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxleHBsb3JlLWZlZWR8MzN8fHxlbnwwfHx8fA%3D%3D&auto=format&fit=crop&w=600&q=60',
          // userName: "Bi kines",
          profileImage: ObjectFactory().appHive.getProfilePic(),
          userName: ObjectFactory().appHive.getName(),
          filteredImage: uint8list,
        ));
    // You can now use this to save the Image to Local Storage or upload it to a Remote Server.
  }

  int fitCount = 1;

  @override
  Widget build(BuildContext context) {
    const double height = 450;
    return Scaffold(
      backgroundColor: Constants.kitGradients[17],
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Constants.kitGradients[17],
        actions: [
          AppBarDogApp(
            leftIcon: Icon(
              Icons.close,
              color: Constants.kitGradients[27],
            ),
            onTapLeftIcon: () {
              pop(context);
            },
            rightIcon: Icon(
              Icons.check,
              color: Constants.kitGradients[27],
            ),
            onTapRightIcon: convertWidgetToImage,
            title: Text(
              "Edit image",
              style: TextStyle(
                fontSize: 17,
                fontWeight: FontWeight.w600,
                color: Constants.kitGradients[27],
              ),
            ),
          )
        ],
        leading: Container(),
      ),
      body: Container(
        width: screenWidth(context, dividedBy: 1),
        height: screenHeight(context, dividedBy: 1),
        child: Column(
          children: [
            widget.image == null
                ? Container(
                    width: screenWidth(context, dividedBy: 1),
                    height: screenWidth(context, dividedBy: 1.5),
                    decoration: BoxDecoration(
                      color: Constants.kitGradients[19],
                    ),
                    child: Center(
                      heightFactor: 1,
                      widthFactor: 1,
                      child: SizedBox(
                        height: 16,
                        width: 16,
                        child: CircularProgressIndicator(
                          valueColor: AlwaysStoppedAnimation(
                              Constants.kitGradients[27]),
                          strokeWidth: 2,
                        ),
                      ),
                    ),
                  )
                : RepaintBoundary(
                    key: _globalKey,
                    child: Container(
                      width: screenWidth(context, dividedBy: 1),
                      height: screenWidth(context, dividedBy: 1.5),
                      // margin: EdgeInsets.symmetric(
                      //     // horizontal: screenWidth(context, dividedBy: 30),
                      //     // vertical: screenHeight(context, dividedBy: 30),
                      //     ),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(4),
                        color: Constants.kitGradients[19].withOpacity(0.3),
                      ),
                      child: ClipRRect(
                        borderRadius: BorderRadius.circular(4),
                        child: Stack(
                          children: [
                            FilteredImageWidget(
                              filter: filter,
                              image: widget.image,
                              successBuilder: (imageBytes) {
                                return Image.memory(
                                  imageBytes,
                                  fit: BoxFit.fitWidth,
                                  // fitCount == 1
                                  //     ? BoxFit.contain
                                  //     : BoxFit.cover,
                                  width: screenWidth(context, dividedBy: 1),
                                  height: screenWidth(context, dividedBy: 1.5),
                                );
                              },
                              errorBuilder: () => Container(height: height),
                              loadingBuilder: () => Container(
                                height: screenWidth(context, dividedBy: 1.5),
                                width: screenWidth(context, dividedBy: 1),
                                color: Colors.transparent,
                                child: Center(
                                  heightFactor: 1,
                                  widthFactor: 1,
                                  child: SizedBox(
                                    height: 16,
                                    width: 16,
                                    child: CircularProgressIndicator(
                                      valueColor: AlwaysStoppedAnimation(
                                          Constants.kitGradients[19]),
                                      strokeWidth: 2,
                                    ),
                                  ),
                                ),
                              ),
                            ),
                            new Positioned(
                              bottom: 10,
                              left: 10,
                              child: GestureDetector(
                                  onTap: () {
                                    fitCount == 1
                                        ? setState(() {
                                            fitCount = 2;
                                          })
                                        : setState(() {
                                            fitCount = 1;
                                          });
                                  },
                                  child: fitCount == 2
                                      ? Image(
                                          image: AssetImage(
                                              "assets/images/normalscreen.png"),
                                          width: 25,
                                          height: 25)
                                      : Image(
                                          image: AssetImage(
                                              "assets/images/fullscreen.png"),
                                          width: 25,
                                          height: 25)),
                            )
                          ],
                        ),
                      ),
                    )),
            SizedBox(
              height: screenWidth(context, dividedBy: 30),
            ),
            widget.image == null
                ? Container()
                : FilteredImageList(
                    filters: presetFiltersList,
                    image: widget.image,
                    onValueChangedFilter: (filter) {
                      setState(() {
                        this.filter = filter;
                      });
                    },
                  ),
          ],
        ),
      ),
    );
  }
}
