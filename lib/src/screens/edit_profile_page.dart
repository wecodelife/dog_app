import 'dart:io';

import 'package:app_template/src/bloc/user_bloc.dart';
import 'package:app_template/src/models/get_user_profile_response.dart';
import 'package:app_template/src/models/user_update_request_model.dart';
import 'package:app_template/src/screens/photo-filter_page.dart';
import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/object_factory.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:app_template/src/widgets/app_bar.dart';
import 'package:app_template/src/widgets/bottom_sheet_item.dart';
import 'package:app_template/src/widgets/onboarding_textfield.dart';
import 'package:app_template/src/widgets/profile_icon.dart';
import 'package:app_template/src/widgets/user_text_field.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:image/image.dart' as img;
import 'package:app_template/src/screens/profile_page.dart';
import 'package:app_template/src/models/username_request_model.dart';

class EditProfilePage extends StatefulWidget {
  // const EditProfilePage({Key? key}) : super(key: key);
  final String profileImage;
  final String description;
  final String userName;
  final String profileName;
  EditProfilePage(
      {this.profileImage, this.description, this.userName, this.profileName});

  @override
  _EditProfilePageState createState() => _EditProfilePageState();
}

class _EditProfilePageState extends State<EditProfilePage> {
  bool loading = false;
  TextEditingController userNameTextEditingController =
      new TextEditingController();
  TextEditingController descriptionTextEditingController =
      new TextEditingController();
  TextEditingController profileNameTextEditingController =
      new TextEditingController();
  UserUpdateRequestModel updateRequestModel;
  UserBloc userBloc = UserBloc();
  GetUserProfileResponse profileData = GetUserProfileResponse();
  bool usernameNotAvailable = true;

  img.Image image;
  File _image;
  bool isLoading;
  final picker = ImagePicker();
  String imageUrl = "";

  Future imgFromGallery(context) async {
    final image = await ImagePicker.pickImage(
        source: ImageSource.gallery, imageQuality: 50);
    pop(context);
    setState(() {
      if (image != null) {
        _image = File(image.path);
      } else {
        print('No image selected.');
      }
    });
    // push(
    //     context,
    //     ColoredFilter(
    //       pickedFile: _image,
    //     ));
  }

  Future imgFromCamera(context) async {
    final image = await ImagePicker.pickImage(
        source: ImageSource.camera, imageQuality: 25);
    pop(context);
    if (image != null) {
      _image = File(image.path);
    } else {
      print('No image selected.');
    }
    // push(
    //     context,
    //     ColoredFilter(
    //       pickedFile: _image,
    //     ));
  }

  void showPicker(context) {
    Future<void> future = showModalBottomSheet(
        context: context,
        backgroundColor: Constants.kitGradients[17],
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.only(
                topLeft: Radius.circular(8), topRight: Radius.circular(8))),
        builder: (BuildContext context) {
          return Container(
            // height:screenHeight(context, dividedBy:4),
            padding: EdgeInsets.symmetric(
              horizontal: screenWidth(context, dividedBy: 30),
              vertical: screenHeight(context, dividedBy: 30),
            ),
            decoration: BoxDecoration(
                color: Constants.kitGradients[17],
                borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(8), topRight: Radius.circular(8))),
            child: Wrap(
              //crossAxisAlignment:CrossAxisAlignment.start,
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.symmetric(
                    horizontal: screenWidth(context, dividedBy: 20),
                    //vertical: screenHeight(context, dividedBy: 20),
                  ),
                  child: Text(
                    "Select Picture from",
                    style: TextStyle(
                      fontSize: 20,
                      fontWeight: FontWeight.bold,
                      fontFamily: "Montserrat",
                      color: Constants.kitGradients[27],
                    ),
                  ),
                ),
                SizedBox(
                  height: screenHeight(context, dividedBy: 15),
                ),
                BottomSheetItem(
                    title: "Photo Library",
                    leading: new Icon(
                      Icons.photo_library,
                      color: Constants.kitGradients[27],
                    ),
                    trailing: null,
                    onTap: () async {
                      imgFromGallery(context);
                      // pop(context);
                    }

                    // },
                    ),
                Divider(
                  color: Constants.kitGradients[27].withOpacity(0.2),
                  indent: 10,
                  endIndent: 10,
                ),
                BottomSheetItem(
                  title: "Camera",
                  leading: new Icon(
                    Icons.photo_camera,
                    color: Constants.kitGradients[27],
                  ),
                  trailing: null,
                  onTap: () async {
                    final image = await ImagePicker.pickImage(
                        source: ImageSource.camera, imageQuality: 25);
                    pop(context);
                    if (image != null) {
                      _image = File(image.path);
                    } else {
                      print('No image selected.');
                    }
                    // push(
                    //     context,
                    //     ColoredFilter(
                    //       pickedFile: _image,
                    //     ));
                  },
                ),
                Divider(
                  color: Constants.kitGradients[27].withOpacity(0.2),
                  indent: 10,
                  endIndent: 10,
                ),
              ],
            ),
          );
        });
    future.then((void value) => print("closed modal"));
  }

  @override
  void initState() {
    // TODO: implement initState
    userNameTextEditingController.text = widget.userName;
    descriptionTextEditingController.text = widget.description;
    profileNameTextEditingController.text = widget.profileName;

    userBloc.getUserProfile(id: ObjectFactory().appHive.getUserId());
    userBloc.getUserProfileResponse.listen((event) {
      print("Profile data loaded Successfully");
      setState(() {
        profileData = event;
        loading = false;
      });
    }).onError((event) {
      setState(() {
        // loading = false;
      });
      showErrorAnimatedToast(context: context, msg: event);
    });
    userBloc.userUpdateResponseModel.listen((event) {
      if (event.status == 200 || event.status == 201) {
        // ObjectFactory().appHive.putUserId(userId: event.id.toString());
        ObjectFactory().appHive.putName(name: event.firstName);
        ObjectFactory().appHive.putProfilePic(event.profileImage);
        print("edit profile Successful   ");
        print("edit profile userid   " + event.id.toString());
        print("Edit profile UserName   " + event.name);
        print(event.id);
        print("Registration Profilepic  " +
            ObjectFactory().appHive.getProfilePic());
        print(event.profileImage.toString() + "gghghg");
        push(context, ProfilePage());
      }
    });
    userBloc.userNameResponse.listen((event) {
      if (event.status == 200 || event.status == 201) {
        setState(() {
          print("Username Response successful");
          print(event.message);
          print("userexists    "+event.userExists.toString());
          usernameNotAvailable = event.userExists;
        });
      }
    }).onError((event) {
      setState(() {
        print("Username Response error");
        // loading = false;
      });
      showErrorAnimatedToast(context: context, msg: event);
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Constants.kitGradients[17],
      appBar: AppBar(
          backgroundColor: Constants.kitGradients[17],
          elevation: 0,
          leading: Container(),
          actions: [
            AppBarDogApp(
              leftIcon: Icon(
                Icons.lock_outline_rounded,
                color: Constants.kitGradients[27],
              ),
              onTapLeftIcon: () {},
              rightIcon: Text(
                "Update",
                style: TextStyle(
                  fontSize: 18,
                  fontWeight: FontWeight.bold,
                  fontFamily: 'Montserrat',
                  color: Constants.kitGradients[27],
                ),
              ),
              onTapRightIcon: () {
                updateRequestModel = UserUpdateRequestModel(
                    firstName: profileNameTextEditingController.text != null
                        ? profileNameTextEditingController.text
                        : profileData.firstName,
                    lastName: profileData.lastName,
                    username: userNameTextEditingController.text != null
                        ? userNameTextEditingController.text
                        : profileData.username,
                    name: profileData.name,
                    dateOfBirth: profileData.dateOfBirth,
                    breedType: profileData.breedType,
                    gender: profileData.gender,
                    profileImage: _image == null ? null : _image,
                    uid: profileData.uid,
                    phoneNumber: profileData.phoneNumber,
                    isUserHasDog: profileData.isUserHasDog,
                    description: descriptionTextEditingController.text != null
                        ? descriptionTextEditingController.text
                        : profileData.description);
                print(
                  "nmhhjhjjg" + updateRequestModel.firstName,
                );
                userBloc.userUpdate(
                  id: ObjectFactory().appHive.getUserId(),
                  userUpdateRequest: updateRequestModel,
                );
              },
              title: Text(
                ObjectFactory().appHive.getName(),
                style: TextStyle(
                  fontSize: 16,
                  fontWeight: FontWeight.w700,
                  fontFamily: 'Montserrat',
                  color: Constants.kitGradients[27],
                ),
              ),
            ),
          ]),
      body: Stack(
        children: [
          Container(
            height: screenHeight(context, dividedBy: 1),
            width: screenWidth(context, dividedBy: 1),
            padding: EdgeInsets.symmetric(
              horizontal: screenWidth(context, dividedBy: 30),
            ),
            child: loading == true
                ? Container(
                    width: screenWidth(context, dividedBy: 1),
                    height: screenWidth(context, dividedBy: 1),
                    color: Constants.kitGradients[17],
                    child: Center(
                      heightFactor: 1,
                      widthFactor: 1,
                      child: SizedBox(
                        height: 16,
                        width: 16,
                        child: CircularProgressIndicator(
                          valueColor: AlwaysStoppedAnimation(
                              Constants.kitGradients[19]),
                          strokeWidth: 2,
                        ),
                      ),
                    ),
                  )
                : SingleChildScrollView(
                    child: Container(
                      width: screenWidth(context, dividedBy: 1),
                      padding: EdgeInsets.symmetric(
                        horizontal: screenWidth(context, dividedBy: 30),
                      ),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          SizedBox(
                              height: screenHeight(context, dividedBy: 40)),
                          Center(
                            child: GestureDetector(
                              onTap: () {
                                showPicker(context);
                              },
                              child: _image == null
                                  ? ProfileIcon(
                                      circleRadius: 3,
                                      profileImage: widget.profileImage,
                                    )
                                  : Container(
                                      width: screenWidth(context, dividedBy: 3),
                                      height:
                                          screenWidth(context, dividedBy: 3),
                                      decoration: BoxDecoration(
                                          shape: BoxShape.circle,
                                          color: Constants.kitGradients[17]
                                              .withOpacity(0.3),
                                          border: Border.all(
                                              color: Constants.kitGradients[17]
                                                  .withOpacity(0.3),
                                              width: 0.0),
                                          image: DecorationImage(
                                              fit: BoxFit.cover,
                                              image: FileImage(_image)))),
                            ),
                          ),
                          SizedBox(
                              height: screenHeight(context, dividedBy: 20)),
                          Text(
                            "UserName",
                            style: TextStyle(
                              fontSize: 16,
                              fontWeight: FontWeight.w700,
                              fontFamily: 'Montserrat',
                              color: Constants.kitGradients[27],
                            ),
                          ),
                          SizedBox(
                              height: screenHeight(context, dividedBy: 50)),
                          FormFeildUserDetails(
                            labelText: "UserName",
                            textEditingController:
                                userNameTextEditingController,
                            userExists: usernameNotAvailable,
                            // isPassword: false,
                            // isnumber: false,
                            autoFocus: false,
                            textArea: false,
                            onChanged: (val) {
                              print("username checking");
                              if (val.length > 3)
                                userBloc.userCheck(
                                    userNameRequest: UserNameRequest(
                                        username: userNameTextEditingController
                                            .text));
                              if (val.length < 4 || val.length == 0)
                                setState(() {
                                  usernameNotAvailable = true;
                                });
                              // if(userNameTextEditingController.text == "")
                              //   setState(() {
                              //     usernameNotAvailable = true;
                              //   });
                            },
                          ),
                          // usernameNotAvailable == true ? SizedBox(
                          //     height: screenHeight(context, dividedBy: 70)) : Container(),
                          // usernameNotAvailable == true ? Text("* Username not Available", style: TextStyle(
                          //   fontSize: 13,
                          //   fontWeight: FontWeight.w400,
                          //   fontFamily: 'Montserrat',
                          //   color: Constants.kitGradients[19],
                          // ),) : Container(),
                          // OnBoardingTextField(
                          //   labelText: "UserName",
                          //   textEditingController:
                          //       userNameTextEditingController,
                          //   hasSuffix: false,
                          //   suffixIcon: Container(),
                          //   onTapIcon: () {},
                          //   textArea: false,
                          //   onTap: () {},
                          //   readOnly: false,
                          // ),
                          SizedBox(
                              height: screenHeight(context, dividedBy: 30)),
                          Text(
                            "Name",
                            style: TextStyle(
                              fontSize: 16,
                              fontWeight: FontWeight.w700,
                              fontFamily: 'Montserrat',
                              color: Constants.kitGradients[27],
                            ),
                          ),
                          SizedBox(
                              height: screenHeight(context, dividedBy: 50)),
                          FormFeildUserDetails(
                            labelText: "ProfileName",
                            textEditingController:
                                profileNameTextEditingController,
                            // isPassword: false,
                            // isnumber: false,
                            autoFocus: false,
                            textArea: false,
                            userExists: true,
                          ),
                          // OnBoardingTextField(
                          //   labelText: "ProfileName",
                          //   textEditingController:
                          //       profileNameTextEditingController,
                          //   hasSuffix: false,
                          //   suffixIcon: Container(),
                          //   onTapIcon: () {},
                          //   textArea: false,
                          //   onTap: () {},
                          //   readOnly: false,
                          // ),
                          SizedBox(
                              height: screenHeight(context, dividedBy: 30)),
                          Text(
                            "Description",
                            style: TextStyle(
                              fontSize: 16,
                              fontWeight: FontWeight.w700,
                              fontFamily: 'Montserrat',
                              color: Constants.kitGradients[27],
                            ),
                          ),
                          SizedBox(
                              height: screenHeight(context, dividedBy: 50)),
                          FormFeildUserDetails(
                            labelText: "Description",
                            textEditingController:
                                descriptionTextEditingController,
                            // isPassword: false,
                            // isnumber: false,
                            autoFocus: false,
                            textArea: true,
                            userExists: true,
                          ),
                          // OnBoardingTextField(
                          //   labelText: "Description",
                          //   textEditingController:
                          //       descriptionTextEditingController,
                          //   hasSuffix: false,
                          //   suffixIcon: Container(),
                          //   onTapIcon: () {},
                          //   textArea: false,
                          //   onTap: () {},
                          //   readOnly: false,
                          // ),
                        ],
                      ),
                    ),
                  ),
          ),
          // Positioned(
          //   bottom: 0,
          //   child: CustomBottomBar(
          //     currentIndex: 2,
          //     onTapSearch: () {
          //       push(context, MarketPlacePage());
          //     },
          //     onTapProfile: () {},
          //     onTapHome: () {
          //       push(
          //           context,
          //           HomePage(
          //             // name: "Bi",
          //             // profilePic:
          //             //     "https://img.17qq.com/images/fjjhjggbebz.jpeg",
          //           ));
          //     },
          //   ),
          // )
        ],
      ),
    );
  }
}
