import 'package:app_template/src/screens/home_page.dart';
import 'package:app_template/src/screens/new_post_page.dart';
import 'package:app_template/src/utils/object_factory.dart';
import 'package:app_template/src/widgets/app_bar.dart';
import 'package:app_template/src/widgets/image_slider.dart';
import 'package:app_template/src/widgets/bottom_bar.dart';
import 'package:app_template/src/widgets/market_product_template.dart';
import 'package:app_template/src/widgets/marketplace_list_shimmer.dart';
import 'package:flutter/material.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/widgets/market_place_card.dart';
import 'package:app_template/src/widgets/heading_tile.dart';
import 'package:app_template/src/screens/item_details_page.dart';
import 'package:app_template/src/screens/items_page.dart';
import 'package:app_template/src/screens/profile_page.dart';
import 'package:app_template/src/bloc/user_bloc.dart';
import 'package:app_template/src/models/get_image_slider.dart';
import 'package:app_template/src/models/get_product_category.dart';
import 'package:app_template/src/models/get_product_details.dart';
import 'package:flutter/services.dart';
import 'package:shimmer/shimmer.dart';
class MarketPlace extends StatefulWidget {
  // const MarketPlace({Key? key}) : super(key: key);

  @override
  _MarketPlaceState createState() => _MarketPlaceState();
}

class _MarketPlaceState extends State<MarketPlace> {
  UserBloc userBloc = UserBloc();
  List<String> clothing = [
    "https://images.unsplash.com/photo-1597603413826-cd1c06b05222?ixid=MnwxMjA3fDB8MHxzZWFyY2h8NHx8ZG9nJTIwYmF0aHxlbnwwfHwwfHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=700&q=60",
    "https://images.unsplash.com/photo-1560807707-8cc77767d783?ixid=MnwxMjA3fDB8MHxzZWFyY2h8OHx8ZG9nJTIwYmF0aHxlbnwwfHwwfHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=700&q=60",
    "https://images.unsplash.com/photo-1544197807-bb503430e22d?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxzZWFyY2h8MTh8fGRvZyUyMGJhdGh8ZW58MHx8MHx8&auto=format&fit=crop&w=700&q=60",
    "https://images.unsplash.com/photo-1597603413826-cd1c06b05222?ixid=MnwxMjA3fDB8MHxzZWFyY2h8NHx8ZG9nJTIwYmF0aHxlbnwwfHwwfHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=700&q=60",
    "https://images.unsplash.com/photo-1560807707-8cc77767d783?ixid=MnwxMjA3fDB8MHxzZWFyY2h8OHx8ZG9nJTIwYmF0aHxlbnwwfHwwfHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=700&q=60",
    "https://images.unsplash.com/photo-1544197807-bb503430e22d?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxzZWFyY2h8MTh8fGRvZyUyMGJhdGh8ZW58MHx8MHx8&auto=format&fit=crop&w=700&q=60"
  ];
  List<String> price = ["200","3 \U24","4 \U24","4 \U24","2 \U24"];
  List<String> categories = [
    "Cookies, Biscuits & Snaks",
    "Basic Dog Bowls",
    "Shower & Bath Accessories"
  ];
  List<String> type = ["Ethenic", "Casual", "Sports", "All"];
  @override

  void initState() {
    // TODO: implement initState
    userBloc.getImageSlider();
    userBloc.getProductCategory();
    userBloc.getProductDetails();
    super.initState();
  }
  Future<bool> _willPopCallback() async {
    pushAndRemoveUntil(context, HomePage(), false);
    return true;
  }


  @override
  Widget build(BuildContext context) {
      SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.dark.copyWith(
          statusBarColor: Constants.kitGradients[17],
          //or set color with: Color(0xFF0000FF)
          statusBarIconBrightness: Brightness.dark));
    return WillPopScope(
      onWillPop: _willPopCallback,
      child: Scaffold(
        backgroundColor: Constants.kitGradients[17],
        appBar: AppBar(
            backgroundColor: Constants.kitGradients[17],
            elevation: 2,
            shadowColor: Constants.kitGradients[17],
            leading: Container(),
            actions: [
              AppBarDogApp(
                  leftIcon: Icon(
                    Icons.lock_outline_rounded,
                    color: Constants.kitGradients[0],
                  ),
                  onTapLeftIcon: () {},
                  // rightIcon: Icon(
                  //   Icons.settings_sharp,
                  //   color: Constants.kitGradients[0],
                  // ),
                  // onTapRightIcon: () {
                  //   showSettings();
                  // },
                  title: Text(
                    "Shop",
                    style: TextStyle(
                      fontSize: 26,
                      fontWeight: FontWeight.bold  ,
                      fontFamily: "Prompt-Light",
                      color: Constants.kitGradients[0],
                    ),
                  )),
            ]),
        body: Stack(
          children: [
            Container(
              height: screenHeight(context, dividedBy: 1),
              width: screenWidth(context, dividedBy: .5),
              // padding: EdgeInsets.symmetric(
              //   horizontal: screenWidth(context, dividedBy: 30),
              // ),
              child: SingleChildScrollView(
                child: Column(
                  //crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    SizedBox(height: screenHeight(context, dividedBy: 100)),
                    StreamBuilder<GetImageSlider>(
                        stream: userBloc.getImageSliderResponse,
                        builder: (context, snapshot) {
                          return snapshot.hasData
                              ? Container(
                                  width: screenWidth(context, dividedBy: 1),
                                  height: screenHeight(context, dividedBy: 2.8),
                                  // color: Constants.kitGradients[24],
                                  child: Center(
                                      child: CustomImageSlider(
                                          sliderImages: snapshot.data)),
                                )
                              :
                          Shimmer.fromColors(
                              baseColor: Constants.kitGradients[26],
                              highlightColor: Constants.kitGradients[12],
                              child:
                              Container(
                            width: screenWidth(context, dividedBy: 1.1),
                            height: screenHeight(context, dividedBy: 2.8),
                                decoration: BoxDecoration(
                                  color: Colors.grey,
                                  borderRadius: BorderRadius.circular(10),
                                ),
                          )
                          );
                        }),
                    SizedBox(height: screenHeight(context, dividedBy: 50)),
                    Container(
                      padding: EdgeInsets.only(
                        top: screenHeight(context, dividedBy: 100),
                        //left: screenWidth(context, dividedBy: 25),
                      ),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          // HeadingTile(heading: categories[0], seeMore: () {}),
                          // Row(
                          //   children: [
                          //     SizedBox(
                          //       width: screenWidth(context, dividedBy: 30),
                          //     ),
                          //     HeadingTile(heading: categories[0], seeMore: () {}),
                          //   ],
                          // ),
                          // SizedBox(height: screenHeight(context, dividedBy: 100)),
                          // Container(
                          //   alignment: Alignment.center,
                          //   child: Column(
                          //     crossAxisAlignment: CrossAxisAlignment.start,
                          //     children: [
                          //       //SizedBox(height: screenHeight(context, dividedBy: 50)),
                          //       Container(
                          //         width: screenWidth(context, dividedBy: 1),
                          //         height: screenHeight(context, dividedBy: 3.4),
                          //         // color:Colors.red,
                          //         child: ListView.builder(
                          //           //shrinkWrap: true,
                          //           scrollDirection: Axis.horizontal,
                          //           itemCount: 4,
                          //           itemBuilder:
                          //               (BuildContext context, int index) {
                          //             return Row(children: [
                          //               SizedBox(
                          //                 width:
                          //                     screenWidth(context, dividedBy: 30),
                          //               ),
                          //               MarketPlaceCard(
                          //                 imgUrl: clothing[index],
                          //                 catType: type[index],
                          //                 circle: false,
                          //                 itemPressed: () {
                          //                   push(
                          //                     context,
                          //                     ItemDetailsPage(
                          //                       itemName: "Cookies, Biscuits & Snaks",
                          //                       itemImage:
                          //                       "https://images.unsplash.com/photo-1597603413826-cd1c06b05222?ixid=MnwxMjA3fDB8MHxzZWFyY2h8NHx8ZG9nJTIwYmF0aHxlbnwwfHwwfHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=700&q=60",
                          //                       itemPrice: "670",
                          //                       itemRating: "8.1",
                          //                       features: features,
                          //                     ),
                          //                   );
                          //                 },
                          //               ),
                          //               SizedBox(
                          //                 width:
                          //                     screenWidth(context, dividedBy: 40),
                          //               )
                          //             ]);
                          //           },
                          //         ),
                          //       ),
                          //     ],
                          //   ),
                          // ),
                          StreamBuilder<GetProductCategory>(
                              stream: userBloc.getProductCategoryResponse,
                              builder: (context, snapshot) {
                                if(snapshot.hasData)
                                    userBloc.getProductDetails();
                                return  snapshot.hasData ?
                                Container(
                                    width: screenWidth(context, dividedBy: 1),
                                    // height: screenHeight(context,dividedBy: 2.8),
                                    child: ListView.builder(
                                        itemCount:
                                        snapshot.data.results.length,
                                        shrinkWrap: true,
                                        physics:
                                        NeverScrollableScrollPhysics(),
                                        itemBuilder: (BuildContext context,
                                            int index) {
                                          return Container(
                                              // width: screenWidth(context,
                                              //     dividedBy: 2.8),
                                              // height: screenHeight(context,
                                              //     dividedBy: 2.85),
                                              child: Container(
                                                width: screenWidth(context,
                                                    dividedBy: 2.85),
                                                // height: screenHeight(context,
                                                //     dividedBy: 2.7),
                                                child:Column(
                                                    // crossAxisAlignment:
                                                    // CrossAxisAlignment
                                                    //     .start,
                                                    children: [
                                                      Row(
                                                        children: [
                                                          SizedBox(
                                                            width:
                                                            screenWidth(
                                                                context,
                                                                dividedBy:
                                                                25),
                                                            height: screenHeight(context,
                                                                dividedBy: 50),
                                                          ),
                                                          HeadingTile(
                                                              heading: snapshot
                                                                  .data.results[index]
                                                                  .name,
                                                              seeMore: () {
                                                                push(
                                                                  context,
                                                                  ItemsPage(
                                                                    categoryName: snapshot.data.results[index].name,

                                                                  ),
                                                                );

                                                              }),
                                                        ],
                                                      ),
                                                      SizedBox(
                                                          height:
                                                          screenHeight(
                                                              context,
                                                              dividedBy:
                                                              70)),
                                                      Container(
                                                        alignment:
                                                        Alignment.center,
                                                        child: Column(
                                                          crossAxisAlignment:
                                                          CrossAxisAlignment
                                                              .start,
                                                          children: [
                                                            //SizedBox(height: screenHeight(context, dividedBy: 50)),
                                                            StreamBuilder<
                                                                GetProductDetails>(
                                                                stream: userBloc
                                                                    .getProductDetailsResponse,
                                                                builder: (context,
                                                                    snapshot) {
                                                                  return snapshot
                                                                      .hasData
                                                                      ? Container(
                                                                    width:
                                                                    screenWidth(
                                                                        context,
                                                                        dividedBy: 1.03),
                                                                    height:
                                                                    screenHeight(
                                                                        context,
                                                                        dividedBy: 3.4),
                                                                    // color:Colors.red,
                                                                    child:
                                                                    ListView
                                                                        .builder(
                                                                      shrinkWrap: true,
                                                                      scrollDirection: Axis
                                                                          .horizontal,
                                                                      itemCount: 5,
                                                                      // itemCount: snapshot.data.results.length,
                                                                      itemBuilder: (
                                                                          BuildContext context,
                                                                          int index,) {
                                                                        return Row(
                                                                            children: [
                                                                              SizedBox(
                                                                                width: screenWidth(
                                                                                    context,
                                                                                    dividedBy: 70),),
                                                                              MarketPlaceCard(
                                                                                imgUrl: snapshot
                                                                                    .data
                                                                                    .results[index]
                                                                                    .image1,
                                                                                catType: snapshot
                                                                                    .data
                                                                                    .results[index]
                                                                                    .name,
                                                                                rating: snapshot
                                                                                    .data
                                                                                    .results[index]
                                                                                    .rating,
                                                                                price:  snapshot
                                                                                    .data
                                                                                    .results[index]
                                                                                    .price,
                                                                                circle: false,
                                                                                itemPressed: () {
                                                                                  push(
                                                                                    context,
                                                                                    ItemDetailsPage(
                                                                                      itemName: snapshot
                                                                                          .data
                                                                                          .results[index]
                                                                                          .name,
                                                                                      itemImage: snapshot
                                                                                          .data
                                                                                          .results[index]
                                                                                          .image1,
                                                                                      itemRating: snapshot
                                                                                          .data
                                                                                          .results[index]
                                                                                          .rating,
                                                                                      itemPrice: price[index],
                                                                                      features: snapshot
                                                                                          .data
                                                                                          .results[index]
                                                                                          .highlights,
                                                                                      productId: snapshot
                                                                                          .data
                                                                                          .results[index]
                                                                                          .id,


                                                                                    ),
                                                                                  );
                                                                                },
                                                                              ),
                                                                              SizedBox(
                                                                                width: screenWidth(
                                                                                    context,
                                                                                    dividedBy: 80),
                                                                              )
                                                                            ]);
                                                                      },
                                                                    ),

                                                                  )

                                                                      : Container(width: screenWidth(context,
                                                                      dividedBy: 1),
                                                                    height: screenHeight(context,
                                                                        dividedBy: 3.27),
                                                                        child: ListView.builder(
                                                                        shrinkWrap: true,
                                                                        scrollDirection: Axis
                                                                            .horizontal,
                                                                        itemCount: 5,
                                                                        // itemCount: snapshot.data.results.length,
                                                                        itemBuilder: (
                                                                            BuildContext context,
                                                                            int index,
                                                                            ) {
                                                                          return ProductTemplate();

                                                                        }

                                                                  ),
                                                                      );
                                                                }
                                                            )
                                                          ],
                                                        ),
                                                      ),
                                                    ]),
                                              ));
                                          // Container(
                                          //   // width:screenWidth(context, dividedBy:1),
                                          //   height:
                                          //       screenHeight(context, dividedBy: 2.8),
                                          //   child: ListView.builder(
                                          //     itemCount: 4,
                                          //     shrinkWrap: true,
                                          //     scrollDirection: Axis.horizontal,
                                          //     physics: NeverScrollableScrollPhysics(),
                                          //     itemBuilder:
                                          //         (BuildContext context, int index) {
                                          //       return Container(
                                          //           width:screenWidth(context, dividedBy:1),
                                          //           height:
                                          //           screenHeight(context, dividedBy: 2.8),
                                          //           child: Column(
                                          //               crossAxisAlignment:
                                          //                   CrossAxisAlignment.start,
                                          //               children: [
                                          //             Row(
                                          //               children: [
                                          //                 SizedBox(
                                          //                   width: screenWidth(
                                          //                       context,
                                          //                       dividedBy: 30),
                                          //                 ),
                                          //                 HeadingTile(
                                          //                     heading: categories[0],
                                          //                     seeMore: () {}),
                                          //               ],
                                          //             ),
                                          //             SizedBox(
                                          //                 height: screenHeight(
                                          //                     context,
                                          //                     dividedBy: 100)),
                                          //             Container(
                                          //               alignment: Alignment.center,
                                          //               child: Column(
                                          //                 crossAxisAlignment:
                                          //                     CrossAxisAlignment
                                          //                         .start,
                                          //                 children: [
                                          //                   //SizedBox(height: screenHeight(context, dividedBy: 50)),
                                          //                   Container(
                                          //                     width: screenWidth(
                                          //                         context,
                                          //                         dividedBy: 1),
                                          //                     height: screenHeight(
                                          //                         context,
                                          //                         dividedBy: 3.3),
                                          //                     // color:Colors.red,
                                          //                     child: ListView.builder(
                                          //                       //shrinkWrap: true,
                                          //                       scrollDirection:
                                          //                           Axis.horizontal,
                                          //                       itemCount: 4,
                                          //                       itemBuilder:
                                          //                           (BuildContext
                                          //                                   context,
                                          //                               int index) {
                                          //                         return Row(
                                          //                             children: [
                                          //                               SizedBox(
                                          //                                 width: screenWidth(
                                          //                                     context,
                                          //                                     dividedBy:
                                          //                                         30),
                                          //                               ),
                                          //                               MarketPlaceCard(
                                          //                                 imgUrl:
                                          //                                     clothing[
                                          //                                         index],
                                          //                                 catType: type[
                                          //                                     index],
                                          //                                 circle:
                                          //                                     false,
                                          //                                 itemPressed:
                                          //                                     () {
                                          //                                       push(
                                          //                                         context,
                                          //                                         ItemDetailsPage(
                                          //                                           itemName: "Cookies, Biscuits & Snaks",
                                          //                                           itemImage:
                                          //                                           "https://images.unsplash.com/photo-1597603413826-cd1c06b05222?ixid=MnwxMjA3fDB8MHxzZWFyY2h8NHx8ZG9nJTIwYmF0aHxlbnwwfHwwfHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=700&q=60",
                                          //                                           itemPrice: "670",
                                          //                                           itemRating: "8.1",
                                          //                                           features: features,
                                          //                                         ),
                                          //                                       );
                                          //                                     },
                                          //                               ),
                                          //                               SizedBox(
                                          //                                 width: screenWidth(
                                          //                                     context,
                                          //                                     dividedBy:
                                          //                                         40),
                                          //                               )
                                          //                             ]);
                                          //                       },
                                          //                     ),
                                          //                   ),
                                          //                 ],
                                          //               ),
                                          //             ),
                                          //           ]));
                                          //     },
                                          //   ));
                                        }
                                        )
                                )
                                    :Container(
                                  width: screenWidth(context,dividedBy: 1),
                                  // height: screenHeight(context,dividedBy: 3.9),
                                  child: ListView.builder(
                                      shrinkWrap: true,

                                      itemCount: 5,
                                      // itemCount: snapshot.data.results.length,
                                      itemBuilder: (
                                      BuildContext context,
                                      int index,
                                  ) {
                                        return MarketPlaceList();
                                  }
                                  ),
                                );

                                    // : Container(
                                    //     width:
                                    //         screenWidth(context, dividedBy: 1),
                                    //     height:
                                    //         screenWidth(context, dividedBy: 1),
                                    //     color: Constants.kitGradients[17],
                                    //     child: Center(
                                    //       heightFactor: 1,
                                    //       widthFactor: 1,
                                    //       child: CircularProgressIndicator(
                                    //         valueColor: AlwaysStoppedAnimation(
                                    //             Constants.kitGradients[19]),
                                    //         strokeWidth: 2,
                                    //       ),
                                    //     ),
                                    //   );
                              }),
                        ],
                      ),
                    ),
                    SizedBox(height: screenHeight(context, dividedBy: 8)),
                  ],
                ),
              ),
            ),
            Positioned(
              bottom: 0,
              child: Container(
                // height: screenHeight(context, dividedBy: 10),
                child: CustomBottomBar(
                  currentIndex: 1,
                  onTapSearch: () {
                    push(context, MarketPlace());
                  },
                  onTapProfile: () {
                    push(context, ProfilePage (
                      profilePic: ObjectFactory().appHive.getProfilePic(),
                      name: ObjectFactory().appHive.getName(),
                    ));
                  },
                  onTapHome: () {
                    push(
                        context,
                        HomePage(
                            // name: "Bi",
                            // profilePic:
                            //     "https://img.17qq.com/images/fjjhjggbebz.jpeg",
                            ));
                  },
                  onTapplusRightIcon: (){
                    push(
                      context,NewPostPage(
                      profileImage: ObjectFactory().appHive.getProfilePic(),
                      userName: ObjectFactory().appHive.getName(),
                      isEdit: false,
                    ));}

                    )

                ),
              ),

          ],
        ),
      ),
    );
  }
}
