import 'package:app_template/src/screens/onboarding_breeds.dart';
import 'package:app_template/src/widgets/onboarding_content.dart';
import 'package:app_template/src/widgets/slide_transition.dart';
import 'package:flutter/material.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:app_template/src/utils/constants.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:app_template/src/models/user_update_request_model.dart';

class OnBoardingDOB extends StatefulWidget {
  //const OnBoardingDOB({Key? key}) : super(key: key);
  final UserUpdateRequestModel updateRequestModel;

  OnBoardingDOB({this.updateRequestModel});
  @override
  _OnBoardingDOBState createState() => _OnBoardingDOBState();
}

class _OnBoardingDOBState extends State<OnBoardingDOB> {
  bool openModal = false;
  //date picker controllers
  DateTime selectedDate = DateTime.now();
  UserUpdateRequestModel updateRequestModel;
  String date = "";
  var currentDate;
  var formattedDate = "choose";
  List<String> data = ["aa", "bb", "cc", "dd"];
  TextEditingController dateTextEditingController = new TextEditingController();

  selectDate() {
    DatePicker.showDatePicker(context,
        theme: DatePickerTheme(
          backgroundColor: Constants.kitGradients[17],
          containerHeight: screenHeight(context, dividedBy: 2.9),
          titleHeight: screenHeight(context, dividedBy: 12),
          doneStyle: TextStyle(
            fontSize: 18,
            fontWeight: FontWeight.w500,
            fontFamily: 'Prompt-Ligh',
            color: Constants.kitGradients[27],
          ),
          itemStyle: TextStyle(
            fontSize: 18,
            fontWeight: FontWeight.w500,
            fontFamily: 'Prompt-Ligh',
            color: Constants.kitGradients[27],
          ),
          cancelStyle: TextStyle(
            fontSize: 18,
            fontWeight: FontWeight.w500,
            fontFamily: 'Prompt-Ligh',
            color: Constants.kitGradients[27],
          ),
        ),
        showTitleActions: true,
        minTime: DateTime(1900, 3, 5),
        maxTime: DateTime(2100, 6, 7), onChanged: (date) {
      print('change $date');
    }, onConfirm: (newDateTime) {
      setState(() {
        selectedDate = newDateTime;
        date = selectedDate.toString();
        currentDate = DateTime.parse(date);
        dateTextEditingController.text =
            "${currentDate.day}/${currentDate.month}/${currentDate.year}";
      });
    }, currentTime: DateTime.now(), locale: LocaleType.en);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Constants.kitGradients[0],
      body: Container(
        child: OnBoardingContent(
          caption: "What is your Dog's DOB ?",
          image:
              "https://images.unsplash.com/photo-1510771463146-e89e6e86560e?ixid=MnwxMjA3fDB8MHxzZWFyY2h8MTN8fGRvZ3MlMjBjYXJ0b29uc3xlbnwwfHwwfHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=700&q=60",
          labelText: "Select",
          buttonTitle: "Next",
          hasSuffix: true,
          readOnly: true,
          textEditingController: dateTextEditingController,
          suffixIcon: Icon(
            Icons.calendar_today_outlined,
            color: Constants.kitGradients[19],
          ),
          onTapIcon: () {
            setState(() {
              openModal = true;
            });
            openModal == true ? print("BottomSheet Opened") : selectDate();
          },
          onTapField: () {
            setState(() {
              openModal = true;
            });
            openModal == false ? print("BottomSheet Opened") : selectDate();
          },
          isGender: false,
          currentIndex: 1,
          onButtonPressed: () {
            updateRequestModel = UserUpdateRequestModel(
                firstName: widget.updateRequestModel.firstName,
                // lastName: widget.updateRequestModel.lastName,
                name: widget.updateRequestModel.name,
                uid: widget.updateRequestModel.uid,
                description: widget.updateRequestModel.description,
                username: widget.updateRequestModel.username,
                dateOfBirth: selectedDate,
                phoneNumber: widget.updateRequestModel.phoneNumber,
                isUserHasDog: widget.updateRequestModel.isUserHasDog);
            push(
                context,
                OnBoardingBreeds(
                  updateRequestModel: updateRequestModel,
                ));
            //push(context, OnBoardingBreeds());
          },
        ),
      ),
    );
  }
}
