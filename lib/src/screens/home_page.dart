import 'package:app_template/src/bloc/user_bloc.dart';
import 'package:app_template/src/models/get_post_response.dart';
import 'package:app_template/src/screens/market_place.dart';
import 'package:app_template/src/screens/new_post_page.dart';
import 'package:app_template/src/screens/profile_page.dart';
import 'package:app_template/src/screens/notification_page.dart';
import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/object_factory.dart';
import 'package:app_template/src/utils/urls.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:app_template/src/widgets/alert_dialog_box.dart';
import 'package:app_template/src/widgets/app_bar.dart';
import 'package:app_template/src/widgets/bottom_bar.dart';
import 'package:app_template/src/widgets/bottom_sheet_item.dart';
import 'package:app_template/src/widgets/home_page_cards.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:intl/intl.dart';
import 'package:app_template/src/services/dynamic_link_service.dart';
import 'package:app_template/src/models/like_post_request.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  UserBloc userBloc = new UserBloc();
  bool isLoading = false;
  List<Result> results;
  bool isLiked;
  int likeId;
  List<int> likedUsers= [];
  bool shareLoad = false;

  @override
  void initState() {
    //addLocation();
    userBloc.getPostResponse.listen((event) async {
      print("all good");
      print("Profile Pic");
      print(ObjectFactory().appHive.getProfilePic());
    });
    userBloc.getPost();
    userBloc.deletePostResponse.listen((event) {
      setState(() {
        // loading = false;
      });
      showSuccessAnimatedToast(
          context: context, msg: "Post Deleted Successfully");
      userBloc.getPost();
    }).onError((event) {
      setState(() {
        // userBloc = false;
      });
      showErrorAnimatedToast(context: context, msg: event);
    });
    userBloc.disLikePostResponse.listen((event) {
      setState(() {
        print("Post dislike response");
        showToast("Post unliked");
        userBloc.getPost();
      });
    });
    userBloc.addPostLikeResponse.listen((event) {
      if (event.status == 200 || event.status == 201) {
        setState(() {
          isLoading = false;
          print("Like Added successfully");
          print(event.message);
          print("Like id " + event.id.toString());
          likeId = event.id;
          userBloc.getPost();
        });
      } else {
        showErrorAnimatedToast(msg: "Something went wrong!", context: context);
      }
    });
    super.initState();
  }

  void showMore(BuildContext context,
      {Function onPressedEdit, Function onPressedDelete, int postId}) {
    showModalBottomSheet(
        context: context,
        //transitionAnimationController: controller,
        // isScrollControlled: earnedPoint.length > 6 ? true : false,
        backgroundColor: Constants.kitGradients[17],
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.only(
                topLeft: Radius.circular(8), topRight: Radius.circular(8))),
        builder: (BuildContext context) {
          SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.dark.copyWith(
              statusBarColor: Constants.kitGradients[17],
              //or set color with: Color(0xFF0000FF)
              statusBarIconBrightness: Brightness.dark));

          return Container(
            width: screenWidth(context, dividedBy: 1),
            //height:screenHeight(context, dividedBy:1.5),
            //height: MediaQuery.of(context).copyWith().size.height * 0.75,
            padding: EdgeInsets.symmetric(
              horizontal: screenWidth(context, dividedBy: 20),
              vertical: screenHeight(context, dividedBy: 20),
            ),
            decoration: BoxDecoration(
                color: Constants.kitGradients[17],
                borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(8), topRight: Radius.circular(8))),
            child: Wrap(children: [
              SizedBox(
                height: screenHeight(context, dividedBy: 30),
              ),
              BottomSheetItem(
                title: "Edit Post",
                leading: Icon(
                  Icons.edit,
                  color: Constants.kitGradients[27],
                ),
                trailing: null,
                onTap: () {
                  //imgFromCamera();
                  onPressedEdit();
                  // Navigator.of(context).pop();
                },
              ),
              Divider(
                color: Constants.kitGradients[27].withOpacity(0.2),
                indent: 10,
                endIndent: 10,
              ),
              BottomSheetItem(
                title: "Delete",
                leading: Icon(
                  Icons.delete,
                  color: Constants.kitGradients[27],
                ),
                trailing: null,
                onTap: () {
                  onPressedDelete();
                  // Navigator.of(context).pop();
                },
              ),
            ]),
          );
        });
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.light.copyWith(
        statusBarColor: Constants.kitGradients[27],
        //or set color with: Color(0xFF0000FF)
        statusBarIconBrightness: Brightness.dark));
    return
        // SafeArea(
        //   child:
      Scaffold(
      backgroundColor: Constants.kitGradients[17],
      resizeToAvoidBottomInset: false,
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(50.0),
        child:AppBar(

          brightness: Brightness.light,
      backgroundColor: Constants.kitGradients[27],
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.vertical(
            bottom: Radius.circular(15),
          ),
        ),

      actions: [
        AppBarDogApp(
    rightIcon: Icon(
    Icons.notifications,
    size: 30,
    color: Constants.kitGradients[17],
    ),

          onTapRightIcon: (){
          (context);
          push(context, NotificationPage(

          ));
          // SizedBox(
          //   width: screenWidth(context, dividedBy: 150),
          // );
          // Spacer();

            // plusrightIcon: Icon(
        // Icons.add_outlined,
        //   size: 30,
        //   color: Constants.kitGradients[17],
        // ),
        // onTapplusRightIcon
        //     : () {
        //   imgFromGallery(context);
          // push(
          //     context, NewPostPage(
          //   profileImage: ObjectFactory().appHive.getProfilePic(),
          //   userName: ObjectFactory().appHive.getName(),
          //   isEdit: false,
          // ));
        },
        //   leftIcon: Icon(
        //     Icons.camera_alt,
        //     color: Colors.white54,
        //   ),
        //   onTapLeftIcon: () {
        //     //imgFromGallery(context);
        //     push(
        //         context,
        //         NewPostPage(
        //           profileImage:  ObjectFactory().appHive.getProfilePic(),
        //           userName: ObjectFactory().appHive.getName(),
        //           isEdit: false,
        //         ));
        //
          
        //   },
            title:Padding(
              padding: const EdgeInsets.only(left:0,top: 0,right: 150,bottom: 1),
              child: Container(
                alignment: Alignment.topLeft,
                width: screenWidth(context, dividedBy: 3.2),
                height: screenHeight(context, dividedBy: 10),
                // height: 45,
                // width: 140,
                decoration: new BoxDecoration(
                  image: new DecorationImage(
                      image: new AssetImage(
                          "assets/images/WOWBOW_logo.png"),
                      fit: BoxFit.cover
                    // height: 300,
                    // width: 100,
                    // style: TextStyle(
                    //   fontSize: 17,
                    //   fontWeight: FontWeight.w600,
                    //   color: Constants.kitGradients[27],
                  ),
                ),
              ),
            ),
        )
      ],
      leading: Container(),
      )
      ),
      body: isLoading == true
        ? Container(
            width: screenWidth(context, dividedBy: 1),
            height: screenHeight(context, dividedBy: 1),
            color: Constants.kitGradients[17],
            child: Center(
              heightFactor: 1,
              widthFactor: 1,
              child: SizedBox(
                height: 16,
                width: 16,
                child: CircularProgressIndicator(
                  valueColor:
                      AlwaysStoppedAnimation(Constants.kitGradients[27]),
                  strokeWidth: 2,
                ),
              ),
            ),
          )
        : Container(
            width: screenWidth(context, dividedBy: 1),
            //height: screenWidth(context, dividedBy: 1),
            // color: Colors.cyan,
            child: Stack(
              children: [
                SingleChildScrollView(
                  controller: new ScrollController(
                    initialScrollOffset: 0.0,
                    keepScrollOffset: true,
                  ),
                  padding: EdgeInsets.only(
                      bottom: MediaQuery.of(context).viewInsets.bottom),
                  child: Container(
                    color: Constants.kitGradients[17],
                    child: Column(
                      children: [
                        SizedBox(
                          height: screenHeight(context, dividedBy: 40),
                        ),
                        // Image(image:AssetImage("assets/images/dogApp_logo.png")),
                        Container(
                            width: screenWidth(context, dividedBy: 1),
                            child: StreamBuilder<GetPostResponse>(
                              stream: userBloc.getPostResponse,
                              builder: (context, snapshot) {
                                return snapshot.hasData
                                    ? SingleChildScrollView(
                                        child: Container(
                                          width: screenWidth(context, dividedBy: 1),
                                          child: ListView.builder(
                                            shrinkWrap: true,
                                            reverse: true,
                                            physics: NeverScrollableScrollPhysics(),
                                            itemCount: snapshot.data.results.length,
                                            itemBuilder:
                                                (BuildContext context,
                                                    int index) {
                                              return snapshot.data.results
                                                          .length > 0
                                                  ? Column(
                                                      children: [
                                                        Container(
                                                          child: HomePageCard(
                                                              profilePic: snapshot.data.results[index].userProfileUrl ==
                                                                      null
                                                                  ? "https://nd.net/wp-content/uploads/2016/04/profile-dummy.png"
                                                                  :
                                                              Urls.imageBaseUrl + snapshot.data.results[index].userProfileUrl,
                                                              name: snapshot.data.results[index].userName ==
                                                                      ""
                                                                  ? "UserName"
                                                                  : snapshot.data.results[index].userName,
                                                              feedImage: snapshot.data.results[index].image1,
                                                              caption: snapshot.data.results[index].description,
                                                              postId: snapshot.data.results[index].id,
                                                              location: "Not available",
                                                              likeCount: snapshot.data.results[index].likeCount,
                                                              postDate: snapshot.data.results[index].createdAt,
                                                              isLiked: isLiked,
                                                              likedList: snapshot.data.results[index].likedBy,
                                                              onLiked: (val) async {
                                                                if (val ==
                                                                    false) {
                                                                  setState((){
                                                                    isLiked = true;
                                                                  });
                                                                  print("postid  " +
                                                                      snapshot.data.results[index].id.toString());
                                                                  print("Liked list length after like " +
                                                                      snapshot.data.results[index].likedBy.length.toString());
                                                                  print("likecount after like  " +
                                                                      snapshot.data.results[index].likeCount.toString());
                                                                  userBloc.addPostLike(
                                                                      likePostRequest: LikePostRequest(
                                                                          createdBy:
                                                                              ObjectFactory().appHive.getUserId(),
                                                                          post: snapshot.data.results[index].id.toString(),
                                                                          isDeleted: false));
                                                                } else {
                                                                  print("postid  " +
                                                                      snapshot.data.results[index].id.toString());
                                                                  print("Liked list length after dislike " +
                                                                      snapshot.data.results[index].likedBy.length.toString());
                                                                  print("likecount after dislike  " +
                                                                      snapshot.data.results[index].likeCount.toString());
                                                                  userBloc.disLikePost(
                                                                      id: likeId.toString(),
                                                                      likePostRequest: LikePostRequest(
                                                                          createdBy:
                                                                              ObjectFactory().appHive.getUserId(),
                                                                          post: snapshot.data.results[index].id.toString(),
                                                                          isDeleted: true));
                                                                }
                                                              },
                                                              onTapShare: (){
                                                                setState(() {
                                                                  shareLoad = true;
                                                                });
                                                                DynamicLinkService()
                                                                    .createFirstPostLink(
                                                                  snapshot.data.results[index].id.toString(),
                                                                  context,
                                                                  description: snapshot.data.results[index].description,
                                                                  imgUrl: snapshot.data.results[index].image1,
                                                                    loadMore: (val) {
                                                                      setState(() {
                                                                        shareLoad = val;
                                                                      });
                                                                    }

                                                                );
                                                              },
                                                              onTapMore: () {
                                                                showMore(
                                                                  context,
                                                                  postId: snapshot.data.results[index].id,
                                                                  onPressedDelete:
                                                                      () {
                                                                    print(
                                                                        "delete tapped");
                                                                    pop(context);
                                                                    showDialog(
                                                                        context:
                                                                            context,
                                                                        builder:
                                                                            (BuildContext context) {
                                                                          return AlertDialogBox(
                                                                            title: "Delete Post",
                                                                            descriptions: "Are you sure do you want to delete this post?",
                                                                            onPressedYes: () async {
                                                                              await userBloc.deletePost(
                                                                                id: snapshot.data.results[index].id.toString(),
                                                                              );
                                                                              // showSuccessAnimatedToast(
                                                                              //     context: context, msg: "Post deleted successfully");
                                                                              pushAndRemoveUntil(context, HomePage(), false);
                                                                            },
                                                                          );
                                                                        });
                                                                  },

                                                                  onPressedEdit:
                                                                      () {
                                                                    print(
                                                                        "Edit tapped");
                                                                    pop(context);
                                                                    push(
                                                                        context,
                                                                        NewPostPage(
                                                                          isEdit: true,
                                                                          addPostData: snapshot.data.results[index],
                                                                          postId: snapshot.data.results[index].id,
                                                                          profileImage: ObjectFactory().appHive.getProfilePic(),
                                                                          userName: ObjectFactory().appHive.getName(),
                                                                          editingImage: snapshot.data.results[index].image1,
                                                                          postDate: snapshot.data.results[index].createdAt,

                                                                        ));
                                                                  },

                                                                );
                                                              }
                                                              // comment: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore.",
                                                              // commentBy: "ABBd"
                                                              ),
                                                        ),
                                                      ],
                                                    )
                                                  : Container(
                                                      width: screenWidth(context, dividedBy: 1),
                                                      height: screenHeight(context, dividedBy: 1.4),
                                                      child: Center(
                                                          child: Text(
                                                        "No Posts available",
                                                        style: TextStyle(
                                                          fontSize: 18,
                                                          fontWeight: FontWeight.w500,
                                                          fontFamily: 'Montserrat',
                                                          color: Constants.kitGradients[27],
                                                        ),
                                                      )));
                                            },
                                          ),
                                        ),
                                      )
                                    : Container(
                                        width: screenWidth(context, dividedBy: 1),
                                        height: screenWidth(context, dividedBy: 1),
                                        color: Constants.kitGradients[17],
                                        child: Center(
                                          heightFactor: 1,
                                          widthFactor: 1,
                                          child: CircularProgressIndicator(
                                            valueColor:
                                                AlwaysStoppedAnimation(
                                                    Constants.kitGradients[19]),
                                            strokeWidth: 2,
                                          ),
                                        ),
                                      );
                              },
                            )),
                        // Container(
                        //   child: ListView.builder(
                        //     shrinkWrap: true,
                        //     physics: NeverScrollableScrollPhysics(),
                        //     itemCount: name.length,
                        //     itemBuilder: (BuildContext context, int index) {
                        //       return Column(children: [
                        //         Container(
                        //             child: HomePageCard(
                        //                 profilePic: profileImage[index],
                        //                 name: name[index],
                        //                 feedImage: imgList[index],
                        //                 caption: caption[index],
                        //                 location: "Delhi, India",
                        //                 likeCount: "21,000",
                        //                 postDate: "19 June 2021",
                        //                 //comment: null,
                        //                 comment:
                        //                     "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore.",
                        //                 commentBy: "ABBd")),
                        //         // SizedBox(
                        //         //   height: screenHeight(context, dividedBy: 40),
                        //         // ),
                        //       ]);
                        //     },
                        //   ),
                        // ),
                        SizedBox(
                          height: screenHeight(context, dividedBy: 13),
                        ),
                      ],
                    ),
                  ),
                ),
                Align(
                  alignment: Alignment.bottomCenter,
                  child: CustomBottomBar(
                    currentIndex: 0,
                    onTapSearch: () {
                      push(context, MarketPlace());
                    },
                    onTapProfile: () {
                      push(
                          context,
                           ProfilePage(
                              name: ObjectFactory().appHive.getName(),
                              // profilePic:
                              //     ObjectFactory().appHive.getProfilePic(),

                             //  'https://images.unsplash.com/photo-1520342868574-5fa3804e551c?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=6ff92caffcdd63681a35134a6770ed3b&auto=format&fit=crop&w=1951&q=80',
                          ),
                      );
                    },
                    onTapHome: () {},
                    onTapplusRightIcon: (){
                      push(
                        context,NewPostPage(
                        profileImage: ObjectFactory().appHive.getProfilePic(),
                        userName: ObjectFactory().appHive.getName(),
                        isEdit: false,
                      ),
                      );
                    },

                  ),
                )
              ],
            ),
          ),

      );
  }
}
