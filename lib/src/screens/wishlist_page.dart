import 'package:app_template/src/screens/market_place.dart';
import 'package:app_template/src/widgets/build_button.dart';
import 'package:flutter/material.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:app_template/src/utils/urls.dart';
import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/widgets/wishlist_tile.dart';
import 'package:app_template/src/bloc/user_bloc.dart';
import 'package:app_template/src/models/get_cart_details.dart';
import 'package:app_template/src/models/add_cart_details_request.dart';
import 'package:app_template/src/models/get_wishlist_details.dart';
import 'package:app_template/src/widgets/alert_dialog_box.dart';
import 'package:app_template/src/utils/object_factory.dart';
import 'package:app_template/src/screens/item_cart_page.dart';



class WishlistPage extends StatefulWidget {
  final String itemName;
  final String itemImage;
  final String itemPrice;
  final int productId;
  WishlistPage({this.itemPrice, this.itemName, this.itemImage,this.productId});
  @override
  _WishlistPageState createState() => _WishlistPageState();
}

class _WishlistPageState extends State<WishlistPage> {
  UserBloc userBloc=UserBloc();
  int count = 1;
  bool isLoading = false;
  @override
  void initState() {
    // TODO: implement initState
    userBloc.getWishlistDetails();
    userBloc.getCartDetails();
    userBloc.deleteWishlistItemResponse.listen((event) {
      setState(() {
        // loading = false;
        // userBloc.getCartDetails();
      });
      showSuccessAnimatedToast(
          context: context, msg: "Wishlist Item Deleted Successfully");
      userBloc.getWishlistDetails();
    }).onError((event) {
      setState(() {
        // userBloc = false;
      });
      showErrorAnimatedToast(context: context, msg: event);
    });
    print("product"+widget.productId.toString());
    userBloc.addCartDetailsResponse.listen((event) async {
      print("Add to cart working good");
      if (event.status == 200 || event.status == 201) {
        setState(() {
          isLoading = false;
          userBloc.getCartDetailsResponse.listen((event) async {
            print("Cart all good");
            print(event.results);
            print("length" + event.results.length.toString());

          });
          showSuccessAnimatedToast(msg: "Add to Cart Successfully", context: context);

        });
      } else {
        //showSuccessAnimatedToast(msg: "Added Successfully", context: context);
      }
    });
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: (){
        Navigator.of(context).pushAndRemoveUntil(MaterialPageRoute(builder: (context) =>
            ItemCartPage()), (Route<dynamic> route) => false);
      },
      child: Scaffold(
          backgroundColor: Constants.kitGradients[17],
          appBar: AppBar(
            //elevation: 0,
            backgroundColor: Constants.kitGradients[17],
            leading: GestureDetector(
              onTap: () {
                pop(context);
              },
              child: Container(
                width: screenWidth(context,
                    dividedBy: 6.6),
                height: screenHeight(context,
                    dividedBy: 15),
                // color: Constants.kitGradients[28],
                child: Icon(
                  Icons.arrow_back,
                  color: Constants.kitGradients[27],
                ),
              ),
            ),
            title: Container(
              child: Row(
                children: [
                  Text(
                    "Wishlist",
                    style: TextStyle(
                      //fontSize: 16,
                      fontWeight: FontWeight.w700,
                      color: Constants.kitGradients[27],
                    ),
                  ),
                  Spacer(),
                  GestureDetector(
                      onTap: () {
                        push(
                            context,
                            ItemCartPage());
                      },
                      child: Container(
                        width: screenWidth(context,
                            dividedBy: 6.6),
                        height: screenHeight(context,
                            dividedBy: 15),
                        // color: Constants.kitGradients[28],
                        child: Icon(
                          Icons.shopping_cart_outlined,
                          color: Constants.kitGradients[27],
                        ),
                      ))
                ],
              ),
            ),
          ),
          body: Stack(
            children: [
              SingleChildScrollView(
                child: Column(
                  children: [
                    SizedBox(
                      height: screenHeight(context, dividedBy: 40),
                    ),
                    StreamBuilder<GetWishlistDetails>(
                        stream: userBloc.getWishlistDetailsResponse,
                        builder: (context, snapshot) {
                          return snapshot.hasData ? Container(
                            padding: EdgeInsets.symmetric(
                              horizontal: screenWidth(context, dividedBy: 30),
                            ),
                            child: ListView.builder(
                              shrinkWrap: true,
                              physics: NeverScrollableScrollPhysics(),
                              itemCount: snapshot.data.results.length,
                              itemBuilder: (BuildContext context, int index) {
                                return Column(
                                  children: [
                                    WishlistTile(
                                      itemName: snapshot.data.results[index].productName,
                                      itemImage:Urls.imageBaseUrl +snapshot.data.results[index].productImage,
                                      itemPrice: snapshot.data.results[index].price,
                                      onTapDelete: (){
                                        print(
                                            "delete tapped");
                                        pop(context);
                                        showDialog(
                                            context:
                                            context,
                                            builder:
                                                (BuildContext context) {
                                              return AlertDialogBox(
                                                title: "Delete Item",
                                                descriptions: "Are you sure do you want to delete this Item?",
                                                onPressedYes: () async {
                                                  await userBloc.deleteWishlistItem(
                                                    id: snapshot.data.results[index].id.toString(),
                                                  );
                                                  // showSuccessAnimatedToast(
                                                  //     context: context, msg: "Post deleted successfully");
                                                  pushAndRemoveUntil(context, WishlistPage(), false);
                                                },
                                              );
                                            });
                                      },
                                      onTapCart:(){
                                        userBloc.addCartDetails(addCartDetailsRequest: AddCartDetailsRequest(
                                          product: snapshot.data.results[index].product,
                                          quantity: 1,
                                          isDeleted: false,
                                          user: int.parse(ObjectFactory().appHive.getUserId()),
                                        )
                                        );
                                        userBloc.deleteWishlistItem(
                                          id: snapshot.data.results[index].id.toString(),
                                        );
                                      }
                                    ),
                                    //SizedBox(height: screenHeight(context, dividedBy: 100)),
                                    Divider(
                                      color: Constants.kitGradients[19].withOpacity(0.5),
                                      // thickness: 4,
                                    ),
                                    //SizedBox(height: screenHeight(context, dividedBy: 100)),
                                  ],
                                );
                              },
                            ),
                          ): Container(

                          );
                        }
                    ),
                    SizedBox(
                      height: screenHeight(context, dividedBy: 10),
                    ),
                  ],
                ),
              ),
            ],
          )),
    );
  }
}
