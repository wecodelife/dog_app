import 'dart:async';

import 'package:app_template/src/screens/home_page.dart';
import 'package:app_template/src/screens/login_page.dart';
import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/object_factory.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:simple_animations/simple_animations.dart';

class SplashScreen extends StatefulWidget {
  // const SplashScreen({Key? key}) : super(key: key);

  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  @override
  void initState() {
    // ObjectFactory()
    //     .appHive
    //     .putToken(token: "db6faa2117b78a5b9bfc4d3cf1ccb380ad78b6e8");
    super.initState();
    Timer(
        Duration(seconds: 9),
        () => pushAndReplacement(
            context,
            ObjectFactory().appHive.getToken() != null
                ?
            HomePage()
                : LoginPage()
        ));
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.dark.copyWith(
        statusBarColor: Constants.kitGradients[24],
        //or set color with: Color(0xFF0000FF)
        statusBarIconBrightness: Brightness.dark));
    return
      Scaffold(
        body: Container(
          height: screenHeight(context, dividedBy: 1),
          width: screenWidth(context, dividedBy: 1),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Spacer(),
              Container(
                width: screenWidth(context, dividedBy: 1),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    ControlledAnimation(
                      duration: Duration(seconds: 7),
                      tween: Tween<double>(begin: 50.0, end: 100.0),
                      curve: Curves.bounceInOut,
                      builder: (context, animation) {
                        return Center(
                          child: Container(
                            width: animation,
                            height: animation,
                            decoration: BoxDecoration(),
                            // child: Image(image:AssetImage("assets/images/dog.png",),),
                          ),
                        );
                      },
                    ),
                    ControlledAnimation(
                      duration: Duration(seconds: 9),
                      tween: Tween<double>(begin: 10.0, end: 350.0),
                      curve: Curves.bounceInOut,
                      builder: (context, animation) {
                        return Center(
                          child: Container(
                              width: animation,
                              height: animation,
                              decoration: BoxDecoration(),
                              // child: Image(image:AssetImage("assets/images/WoBow.png",),)
                              child: Image(
                                image: AssetImage(
                                  "assets/images/WOWBOW_logo.png",
                                ),
                              )),
                        );
                      },
                    ),
                  ],
                ),
              ),
              Spacer(),
              Text(
                "From WeCodeLife",
                // textAlign: TextAlign.left,
                style: TextStyle(
                    color: Constants.kitGradients[30],
                    fontFamily: "Montserrat",
                    fontSize: 14,
                    fontWeight: FontWeight.w400),
              ),
              SizedBox(
                height: screenHeight(context, dividedBy: 30),
              ),
            ],
          ),
        ),
      );
  }
}
