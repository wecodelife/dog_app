import 'package:app_template/src/widgets/app_bar.dart';
import 'package:flutter/material.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:app_template/src/utils/constants.dart';
import 'package:flutter/services.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_messaging/firebase_messaging.dart';


class NotificationPage extends StatefulWidget {
  //const NotificationPage({Key? key}) : super(key: key);

  @override
  _NotificationPageState createState() => _NotificationPageState();
}

class _NotificationPageState extends State<NotificationPage> {

  // Future<void> onBackgroundMessage(RemoteMessage message) async {
  //   await Firebase.initializeApp();
  //
  //   if (message.data.containsKey('data')) {
  //     // Handle data message
  //     final data = message.data['data'];
  //   }
  //
  //   if (message.data.containsKey('notification')) {
  //     // Handle notification message
  //     final notification = message.data['notification'];
  //   }
  //   // Or do other work.
  // }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.dark.copyWith(
        statusBarColor: Constants.kitGradients[17],
        //or set color with: Color(0xFF0000FF)
        statusBarIconBrightness: Brightness.dark));
    return Scaffold(

      appBar: AppBar(
        backgroundColor: Constants.kitGradients[17],

        elevation: 0,
        leading: Container(),
        actions: [
          AppBarDogApp(
            leftIcon: GestureDetector(
              onTap: () {
                pop(context);
              },
              child: Icon(
                Icons.arrow_back,
                color: Constants.kitGradients[27],
              ),
            ),
            onTapLeftIcon: () {},
            // rightIcon: Icon(
            //   Icons.settings_sharp,
            //   color: Constants.kitGradients[27],
            // ),
            // onTapRightIcon: () {},
            title: Text(
              "Notifications",
              style: TextStyle(
                fontSize: 16,
                fontWeight: FontWeight.w700,
                fontFamily: 'Montserrat',
                color: Constants.kitGradients[27],
              ),
            ),
          ),
        ],
      ),
      body:SingleChildScrollView(
        child: Container(
          color:Constants.kitGradients[17],
          child:Column(
            children: [
              SizedBox(height: screenHeight(context, dividedBy: 50)),
              Container(
                child:ListView.builder(
                  itemCount:10,
                  shrinkWrap: true,
                  physics: NeverScrollableScrollPhysics(),
                  itemBuilder:(BuildContext context, int index) {
                    return Container(
                      width: screenWidth(context, dividedBy: 1),
                      padding: EdgeInsets.symmetric(
                        horizontal: screenWidth(context, dividedBy: 30),
                        //vertical: screenHeight(context, dividedBy: 40),
                      ),
                      color: Constants.kitGradients[17],
                      child:Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text("Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. ",
                            textAlign: TextAlign.justify,
                            style: TextStyle(
                              fontSize: 14,

                              fontWeight: FontWeight.w400,
                              fontFamily: 'Montserrat',
                              color: Constants.kitGradients[27],
                            ),),
                          SizedBox(height: screenHeight(context, dividedBy:90)),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: [
                              Text("19/02/2021", style: TextStyle(
                                fontSize: 12,
                                fontWeight: FontWeight.w400,
                                fontFamily: 'Montserrat',
                                color: Constants.kitGradients[27],
                              ),),
                              SizedBox(
                                width: screenWidth(context, dividedBy: 30),
                              ),
                            ],
                          ),
                          Divider(
                            color:
                            Constants.kitGradients[27].withOpacity(0.2),
                            // indent: 10,
                            // endIndent: 10,
                          ),
                        ],
                      )
                    );
                  }
                )
              )
            ],
          )
        ),
      )
    );
  }
}
