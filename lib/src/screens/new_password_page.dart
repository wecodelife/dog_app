import 'package:flutter/material.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:app_template/src/widgets/user_text_field.dart';
import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/screens/login_page.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/services.dart';
import 'package:app_template/src/widgets/build_button.dart';
import 'package:app_template/src/widgets/app_bar.dart';

class CreateNewPassword extends StatefulWidget {
  @override
  _CreateNewPasswordState createState() => _CreateNewPasswordState();
}

class _CreateNewPasswordState extends State<CreateNewPassword> {
  @override
  Widget build(BuildContext context) {
    return AnnotatedRegion<SystemUiOverlayStyle>(
      value: SystemUiOverlayStyle(
        statusBarColor: Colors.transparent,
      ),
      child: Scaffold(
        backgroundColor: Constants.kitGradients[17],
        body: Container(
          height: screenHeight(context, dividedBy: 1),
          width: screenWidth(context, dividedBy: 1),
          decoration: BoxDecoration(
            image: DecorationImage(
              fit: BoxFit.cover,
              image: AssetImage(Constants.BACKGROUND_IMAGE),
              colorFilter: new ColorFilter.mode(
                  Colors.black.withOpacity(0.6), BlendMode.darken),
            ),
          ),
          child: Stack(
            children: [
              Positioned(
                top: screenHeight(context, dividedBy: 40),
                child: Container(
                  height: screenHeight(context, dividedBy: 10),
                  width: screenWidth(context, dividedBy: 1),
                  child: Container(
                    height: screenHeight(context, dividedBy: 10),
                    width: screenWidth(context, dividedBy: 1),
                    child: AppBarDogApp(
                      leftIcon: Icon(
                        Icons.arrow_back_ios,
                        color: Constants.kitGradients[27],
                      ),
                      onTapLeftIcon: () {},
                      rightIcon: Icon(
                        Icons.info_outline_rounded,
                        color: Constants.kitGradients[27],
                      ),
                      onTapRightIcon: () {},
                      title: Text(
                        "Create new Password",
                        style: TextStyle(
                          fontSize: 17,
                          fontWeight: FontWeight.w600,
                          color: Constants.kitGradients[27],
                        ),
                      ),
                    ),
                  ),
                ),
              ),
              Positioned(
                top: screenHeight(context, dividedBy: 20),
                child: SingleChildScrollView(
                  child: Container(
                    width: screenWidth(context, dividedBy: 1),
                    padding: EdgeInsets.symmetric(
                      horizontal: screenWidth(context, dividedBy: 20),
                      //vertical: screenHeight(context, dividedBy: 6),
                    ),
                    child: Column(
                      children: [
                        SizedBox(
                          height: screenHeight(context, dividedBy: 20),
                        ),
                        SizedBox(
                          height: screenHeight(context, dividedBy: 20),
                        ),
                        Text(
                          "Your new password must be different from previousy used ",
                          textAlign: TextAlign.left,
                          style: TextStyle(
                              color: Constants.kitGradients[27],
                              fontFamily: 'OpenSansRegular',
                              fontSize: 16,
                              fontWeight: FontWeight.w400),
                        ),
                        SizedBox(
                          height: screenHeight(context, dividedBy: 30),
                        ),
                        FormFeildUserDetails(
                          labelText: "Current Password",
                          // isPassword: true,
                        ),
                        SizedBox(
                          height: screenHeight(context, dividedBy: 30),
                        ),
                        FormFeildUserDetails(
                          labelText: "New Password",
                          // isPassword: true,
                        ),
                        SizedBox(
                          height: screenHeight(context, dividedBy: 30),
                        ),
                        FormFeildUserDetails(
                          labelText: "Confirm Password",
                          // isPassword: true,
                        ),
                        SizedBox(
                          height: screenHeight(context, dividedBy: 20),
                        ),
                        Center(
                          child:
                          BuildButton(
                            buttonWidth: 2.5,
                            isDisabled: true,
                            title: "Reset Password",
                            onPressed: () {
                              push(context, LoginPage());
                            },
                          ),
                        )
                      ],
                    ),
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
