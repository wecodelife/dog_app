// import 'dart:io';
// import 'dart:math' as math;
//
// import 'package:flutter/material.dart';
// import 'package:flutter/rendering.dart' show ViewportOffset;
// import 'package:app_template/src/screens/home_page.dart';
// import 'package:app_template/src/screens/new_post_page.dart';
// import 'package:app_template/src/widgets/app_bar.dart';
// import 'package:app_template/src/widgets/filtered_image_list.dart';
// import 'package:app_template/src/widgets/filtered_image_widget.dart';
// import 'package:flutter/material.dart';
// import 'package:app_template/src/utils/utils.dart';
// import 'package:app_template/src/utils/constants.dart';
// import 'package:flutter/rendering.dart';
// import 'package:image_picker/image_picker.dart';
// import 'package:image/image.dart' as img;
// import 'package:photofilters/filters/filters.dart';
// import 'dart:ui' as ui;
// import 'dart:typed_data';
//
// import 'package:photofilters/filters/preset_filters.dart';
//
// @immutable
// class ExampleInstagramFilterSelection extends StatefulWidget {
//   final File selectedImage;
//   ExampleInstagramFilterSelection({this.selectedImage});
//   @override
//   _ExampleInstagramFilterSelectionState createState() =>
//       _ExampleInstagramFilterSelectionState();
// }
//
// class _ExampleInstagramFilterSelectionState
//     extends State<ExampleInstagramFilterSelection> {
//   final _filters = [
//     Colors.white,
//     ...List.generate(
//       Colors.primaries.length,
//       (index) => Colors.primaries[(index * 4) % Colors.primaries.length],
//     )
//   ];
//
//   final _filterColor = ValueNotifier<Color>(Colors.white);
//
//   void _onFilterChanged(Color value) {
//     _filterColor.value = value;
//   }
//
//   @override
//   Widget build(BuildContext context) {
//     return Material(
//       color: Colors.black,
//       child: Stack(
//         children: [
//           Positioned.fill(
//             child: _buildPhotoWithFilter(),
//           ),
//           Positioned(
//             left: 0.0,
//             right: 0.0,
//             bottom: 0.0,
//             child: _buildFilterSelector(widget.selectedImage),
//           ),
//           Positioned(
//               child: GestureDetector(onTap: () {}, child: Icon(Icons.check)))
//         ],
//       ),
//     );
//   }
//
//   Widget _buildPhotoWithFilter() {
//     return ValueListenableBuilder(
//       valueListenable: _filterColor,
//       builder: (context, value, child) {
//         final color = value as Color;
//         return Image.file(
//           widget.selectedImage,
//           color: color.withOpacity(0.5),
//           colorBlendMode: BlendMode.color,
//           fit: BoxFit.cover,
//         );
//       },
//     );
//   }
//
//   Widget _buildFilterSelector(File image) {
//     return FilterSelector(
//       onFilterChanged: _onFilterChanged,
//       filters: _filters,
//       image: image,
//     );
//   }
// }
//
// @immutable
// class FilterSelector extends StatefulWidget {
//   const FilterSelector({
//     this.image,
//     this.filters,
//     this.onFilterChanged,
//     this.padding = const EdgeInsets.symmetric(vertical: 24.0),
//   });
//
//   final List<Color> filters;
//   final void Function(Color selectedColor) onFilterChanged;
//   final EdgeInsets padding;
//   final File image;
//
//   @override
//   _FilterSelectorState createState() => _FilterSelectorState();
// }
//
// class _FilterSelectorState extends State<FilterSelector> {
//   static const _filtersPerScreen = 5;
//   static const _viewportFractionPerItem = 1.0 / _filtersPerScreen;
//
//   PageController _controller = new PageController();
//   int _page;
//
//   int get filterCount => widget.filters.length;
//
//   Color itemColor(int index) => widget.filters[index % filterCount];
//
//   @override
//   void initState() {
//     super.initState();
//     _page = 0;
//     _controller = PageController(
//       initialPage: _page,
//       viewportFraction: _viewportFractionPerItem,
//     );
//     _controller.addListener(_onPageChanged);
//   }
//
//   void _onPageChanged() {
//     final page = (_controller.page ?? 0).round();
//     if (page != _page) {
//       _page = page;
//       widget.onFilterChanged(widget.filters[page]);
//     }
//   }
//
//   img.Image image;
//   Filter filter = presetFiltersList.first;
//   final GlobalKey _globalKey = GlobalKey();
//   Uint8List uint8list;
//
//   void _onFilterTapped(int index) async {
//     _controller.animateToPage(
//       index,
//       duration: const Duration(milliseconds: 450),
//       curve: Curves.ease,
//     );
//
//     RenderRepaintBoundary repaintBoundary =
//         _globalKey.currentContext.findRenderObject();
//     ui.Image boxImage = await repaintBoundary.toImage(pixelRatio: 1);
//     ByteData byteData =
//         await boxImage.toByteData(format: ui.ImageByteFormat.png);
//     uint8list = byteData.buffer.asUint8List();
//     // You can now use this to save the Image to Local Storage or upload it to a Remote Server.
//   }
//
//   @override
//   void dispose() {
//     _controller.dispose();
//     super.dispose();
//   }
//
//   @override
//   Widget build(BuildContext context) {
//     return Scrollable(
//       controller: _controller,
//       axisDirection: AxisDirection.right,
//       physics: PageScrollPhysics(),
//       viewportBuilder: (context, viewportOffset) {
//         return LayoutBuilder(
//           builder: (context, constraints) {
//             final itemSize = constraints.maxWidth * _viewportFractionPerItem;
//             viewportOffset
//               ..applyViewportDimension(constraints.maxWidth)
//               ..applyContentDimensions(0.0, itemSize * (filterCount - 1));
//
//             return Stack(
//               alignment: Alignment.bottomCenter,
//               children: [
//                 _buildShadowGradient(itemSize),
//                 _buildCarousel(
//                   viewportOffset: viewportOffset,
//                   itemSize: itemSize,
//                 ),
//                 _buildSelectionRing(itemSize, widget.image),
//               ],
//             );
//           },
//         );
//       },
//     );
//   }
//
//   Widget _buildShadowGradient(double itemSize) {
//     return SizedBox(
//       height: itemSize * 2 + widget.padding.vertical,
//       child: const DecoratedBox(
//         decoration: BoxDecoration(
//           gradient: LinearGradient(
//             begin: Alignment.topCenter,
//             end: Alignment.bottomCenter,
//             colors: [
//               Colors.transparent,
//               Colors.black,
//             ],
//           ),
//         ),
//         child: SizedBox.expand(),
//       ),
//     );
//   }
//
//   Widget _buildCarousel({
//     ViewportOffset viewportOffset,
//     double itemSize,
//   }) {
//     return Container(
//       height: itemSize,
//       margin: widget.padding,
//       child: Flow(
//         delegate: CarouselFlowDelegate(
//           viewportOffset: viewportOffset,
//           filtersPerScreen: _filtersPerScreen,
//         ),
//         children: [
//           for (int i = 0; i < filterCount; i++)
//             FilterItem(
//               onFilterSelected: () => _onFilterTapped(i),
//               color: itemColor(i),
//             ),
//         ],
//       ),
//     );
//   }
//
//   Widget _buildSelectionRing(double itemSize, File image) {
//     return IgnorePointer(
//       child: Padding(
//         padding: widget.padding,
//         child: SizedBox(
//           width: itemSize,
//           height: itemSize,
//           child: DecoratedBox(
//             decoration: BoxDecoration(
//               shape: BoxShape.circle,
//               border: Border.fromBorderSide(
//                 BorderSide(width: 6.0, color: Colors.white),
//               ),
//             ),
//             child: ClipRRect(child: Image.file(image)),
//           ),
//         ),
//       ),
//     );
//   }
// }
//
// class CarouselFlowDelegate extends FlowDelegate {
//   CarouselFlowDelegate({
//     this.viewportOffset,
//     this.filtersPerScreen,
//   }) : super(repaint: viewportOffset);
//
//   final ViewportOffset viewportOffset;
//   final int filtersPerScreen;
//
//   @override
//   void paintChildren(FlowPaintingContext context) {
//     final count = context.childCount;
//
//     // All available painting width
//     final size = context.size.width;
//
//     // The distance that a single item "page" takes up from the perspective
//     // of the scroll paging system. We also use this size for the width and
//     // height of a single item.
//     final itemExtent = size / filtersPerScreen;
//
//     // The current scroll position expressed as an item fraction, e.g., 0.0,
//     // or 1.0, or 1.3, or 2.9, etc. A value of 1.3 indicates that item at
//     // index 1 is active, and the user has scrolled 30% towards the item at
//     // index 2.
//     final active = viewportOffset.pixels / itemExtent;
//
//     // Index of the first item we need to paint at this moment.
//     // At most, we paint 3 items to the left of the active item.
//     final min = math.max(0, active.floor() - 3).toInt();
//
//     // Index of the last item we need to paint at this moment.
//     // At most, we paint 3 items to the right of the active item.
//     final max = math.min(count - 1, active.ceil() + 3).toInt();
//
//     // Generate transforms for the visible items and sort by distance.
//     for (var index = min; index <= max; index++) {
//       final itemXFromCenter = itemExtent * index - viewportOffset.pixels;
//       final percentFromCenter = 1.0 - (itemXFromCenter / (size / 2)).abs();
//       final itemScale = 0.5 + (percentFromCenter * 0.5);
//       final opacity = 0.25 + (percentFromCenter * 0.75);
//
//       final itemTransform = Matrix4.identity()
//         ..translate((size - itemExtent) / 2)
//         ..translate(itemXFromCenter)
//         ..translate(itemExtent / 2, itemExtent / 2)
//         ..multiply(Matrix4.diagonal3Values(itemScale, itemScale, 1.0))
//         ..translate(-itemExtent / 2, -itemExtent / 2);
//
//       context.paintChild(
//         index,
//         transform: itemTransform,
//         opacity: opacity,
//       );
//     }
//   }
//
//   @override
//   bool shouldRepaint(covariant CarouselFlowDelegate oldDelegate) {
//     return oldDelegate.viewportOffset != viewportOffset;
//   }
// }
//
// @immutable
// class FilterItem extends StatelessWidget {
//   FilterItem({
//     this.color,
//     this.onFilterSelected,
//     this.selectedImage,
//   });
//
//   final Color color;
//   final VoidCallback onFilterSelected;
//   final File selectedImage;
//
//   @override
//   Widget build(BuildContext context) {
//     return GestureDetector(
//       onTap: onFilterSelected,
//       child: AspectRatio(
//         aspectRatio: 1.0,
//         child: Padding(
//           padding: const EdgeInsets.all(8.0),
//           child: ClipOval(
//             child: Image.file(
//               selectedImage,
//               // 'https://flutter.dev/docs/cookbook/img-files/effects/instagram-buttons/millenial-texture.jpg',
//               color: color.withOpacity(0.5),
//               colorBlendMode: BlendMode.hardLight,
//             ),
//           ),
//         ),
//       ),
//     );
//   }
// }

import 'dart:io';
import 'dart:typed_data';
import 'dart:ui' as ui;

import 'package:app_template/src/screens/new_post_page.dart';
import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/object_factory.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:app_template/src/widgets/app_bar.dart';
import 'package:app_template/src/widgets/filters.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/services.dart';
import 'package:path_provider/path_provider.dart';

class ColoredFilter extends StatefulWidget {
  //const ColoredFilter({Key? key}) : super(key: key);
  final File pickedFile;
  ColoredFilter({this.pickedFile});

  @override
  _ColoredFilterState createState() => _ColoredFilterState();
}

ColorFilter selectedFilter = ColorFilter.matrix(filters[0]);

class _ColoredFilterState extends State<ColoredFilter> {
  final GlobalKey _globalKey = GlobalKey();
  File convertedImage;
  double height;
  double width;
  void convertWidgetToImage() async {
    RenderRepaintBoundary repaintBoundary =
        _globalKey.currentContext.findRenderObject();
    ui.Image boxImage = await repaintBoundary.toImage(pixelRatio: 1);
    ByteData byteData =
        await boxImage.toByteData(format: ui.ImageByteFormat.png);
    Uint8List uint8list = byteData.buffer.asUint8List();

    final directory = (await getApplicationDocumentsDirectory())
        .path; // to get path of the file
    String fileName = DateTime.now()
        .toIso8601String(); // the name needs to be unique every time you take a screenshot
    var path = '$directory/$fileName.png';
    File image = await File(path).writeAsBytes(uint8list);
    print("Image Path  " + image.toString());
    push(
        context,
        NewPostPage(
          profileImage: ObjectFactory().appHive.getProfilePic(),
          userName: ObjectFactory().appHive.getName(),
          filteredImage: uint8list,
          imageFile: image,
        ));
  }

  void getSize(File img) async {
    var decodedImage = await decodeImageFromList(img.readAsBytesSync());
    if (decodedImage.width > decodedImage.height) {
      setState(() {
        height = screenWidth(
              context,
            ) *
            (decodedImage.height.toDouble() / decodedImage.width.toDouble());
      });
    } else {
      setState(() {
        height = screenHeight(context,
            dividedBy: (decodedImage.height.toDouble() /
                decodedImage.width.toDouble()));
      });
    }
    // setState(() {
    //   height = decodedImage.height.toDouble();
    //   width = decodedImage.width.toDouble();
    // });
    print("a" + decodedImage.width.toString());
    print("d" + decodedImage.height.toString());
    print("a" + screenHeight(context).toString());
    print("d" + screenWidth(context).toString());
    print("w" + width.toString());
    print("g" + height.toString());
  }

  Future<void> writeToFile(ByteData data, String path) {
    final buffer = data.buffer;
    return new File(path).writeAsBytes(
        buffer.asUint8List(data.offsetInBytes, data.lengthInBytes));
  }

  @override
  void initState() {
    getSize(widget.pickedFile);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Constants.kitGradients[17],
      appBar: AppBar(
        elevation: 1,
        backgroundColor: Constants.kitGradients[17],
        actions: [
          AppBarDogApp(
            leftIcon: Icon(
              Icons.arrow_back,
              color: Constants.kitGradients[27],
            ),
            onTapLeftIcon: () {
              pop(context);
            },
            tickIcon: Container(
              width: screenWidth(context,
                  dividedBy: 6.6),
              height: screenHeight(context,
                  dividedBy: 15),
              // color: Constants.kitGradients[28],
              child: Icon(
                Icons.done,
                color: Constants.kitGradients[27],
              ),
            ),
            onTapTickIcon: () {
              convertWidgetToImage();
            },
            title: Text(
              "Edit photo",
              style: TextStyle(
                fontSize: 17,
                fontFamily: 'Montserrat',
                fontWeight: FontWeight.w600,
                color: Constants.kitGradients[27],
              ),
            ),
          )
        ],
        leading: Container(),
      ),
      body: height != null
          ? SingleChildScrollView(
              child: Container(
                width: screenWidth(context, dividedBy: 1),
                // height: screenHeight(context, dividedBy: 1.2),
                // constraints: BoxConstraints(
                //   maxWidth: screenWidth(context, dividedBy: 1),
                //   maxHeight: screenHeight(context, dividedBy: 1),
                // ),
                child: Column(
                  children: [
                    SizedBox(
                      height: screenHeight(context,
                          dividedBy:
                              height < screenHeight(context, dividedBy: 1.5)
                                  ? 5
                                  : 90),
                    ),
                    Container(
                         width: screenWidth(context, dividedBy: 1),
                         height: height < screenHeight(context, dividedBy: 2.5)
                             ? screenHeight(context, dividedBy: 3.5)
                            :
                        screenHeight(context, dividedBy: 2),
                        //color: Colors.yellow,
                        child:
                            // PageView.builder(
                            //                   itemCount: filters.length,
                            //                   physics: new NeverScrollableScrollPhysics(),
                            //                   itemBuilder: (context, index) {
                            //                     return
                            RepaintBoundary(
                      key: _globalKey,
                      child: Container(
                        width: screenWidth(context, dividedBy: 1),
                        height:height,
                       // screenWidth(context, dividedBy: 1.2),
                        // margin: EdgeInsets.symmetric(
                        //   horizontal: screenWidth(context, dividedBy: 30),
                        //   //vertical: screenHeight(context, dividedBy: 30),
                        // ),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(4),
                          //   color:
                          //       Constants.kitGradients[19].withOpacity(0.3),
                        ),
                        child: ClipRRect(
                          borderRadius: BorderRadius.circular(4),
                          child: ColorFiltered(
                            colorFilter: selectedFilter,
                            // child: AspectRatio(
                            //   aspectRatio: height / width,
                            child: Image.file(
                              widget.pickedFile,
                              fit: BoxFit.fitWidth,
                              // width: screenWidth(context, dividedBy: 1),
                              // height: height,
                              // screenWidth(context, dividedBy: 1.2),
                              // ),
                            ),
                          ),
                        ),
                      ),
                    )
                        //   },
                        // ),
                        ),
                    SizedBox(
                      height: screenHeight(context,
                          dividedBy:
                              height < screenHeight(context, dividedBy: 1.5)
                                  ? 5
                                  : 90),
                    ),
                    Container(
                        child: Container(
                      //width: screenWidth(context, dividedBy: 1),
                      height: screenHeight(context, dividedBy: 4),
                      padding: EdgeInsets.symmetric(
                          vertical: screenHeight(context, dividedBy: 60)),
                      // color: Colors.red,
                      child: ListView.builder(
                        scrollDirection: Axis.horizontal,
                        itemCount: filters.length,
                        shrinkWrap: true,
                        itemBuilder: (BuildContext context, int index) {
                          return GestureDetector(
                            onTap: () {
                              setState(() {
                                selectedFilter =
                                    ColorFilter.matrix(filters[index]);
                              });
                            },
                            child: Container(
                              width: screenWidth(context, dividedBy: 4),
                              //height: screenWidth(context, dividedBy: 4),
                              margin: EdgeInsets.all(
                                screenWidth(context, dividedBy: 80),
                              ),
                              // decoration: BoxDecoration(
                              //   color: Constants.kitGradients[19],
                              //   borderRadius: BorderRadius.circular(
                              //       screenWidth(context, dividedBy: 4) / 2),
                              // ),
                              child: Column(
                                mainAxisSize: MainAxisSize.max,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  ClipRRect(
                                    borderRadius: BorderRadius.circular(
                                        screenWidth(context, dividedBy: 4) / 2),
                                    child: ColorFiltered(
                                      colorFilter:
                                          ColorFilter.matrix(filters[index]),
                                      child: Image.file(
                                        widget.pickedFile,
                                        fit: BoxFit.cover,
                                        width:
                                            screenWidth(context, dividedBy: 4),
                                        height:
                                            screenWidth(context, dividedBy: 4),
                                      ),
                                    ),
                                  ),
                                  SizedBox(
                                    height:
                                        screenHeight(context, dividedBy: 80),
                                  ),
                                  Text(
                                    filtersName[index],
                                    textAlign: TextAlign.center,
                                    style: TextStyle(
                                      fontSize: 15,
                                      fontWeight: FontWeight.bold,
                                      fontFamily: "Montserrat",
                                      color: Constants.kitGradients[27],
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          );
                        },
                      ),
                    ))
                  ],
                ),
              ),
            )
          : Container(),
    );
  }
}
