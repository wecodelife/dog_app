import 'package:app_template/src/bloc/user_bloc.dart';
import 'package:app_template/src/models/get_breed_response_model.dart';
import 'package:app_template/src/screens/onboarding_gender.dart';
import 'package:app_template/src/screens/onboarding_image.dart';
import 'package:app_template/src/widgets/onboarding_content.dart';
import 'package:app_template/src/widgets/slide_transition.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:app_template/src/utils/constants.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:app_template/src/models/user_update_request_model.dart';

class OnBoardingBreeds extends StatefulWidget {
  //const OnBoardingBreeds({Key? key}) : super(key: key);
  final UserUpdateRequestModel updateRequestModel;

  OnBoardingBreeds({this.updateRequestModel});
  @override
  _OnBoardingBreedsState createState() => _OnBoardingBreedsState();
}

class _OnBoardingBreedsState extends State<OnBoardingBreeds> {
  // List<String> breed = [];
  UserBloc userBloc = UserBloc();
  int selectedValue;
  bool loading = false;
  List<Result> results;
  UserUpdateRequestModel updateRequest;
  String breedSelected = "Select";
  bool openModal = false;
  bool initialItem = false;
  showPicker() {
    showModalBottomSheet(
        backgroundColor: Constants.kitGradients[17],
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.only(
                topLeft: Radius.circular(8), topRight: Radius.circular(8))),
        context: context,
        builder: (BuildContext context) {
          return Container(
            height: screenHeight(context, dividedBy: 2.4),
            padding: EdgeInsets.symmetric(
              horizontal: screenWidth(context, dividedBy: 30),
              vertical: screenHeight(context, dividedBy: 40),
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  children: [
                    GestureDetector(
                      onTap: () {
                        pop(context);
                        breedTextEditingController.clear();
                      },
                      child: Text(
                        "Cancel",
                        style: TextStyle(
                          fontSize: 18,
                          fontWeight: FontWeight.w500,
                          fontFamily: 'Prompt-Ligh',
                          color: Constants.kitGradients[27],
                        ),
                      ),
                    ),
                    Spacer(),
                    GestureDetector(
                      onTap: () {
                        pop(context);
                        setState(() {

                          initialItem = selectedValue == 0 ? true : false;
                        });

                      },
                      child: Text(
                        "Select",
                        style: TextStyle(
                          fontSize: 18,
                          fontWeight: FontWeight.w500,
                          fontFamily: 'Prompt-Ligh',
                          color: Constants.kitGradients[27],
                        ),
                      ),
                    ),
                  ],
                ),
                SizedBox(
                  height: screenHeight(context, dividedBy: 30),
                ),
                Container(
                  height: screenHeight(context, dividedBy: 3.5),
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(8),
                          topRight: Radius.circular(8))),
                  child: ClipRRect(
                    borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(8),
                        topRight: Radius.circular(8)),
                    child: CupertinoPicker(
                      backgroundColor: Constants.kitGradients[17],
                      diameterRatio: 10,
                      looping: false,
                      // scrollController: FixedExtentScrollController(initialItem: 1),
                      onSelectedItemChanged: (value) {
                        setState(() {
                          selectedValue = value;
                          breedTextEditingController.text = results[value].name.toString();
                          print(selectedValue);
                        });
                      },
                      itemExtent: screenHeight(context, dividedBy: 16),
                      children: results.map((item) {
                        return Container(
                            alignment: Alignment.center,
                            padding: EdgeInsets.symmetric(
                              horizontal: screenWidth(context, dividedBy: 30),
                            ),
                            child: Text(
                              item.name,
                              style: TextStyle(
                                fontSize: 18,
                                fontWeight: FontWeight.w400,
                                fontFamily: 'Prompt-Ligh',
                                color: Constants.kitGradients[27],
                              ),
                            ));
                      }).toList(),
                    ),
                  ),
                )
              ],
            ),
          );
        });
  }

  String breedType;
  TextEditingController breedTextEditingController =
      new TextEditingController();
  @override
  void initState() {
    userBloc.getBreed();
    print("dsgfdg" + widget.updateRequestModel.dateOfBirth.toString());
    print("worked well");
    userBloc.getBreedResponse.listen((event) {
      results = event.results;
      setState(() {
        loading = true;
      });
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Constants.kitGradients[17],
      body: Container(
        child: OnBoardingContent(
          caption: "What is your Dog's Breed Type ?",
          image:
              "https://images.unsplash.com/photo-1510771463146-e89e6e86560e?ixid=MnwxMjA3fDB8MHxzZWFyY2h8MTN8fGRvZ3MlMjBjYXJ0b29uc3xlbnwwfHwwfHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=700&q=60",
          buttonTitle: "Next",
          currentIndex: 2,
          labelText:openModal == true ?  results[0].name.toString() : breedSelected,
          isGender: false,
          hasSuffix: true,
          readOnly: breedTextEditingController.text == "Other" ? false : true,
          textEditingController: breedTextEditingController,
          suffixIcon: Icon(
            Icons.arrow_drop_down,
            color: Constants.kitGradients[19],
          ),
          onTapIcon: () {
            if (loading == true) {
              setState(() {
                openModal = true;
                breedTextEditingController.text = results[0].name.toString();
              });
              openModal == true ? print("BottomSheet Opened") : showPicker();
              print(selectedValue);
            } else
              CircularProgressIndicator();
          },
          onTapField: () {
            if (loading == true) {
             setState((){
               openModal = true;
               breedTextEditingController.text = results[0].name.toString();
             });
             openModal == false ? print("BottomSheet Opened") : showPicker();
            } else
              CircularProgressIndicator();
          },
          onButtonPressed: () {
            print("breeedddd  selected  "+selectedValue.toString());
            updateRequest = UserUpdateRequestModel(
                firstName: widget.updateRequestModel.firstName,
                // lastName: widget.updateRequestModel.lastName,
                name: widget.updateRequestModel.name,
                dateOfBirth: widget.updateRequestModel.dateOfBirth,
                breedType: selectedValue == null ? 1 : selectedValue,
                phoneNumber: widget.updateRequestModel.phoneNumber,
                uid: widget.updateRequestModel.uid,
                description: widget.updateRequestModel.description,
                username: widget.updateRequestModel.username,
                isUserHasDog: widget.updateRequestModel.isUserHasDog);
            // Navigator.of(context)
            //     .push(SlideRightRoute(page: OnBoardingGender(updateRequestModel: updateRequest,)));
            push(context, OnBoardingGender(updateRequestModel: updateRequest));
          },
        ),
      ),
    );
  }
}
