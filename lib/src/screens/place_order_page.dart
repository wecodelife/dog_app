import 'package:app_template/src/models/get_cart_details.dart';
import 'package:app_template/src/utils/urls.dart';
import 'package:app_template/src/screens/add_address.dart';
import 'package:app_template/src/widgets/build_button.dart';
import 'package:app_template/src/widgets/estimate_product_delivery.dart';
import 'package:flutter/material.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:app_template/src/utils/constants.dart';
import 'package:flutter/services.dart';
import 'package:app_template/src/bloc/user_bloc.dart';
import 'package:app_template/src/models/get_product_details.dart';

class PlaceOrderPage extends StatefulWidget {
  // const PaceOrderPage({Key? key}) : super(key: key);
  //final List<Result> items;
  // final List<int> itemCount;
  final String itemImage;
  PlaceOrderPage({
    this.itemImage,
  });


  @override
  _PlaceOrderPageState createState() => _PlaceOrderPageState();
}

class _PlaceOrderPageState extends State<PlaceOrderPage> {
  double sum = 0;
  List<String> priceList=[];
  List<String> priceDetails = [
    "Total Price",
    "Discount Price",
    "Delivery Charge",
    "Total Amount"
  ];
  UserBloc userBloc = UserBloc();
  @override
  void initState() {
    // TODO: implement initState
    // for(int i = 1; i <= widget.items.length ; i ++) {
    //   sum= sum + double.parse(widget.items[i].sellingPrice);
    // }
    // print("total Sum: "+ sum.toString());
    userBloc.getProductDetails();
    userBloc.getCartDetails();
    userBloc.getCartDetailsResponse.listen((event) {});

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.dark.copyWith(
        statusBarColor: Constants.kitGradients[17],
        //or set color with: Color(0xFF0000FF)
        statusBarIconBrightness: Brightness.dark));
    return Scaffold(
      backgroundColor: Constants.kitGradients[17],
      appBar: AppBar(
        //elevation: 0,
        backgroundColor: Constants.kitGradients[17],
        leading: GestureDetector(
          onTap: () {
            pop(context);
          },
          child: Icon(
            Icons.arrow_back,
            color: Constants.kitGradients[27],
          ),
        ),
        title: Container(
          child: Row(
            children: [
              Text(
                "Order Summary",
                style: TextStyle(
                  //fontSize: 16,
                  fontWeight: FontWeight.w700,
                  color: Constants.kitGradients[27],
                ),
              ),
              Spacer(),
              GestureDetector(
                  onTap: () {
                    // push(context, WishlistPage());
                  },
                  child: Icon(
                    Icons.favorite_outline,
                    color: Constants.kitGradients[27],
                  ))
            ],
          ),
        ),
      ),
      body: Stack(
        children:[
          SingleChildScrollView(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(height: screenHeight(context,
                    dividedBy: 40),),
                Center(
                  child: Container(
                   // color: Constants.kitGradients[19].withOpacity(0.12),
                    width: screenWidth(context,
                        dividedBy: 1.15),
                    height: screenHeight(context,
                        dividedBy: 4),
                    decoration: BoxDecoration(
                      color: Constants.kitGradients[19].withOpacity(0.12),
                      borderRadius: BorderRadius.circular(7),
                    ),
                    child: Padding(
                      padding: const EdgeInsets.only(left:18.0,top: 14),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                        Text("Megha T M",
                          style: TextStyle(
                          fontSize: 16,
                            fontFamily: 'Prompt-Light',
                          //fontWeight: FontWeight.w700,
                          color: Constants.kitGradients[27],
                        ),),
                          Text("Thadathil House, Thangaloor P O",
                            style: TextStyle(
                              fontSize: 16,
                              fontFamily: 'Prompt-Light',
                              //fontWeight: FontWeight.w700,
                              color: Constants.kitGradients[27],
                            ),),
                          Text("Thrissur, Kerala",
                            style: TextStyle(
                              fontSize: 16,
                              fontFamily: 'Prompt-Light',
                              //fontWeight: FontWeight.w700,
                              color: Constants.kitGradients[27],
                            ),),
                          Text("Mobile: 7510256951",
                            style: TextStyle(
                              fontSize: 16,
                              fontFamily: 'Prompt-Light',
                              //fontWeight: FontWeight.w700,
                              color: Constants.kitGradients[27],
                            ),),
                        Spacer(),
                        Padding(
                          padding: const EdgeInsets.only(left:15.0),
                          child: BuildButton(
                            buttonWidth: 1.4,
                            buttonHeight: 19.5,
                            isDisabled: true,
                            title: "CHANGE OR ADD ADDRESS",
                            onPressed: () {
                              push(context, AddAddress());
                            },

                          ),
                        ),
                        SizedBox(height: screenHeight(context,
                            dividedBy: 70),),
                      ],),
                    ),
                  ),
                ),
                SizedBox(height: screenHeight(context,
                    dividedBy: 40),),
            Padding(
              padding: const EdgeInsets.only(left:28.0),
              child: Text("DELIVERY ESTIMATE",
                style: TextStyle(
                fontSize: 16,
                fontWeight: FontWeight.w700,
                color: Constants.kitGradients[27],
              ),),
            ),
                SizedBox(height: screenHeight(context,
                    dividedBy: 60),),
                StreamBuilder<GetCartDetails>(
                  stream: userBloc.getCartDetailsResponse,
                  builder: (context, snapshot) {
                    return snapshot.hasData
                        ?
                      Column(
                      children: [
                        ListView.builder(
    shrinkWrap: true,
    physics: NeverScrollableScrollPhysics(),
    itemCount: snapshot.data.results.length,
    itemBuilder:
    (BuildContext context, int index) {
        return Column(
          children: [
            EstimateProductDelivery(
              itemImage: Urls.imageBaseUrl +snapshot.data.results[index].productImage,
            ),
            SizedBox(height: screenHeight(context,
                dividedBy: 80),),
          ],
        );
    }),
                      ],
                    ):
                   Container() ;
                  }
                ),
    ],
        )
    ),
        //  Spacer(),
          Align(
            alignment: Alignment.bottomCenter,
            child: Container(
              width: screenWidth(context, dividedBy: 1),
              decoration: BoxDecoration(
                  boxShadow: [
                    BoxShadow(
                      color: Constants.kitGradients[17].withOpacity(0.5),
                      offset: Offset(0.0, -10.0), //(x,y)
                      //blurRadius: 6.0,
                    ),
                  ],
              ),
              child:  Padding(
                padding: const EdgeInsets.all(18.0),
                child: BuildButton(
                  buttonWidth: 2.4,
                  buttonHeight: 15,
                  isDisabled: true,
                  title: "CONTINUE",
                  onPressed: () {
                    push(context, PlaceOrderPage(itemImage: widget.itemImage,));
                  },

                ),
              ),
            ),
          ),
          //Spacer(),
          // Align(
          //   alignment: Alignment.bottomCenter,
          //   child: Padding(
          //     padding: const EdgeInsets.only(right: 12,left: 15),
          //     child: Row(
          //       children: [
          //         Column(
          //           children: [
          //             Text("₹ 1599",
          //               style: TextStyle(
          //                 color: Constants.kitGradients[27],
          //                 fontWeight: FontWeight.bold,
          //                 fontFamily: 'Prompt-Light',
          //                 fontSize: 20,
          //               ),),
          //             Text("View Details",
          //               style: TextStyle(
          //                 color: Constants.kitGradients[27],
          //                 fontWeight: FontWeight.bold,
          //                 fontFamily: 'Prompt-Light',
          //                 fontSize: 12,
          //               ),)
          //           ],
          //         ),
          //         Spacer(),
          //         Container(
          //           width: screenWidth(context, dividedBy: 1.6),
          //           height: screenHeight(context, dividedBy: 9),
          //           color: Constants.kitGradients[17],
          //           padding: EdgeInsets.symmetric(
          //             horizontal: screenWidth(context, dividedBy: 30),
          //             vertical: screenHeight(context, dividedBy: 40),
          //           ),
          //           child: BuildButton(
          //             buttonWidth: 1.5,
          //             buttonHeight: 15,
          //             isDisabled: true,
          //             title: "Confirm Order",
          //             onPressed: () {
          //              // push(context, PlaceOrderPage(items: selectedItems));
          //             },
          //           ),
          //         ),
          //       ],
          //     ),
          //   ),
          // )

    ]
      ),
    );
  }
}
