import 'package:app_template/src/bloc/user_bloc.dart';
import 'package:app_template/src/models/login_request_model.dart';
import 'package:app_template/src/screens/home_page.dart';
import 'package:app_template/src/screens/otp_page.dart';
import 'package:app_template/src/screens/sign_up_page.dart';
import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/object_factory.dart';
import 'package:app_template/src/utils/urls.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:app_template/src/widgets/build_button.dart';
import 'package:app_template/src/widgets/country_code.dart';
import 'package:app_template/src/widgets/country_code_picker.dart';
import 'package:app_template/src/widgets/onboarding_button.dart';
import 'package:app_template/src/widgets/onboarding_textfield.dart';
import 'package:app_template/src/widgets/user_text_field.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:sms_autofill/sms_autofill.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  TextEditingController mobileTextEditingController =
      new TextEditingController();
  TextEditingController otpTextEditingController = TextEditingController();
  String code = "+91";
  bool isLoading = true;
  int id;
  UserBloc userBloc = UserBloc();
  bool otpSend = false;
  String _code = "";

  void _onCountryChange(CountryCode countryCode) {
    //TODO : manipulate the selected country code here
    print("New Country selected: " + countryCode.toString());
    code = countryCode.toString();
  }

  FirebaseAuth _auth = FirebaseAuth.instance;

  String verificationId;

  bool showLoading = false;

  void signInWithPhoneAuthCredential(
      PhoneAuthCredential phoneAuthCredential) async {
    setState(() {
      showLoading = true;
    });

    try {
      final authCredential =
          await _auth.signInWithCredential(phoneAuthCredential);

      setState(() {
        showLoading = false;
      });

      if (authCredential?.user != null) {
        print("Firebase login success ");
        await userBloc.login(
            loginRequest: LoginRequest(uid: _auth.currentUser.uid));
        // updateRequestModel = UserUpdateRequestModel(
        //     firstName:
        //     widget.firstName,
        //     lastName:
        //     widget.lastName,
        //     phoneNumber:mobileTextEditingController.text );
        // push(
        //     context,
        //     HaveADogPage(
        //       updateRequestModel:
        //       updateRequestModel,
        //     ));
        // Navigator.push(context, MaterialPageRoute(builder: (context)=> HomeScreen()));
      }
    } on FirebaseAuthException catch (e) {
      setState(() {
        showLoading = false;
      });
    }
  }

  final FirebaseAuth auth = FirebaseAuth.instance;
  // final uid = FirebaseAuth.instance.currentUser.uid;
  //final  Uid uid = auth.currentUser.uid;
  detailsCheck() {
    if (mobileTextEditingController.text != "") {
      print(code);
      User user = FirebaseAuth.instance.currentUser;
      FirebaseAuth.instance.verifyPhoneNumber(
        phoneNumber: code + mobileTextEditingController.text,
        timeout: Duration(seconds: 60),
        verificationCompleted: (AuthCredential authCredential) {
          FirebaseAuth.instance
              .signInWithCredential(authCredential)
              .then((value) async {
            // emit(PhoneVerified(phone: "+91" + phoneNumber));
            print("firebaseauth" + authCredential.signInMethod);
            await userBloc.login(
                loginRequest: LoginRequest(
                    uid: auth.currentUser.uid)); //294VytGXCSZBXnMVUIAzmVzl3el2
            print("token=======>" + authCredential.token.toString());
          });
        },
        verificationFailed: (FirebaseAuthException authException) {
          setState(() {
            isLoading = false;
          });
          print(authException.toString());
          // showSnackbar(authException.message);
        },
        codeAutoRetrievalTimeout: (value) {
          print("code auto retrieval timeout  :  " + value);
          setState(() {
            isLoading = false;
          });
          // emit(PhoneVerified(verificationId: value, phone: "+91" + phoneNumber));
        },
        codeSent: (value, [data]) {
          print(value);
          showSuccessAnimatedToast(
              msg: "OTP generated sucessfully", context: context);
          push(
              context,
              OTP_Page(
                vId: value,
                mobileNumber: mobileTextEditingController.text,
                newUser: false,
              ));
        },
      );
    } else {
      showToast("Please enter valid mobile number");
    }
  }

  @override
  void initState() {
    userBloc.loginResponse.listen((event) {
      if (event.status == 200 || event.status == 201) {
        if (event.data.userData.firstName != '') {
          ObjectFactory().appHive.putToken(token: event.data.token);
          ObjectFactory().appHive.putName(name: event.data.userData.firstName);
          ObjectFactory().appHive.putProfilePic(
              Urls.imageBaseUrl + event.data.userData.profileImage);
          ObjectFactory()
              .appHive
              .putUserId(userId: event.data.userData.id.toString());
          print("Login Registration userid   " +
              event.data.userData.id.toString());
          print(
              "Login Registration UserName   " + event.data.userData.firstName);
          setState(() {
            showLoading = false;
            print("Success");
          });
          Navigator.of(context).pushAndRemoveUntil(
              MaterialPageRoute(builder: (context) => HomePage()),
              (Route<dynamic> route) => false);
        } else {
          showErrorAnimatedToast(msg: "User not registered", context: context);
        }
      } else {
        showErrorAnimatedToast(msg: "something went wrong", context: context);
      }
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return AnnotatedRegion<SystemUiOverlayStyle>(
      value: SystemUiOverlayStyle(
        statusBarColor: Colors.transparent,
      ),
      child: Scaffold(
        backgroundColor: Constants.kitGradients[17],
        body: otpSend == false
            ? Container(
                height: screenHeight(context, dividedBy: 1),
                width: screenWidth(context, dividedBy: 1),
                decoration: BoxDecoration(
                  color: Constants.kitGradients[17],
                  image: DecorationImage(
                    fit: BoxFit.cover,
                    image: AssetImage(Constants.BACKGROUND_IMAGE),
                    colorFilter: new ColorFilter.mode(
                        Colors.black.withOpacity(0.6), BlendMode.darken),
                  ),
                ),
                child: SingleChildScrollView(
                  child: Container(
                    width: screenWidth(context, dividedBy: 1),
                    padding: EdgeInsets.symmetric(
                      horizontal: screenWidth(context, dividedBy: 20),
                      vertical: screenHeight(context, dividedBy: 6),
                    ),
                    child: Center(
                      child: Column(
                        //mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          SizedBox(
                            height: screenHeight(context, dividedBy: 10),
                          ),
                          Text(
                            "Hey Welcome Back, \nLogin Now",
                            textAlign: TextAlign.left,
                            style: TextStyle(
                                color: Constants.kitGradients[27],
                                fontFamily: "Montserrat-ExtraBold",
                                fontSize: 24,
                                fontWeight: FontWeight.bold),
                          ),
                          SizedBox(
                            height: screenHeight(context, dividedBy: 90),
                          ),
                          Text(
                            "Discover the best pet near you and find your favourite one",
                            textAlign: TextAlign.left,
                            style: TextStyle(
                                color: Constants.kitGradients[27],
                                fontFamily: "Montserrat",
                                fontSize: 16,
                                fontWeight: FontWeight.w400),
                          ),
                          SizedBox(
                            height: screenHeight(context, dividedBy: 30),
                          ),
                          Row(
                            children: [
                              Container(
                                padding: EdgeInsets.all(1),
                                height: screenHeight(context, dividedBy: 16),
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(20),
                                    border: Border.all(
                                        color: Constants.kitGradients[27],
                                        width: 0.0)),
                                child: Center(
                                  child: Theme(
                                    data: ThemeData(
                                      backgroundColor:
                                          Constants.kitGradients[27],
                                    ),
                                    child: CountryCodePicker(
                                      showFlag: false,
                                      initialSelection: "+91",
                                      onChanged: _onCountryChange,
                                      textStyle: TextStyle(
                                        color: Constants.kitGradients[27],
                                        fontFamily: 'Montserrat',
                                        fontSize: 16,
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                              SizedBox(
                                width: screenHeight(context, dividedBy: 100),
                              ),
                              GestureDetector(
                                onTap: () {},
                                child: Container(
                                  width: screenWidth(context, dividedBy: 1.6),
                                  child: OnBoardingTextField(
                                    labelText: "Mobile Number",
                                    textEditingController: mobileTextEditingController,
                                    hasSuffix: false,
                                    suffixIcon: Container(),
                                    onTapIcon: () {},
                                    textArea: false,
                                    onTap: () {},
                                    readOnly: false,
                                      userExists: true,
                                      isNumber: true
                                  ),
                                  // FormFeildUserDetails(
                                  //   labelText: "Mobile Number",
                                  //   textEditingController:
                                  //       mobileTextEditingController,
                                  //   isPassword: false,
                                  //   autoFocus: false,
                                  //   isnumber: true,
                                  // ),
                                ),
                              ),
                            ],
                          ),
                          SizedBox(
                            height: screenHeight(context, dividedBy: 25),
                          ),
                          Align(
                              alignment: Alignment.center,
                              child: Column(
                                children: [
                                  BuildButton(
                                    buttonWidth: 2.5,
                                    isDisabled: true,
                                    title: "Next",
                                    isLoading: showLoading,
                                    onPressed: () async {
                                      setState(() {
                                        showLoading = true;
                                      });

                                      await _auth.verifyPhoneNumber(
                                        phoneNumber: code +
                                            mobileTextEditingController.text,
                                        verificationCompleted:
                                            (phoneAuthCredential) async {
                                          setState(() {
                                            showLoading = false;
                                          });
                                          //signInWithPhoneAuthCredential(phoneAuthCredential);
                                        },
                                        verificationFailed:
                                            (verificationFailed) async {
                                          setState(() {
                                            showLoading = false;
                                          });
                                          print(verificationFailed.message);
                                          showToast("verification failed " +
                                              verificationFailed.message);
                                        },
                                        codeSent: (verificationId,
                                            resendingToken) async {
                                          setState(() {
                                            showLoading = false;
                                            otpSend = true;
                                            this.verificationId =
                                                verificationId;
                                          });
                                        },
                                        codeAutoRetrievalTimeout:
                                            (verificationId) async {},
                                      );
                                      // setState(() {
                                      //   isLoading = true;
                                      // });
                                      // userBloc.login(
                                      //     loginRequest: LoginRequest(
                                      //         uid: "QgTZVwn28lcwiNnIT2nN8iDLgge2"));
                                      // print("token=======>" + authCredential.token.toString());
                                    },
                                  ),
                                  SizedBox(
                                    height:
                                        screenHeight(context, dividedBy: 25),
                                  ),
                                  Divider(
                                    color: Constants.kitGradients[27],
                                  ),
                                  SizedBox(
                                    height:
                                        screenHeight(context, dividedBy: 30),
                                  ),
                                  GestureDetector(
                                    onTap: () {
                                      push(context, SignUpPage());
                                      // Navigator.of(context).push(
                                      //     SlideRightRoute(page: OnBoardingName()));
                                      //  push(context, OnBoardingName());
                                    },
                                    child: Text(
                                      "Start a New Account >>",
                                      style: TextStyle(
                                          color: Constants.kitGradients[27],
                                          fontFamily: 'OpenSansRegular',
                                          fontSize: 17,
                                          fontWeight: FontWeight.w400),
                                    ),
                                  ),
                                ],
                              )),
                          SizedBox(
                            height: screenHeight(context, dividedBy: 25),
                          ),
                        ],
                      ),
                    ),
                  ),
                ))
            : Container(
                height: screenHeight(context, dividedBy: 1),
                width: screenWidth(context, dividedBy: 1),
                decoration: BoxDecoration(
                  image: DecorationImage(
                    fit: BoxFit.cover,
                    image: AssetImage(Constants.BACKGROUND_IMAGE),
                    colorFilter: new ColorFilter.mode(
                        Colors.black.withOpacity(0.6), BlendMode.darken),
                  ),
                ),
                child: ListView(
                  children: [
                    Container(
                        padding: EdgeInsets.symmetric(
                            horizontal: screenWidth(context, dividedBy: 20)),
                        child: Center(
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              SizedBox(
                                height: screenHeight(context, dividedBy: 3.3),
                              ),

                              // FormFeildUserDetails(
                              //   labelText: "Enter OTP",
                              //   textEditingController:
                              //       OTPTextEditingController,
                              //   isPassword: false,
                              //   isnumber: true,
                              // ),
                              Text(
                                "Find Your Perfect Puppy,            \nWith Us !          ",
                                //textAlign: TextAlign.left,
                                style: TextStyle(
                                    color: Constants.kitGradients[27],
                                    fontFamily: "Montserrat-ExtraBold",
                                    fontSize: 24,
                                    fontWeight: FontWeight.bold),
                              ),
                              SizedBox(
                                height: screenHeight(context, dividedBy: 90),
                              ),
                              Text(
                                "Join us and Discover the world of puppies ..                                          We are here to find your dream Pet            ",
                                style: TextStyle(
                                    color: Constants.kitGradients[27],
                                    fontFamily: "Montserrat",
                                    fontSize: 16,
                                    fontWeight: FontWeight.w400),
                              ),
                              SizedBox(
                                height: screenHeight(context, dividedBy: 30),
                              ),

                              Text(
                                "Please Enter the OTP send to your Number",
                                textAlign: TextAlign.left,
                                style: TextStyle(
                                    color: Constants.kitGradients[27],
                                    fontFamily: "Montserrat",
                                    fontSize: 16,
                                    fontWeight: FontWeight.w400),
                              ),
                              SizedBox(
                                height: screenHeight(context, dividedBy: 40),
                              ),
                              Container(
                                padding: EdgeInsets.symmetric(horizontal: 10),
                                width: screenWidth(context, dividedBy: 1),
                                child: PinFieldAutoFill(
                                  controller: otpTextEditingController,
                                  decoration: UnderlineDecoration(
                                    textStyle: TextStyle(
                                      color: Constants.kitGradients[27],
                                      fontFamily: 'Montserrat',
                                      fontSize: 16,
                                    ),
                                    colorBuilder: FixedColorBuilder(
                                        Colors.grey.withOpacity(0.5)),
                                  ),
                                  currentCode: otpTextEditingController.text,
                                  onCodeSubmitted: (code) {
                                    // setState(() {
                                    //   otpTextEditingController.text = code;
                                    // });
                                    print("tfgf" + code);
                                  },
                                  onCodeChanged: (code) {
                                    print("dedd" + code);
                                    print("dedde" +
                                        otpTextEditingController.text);
                                    // if (code.length == 6) {
                                    //   // FocusScope.of(context).requestFocus(FocusNode());
                                    // }
                                  },
                                ),
                              ),
                              SizedBox(
                                height: screenHeight(context, dividedBy: 20),
                              ),
                              Row(
                                  mainAxisAlignment: MainAxisAlignment.end,
                                  children: [
                                    OnBoardingButton(
                                      title: "Next",
                                      // widget.newUser ? "Next" : "Login",
                                      onPressed: () async {
                                        PhoneAuthCredential
                                            phoneAuthCredential =
                                            PhoneAuthProvider.credential(
                                                verificationId: verificationId,
                                                smsCode:
                                                    otpTextEditingController
                                                        .text);

                                        signInWithPhoneAuthCredential(
                                            phoneAuthCredential);
                                      },
                                      isLoading: showLoading,
                                      width: 3,
                                    ),
                                    SizedBox(
                                        width: screenWidth(context,
                                            dividedBy: 20)),
                                  ]),
                              // BuildButton(
                              //   onPressed: () {
                              //     verifyOtp(OTPTextEditingController.text,vId, mobileTextEditingController.text);
                              //   },
                              //   title: "Submit",
                              // ),
                            ],
                          ),
                        )),
                  ],
                ),
              ),
      ),
    );
  }
}

// SingleChildScrollView(
//   child: Container(
//     width: screenWidth(context,
//         dividedBy: 1),
//     // height: screenWidth(context,
//     //     dividedBy: 1),
//     padding: EdgeInsets.symmetric(vertical:screenHeight(context, dividedBy:3.5 ), ),
//     color: Constants.kitGradients[0],
//     child:
//     // Column(
//     //   mainAxisAlignment: MainAxisAlignment.center,
//     //   children: [
//     //     // SizedBox(
//     //     //   height: screenHeight(context, dividedBy: 4),
//     //     // ),
//     //     SvgPicture.asset("assets/images/wobow_logo.svg",
//     //       width: screenWidth(context,
//     //           dividedBy: 1),
//     //       height: screenWidth(context,
//     //           dividedBy: 1.5),
//     //     ),
//     //   ],
//     // ),
//   ),
// ),
// ),
