import 'package:flutter/material.dart';
import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:app_template/src/screens/item_cart_page.dart';
import 'package:app_template/src/widgets/item_page_card.dart';
import 'package:app_template/src/bloc/user_bloc.dart';
import 'package:app_template/src/models/get_product_category.dart';
import 'package:app_template/src/models/get_product_details.dart';
import 'package:app_template/src/screens/item_details_page.dart';


class ItemsPage extends StatefulWidget {
  final String categoryName;
 /* final String itemName;
  final String itemImage;
  final String itemPrice;*/
  ItemsPage(
  {
    this.categoryName,
   /* this.itemName,
    this.itemPrice,
    this.itemImage*/
  });

  @override
  _ItemsPageState createState() => _ItemsPageState();
}

class _ItemsPageState extends State<ItemsPage> {
  UserBloc userBloc = UserBloc();
  @override
  void initState() {
    // TODO: implement initState
    userBloc.getImageSlider();
    userBloc.getProductCategory();
    userBloc.getProductDetails();
    super.initState();
  }
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Constants.kitGradients[17],
      appBar: AppBar(
        backgroundColor: Constants.kitGradients[17],
        centerTitle: false,
        leading: GestureDetector(
          onTap: () {
            pop(context);
          },
          child: Icon(
            Icons.arrow_back,
            color: Constants.kitGradients[27],
          ),
        ),
        title: Container(
          child: Row(
            children: [
              Text(
                widget.categoryName,
                style: TextStyle(
                  //fontSize: 16,
                  fontWeight: FontWeight.w700,
                  fontFamily: 'Prompt-Light',
                  color: Constants.kitGradients[27],
                ),
              ),
              Spacer(),
              GestureDetector(
                  onTap: () {
                    push(
                        context,
                        ItemCartPage(
                        ));
                  },
                  child: Icon(
                    Icons.shopping_cart_outlined,
                    color: Constants.kitGradients[27],
                  ))
            ],
          ),
        ),
      ),
      body: Stack(
        children: [
        SingleChildScrollView(
        child: Container(
          alignment:
          Alignment.center,
          child: Column(
            crossAxisAlignment:
            CrossAxisAlignment
                .start,
            children: [
              //SizedBox(height: screenHeight(context, dividedBy: 50)),
              StreamBuilder<
                  GetProductDetails>(
                  stream: userBloc
                      .getProductDetailsResponse,
                  builder: (context,
                      snapshot) {
                    return snapshot
                        .hasData
                        ? Container(
                      width:
                      screenWidth(context, dividedBy: 1),
                      height:
                      screenHeight(context, dividedBy:1),
                      // color:Colors.red,
                      child:
                     GridView.builder(
                        //shrinkWrap: true,
                       // scrollDirection: Axis.horizontal,

                        itemCount: snapshot.data.results.length,
                       gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                         crossAxisCount: 2,
                         childAspectRatio: 0.75,
                         mainAxisSpacing:1 ,
                         crossAxisSpacing: 1,
                       ),
                        itemBuilder: (BuildContext context, int index) {
                          return Row(children: [
                            SizedBox(
                              width: screenWidth(context, dividedBy: 30),
                            ),
                            ItemPageCard(
                              imgUrl: snapshot.data.results[index].image1,
                              catType: snapshot.data.results[index].name,
                              rating: snapshot.data.results[index].rating,
                              circle: false,
                              itemPressed: () {
                                push(
                                  context,
                                  ItemDetailsPage(
                                    itemName: snapshot.data.results[index].name,
                                    itemImage: snapshot.data.results[index].image1,
                                    itemPrice: snapshot.data.results[index].price,
                                    itemRating: snapshot.data.results[index].rating,
                                    features: snapshot.data.results[index].highlights,
                                    productId: snapshot.data.results[index].id,


                                  ),
                                );
                              },
                            ),
                            SizedBox(
                              width: screenWidth(context, dividedBy: 40),
                            )
                          ]);
                        },
                      ),
                    )
                        : Container();
                  }),
            ],
          ),
        )
        )
        ],
      ),
    );
  }
}
