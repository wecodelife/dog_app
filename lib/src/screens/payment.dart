import 'package:flutter/material.dart';
import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:webview_flutter/webview_flutter.dart';

class Payment extends StatefulWidget {
 // const Payment({Key? key}) : super(key: key);

  @override
  _PaymentState createState() => _PaymentState();
}

class _PaymentState extends State<Payment> {
  bool isLoading = true;
  WebViewController _controller;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: isLoading == false
          ? Container(
        width: screenWidth(context, dividedBy: 1),
        height: screenHeight(context, dividedBy: 1),
        child: Center(
          heightFactor: 1,
          widthFactor: 1,
          child: SizedBox(
            height: 16,
            width: 16,
            child: CircularProgressIndicator(
              valueColor: AlwaysStoppedAnimation(
                  Constants.kitGradients[2].withOpacity(0.4)),
              strokeWidth: 2,
            ),
          ),
        ),
      )
          : Container(
        child: Stack(
          children: [
            Container(
              margin: EdgeInsets.only(
                  top: screenHeight(context, dividedBy: 20)),
              child: WebView(
                 // initialUrl: widget.url,
                  javascriptMode: JavascriptMode.unrestricted,
                  onWebViewCreated: (controller) {
                    setState(() {
                     _controller = controller;
                    });
                  },
                  onPageFinished: (url) {
                    // _controller.clearCache();
                    // if (status == "SUCCESS") {
                    //   print("Success, User subscription request :" );
                    //   userBloc.userSubscriptionPlans(
                    //       userSubscriptionRequest: UserSubscriptionRequest(
                    //         isActive: true,
                    //         orderId: widget.orderId,
                    //         status: "Success",
                    //         compliments: widget.compliments,
                    //         subscription: widget.subscriptionId,
                    //         user: ObjectFactory().appHive.getId(),
                    //       ));
                    // }
                  }
              ),
            ),
          ],
        ),
      ),
    );
  }
}
