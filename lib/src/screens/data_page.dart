List<Map<String, dynamic>> data = [
  {
    'image':
        "https://images.unsplash.com/photo-1467003909585-2f8a72700288?ixid=MnwxMjA3fDB8MHxzZWFyY2h8OXx8Zm9vZHxlbnwwfHwwfHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=600&q=60",
    'name': "Beef Enchiladas",
    'price': '860',
    'type': "Non-veg",
    'msg': "Hii",
    'time': "today",
    'rating': 8.7,
    'hotel':'Trivandrum'
  },
  {
    'image':
        "https://images.unsplash.com/photo-1482049016688-2d3e1b311543?ixid=MnwxMjA3fDB8MHxzZWFyY2h8Mnx8Zm9vZHxlbnwwfHwwfHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=600&q=60",
    'name': "Beefy Casserole",
    'price': '520',
    'type': "Non-veg",
    'msg': "Hii",
    'time': "today",
    'rating': 8.5,
    'hotel':'Kannur'
  },
  {
    'image':
        "https://images.unsplash.com/photo-1481931098730-318b6f776db0?ixid=MnwxMjA3fDB8MHxzZWFyY2h8MTZ8fGZvb2R8ZW58MHx8MHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=600&q=60",
    'name': "Cheesy Turkey Broccoli Pasta.",
    'price': '200',
    'type': "Veg",
    'msg': "Hii",
    'time': "today",
    'rating': 7.9,
    'hotel':'Calicut'
  },
  {
    'image':
        "https://images.unsplash.com/photo-1484723091739-30a097e8f929?ixid=MnwxMjA3fDB8MHxzZWFyY2h8NXx8Zm9vZHxlbnwwfHwwfHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=600&q=60",
    'name': "Biscuit Topped Chicken Pot Pie",
    'price': '720',
    'type': "Non-veg",
    'msg': "Hii",
    'time': "today",
    'rating': 8.0,
    'hotel':'Thrissur'
  },
  {
    'image':
        "https://images.unsplash.com/photo-1476718406336-bb5a9690ee2a?ixid=MnwxMjA3fDB8MHxzZWFyY2h8Nnx8Zm9vZHxlbnwwfHwwfHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=600&q=60",
    'name': "Cheesy Ranch Tuna Surprise",
    'price': '520',
    'type': "Non-veg",
    'msg': "Hii",
    'time': "today",
    'rating': 9.0,
    'hotel':'Kochi'
  },
  {
    'image':
        "https://images.unsplash.com/photo-1476224203421-9ac39bcb3327?ixid=MnwxMjA3fDB8MHxzZWFyY2h8MTJ8fGZvb2R8ZW58MHx8MHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=600&q=60",
    'name': "Cheesy Rice and Broccoli Casserole",
    'price': '320',
    'type': "Veg",
    'msg': "Hii",
    'time': "today",
    'rating': 8.9,
    'hotel':'Thrissur'
  }
];

List<String> categories = ["Cookies, Biscuits & Snaks", "Basic Dog Bowls", "Shower & Bath Accessories"];

List<Map<String, dynamic>> items = [
  {
    "category": "Suggested for you",
    "type": ["Pedigree", "Paw & Bone Bowl", "Drools", ],
    "images": [
      "https://images.unsplash.com/photo-1589924691995-400dc9ecc119?ixid=MnwxMjA3fDB8MHxzZWFyY2h8NHx8ZG9nJTIwZm9vZHxlbnwwfHwwfHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=700&q=60",
      "https://images.unsplash.com/photo-1589924691995-400dc9ecc119?ixid=MnwxMjA3fDB8MHxzZWFyY2h8NHx8ZG9nJTIwZm9vZHxlbnwwfHwwfHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=700&q=60",
      "https://images.unsplash.com/photo-1589924691995-400dc9ecc119?ixid=MnwxMjA3fDB8MHxzZWFyY2h8NHx8ZG9nJTIwZm9vZHxlbnwwfHwwfHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=700&q=60"
    ],
  },
  {
    "category": "Cookies, Biscuits & Snacks",
    "type": ["Pedigree", "Meat Up", "Drools", ],
    "images": [
      "https://images.unsplash.com/photo-1589924691995-400dc9ecc119?ixid=MnwxMjA3fDB8MHxzZWFyY2h8NHx8ZG9nJTIwZm9vZHxlbnwwfHwwfHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=700&q=60",
      "https://images.unsplash.com/photo-1589924691995-400dc9ecc119?ixid=MnwxMjA3fDB8MHxzZWFyY2h8NHx8ZG9nJTIwZm9vZHxlbnwwfHwwfHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=700&q=60",
      "https://images.unsplash.com/photo-1589924691995-400dc9ecc119?ixid=MnwxMjA3fDB8MHxzZWFyY2h8NHx8ZG9nJTIwZm9vZHxlbnwwfHwwfHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=700&q=60"
      ],
  },
  {
    "category": "Basic Dog Bowls",
    "type": ["Stainless Steel Bowls", "Paw & Bone Bowl", "Slow Feeder Bowl", ],
    "images": [
     "https://images.unsplash.com/photo-1601758228006-964e41e5e8eb?ixid=MnwxMjA3fDB8MHxzZWFyY2h8MTR8fGRvZyUyMGZvb2QlMjBib3dsfGVufDB8fDB8fA%3D%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=700&q=60",
      "https://images.unsplash.com/photo-1601758228006-964e41e5e8eb?ixid=MnwxMjA3fDB8MHxzZWFyY2h8MTR8fGRvZyUyMGZvb2QlMjBib3dsfGVufDB8fDB8fA%3D%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=700&q=60",
      "https://images.unsplash.com/photo-1601758228006-964e41e5e8eb?ixid=MnwxMjA3fDB8MHxzZWFyY2h8MTR8fGRvZyUyMGZvb2QlMjBib3dsfGVufDB8fDB8fA%3D%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=700&q=60"
    ],
  },
  {
    "category": "Shower & Bath Accessories",
    "type": ["Wsahing Sprayers", "Wsahing Sprayers", "Wsahing Sprayers", ],
    "images": [
      "https://images.unsplash.com/photo-1611173622933-91942d394b04?ixid=MnwxMjA3fDB8MHxzZWFyY2h8Mnx8ZG9nJTIwYmF0aHxlbnwwfHwwfHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=700&q=60",
      "https://images.unsplash.com/photo-1611173622933-91942d394b04?ixid=MnwxMjA3fDB8MHxzZWFyY2h8Mnx8ZG9nJTIwYmF0aHxlbnwwfHwwfHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=700&q=60",
      "https://images.unsplash.com/photo-1611173622933-91942d394b04?ixid=MnwxMjA3fDB8MHxzZWFyY2h8Mnx8ZG9nJTIwYmF0aHxlbnwwfHwwfHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=700&q=60",
      ],
  }
];

List<Map<String, dynamic>> messages = [
  {
    'message': 'Hello,',
    'type': 'receiver',
  },
  {
    'message': 'How have you been?',
    'type': 'receiver',
  },
  {
    'message': 'Hey, I am doing fine dude. wbu?',
    'type': 'sender',
  },
  {
    'message': 'ehhhh, doing OK',
    'type': 'receiver',
  },
];
