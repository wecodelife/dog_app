import 'package:app_template/src/screens/market_place.dart';
import 'package:app_template/src/screens/place_order_page.dart';
import 'package:app_template/src/screens/wishlist_page.dart';
import 'package:app_template/src/utils/urls.dart';
import 'package:app_template/src/widgets/build_button.dart';
import 'package:flutter/material.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/widgets/item_cart_tile.dart';
import 'package:app_template/src/bloc/user_bloc.dart';
import 'package:app_template/src/models/get_cart_details.dart';
import 'package:app_template/src/widgets/alert_dialog_box.dart';
import 'package:app_template/src/widgets/price_list_tile.dart';
import 'package:app_template/src/utils/object_factory.dart';
import 'package:app_template/src/models/add_wishlist_request.dart';
import 'package:app_template/src/models/update_cart_count_request.dart';
import 'package:flutter/services.dart';

class ItemCartPage extends StatefulWidget {
  final String itemName;
  final String itemImage;
  final String itemPrice;
  final int productId;
  ItemCartPage({this.itemPrice, this.itemName, this.itemImage, this.productId});
  @override
  _ItemCartPageState createState() => _ItemCartPageState();
}

class _ItemCartPageState extends State<ItemCartPage> {
  int total = 0;
  int totalAmount = 0;
  int discount=0;
  UserBloc userBloc = UserBloc();
  //double sum =0;
  int count = 1;
  bool isLoading = false;
  List<Result> selectedItems = [];
  bool selected = true;
 List<ItemCartPage> _cart = [];
  List<int> indexes =[];
  int itemCount;
  @override
  void initState() {
    // TODO: implement initState
    userBloc.getCartDetails();
    userBloc.getProductDetails();
    userBloc.getCartDetailsResponse.listen((event) {
    //  print(""+event.id.toString());
      setState(() {
        total=0;
     discount=0;
        totalAmount=0;
        for (int i = 0; i < event.results.length; i++) {
          total = total + int.parse(event.results[i].price);
          discount=discount + int.parse(event.results[i].price) - int.parse(event.results[i].sellingPrice);
        }
        totalAmount=totalAmount + total-discount;

      });
    });

    userBloc.getWishlistDetails();
    userBloc.getWishlistDetailsResponse.listen((event) {
      setState(() {
        userBloc.deleteCartItem();
      });
    });

    userBloc.updateCartCountResponse.listen((event) {
      userBloc.getCartDetails();
      setState(() {

      });
    });

    userBloc.deleteCartItemResponse.listen((event) {
      setState(() {
        // loading = false;
        // userBloc.getCartDetails();
      });
      showSuccessAnimatedToast(
          context: context, msg: "Cart Item Deleted Successfully");
      userBloc.getCartDetails();
    }).onError((event) {
      setState(() {
        // userBloc = false;
      });
      showErrorAnimatedToast(context: context, msg: event);
    });


    //print("product" + widget.productId.toString());

    userBloc.addWishlistResponse.listen((event) async {
      print("Add to wishlist working good"+event.id.toString());
      userBloc.deleteCartItem();
      setState(() {
         print("Add to wishlist working good");
        // isLoading = false;
          userBloc.deleteCartItem();
          print("wishlist Added successfully");
          userBloc.getWishlistDetailsResponse.listen((event) async {
            print("wishlist all good");
            print(event.results);
            print("length" + event.results.length.toString());
          });
        });
      showSuccessAnimatedToast(msg: "Added Successfully", context: context);
    });

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.dark.copyWith(
        statusBarColor: Constants.kitGradients[17],
        //or set color with: Color(0xFF0000FF)
        statusBarIconBrightness: Brightness.dark));
    return Scaffold(
        backgroundColor: Constants.kitGradients[17],

        appBar: AppBar(
          //elevation: 0,
          backgroundColor: Constants.kitGradients[17],
          leading: GestureDetector(
            onTap: () {
              pop(context);
            },
            child: Container( width: screenWidth(context,
                dividedBy: 6.6),
              height: screenHeight(context,
                  dividedBy: 15),
              // color: Constants.kitGradients[28],
              child: Icon(
                Icons.arrow_back,
                color: Constants.kitGradients[27],
              ),
            ),
          ),
          title: Container(
            child: Row(
              children: [
                Text(
                  "Your Cart",
                  style: TextStyle(
                    //fontSize: 16,
                    fontWeight: FontWeight.w700,
                    color: Constants.kitGradients[27],
                  ),
                ),
                Spacer(),
                GestureDetector(
                    onTap: () {
                      push(context, WishlistPage());
                    },
                    child: Container( width: screenWidth(context,
                        dividedBy: 6.6),
                      height: screenHeight(context,
                          dividedBy: 15),
                      // color: Constants.kitGradients[28],
                      child: Icon(
                        Icons.favorite_outline,
                        color: Constants.kitGradients[27],
                      ),
                    ))
              ],
            ),
          ),
        ),
        body: Stack(
          children: [
            SingleChildScrollView(
              child: Column(
                children: [
                  SizedBox(
                    height: screenHeight(context, dividedBy: 40),
                  ),
                  StreamBuilder<GetCartDetails>(
                      stream: userBloc.getCartDetailsResponse,
                      builder: (context, snapshot) {

                        return snapshot.hasData
                            ? Container(
                                padding: EdgeInsets.symmetric(
                                  horizontal:
                                      screenWidth(context, dividedBy: 30),
                                ),
                                child: Column(
                                  children: [
                                    ListView.builder(
                                      shrinkWrap: true,
                                      physics: NeverScrollableScrollPhysics(),
                                      itemCount: snapshot.data.results.length,
                                      itemBuilder:
                                          (BuildContext context, int index) {
                                        if(snapshot.hasData && itemCount!=null)
                                        itemCount = snapshot.data.results[index].quantity;
                                        return Column(
                                          children: [
                                            ItemCartTile(
                                                itemName: snapshot.data
                                                    .results[index].productName,
                                                itemImage: Urls.imageBaseUrl +
                                                    snapshot.data.results[index]
                                                        .productImage,
                                                itemPrice: snapshot.data
                                                    .results[index].price  ,

                                                selected:selected,
                                                onItemCountChange: (val)  {
                                                  print("itemcount" + itemCount.toString());
                                                  print("value" + val.toString());
                                                  userBloc.updateCartCount(
                                                    snapshot.data.results[index].id.toString(),
                                                    UpdateCartCountRequest(quantity: val)
                                                  );
                                                    setState(() {
                                                      // itemCount = val;
                                                      if(snapshot.data.results[index].quantity > val) {
                                                        // itemCount--;
                                                        total = total -
                                                            int.parse(
                                                                snapshot.data
                                                                    .results[index]
                                                                    .price);
                                                        discount=discount - int.parse( snapshot.data.results[index].price) -
                                                            int.parse( snapshot.data.results[index].sellingPrice);
                                                        totalAmount=totalAmount-(total-discount);
                                                      } else {
                                                        // itemCount++;
                                                        total = total +
                                                            int.parse(
                                                                snapshot.data
                                                                    .results[index]
                                                                    .price);
                                                        discount=discount + int.parse( snapshot.data.results[index].price) -
                                                            int.parse( snapshot.data.results[index].sellingPrice);
                                                        totalAmount=totalAmount+(total-discount);
                                                      }

                                                      });
                                                    // setState(() {
                                                    //
                                                    //   // itemCount = val;
                                                    //
                                                    // });
                                                },
                                                onSelected: () {
                                                 /* if(selectedItems.contains(snapshot.data
                                                      .results[index])){
                                                    selectedItems.remove(snapshot.data
                                                        .results[index]);
                                                    setState((){
                                                      selected = true;
                                                    });
                                                    print("Removed a item  ");
                                                    print("_________________");
                                                    print(selectedItems.toString());
                                                  } else {
                                                    selectedItems.add(snapshot.data
                                                        .results[index]);
                                                    setState((){
                                                      selected = true;
                                                      selectedItems.forEach((element)
                                                      {
                                                        total = total + double.parse(element.sellingPrice);
                                                      });
                                                    });
                                                    print("Added a item  ");
                                                    print("_________________");
                                                    print(selectedItems);
                                                  }*/
                                                },

                                                onTapDelete: () {
                                                  print("delete tapped");
                                                 // pop(context);
                                                  showDialog(
                                                      context: context,
                                                      builder:
                                                          (BuildContext context) {
                                                        return AlertDialogBox(
                                                          title: "Delete Item",
                                                          descriptions:
                                                              "Are you sure do you want to delete this Item?",
                                                          onPressedYes: () async {
                                                            await userBloc
                                                                .deleteCartItem(
                                                              id: snapshot
                                                                  .data
                                                                  .results[index]
                                                                  .id
                                                                  .toString(),

                                                            );
                                                          //  pop(context);

                                                            // showSuccessAnimatedToast(
                                                            //     context: context, msg: "Post deleted successfully");
                                                            pushAndRemoveUntil(
                                                                context,
                                                                ItemCartPage(),
                                                                false);

                                                          },
                                                        );
                                                      });
                                                },
                                                onTapWishlist: () {
                                                  userBloc.addWishlistDetails(
                                                      addWishlistRequest:
                                                          AddWishlistRequest(
                                                    product: snapshot.data
                                                        .results[index].product,
                                                    quantity: 1,
                                                    isDeleted: false,
                                                    user: int.parse(
                                                        ObjectFactory()
                                                            .appHive
                                                            .getUserId()),
                                                  ));
                                                  userBloc
                                                      .deleteCartItem(
                                                    id: snapshot
                                                        .data
                                                        .results[index]
                                                        .id
                                                        .toString(),

                                                  );
                                                }),
                                            //SizedBox(height: screenHeight(context, dividedBy: 100)),
                                            Divider(
                                              color: Constants.kitGradients[19]
                                                  .withOpacity(0.5),
                                              // thickness: 4,
                                            ),
                                            //SizedBox(height: screenHeight(context, dividedBy: 100)),
                                          ],

                                        );
                                      },
                                    ),

                                  ],
                                ),

                              )
                            : Container(
                                width: screenWidth(context, dividedBy: 1),
                                height: screenWidth(context, dividedBy: 1),
                                color: Constants.kitGradients[17],
                                child: Center(
                                  heightFactor: 1,
                                  widthFactor: 1,
                                  child: CircularProgressIndicator(
                                    valueColor: AlwaysStoppedAnimation(
                                        Constants.kitGradients[19]),
                                    strokeWidth: 2,
                                  ),
                                ),
                              );
                      }),
                  Container(
                    height: screenWidth(context, dividedBy: 1.5),
                    padding: EdgeInsets.symmetric(
                      horizontal:
                      screenWidth(context, dividedBy: 30),
                    ),
                    child: Padding(
                      padding: const EdgeInsets.only(top:12,left: 12,right: 12,bottom: 12),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text("Order Details",
                            style: TextStyle(
                              color: Constants.kitGradients[27],
                              fontWeight: FontWeight.bold,
                              fontFamily: 'Prompt-Light',
                              fontSize: 18,
                            ),),

                  // ListView.builder(
                  //     itemCount: 3,
                  //     shrinkWrap: true,
                  //     itemBuilder: (BuildContext context, int index) {
                  //       return ListTile(
                  //        leading: Text("Total Price",
                  //          style: TextStyle(
                  //            color: Constants.kitGradients[27],
                  //            fontWeight: FontWeight.bold,
                  //            fontFamily: 'Prompt-Light',
                  //            fontSize: 15,
                  //          ),),
                  //         trailing: Text("₹"+total.toString(),
                  //           style: TextStyle(
                  //             color: Constants.kitGradients[27],
                  //             fontWeight: FontWeight.bold,
                  //             fontFamily: 'Prompt-Light',
                  //             fontSize: 15,
                  //           ),),
                  //       );
                  //     }
                  // ),
                          Padding(
                            padding: const EdgeInsets.only(top:8.0),
                            child: PriceListTile(
                              price: "₹ "+total.toString(),
                              text: "Total Price",
                            ),
                          ),
                          PriceListTile(
                            price: "₹ "+discount.toString(),
                            text: "Discount Price",
                          ),
                          PriceListTile(
                            price: "₹ 0",
                            text: "Delivery Charge",
                          ),
                          PriceListTile(
                            price: "₹ "+totalAmount.toString(),
                            text: "Total Amount",
                          ),

                        ],
                      ),
                    ),
                  ),
                  SizedBox(
                    height: screenHeight(context, dividedBy: 28),
                  ),
                ],
              ),
            ),

            Align(
              alignment: Alignment.bottomCenter,
              child: Container(
                width: screenWidth(context, dividedBy: 1),
                decoration: BoxDecoration(
                  border: Border(top:BorderSide(
                    color: Constants.kitGradients[19].withOpacity(0.5),)
                  )
                ),
                child: Row(
                  children: [

                    Container(
                      width: screenWidth(context, dividedBy: 1),
                      height: screenHeight(context, dividedBy: 9),
                      color: Constants.kitGradients[17],
                      padding: EdgeInsets.symmetric(
                        horizontal: screenWidth(context, dividedBy: 30),
                        vertical: screenHeight(context, dividedBy: 40),
                      ),
                      child: Row(
                        children: [
                          Padding(
                            padding: const EdgeInsets.only(left:13.0),
                            child: Column(
                              children: [
                                Text("₹ "+totalAmount.toString(),
                                  style: TextStyle(
                                    color: Constants.kitGradients[27],
                                    fontWeight: FontWeight.bold,
                                    fontFamily: 'Prompt-Light',
                                    fontSize: 20,
                                  ),),
                                Text("View Details",
                                  style: TextStyle(
                                    color: Constants.kitGradients[27],
                                    fontWeight: FontWeight.bold,
                                    fontFamily: 'Prompt-Light',
                                    fontSize: 12,
                                  ),)
                              ],
                            ),
                          ),
                          Spacer(),
                          Container(
                            width: screenWidth(context, dividedBy: 1.8),
                            height: screenHeight(context, dividedBy: 9),
                            child: BuildButton(
                              buttonWidth: 2,
                              buttonHeight: 15,
                              isDisabled: true,
                              title: "PLACE ORDER",
                              onPressed: () {
                                push(context, PlaceOrderPage(itemImage: widget.itemImage,));
                              },

                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            )
          ],
        ));
  }
}