import 'package:app_template/src/models/login_request_model.dart';
import 'package:app_template/src/models/user_update_request_model.dart';
import 'package:app_template/src/screens/home_page.dart';
import 'package:app_template/src/screens/onboarding_name.dart';
import 'package:app_template/src/utils/object_factory.dart';
import 'package:app_template/src/widgets/build_button.dart';
import 'package:flutter/material.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:app_template/src/utils/constants.dart';
import 'package:flutter/services.dart';
import 'package:app_template/src/bloc/user_bloc.dart';

class HaveADogPage extends StatefulWidget {
  //const HaveADogPage({Key? key}) : super(key: key);
  final UserUpdateRequestModel updateRequestModel;
  HaveADogPage({this.updateRequestModel});

  @override
  _HaveADogPageState createState() => _HaveADogPageState();
}

class _HaveADogPageState extends State<HaveADogPage> {
  UserUpdateRequestModel updateRequestModel;
  UserBloc userBloc = UserBloc();
  String id;

  // @override
  // void initState() {
  //   super.initState();
  //   print("fdfgfd" + widget.updateRequestModel.toString());
  //   userBloc.login(
  //       loginRequest: LoginRequest(uid: "bOSPE5aIyRMNQ1BeCLkr1ajsKch2"));
  //   userBloc.loginResponse.listen((event) {
  //     if (event.status == 200 || event.status == 201) {
  //       print("GHJ success");
  //       ObjectFactory().appHive.putToken(token: event.data.token);
  //       print(event.data.userData.id);
  //       id = event.data.userData.id.toString();
  //     }
  //   });
  // }

  @override
  Widget build(BuildContext context) {
    return AnnotatedRegion<SystemUiOverlayStyle>(
      value: SystemUiOverlayStyle(
        statusBarColor: Colors.transparent,
      ),
      child: Scaffold(
        backgroundColor: Constants.kitGradients[17],
        body: Container(
            height: screenHeight(context, dividedBy: 1),
            width: screenWidth(context, dividedBy: 1),
            decoration: BoxDecoration(
              color: Constants.kitGradients[17],
              image: DecorationImage(
                fit: BoxFit.cover,
                image: AssetImage(Constants.BACKGROUND_IMAGE),
                colorFilter: new ColorFilter.mode(
                    Colors.black.withOpacity(0.6), BlendMode.darken),
              ),
            ),
            child: SingleChildScrollView(
              child: Container(
                width: screenWidth(context, dividedBy: 1),
                padding: EdgeInsets.symmetric(
                  horizontal: screenWidth(context, dividedBy: 20),
                  vertical: screenHeight(context, dividedBy: 6),
                ),
                child: Center(
                  child: Column(
                    //mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      SizedBox(
                        height: screenHeight(context, dividedBy: 5),
                      ),
                      Text(
                        "Do You Have a Dog ?",
                        textAlign: TextAlign.left,
                        style: TextStyle(
                            color: Constants.kitGradients[27],
                            fontFamily: "Montserrat-ExtraBold",
                            fontSize: 24,
                            fontWeight: FontWeight.bold),
                      ),
                      SizedBox(
                        height: screenHeight(context, dividedBy: 60),
                      ),
                      Text(
                        "Tell us more about your dog to get what you want, it will make you more comfortable",
                        textAlign: TextAlign.left,
                        style: TextStyle(
                            color: Constants.kitGradients[27],
                            fontFamily: "Montserrat",
                            fontSize: 16,
                            fontWeight: FontWeight.w400),
                      ),
                      SizedBox(
                        height: screenHeight(context, dividedBy: 15),
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          BuildButton(
                            buttonWidth: 3,
                            isDisabled: true,
                            title: "No",
                            onPressed: () async {
                              updateRequestModel = UserUpdateRequestModel(
                                  firstName:
                                  widget.updateRequestModel.firstName,
                                  description: widget.updateRequestModel.description,
                                  username: widget.updateRequestModel.username,
                                  phoneNumber: widget.updateRequestModel.phoneNumber,
                                  isUserHasDog: false);
                              userBloc.userUpdate(
                                  userUpdateRequest: updateRequestModel);
                              push(context, HomePage());
                            },
                          ),
                          SizedBox(
                            width: screenWidth(context, dividedBy: 15),
                          ),
                          BuildButton(
                            buttonWidth: 3,
                            isDisabled: true,
                            title: "Yes",
                            onPressed: () async{
                              print("haveadog  ");
                              // print("firstName  " + updateRequestModel.firstName);
                              // print("lastName  " + updateRequestModel.lastName);
                              // print("Phone Number  " + updateRequestModel.phoneNumber);
                              updateRequestModel = UserUpdateRequestModel(
                                  firstName:
                                  widget.updateRequestModel.firstName,
                                  // lastName: widget.updateRequestModel.lastName,
                                  description: widget.updateRequestModel.description,
                                  uid: widget.updateRequestModel.uid,
                                  username: widget.updateRequestModel.username,
                                  phoneNumber:
                                  widget.updateRequestModel.phoneNumber,
                                  isUserHasDog: true);
                              push(
                                  context,
                                  OnBoardingName(
                                      updateRequestModel: updateRequestModel));
                            },
                          ),
                        ],
                      ),
                      SizedBox(
                        height: screenHeight(context, dividedBy: 25),
                      ),
                      // Align(
                      //     alignment: Alignment.center,
                      //     child: Column(
                      //       children: [
                      //         BuildButton(
                      //           buttonWidth: 2.5,
                      //           isDisabled: true,
                      //           title: "Next",
                      //           onPressed: () {
                      //             //detailsCheck();
                      //           },
                      //         ),
                      //       ],
                      //     )),
                      SizedBox(
                        height: screenHeight(context, dividedBy: 25),
                      ),
                    ],
                  ),
                ),
              ),
            )),
      ),
    );
  }
}
