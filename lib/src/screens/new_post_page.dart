import 'dart:io';
import 'dart:typed_data';

import 'package:app_template/src/bloc/user_bloc.dart';
import 'package:app_template/src/models/add_post_request.dart';
import 'package:app_template/src/models/get_post_response.dart';
import 'package:app_template/src/models/post_update_request_model.dart';
import 'package:app_template/src/screens/home_page.dart';
import 'package:app_template/src/screens/photo-filter_page.dart';
import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/object_factory.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:app_template/src/widgets/app_bar.dart';
import 'package:app_template/src/widgets/bottom_sheet_item.dart';
import 'package:app_template/src/widgets/onboarding_textfield.dart';
import 'package:app_template/src/widgets/profile_icon.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:image/image.dart' as img;
import 'package:image_picker/image_picker.dart';
import 'package:intl/intl.dart';

class NewPostPage extends StatefulWidget {
  // const NewPostPage({Key? key}) : super(key: key);
  final Result addPostData;
  final Uint8List filteredImage;
  final DateTime postDate;
  final File imageFile;
  final bool isEdit;
  final String editingImage;
  String profileImage;
  String userName;
  int postId;
  NewPostPage(
      {this.addPostData,
      this.filteredImage,
      this.imageFile,
      this.editingImage,
      this.postId,
      this.profileImage,
      this.userName,
        this.postDate,
        this.isEdit});
  @override
  _NewPostPageState createState() => _NewPostPageState();
}

class _NewPostPageState extends State<NewPostPage> {
  TextEditingController captionTextEditingController =
      new TextEditingController();
  TextEditingController locationTextEditingController =
      new TextEditingController();

  final GlobalKey<NavigatorState> navigatorKey =
      new GlobalKey<NavigatorState>();

  UserBloc userBloc = UserBloc();
  int userId;

  //image picker
  img.Image image;
  File _image;
  bool isLoading;
  final picker = ImagePicker();
  String imageUrl = "";

  Future imgFromGallery(context) async {
    final image = await ImagePicker.pickImage(
        source: ImageSource.gallery, imageQuality: 50);
    pop(context);
    setState(() {
      if (image != null) {
        _image = File(image.path);
      } else {
        print('No image selected.');
      }
    });
    push(
        context,
        ColoredFilter(
          pickedFile: _image,
        ));
  }

  @override
  void initState() {
    if (widget.addPostData != null) {
      captionTextEditingController.text = widget.addPostData.description;
      locationTextEditingController.text = widget.addPostData.lat.toString();
      imageUrl = widget.addPostData.image1;
    }
    userId = int.parse(ObjectFactory().appHive.getUserId());
    userBloc.addPostResponse.listen((event) {
      if (event.status == 200 || event.status == 201) {
        setState(() {
          isLoading = false;
        });
        print("Successful");
        showSuccessAnimatedToast(
            context: context, msg: "Post added successfully");
        pushAndRemoveUntil(context, HomePage(), false);
      } else {
        showErrorAnimatedToast(context: context, msg: "Something went wrong");
      }
    });

    userBloc.postUpdate.listen((event) {
      if (event.status == 200 || event.status == 201) {
        setState(() {
          isLoading = false;
        });
        print("Post Update Successful");
        showSuccessAnimatedToast(
            context: context, msg: "Post updated successfully");
        pushAndRemoveUntil(context, HomePage(), false);
      } else {
        showErrorAnimatedToast(context: context, msg: "Something went wrong");
      }
    });
    super.initState();
  }

  int selected;
  void showPicker(context) {
    Future<void> future = showModalBottomSheet(
        context: context,
        backgroundColor: Constants.kitGradients[17],
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.only(
                topLeft: Radius.circular(8), topRight: Radius.circular(8))),
        builder: (BuildContext context) {
          SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.dark.copyWith(
              statusBarColor: Constants.kitGradients[17],
              //or set color with: Color(0xFF0000FF)
              statusBarIconBrightness: Brightness.dark));
          return Container(
            // height:screenHeight(context, dividedBy:4),
            padding: EdgeInsets.symmetric(
              horizontal: screenWidth(context, dividedBy: 30),
              vertical: screenHeight(context, dividedBy: 30),
            ),
            decoration: BoxDecoration(
                color: Constants.kitGradients[17],
                borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(8), topRight: Radius.circular(8))),
            child: Wrap(
              //crossAxisAlignment:CrossAxisAlignment.start,
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.symmetric(
                    horizontal: screenWidth(context, dividedBy: 20),
                    //vertical: screenHeight(context, dividedBy: 20),
                  ),
                  child: Text(
                    "Select Picture from",
                    style: TextStyle(
                      fontSize: 20,
                      fontWeight: FontWeight.bold,
                      fontFamily: "Montserrat",
                      color: Constants.kitGradients[27],
                    ),
                  ),
                ),
                SizedBox(
                  height: screenHeight(context, dividedBy: 15),
                ),
                BottomSheetItem(
                    title: "Photo Library",
                    leading: new Icon(
                      Icons.photo_library,
                      color: Constants.kitGradients[27],
                    ),
                    trailing: null,
                    onTap: () async {
                      imgFromGallery(context);
                      // pop(context);
                    }

                    // },
                    ),
                Divider(
                  color: Constants.kitGradients[27].withOpacity(0.2),
                  indent: 10,
                  endIndent: 10,
                ),
                BottomSheetItem(
                  title: "Camera",
                  leading: new Icon(
                    Icons.photo_camera,
                    color: Constants.kitGradients[27],
                  ),
                  trailing: null,
                  onTap: () async {
                    final image = await ImagePicker.pickImage(
                        source: ImageSource.camera, imageQuality: 25);
                    pop(context);
                    if (image != null) {
                      _image = File(image.path);
                    } else {
                      print('No image selected.');
                    }
                    push(
                        context,
                        ColoredFilter(
                          pickedFile: _image,
                        ));
                  },
                ),
                Divider(
                  color: Constants.kitGradients[27].withOpacity(0.2),
                  indent: 10,
                  endIndent: 10,
                ),
              ],
            ),
          );
        });
    future.then((void value) => print("closed modal"));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Constants.kitGradients[17],
      appBar: AppBar(
        elevation: 1,
        backgroundColor: Constants.kitGradients[17],
        actions: [
          AppBarDogApp(
            leftIcon: Container(
              width: screenWidth(context,
                  dividedBy: 6.6),
              height: screenHeight(context,
                  dividedBy: 15),
              color: Constants.kitGradients[28],
              child: Icon(
                Icons.arrow_back,
                color: Constants.kitGradients[27],
              ),
            ),
            onTapLeftIcon: () {
              pop(context);
            },
            tickIcon: Container(
              width: screenWidth(context,
                  dividedBy: 6.6),
              height: screenHeight(context,
                  dividedBy: 15),
              // color: Constants.kitGradients[28],
              child: Icon( Icons.done,
                color: Constants.kitGradients[27],),
            ),

      // Text(
      //         widget.isEdit!= true ? "Done" : "Update Post",
      //         style: TextStyle(
      //           fontSize: 17,
      //           fontFamily: 'Montserrat-bold',
      //           fontWeight: FontWeight.w600,
      //           color: Constants.kitGradients[27],
      //         ),
      //       ),
            onTapTickIcon: () {
              setState(() {
                isLoading = true;
              });
              widget.isEdit != true
                  ? userBloc.addPost(
                      addPostRequest: AddPostRequest(
                          description: captionTextEditingController.text,
                          image1: widget.imageFile,
                          lat: "10.5276",
                          lon: "76.2144",
                          createdBy:
                              int.parse(ObjectFactory().appHive.getUserId()),
                          likedBy: [2]))
                  : userBloc.updatePost(
                      id: widget.postId.toString(),
                      updatePostRequest: UpdatePostRequest(
                          description: captionTextEditingController.text,
                          // image1: widget.addPostData.image1,
                          lat: "10.5276",
                          lon: "76.2144",
                          createdBy:
                              int.parse(ObjectFactory().appHive.getUserId()),
                          likedBy: [2]));
              // if (widget.isEdit == false) {
              //   userBloc.addPost(
              //       addPostRequest: AddPostRequest(
              //           description: captionTextEditingController.text,
              //           image1: widget.imageFile,
              //           lat: "10.5276",
              //           lon: "76.2144",
              //           createdBy:
              //               int.parse(ObjectFactory().appHive.getUserId()),
              //           likedBy: [2]));
              // } else {
              //   userBloc.updatePost(
              //       id: widget.postId.toString(),
              //       updatePostRequest: UpdatePostRequest(
              //           description: captionTextEditingController.text,
              //           // image1: widget.addPostData.image1,
              //           lat: "10.5276",
              //           lon: "76.2144",
              //           createdBy:
              //               int.parse(ObjectFactory().appHive.getUserId()),
              //           likedBy: [2]));
              // }
            },
            title: Text(
              widget.isEdit != true ? "New Post" : "Edit Post",
              style: TextStyle(
                fontSize: 17,
                fontFamily: 'Montserrat',
                fontWeight: FontWeight.w600,
                color: Constants.kitGradients[27],
              ),
            ),

          )

        ],
        leading: Container(),
      ),
      body: isLoading != true
          ? SingleChildScrollView(
              child: Container(
                padding: EdgeInsets.symmetric(
                  horizontal: screenWidth(context, dividedBy: 30),
                  //vertical: screenHeight(context, dividedBy: 20),
                ),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    SizedBox(
                      height: screenHeight(context, dividedBy: 50),
                    ),
                    Row(
                      children: [
                        ProfileIcon(
                          profileImage: widget.profileImage,
                          circleRadius: 7,
                        ),
                        SizedBox(width: screenWidth(context, dividedBy: 40)),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              widget.userName,
                              style: TextStyle(
                                fontSize: 17,
                                fontFamily: 'Montserrat',
                                fontWeight: FontWeight.w600,
                                color: Constants.kitGradients[27],
                              ),
                            ),
                            SizedBox(
                              height: screenHeight(context, dividedBy: 200),
                            ),
                            GestureDetector(
                                child: Row(
                              // crossAxisAlignment: CrossAxisAlignment.start,
                              // mainAxisAlignment: MainAxisAlignment.start,
                              children: [
                                Icon(Icons.privacy_tip,
                                    color: Constants.kitGradients[27],
                                    size: 12),
                                SizedBox(
                                    width:
                                        screenWidth(context, dividedBy: 100)),
                                Text(
                                  "Public",
                                  style: TextStyle(
                                    fontSize: 12,
                                    fontFamily: 'Montserrat',
                                    fontWeight: FontWeight.w600,
                                    color: Constants.kitGradients[27],
                                  ),
                                )
                              ],
                            )),
                          ],
                        )
                      ],
                    ),
                    SizedBox(height: screenHeight(context, dividedBy: 30)),
                    Container(
                      width: screenWidth(context, dividedBy: 1),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          GestureDetector(
                            onTap: () {
                              widget.isEdit == false
                                  ? showPicker(context)
                                  : setState(() {});
                            },
                            child: widget.isEdit == true
                                ? CachedNetworkImage(
                                    height:
                                        screenWidth(context, dividedBy: 3.3),
                                    width: screenWidth(context, dividedBy: 3.5),
                                    fit: BoxFit.scaleDown,
                                    imageUrl: widget.addPostData.image1,
                                    imageBuilder: (context, imageProvider) =>
                                        Container(
                                      height:
                                          screenWidth(context, dividedBy: 3.3),
                                      width:
                                          screenWidth(context, dividedBy: 3.5),
                                      decoration: BoxDecoration(
                                        color: Constants.kitGradients[27]
                                            .withOpacity(0.12),
                                        borderRadius: BorderRadius.circular(8),
                                        image: DecorationImage(
                                          image: imageProvider,
                                          fit: BoxFit.scaleDown,
                                        ),
                                      ),
                                    ),
                                    placeholder: (context, url) => Center(
                                      heightFactor: 1,
                                      widthFactor: 1,
                                      child: SizedBox(
                                        height: 16,
                                        width: 16,
                                        child: CircularProgressIndicator(
                                          valueColor: AlwaysStoppedAnimation(
                                              Constants.kitGradients[8]),
                                          strokeWidth: 2,
                                        ),
                                      ),
                                    ),
                                  )
                                : Container(
                                    height:
                                        screenWidth(context, dividedBy: 1.1 ),
                                    width: screenWidth(context, dividedBy: 1.2),
                                    decoration: BoxDecoration(
                                      color: Constants.kitGradients[12]
                                          .withOpacity(0.12),
                                      borderRadius: BorderRadius.circular(25),
                                    ),
                                    child: ClipRRect(
                                        borderRadius: BorderRadius.circular(25),
                                        child: widget.filteredImage == null
                                            ? Icon(
                                                Icons.add_a_photo_rounded,
                                                color:
                                                    Constants.kitGradients[27],
                                                size: 30,
                                              )
                                            : Image.memory(
                                                widget.filteredImage,
                                                // _image,
                                                fit: BoxFit.cover,
                                                height: screenWidth(context,
                                                    dividedBy: 3.2),
                                                width: screenWidth(context,
                                                    dividedBy: 3.5),
                                              )),
                                  ),
                          ),
                          SizedBox(width: screenWidth(context, dividedBy: 20)),
                          Padding(
                            padding: const EdgeInsets.all(28.0),
                            child: Container(
                              // height: screenWidth(context, dividedBy: 10),
                              //
                              // width: screenWidth(context, dividedBy: 1),
                              child:
                              OnBoardingTextField(

                                labelText: widget.isEdit == true
                                    ? widget.addPostData.description
                                    : "Write a caption...",

                                textEditingController:
                                    captionTextEditingController,
                                hasSuffix: false,
                                suffixIcon: Container(),
                                onTapIcon: () {},
                                textArea: true,
                                onTap: () {},
                                userExists: true,
                                readOnly: false,

                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    SizedBox(height: screenHeight(context, dividedBy: 30)),
                    // Text(
                    //   "Add Location",
                    //   style: TextStyle(
                    //     fontSize: 14,
                    //     fontFamily: 'Montserrat',
                    //     fontWeight: FontWeight.w600,
                    //     color: Constants.kitGradients[27],
                    //   ),
                    // ),
                    // SizedBox(height: screenHeight(context, dividedBy: 50)),
                    // Container(
                    //   child: OnBoardingTextField(
                    //     labelText: "Location",
                    //     textEditingController: locationTextEditingController,
                    //     hasSuffix: false,
                    //     suffixIcon: Container(),
                    //     onTapIcon: () {},
                    //     textArea: false,
                    //     onTap: () {},
                    //     readOnly: false,
                    //       userExists: true,
                    //       isNumber: false
                    //   ),
                    // ),
                    // Container(
                    //   padding: EdgeInsets.symmetric(
                    //     horizontal: screenWidth(context, dividedBy: 30),
                    //   ),
                    //   width: screenWidth(context, dividedBy: 1),
                    //   child: Row(mainAxisAlignment: MainAxisAlignment.start, children: [
                    //     Text(
                    //       "Posted on : " +DateFormat('yyyy-MM-dd')
                    //           .format(widget.postDate),
                    //       style: TextStyle(
                    //         fontSize: 12,
                    //         fontWeight: FontWeight.w400,
                    //         fontFamily: 'Montserrat',
                    //         color: Constants.kitGradients[27],
                    //       ),
                    //     ),
                    //   ]),
                    // ),

                    SizedBox(
                      height: screenHeight(context, dividedBy: 15),
                    ),
                    // BuildButton(
                    //   buttonWidth: 2.5,
                    //   buttonHeight: 15,
                    //   title: "Add",
                    //   onPressed:(){
                    //     userBloc.addPost(
                    //         addPostRequest: AddPostRequest(
                    //           description: captionTextEditingController.text,
                    //           image1: widget.imageFile,
                    //           lat: "10.5276",
                    //           lon:"76.2144",
                    //           createdBy: int.parse(ObjectFactory().appHive.getUserId()),
                    //           likedBy: [2]
                    //         )
                    //     );
                    //   }
                    // )
                    // Divider(color: Constants.kitGradients[27].withOpacity(0.2),),
                    // Text("Tags",
                    //   style: TextStyle(
                    //     fontSize: 14,
                    //     fontFamily: 'Montserrat',
                    //     fontWeight: FontWeight.w600,
                    //     color: Constants.kitGradients[27],
                    //   ),),
                    // SizedBox(height: screenHeight(context, dividedBy: 50)),
                    // Container(
                    //   child: OnBoardingTextField(
                    //     labelText: "",
                    //     textEditingController: captionTextEditingController,
                    //     hasSuffix: false,
                    //     suffixIcon: Container(),
                    //     onTapIcon: () {},
                    //     textArea: false,
                    //     onTap: () {},
                    //     readOnly: false,
                    //   ),
                    // ),
                  ],
                ),
              ),
            )
          : Container(
              width: screenWidth(context, dividedBy: 1),
              height: screenHeight(context, dividedBy: 1),
              // color: Constants.kitGradients[0],
              child: Center(
                child: SpinKitFadingCircle(
                  color: Constants.kitGradients[4],
                ),
              ),
            ),
    );
  }
}
