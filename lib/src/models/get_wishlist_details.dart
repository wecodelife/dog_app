// To parse this JSON data, do
//
//     final getWishlistDetails = getWishlistDetailsFromJson(jsonString);

import 'dart:convert';

GetWishlistDetails getWishlistDetailsFromJson(String str) => GetWishlistDetails.fromJson(json.decode(str));

String getWishlistDetailsToJson(GetWishlistDetails data) => json.encode(data.toJson());

class GetWishlistDetails {
  GetWishlistDetails({
    this.count,
    this.next,
    this.previous,
    this.results,
    this.status,
    this.message,
  });

  final int count;
  final dynamic next;
  final dynamic previous;
  final List<Result> results;
  final int status;
  final String message;

  factory GetWishlistDetails.fromJson(Map<String, dynamic> json) => GetWishlistDetails(
    count: json["count"] == null ? null : json["count"],
    next: json["next"],
    previous: json["previous"],
    results: json["results"] == null ? null : List<Result>.from(json["results"].map((x) => Result.fromJson(x))),
    status: json["status"] == null ? null : json["status"],
    message: json["message"] == null ? null : json["message"],
  );

  Map<String, dynamic> toJson() => {
    "count": count == null ? null : count,
    "next": next,
    "previous": previous,
    "results": results == null ? null : List<dynamic>.from(results.map((x) => x.toJson())),
    "status": status == null ? null : status,
    "message": message == null ? null : message,
  };
}

class Result {
  Result({
    this.id,
    this.productName,
    this.productImage,
    this.sellingPrice,
    this.price,
    this.quantity,
    this.createdAt,
    this.isDeleted,
    this.product,
    this.user,
  });

  final int id;
  final String productName;
  final String productImage;
  final String sellingPrice;
  final String price;
  final int quantity;
  final DateTime createdAt;
  final bool isDeleted;
  final int product;
  final int user;

  factory Result.fromJson(Map<String, dynamic> json) => Result(
    id: json["id"] == null ? null : json["id"],
    productName: json["product_name"] == null ? null : json["product_name"],
    productImage: json["product_image"] == null ? null : json["product_image"],
    sellingPrice: json["selling_price"] == null ? null : json["selling_price"],
    price: json["price"] == null ? null : json["price"],
    quantity: json["quantity"] == null ? null : json["quantity"],
    createdAt: json["created_at"] == null ? null : DateTime.parse(json["created_at"]),
    isDeleted: json["is_deleted"] == null ? null : json["is_deleted"],
    product: json["product"] == null ? null : json["product"],
    user: json["user"] == null ? null : json["user"],
  );

  Map<String, dynamic> toJson() => {
    "id": id == null ? null : id,
    "product_name": productName == null ? null : productName,
    "product_image": productImage == null ? null : productImage,
    "selling_price": sellingPrice == null ? null : sellingPrice,
    "price": price == null ? null : price,
    "quantity": quantity == null ? null : quantity,
    "created_at": createdAt == null ? null : createdAt.toIso8601String(),
    "is_deleted": isDeleted == null ? null : isDeleted,
    "product": product == null ? null : product,
    "user": user == null ? null : user,
  };
}
