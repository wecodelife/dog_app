// // To parse this JSON data, do
// //
// //     final loginResponse = loginResponseFromJson(jsonString);
//
// import 'dart:convert';
//
// LoginResponse loginResponseFromJson(String str) => LoginResponse.fromJson(json.decode(str));
//
// String loginResponseToJson(LoginResponse data) => json.encode(data.toJson());
//
// class LoginResponse {
//   LoginResponse({
//     this.statuscode,
//     this.statusmessage,
//     this.errorcode,
//     this.errormessage,
//     this.data,
//   });
//
//   final int statuscode;
//   final dynamic statusmessage;
//   final int errorcode;
//   final String errormessage;
//   final Data data;
//
//   factory LoginResponse.fromJson(Map<String, dynamic> json) => LoginResponse(
//     statuscode: json["statuscode"] == null ? null : json["statuscode"],
//     statusmessage: json["statusmessage"],
//     errorcode: json["errorcode"] == null ? null : json["errorcode"],
//     errormessage: json["errormessage"] == null ? null : json["errormessage"],
//     data: json["data"] == null ? null : Data.fromJson(json["data"]),
//   );
//
//   Map<String, dynamic> toJson() => {
//     "statuscode": statuscode == null ? null : statuscode,
//     "statusmessage": statusmessage,
//     "errorcode": errorcode == null ? null : errorcode,
//     "errormessage": errormessage == null ? null : errormessage,
//     "data": data == null ? null : data.toJson(),
//   };
// }
//
// class Data {
//   Data({
//     this.userid,
//     this.loginmessage,
//     this.maxinactivesession,
//     this.profileupdaterequired,
//     this.mobileverificationrequired,
//     this.emailverificationrequired,
//     this.resetpassword,
//     this.authtoken,
//   });
//
//   final String userid;
//   final dynamic loginmessage;
//   final int maxinactivesession;
//   final bool profileupdaterequired;
//   final bool mobileverificationrequired;
//   final bool emailverificationrequired;
//   final bool resetpassword;
//   final String authtoken;
//
//   factory Data.fromJson(Map<String, dynamic> json) => Data(
//     userid: json["userid"] == null ? null : json["userid"],
//     loginmessage: json["loginmessage"],
//     maxinactivesession: json["maxinactivesession"] == null ? null : json["maxinactivesession"],
//     profileupdaterequired: json["profileupdaterequired"] == null ? null : json["profileupdaterequired"],
//     mobileverificationrequired: json["mobileverificationrequired"] == null ? null : json["mobileverificationrequired"],
//     emailverificationrequired: json["emailverificationrequired"] == null ? null : json["emailverificationrequired"],
//     resetpassword: json["resetpassword"] == null ? null : json["resetpassword"],
//     authtoken: json["authtoken"] == null ? null : json["authtoken"],
//   );
//
//   Map<String, dynamic> toJson() => {
//     "userid": userid == null ? null : userid,
//     "loginmessage": loginmessage,
//     "maxinactivesession": maxinactivesession == null ? null : maxinactivesession,
//     "profileupdaterequired": profileupdaterequired == null ? null : profileupdaterequired,
//     "mobileverificationrequired": mobileverificationrequired == null ? null : mobileverificationrequired,
//     "emailverificationrequired": emailverificationrequired == null ? null : emailverificationrequired,
//     "resetpassword": resetpassword == null ? null : resetpassword,
//     "authtoken": authtoken == null ? null : authtoken,
//   };
// }

// To parse this JSON data, do
//
//     final loginResponse = loginResponseFromJson(jsonString);

import 'dart:convert';

LoginResponse loginResponseFromJson(String str) => LoginResponse.fromJson(json.decode(str));

String loginResponseToJson(LoginResponse data) => json.encode(data.toJson());

class LoginResponse {
  LoginResponse({
    this.status,
    this.message,
    this.data,
  });

  int status;
  String message;
  Data data;

  factory LoginResponse.fromJson(Map<String, dynamic> json) => LoginResponse(
    status: json["status"],
    message: json["message"],
    data: Data.fromJson(json["data"]),
  );

  Map<String, dynamic> toJson() => {
    "status": status,
    "message": message,
    "data": data.toJson(),
  };
}

class Data {
  Data({
    this.token,
    this.userData,
  });

  String token;
  UserData userData;

  factory Data.fromJson(Map<String, dynamic> json) => Data(
    token: json["token"],
    userData: UserData.fromJson(json["user_data"]),
  );

  Map<String, dynamic> toJson() => {
    "token": token,
    "user_data": userData.toJson(),
  };
}

class UserData {
  UserData({
    this.id,
    this.lastLogin,
    this.isSuperuser,
    this.firstName,
    this.lastName,
    this.isStaff,
    this.dateJoined,
    this.username,
    this.uid,
    this.email,
    this.name,
    this.lat,
    this.lon,
    this.profileImage,
    this.description,
    this.isActive,
    this.age,
    this.breedType,
    this.groups,
    this.userPermissions,
  });

  int id;
  dynamic lastLogin;
  bool isSuperuser;
  String firstName;
  String lastName;
  bool isStaff;
  DateTime dateJoined;
  dynamic username;
  String uid;
  dynamic email;
  dynamic name;
  dynamic lat;
  dynamic lon;
  dynamic profileImage;
  dynamic description;
  bool isActive;
  dynamic age;
  dynamic breedType;
  List<dynamic> groups;
  List<dynamic> userPermissions;

  factory UserData.fromJson(Map<String, dynamic> json) => UserData(
    id: json["id"],
    lastLogin: json["last_login"],
    isSuperuser: json["is_superuser"],
    firstName: json["first_name"],
    lastName: json["last_name"],
    isStaff: json["is_staff"],
    dateJoined: DateTime.parse(json["date_joined"]),
    username: json["username"],
    uid: json["uid"],
    email: json["email"],
    name: json["name"],
    lat: json["lat"],
    lon: json["lon"],
    profileImage: json["profile_image"],
    description: json["description"],
    isActive: json["is_active"],
    age: json["age"],
    breedType: json["breed_type"],
    groups: List<dynamic>.from(json["groups"].map((x) => x)),
    userPermissions: List<dynamic>.from(json["user_permissions"].map((x) => x)),
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "last_login": lastLogin,
    "is_superuser": isSuperuser,
    "first_name": firstName,
    "last_name": lastName,
    "is_staff": isStaff,
    "date_joined": dateJoined.toIso8601String(),
    "username": username,
    "uid": uid,
    "email": email,
    "name": name,
    "lat": lat,
    "lon": lon,
    "profile_image": profileImage,
    "description": description,
    "is_active": isActive,
    "age": age,
    "breed_type": breedType,
    "groups": List<dynamic>.from(groups.map((x) => x)),
    "user_permissions": List<dynamic>.from(userPermissions.map((x) => x)),
  };
}
