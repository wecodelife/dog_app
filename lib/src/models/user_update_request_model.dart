// // To parse this JSON data, do
// //
// //     final userUpdateRequestModel = userUpdateRequestModelFromJson(jsonString);
//
// // To parse this JSON data, do
// //
// //     final userUpdateRequestModel = userUpdateRequestModelFromJson(jsonString);
//
// // import 'dart:convert';
// //
// // UserUpdateRequestModel userUpdateRequestModelFromJson(String str) => UserUpdateRequestModel.fromJson(json.decode(str));
// //
// // String userUpdateRequestModelToJson(UserUpdateRequestModel data) => json.encode(data.toJson());
// //
// // class UserUpdateRequestModel {
// //   UserUpdateRequestModel({
// //     this.lastLogin,
// //     this.isSuperuser,
// //     this.firstName,
// //     this.lastName,
// //     this.isStaff,
// //     this.dateJoined,
// //     this.gender,
// //     this.profileImage,
// //     this.username,
// //     this.uid,
// //     this.email,
// //     this.name,
// //     this.phoneNumber,
// //     this.password,
// //     this.lat,
// //     this.lon,
// //     this.description,
// //     this.isActive,
// //     this.isUserHasDog,
// //     this.age,
// //     this.dateOfBirth,
// //     this.breedType,
// //   });
// //
// //   DateTime lastLogin;
// //   bool isSuperuser;
// //   String firstName;
// //   String lastName;
// //   bool isStaff;
// //   DateTime dateJoined;
// //   int gender;
// //   dynamic profileImage;
// //   String username;
// //   String uid;
// //   String email;
// //   String name;
// //   String phoneNumber;
// //   String password;
// //   String lat;
// //   String lon;
// //   String description;
// //   bool isActive;
// //   bool isUserHasDog;
// //   String age;
// //   DateTime dateOfBirth;
// //   int breedType;
// //
// //   factory UserUpdateRequestModel.fromJson(Map<String, dynamic> json) => UserUpdateRequestModel(
// //     lastLogin: DateTime.parse(json["last_login"]),
// //     isSuperuser: json["is_superuser"],
// //     firstName: json["first_name"],
// //     lastName: json["last_name"],
// //     isStaff: json["is_staff"],
// //     dateJoined: DateTime.parse(json["date_joined"]),
// //     gender: json["gender"],
// //     profileImage: json["profile_image"],
// //     username: json["username"],
// //     uid: json["uid"],
// //     email: json["email"],
// //     name: json["name"],
// //     phoneNumber: json["phone_number"],
// //     password: json["password"],
// //     lat: json["lat"],
// //     lon: json["lon"],
// //     description: json["description"],
// //     isActive: json["is_active"],
// //     isUserHasDog: json["is_user_has_dog"],
// //     age: json["age"],
// //     dateOfBirth: DateTime.parse(json["date_of_birth"]),
// //     breedType: json["breed_type"],
// //   );
// //
// //   Map<String, dynamic> toJson() => {
// //     "last_login": lastLogin.toIso8601String(),
// //     "is_superuser": isSuperuser,
// //     "first_name": firstName,
// //     "last_name": lastName,
// //     "is_staff": isStaff,
// //     "date_joined": dateJoined.toIso8601String(),
// //     "gender": gender,
// //     "profile_image": profileImage,
// //     "username": username,
// //     "uid": uid,
// //     "email": email,
// //     "name": name,
// //     "phone_number": phoneNumber,
// //     "password": password,
// //     "lat": lat,
// //     "lon": lon,
// //     "description": description,
// //     "is_active": isActive,
// //     "is_user_has_dog": isUserHasDog,
// //     "age": age,
// //     "date_of_birth": dateOfBirth.toIso8601String(),
// //     "breed_type": breedType,
// //   };
// // }
//
// // To parse this JSON data, do
// //
// //     final userUpdateRequestModel = userUpdateRequestModelFromJson(jsonString);
//
// // import 'dart:convert';
// //
// // UserUpdateRequestModel userUpdateRequestModelFromJson(String str) => UserUpdateRequestModel.fromJson(json.decode(str));
// //
// // String userUpdateRequestModelToJson(UserUpdateRequestModel data) => json.encode(data.toJson());
// //
// // class UserUpdateRequestModel {
// //   UserUpdateRequestModel({
// //     this.lastLogin,
// //     this.isSuperuser,
// //     this.firstName,
// //     this.lastName,
// //     this.isStaff,
// //     this.dateJoined,
// //     this.gender,
// //     this.profileImage,
// //     this.username,
// //     this.uid,
// //     this.email,
// //     this.name,
// //     this.phoneNumber,
// //     this.password,
// //     this.lat,
// //     this.lon,
// //     this.description,
// //     this.isActive,
// //     this.isUserHasDog,
// //     this.age,
// //     this.dateOfBirth,
// //     this.breedType,
// //   });
// //
// //   final DateTime lastLogin;
// //   final bool isSuperuser;
// //   final String firstName;
// //   final String lastName;
// //   final bool isStaff;
// //   final DateTime dateJoined;
// //   final int gender;
// //   final dynamic profileImage;
// //   final String username;
// //   final String uid;
// //   final String email;
// //   final String name;
// //   final String phoneNumber;
// //   final String password;
// //   final String lat;
// //   final String lon;
// //   final String description;
// //   final bool isActive;
// //   final bool isUserHasDog;
// //   final String age;
// //   final DateTime dateOfBirth;
// //   final int breedType;
// //
// //   factory UserUpdateRequestModel.fromJson(Map<String, dynamic> json) => UserUpdateRequestModel(
// //     lastLogin: json["last_login"] == null ? null : DateTime.parse(json["last_login"]),
// //     isSuperuser: json["is_superuser"] == null ? null : json["is_superuser"],
// //     firstName: json["first_name"] == null ? null : json["first_name"],
// //     lastName: json["last_name"] == null ? null : json["last_name"],
// //     isStaff: json["is_staff"] == null ? null : json["is_staff"],
// //     dateJoined: json["date_joined"] == null ? null : DateTime.parse(json["date_joined"]),
// //     gender: json["gender"] == null ? null : json["gender"],
// //     profileImage: json["profile_image"],
// //     username: json["username"] == null ? null : json["username"],
// //     uid: json["uid"] == null ? null : json["uid"],
// //     email: json["email"] == null ? null : json["email"],
// //     name: json["name"] == null ? null : json["name"],
// //     phoneNumber: json["phone_number"] == null ? null : json["phone_number"],
// //     password: json["password"] == null ? null : json["password"],
// //     lat: json["lat"] == null ? null : json["lat"],
// //     lon: json["lon"] == null ? null : json["lon"],
// //     description: json["description"] == null ? null : json["description"],
// //     isActive: json["is_active"] == null ? null : json["is_active"],
// //     isUserHasDog: json["is_user_has_dog"] == null ? null : json["is_user_has_dog"],
// //     age: json["age"] == null ? null : json["age"],
// //     dateOfBirth: json["date_of_birth"] == null ? null : DateTime.parse(json["date_of_birth"]),
// //     breedType: json["breed_type"] == null ? null : json["breed_type"],
// //   );
// //
// //   Map<String, dynamic> toJson() => {
// //     "last_login": lastLogin == null ? null : lastLogin.toIso8601String(),
// //     "is_superuser": isSuperuser == null ? null : isSuperuser,
// //     "first_name": firstName == null ? null : firstName,
// //     "last_name": lastName == null ? null : lastName,
// //     "is_staff": isStaff == null ? null : isStaff,
// //     "date_joined": dateJoined == null ? null : dateJoined.toIso8601String(),
// //     "gender": gender == null ? null : gender,
// //     "profile_image": profileImage,
// //     "username": username == null ? null : username,
// //     "uid": uid == null ? null : uid,
// //     "email": email == null ? null : email,
// //     "name": name == null ? null : name,
// //     "phone_number": phoneNumber == null ? null : phoneNumber,
// //     "password": password == null ? null : password,
// //     "lat": lat == null ? null : lat,
// //     "lon": lon == null ? null : lon,
// //     "description": description == null ? null : description,
// //     "is_active": isActive == null ? null : isActive,
// //     "is_user_has_dog": isUserHasDog == null ? null : isUserHasDog,
// //     "age": age == null ? null : age,
// //     "date_of_birth": dateOfBirth == null ? null : dateOfBirth.toIso8601String(),
// //     "breed_type": breedType == null ? null : breedType,
// //   };
// // }
//
// // To parse this JSON data, do
// //
// //     final userUpdateRequestModel = userUpdateRequestModelFromJson(jsonString);
//
// import 'dart:convert';
//
// UserUpdateRequestModel userUpdateRequestModelFromJson(String str) => UserUpdateRequestModel.fromJson(json.decode(str));
//
// String userUpdateRequestModelToJson(UserUpdateRequestModel data) => json.encode(data.toJson());
//
// class UserUpdateRequestModel {
//   UserUpdateRequestModel({
//     this.lastLogin,
//     this.isSuperuser,
//     this.firstName,
//     this.lastName,
//     this.isStaff,
//     this.dateJoined,
//     this.username,
//     this.uid,
//     this.email,
//     this.name,
//     this.phoneNumber,
//     this.lat,
//     this.lon,
//     this.profileImage,
//     this.description,
//     this.isActive,
//     this.isUserHasDog,
//     this.age,
//     this.gender,
//     this.dateOfBirth,
//     this.breedType,
//   });
//
//   final dynamic lastLogin;
//   final bool isSuperuser;
//   final String firstName;
//   final String lastName;
//   final bool isStaff;
//   final DateTime dateJoined;
//   final dynamic username;
//   final String uid;
//   final dynamic email;
//   final String name;
//   final dynamic phoneNumber;
//   final dynamic lat;
//   final dynamic lon;
//   final dynamic profileImage;
//   final dynamic description;
//   final bool isActive;
//   final bool isUserHasDog;
//   final dynamic age;
//   final int gender;
//   final dynamic dateOfBirth;
//   final dynamic breedType;
//
//   factory UserUpdateRequestModel.fromJson(Map<String, dynamic> json) => UserUpdateRequestModel(
//     lastLogin: json["last_login"],
//     isSuperuser: json["is_superuser"] == null ? null : json["is_superuser"],
//     firstName: json["first_name"] == null ? null : json["first_name"],
//     lastName: json["last_name"] == null ? null : json["last_name"],
//     isStaff: json["is_staff"] == null ? null : json["is_staff"],
//     dateJoined: json["date_joined"] == null ? null : DateTime.parse(json["date_joined"]),
//     username: json["username"],
//     uid: json["uid"] == null ? null : json["uid"],
//     email: json["email"],
//     name: json["name"] == null ? null : json["name"],
//     phoneNumber: json["phone_number"],
//     lat: json["lat"],
//     lon: json["lon"],
//     profileImage: json["profile_image"],
//     description: json["description"],
//     isActive: json["is_active"] == null ? null : json["is_active"],
//     isUserHasDog: json["is_user_has_dog"] == null ? null : json["is_user_has_dog"],
//     age: json["age"],
//     gender: json["gender"] == null ? null : json["gender"],
//     dateOfBirth: json["date_of_birth"],
//     breedType: json["breed_type"],
//   );
//
//   Map<String, dynamic> toJson() => {
//     "last_login": lastLogin,
//     "is_superuser": isSuperuser == null ? null : isSuperuser,
//     "first_name": firstName == null ? null : firstName,
//     "last_name": lastName == null ? null : lastName,
//     "is_staff": isStaff == null ? null : isStaff,
//     "date_joined": dateJoined == null ? null : dateJoined.toIso8601String(),
//     "username": username,
//     "uid": uid == null ? null : uid,
//     "email": email,
//     "name": name == null ? null : name,
//     "phone_number": phoneNumber,
//     "lat": lat,
//     "lon": lon,
//     "profile_image": profileImage,
//     "description": description,
//     "is_active": isActive == null ? null : isActive,
//     "is_user_has_dog": isUserHasDog == null ? null : isUserHasDog,
//     "age": age,
//     "gender": gender == null ? null : gender,
//     "date_of_birth": dateOfBirth,
//     "breed_type": breedType,
//   };
// }

// To parse this JSON data, do
//
//     final userUpdateRequestModel = userUpdateRequestModelFromJson(jsonString);

import 'dart:convert';

UserUpdateRequestModel userUpdateRequestModelFromJson(String str) => UserUpdateRequestModel.fromJson(json.decode(str));

String userUpdateRequestModelToJson(UserUpdateRequestModel data) => json.encode(data.toJson());

class UserUpdateRequestModel {
  UserUpdateRequestModel({
    this.lastLogin,
    this.isSuperuser,
    this.firstName,
    this.lastName,
    this.isStaff,
    this.dateJoined,
    this.username,
    this.uid,
    this.email,
    this.name,
    this.phoneNumber,
    this.lat,
    this.lon,
    this.profileImage,
    this.description,
    this.isActive,
    this.isUserHasDog,
    this.age,
    this.gender,
    this.dateOfBirth,
    this.breedType,
  });

  final DateTime lastLogin;
  final bool isSuperuser;
  final String firstName;
  final String lastName;
  final bool isStaff;
  final DateTime dateJoined;
  final String username;
  final String uid;
  final String email;
  final String name;
  final String phoneNumber;
  final String lat;
  final String lon;
  final dynamic profileImage;
  final String description;
  final bool isActive;
  final bool isUserHasDog;
  final String age;
  final int gender;
  final DateTime dateOfBirth;
  final int breedType;

  factory UserUpdateRequestModel.fromJson(Map<String, dynamic> json) => UserUpdateRequestModel(
    lastLogin: json["last_login"] == null ? null : DateTime.parse(json["last_login"]),
    isSuperuser: json["is_superuser"] == null ? null : json["is_superuser"],
    firstName: json["first_name"] == null ? null : json["first_name"],
    lastName: json["last_name"] == null ? null : json["last_name"],
    isStaff: json["is_staff"] == null ? null : json["is_staff"],
    dateJoined: json["date_joined"] == null ? null : DateTime.parse(json["date_joined"]),
    username: json["username"] == null ? null : json["username"],
    uid: json["uid"] == null ? null : json["uid"],
    email: json["email"] == null ? null : json["email"],
    name: json["name"] == null ? null : json["name"],
    phoneNumber: json["phone_number"] == null ? null : json["phone_number"],
    lat: json["lat"] == null ? null : json["lat"],
    lon: json["lon"] == null ? null : json["lon"],
    profileImage: json["profile_image"] == null ? null : json["profile_image"],
    description: json["description"] == null ? null : json["description"],
    isActive: json["is_active"] == null ? null : json["is_active"],
    isUserHasDog: json["is_user_has_dog"] == null ? null : json["is_user_has_dog"],
    age: json["age"] == null ? null : json["age"],
    gender: json["gender"] == null ? null : json["gender"],
    dateOfBirth: json["date_of_birth"] == null ? null : DateTime.parse(json["date_of_birth"]),
    breedType: json["breed_type"] == null ? null : json["breed_type"],
  );

  Map<String, dynamic> toJson() => {
    "last_login": lastLogin == null ? null : lastLogin.toIso8601String(),
    "is_superuser": isSuperuser == null ? null : isSuperuser,
    "first_name": firstName == null ? null : firstName,
    "last_name": lastName == null ? null : lastName,
    "is_staff": isStaff == null ? null : isStaff,
    "date_joined": dateJoined == null ? null : dateJoined.toIso8601String(),
    "username": username == null ? null : username,
    "uid": uid == null ? null : uid,
    "email": email == null ? null : email,
    "name": name == null ? null : name,
    "phone_number": phoneNumber == null ? null : phoneNumber,
    "lat": lat == null ? null : lat,
    "lon": lon == null ? null : lon,
    "profile_image": profileImage == null ? null : profileImage,
    "description": description == null ? null : description,
    "is_active": isActive == null ? null : isActive,
    "is_user_has_dog": isUserHasDog == null ? null : isUserHasDog,
    "age": age == null ? null : age,
    "gender": gender == null ? null : gender,
    "date_of_birth": dateOfBirth == null ? null : dateOfBirth.toIso8601String(),
    "breed_type": breedType == null ? null : breedType,
  };
}
