// To parse this JSON data, do
//
//     final likePostResponse = likePostResponseFromJson(jsonString);

import 'dart:convert';

LikePostResponse likePostResponseFromJson(String str) => LikePostResponse.fromJson(json.decode(str));

String likePostResponseToJson(LikePostResponse data) => json.encode(data.toJson());

class LikePostResponse {
  LikePostResponse({
    this.id,
    this.username,
    this.createdAt,
    this.isDeleted,
    this.createdBy,
    this.post,
    this.status,
    this.message,
  });

  final int id;
  final String username;
  final DateTime createdAt;
  final bool isDeleted;
  final int createdBy;
  final int post;
  final int status;
  final String message;

  factory LikePostResponse.fromJson(Map<String, dynamic> json) => LikePostResponse(
    id: json["id"] == null ? null : json["id"],
    username: json["username"] == null ? null : json["username"],
    createdAt: json["created_at"] == null ? null : DateTime.parse(json["created_at"]),
    isDeleted: json["is_deleted"] == null ? null : json["is_deleted"],
    createdBy: json["created_by"] == null ? null : json["created_by"],
    post: json["post"] == null ? null : json["post"],
    status: json["status"] == null ? null : json["status"],
    message: json["message"] == null ? null : json["message"],
  );

  Map<String, dynamic> toJson() => {
    "id": id == null ? null : id,
    "username": username == null ? null : username,
    "created_at": createdAt == null ? null : createdAt.toIso8601String(),
    "is_deleted": isDeleted == null ? null : isDeleted,
    "created_by": createdBy == null ? null : createdBy,
    "post": post == null ? null : post,
    "status": status == null ? null : status,
    "message": message == null ? null : message,
  };
}
