// // To parse this JSON data, do
// //
// //     final getPostCommentResponse = getPostCommentResponseFromJson(jsonString);
//
// import 'dart:convert';
//
// GetPostCommentResponse getPostCommentResponseFromJson(String str) => GetPostCommentResponse.fromJson(json.decode(str));
//
// String getPostCommentResponseToJson(GetPostCommentResponse data) => json.encode(data.toJson());
//
// class GetPostCommentResponse {
//   GetPostCommentResponse({
//     this.count,
//     this.next,
//     this.previous,
//     this.results,
//     this.status,
//     this.message,
//   });
//
//   final int count;
//   final dynamic next;
//   final dynamic previous;
//   final List<Result> results;
//   final int status;
//   final String message;
//
//   factory GetPostCommentResponse.fromJson(Map<String, dynamic> json) => GetPostCommentResponse(
//     count: json["count"] == null ? null : json["count"],
//     next: json["next"],
//     previous: json["previous"],
//     results: json["results"] == null ? null : List<Result>.from(json["results"].map((x) => Result.fromJson(x))),
//     status: json["status"] == null ? null : json["status"],
//     message: json["message"] == null ? null : json["message"],
//   );
//
//   Map<String, dynamic> toJson() => {
//     "count": count == null ? null : count,
//     "next": next,
//     "previous": previous,
//     "results": results == null ? null : List<dynamic>.from(results.map((x) => x.toJson())),
//     "status": status == null ? null : status,
//     "message": message == null ? null : message,
//   };
// }
//
// class Result {
//   Result({
//     this.id,
//     this.username,
//     this.comment,
//     this.createdAt,
//     this.isDeleted,
//     this.createdBy,
//     this.postId,
//   });
//
//   final int id;
//   final dynamic username;
//   final String comment;
//   final DateTime createdAt;
//   final bool isDeleted;
//   final int createdBy;
//   final int postId;
//
//   factory Result.fromJson(Map<String, dynamic> json) => Result(
//     id: json["id"] == null ? null : json["id"],
//     username: json["username"],
//     comment: json["comment"] == null ? null : json["comment"],
//     createdAt: json["created_at"] == null ? null : DateTime.parse(json["created_at"]),
//     isDeleted: json["is_deleted"] == null ? null : json["is_deleted"],
//     createdBy: json["created_by"] == null ? null : json["created_by"],
//     postId: json["post_id"] == null ? null : json["post_id"],
//   );
//
//   Map<String, dynamic> toJson() => {
//     "id": id == null ? null : id,
//     "username": username,
//     "comment": comment == null ? null : comment,
//     "created_at": createdAt == null ? null : createdAt.toIso8601String(),
//     "is_deleted": isDeleted == null ? null : isDeleted,
//     "created_by": createdBy == null ? null : createdBy,
//     "post_id": postId == null ? null : postId,
//   };
// }

// To parse this JSON data, do
//
//     final getPostCommentResponse = getPostCommentResponseFromJson(jsonString);

import 'dart:convert';

GetPostCommentResponse getPostCommentResponseFromJson(String str) => GetPostCommentResponse.fromJson(json.decode(str));

String getPostCommentResponseToJson(GetPostCommentResponse data) => json.encode(data.toJson());

class GetPostCommentResponse {
  GetPostCommentResponse({
    this.count,
    this.next,
    this.previous,
    this.results,
    this.status,
    this.message,
  });

  final int count;
  final dynamic next;
  final dynamic previous;
  final List<Result> results;
  final int status;
  final String message;

  factory GetPostCommentResponse.fromJson(Map<String, dynamic> json) => GetPostCommentResponse(
    count: json["count"] == null ? null : json["count"],
    next: json["next"],
    previous: json["previous"],
    results: json["results"] == null ? null : List<Result>.from(json["results"].map((x) => Result.fromJson(x))),
    status: json["status"] == null ? null : json["status"],
    message: json["message"] == null ? null : json["message"],
  );

  Map<String, dynamic> toJson() => {
    "count": count == null ? null : count,
    "next": next,
    "previous": previous,
    "results": results == null ? null : List<dynamic>.from(results.map((x) => x.toJson())),
    "status": status == null ? null : status,
    "message": message == null ? null : message,
  };
}

class Result {
  Result({
    this.id,
    this.username,
    this.comment,
    this.createdAt,
    this.isDeleted,
    this.createdBy,
    this.postId,
  });

  final int id;
  final dynamic username;
  final String comment;
  final DateTime createdAt;
  final bool isDeleted;
  final int createdBy;
  final int postId;

  factory Result.fromJson(Map<String, dynamic> json) => Result(
    id: json["id"] == null ? null : json["id"],
    username: json["username"],
    comment: json["comment"] == null ? null : json["comment"],
    createdAt: json["created_at"] == null ? null : DateTime.parse(json["created_at"]),
    isDeleted: json["is_deleted"] == null ? null : json["is_deleted"],
    createdBy: json["created_by"] == null ? null : json["created_by"],
    postId: json["post_id"] == null ? null : json["post_id"],
  );

  Map<String, dynamic> toJson() => {
    "id": id == null ? null : id,
    "username": username,
    "comment": comment == null ? null : comment,
    "created_at": createdAt == null ? null : createdAt.toIso8601String(),
    "is_deleted": isDeleted == null ? null : isDeleted,
    "created_by": createdBy == null ? null : createdBy,
    "post_id": postId == null ? null : postId,
  };
}

