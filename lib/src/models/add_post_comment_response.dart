// To parse this JSON data, do
//
//     final addPostCommentResponse = addPostCommentResponseFromJson(jsonString);

import 'dart:convert';

AddPostCommentResponse addPostCommentResponseFromJson(String str) => AddPostCommentResponse.fromJson(json.decode(str));

String addPostCommentResponseToJson(AddPostCommentResponse data) => json.encode(data.toJson());

class AddPostCommentResponse {
  AddPostCommentResponse({
    this.id,
    this.username,
    this.profileImage,
    this.comment,
    this.createdAt,
    this.isDeleted,
    this.createdBy,
    this.post,
    this.status,
    this.message,
  });

  final int id;
  final String username;
  final String profileImage;
  final String comment;
  final DateTime createdAt;
  final bool isDeleted;
  final int createdBy;
  final int post;
  final int status;
  final String message;

  factory AddPostCommentResponse.fromJson(Map<String, dynamic> json) => AddPostCommentResponse(
    id: json["id"] == null ? null : json["id"],
    username: json["username"] == null ? null : json["username"],
    profileImage: json["profile_image"] == null ? null : json["profile_image"],
    comment: json["comment"] == null ? null : json["comment"],
    createdAt: json["created_at"] == null ? null : DateTime.parse(json["created_at"]),
    isDeleted: json["is_deleted"] == null ? null : json["is_deleted"],
    createdBy: json["created_by"] == null ? null : json["created_by"],
    post: json["post"] == null ? null : json["post"],
    status: json["status"] == null ? null : json["status"],
    message: json["message"] == null ? null : json["message"],
  );

  Map<String, dynamic> toJson() => {
    "id": id == null ? null : id,
    "username": username == null ? null : username,
    "profile_image": profileImage == null ? null : profileImage,
    "comment": comment == null ? null : comment,
    "created_at": createdAt == null ? null : createdAt.toIso8601String(),
    "is_deleted": isDeleted == null ? null : isDeleted,
    "created_by": createdBy == null ? null : createdBy,
    "post": post == null ? null : post,
    "status": status == null ? null : status,
    "message": message == null ? null : message,
  };
}
