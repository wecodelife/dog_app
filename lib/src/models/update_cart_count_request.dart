// To parse this JSON data, do
//
//     final updateCartCountRequest = updateCartCountRequestFromJson(jsonString);

import 'dart:convert';

UpdateCartCountRequest updateCartCountRequestFromJson(String str) => UpdateCartCountRequest.fromJson(json.decode(str));

String updateCartCountRequestToJson(UpdateCartCountRequest data) => json.encode(data.toJson());

class UpdateCartCountRequest {
  UpdateCartCountRequest({
    this.quantity,
  });

  final int quantity;

  factory UpdateCartCountRequest.fromJson(Map<String, dynamic> json) => UpdateCartCountRequest(
    quantity: json["quantity"] == null ? null : json["quantity"],
  );

  Map<String, dynamic> toJson() => {
    "quantity": quantity == null ? null : quantity,
  };
}
