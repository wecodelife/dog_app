// To parse this JSON data, do
//
//     final getImageSlider = getImageSliderFromJson(jsonString);

import 'dart:convert';

GetImageSlider getImageSliderFromJson(String str) => GetImageSlider.fromJson(json.decode(str));

String getImageSliderToJson(GetImageSlider data) => json.encode(data.toJson());

class GetImageSlider {
  GetImageSlider({
    this.count,
    this.next,
    this.previous,
    this.results,
    this.status,
    this.message,
  });

  final int count;
  final dynamic next;
  final dynamic previous;
  final List<Result> results;
  final int status;
  final String message;

  factory GetImageSlider.fromJson(Map<String, dynamic> json) => GetImageSlider(
    count: json["count"] == null ? null : json["count"],
    next: json["next"],
    previous: json["previous"],
    results: json["results"] == null ? null : List<Result>.from(json["results"].map((x) => Result.fromJson(x))),
    status: json["status"] == null ? null : json["status"],
    message: json["message"] == null ? null : json["message"],
  );

  Map<String, dynamic> toJson() => {
    "count": count == null ? null : count,
    "next": next,
    "previous": previous,
    "results": results == null ? null : List<dynamic>.from(results.map((x) => x.toJson())),
    "status": status == null ? null : status,
    "message": message == null ? null : message,
  };
}

class Result {
  Result({
    this.id,
    this.title,
    this.image,
    this.productId,
    this.createdAt,
    this.isDeleted,
    this.createdBy,
  });

  final int id;
  final String title;
  final String image;
  final dynamic productId;
  final DateTime createdAt;
  final bool isDeleted;
  final int createdBy;

  factory Result.fromJson(Map<String, dynamic> json) => Result(
    id: json["id"] == null ? null : json["id"],
    title: json["title"] == null ? null : json["title"],
    image: json["image"] == null ? null : json["image"],
    productId: json["product_id"],
    createdAt: json["created_at"] == null ? null : DateTime.parse(json["created_at"]),
    isDeleted: json["is_deleted"] == null ? null : json["is_deleted"],
    createdBy: json["created_by"] == null ? null : json["created_by"],
  );

  Map<String, dynamic> toJson() => {
    "id": id == null ? null : id,
    "title": title == null ? null : title,
    "image": image == null ? null : image,
    "product_id": productId,
    "created_at": createdAt == null ? null : createdAt.toIso8601String(),
    "is_deleted": isDeleted == null ? null : isDeleted,
    "created_by": createdBy == null ? null : createdBy,
  };
}
