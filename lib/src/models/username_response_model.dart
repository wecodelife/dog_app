// To parse this JSON data, do
//
//     final userNameResponse = userNameResponseFromJson(jsonString);

import 'dart:convert';

UserNameResponse userNameResponseFromJson(String str) => UserNameResponse.fromJson(json.decode(str));

String userNameResponseToJson(UserNameResponse data) => json.encode(data.toJson());

class UserNameResponse {
  UserNameResponse({
    this.status,
    this.userExists,
    this.message,
  });

  final int status;
  final bool userExists;
  final String message;

  factory UserNameResponse.fromJson(Map<String, dynamic> json) => UserNameResponse(
    status: json["status"] == null ? null : json["status"],
    userExists: json["user_exists"] == null ? null : json["user_exists"],
    message: json["message"] == null ? null : json["message"],
  );

  Map<String, dynamic> toJson() => {
    "status": status == null ? null : status,
    "user_exists": userExists == null ? null : userExists,
    "message": message == null ? null : message,
  };
}
