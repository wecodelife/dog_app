// To parse this JSON data, do
//
//     final addPostCommentRequest = addPostCommentRequestFromJson(jsonString);

import 'dart:convert';

AddPostCommentRequest addPostCommentRequestFromJson(String str) => AddPostCommentRequest.fromJson(json.decode(str));

String addPostCommentRequestToJson(AddPostCommentRequest data) => json.encode(data.toJson());

class AddPostCommentRequest {
  AddPostCommentRequest({
    this.comment,
    this.isDeleted,
    this.createdBy,
    this.post,
  });

  final String comment;
  final bool isDeleted;
  final int createdBy;
  final int post;

  factory AddPostCommentRequest.fromJson(Map<String, dynamic> json) => AddPostCommentRequest(
    comment: json["comment"] == null ? null : json["comment"],
    isDeleted: json["is_deleted"] == null ? null : json["is_deleted"],
    createdBy: json["created_by"] == null ? null : json["created_by"],
    post: json["post"] == null ? null : json["post"],
  );

  Map<String, dynamic> toJson() => {
    "comment": comment == null ? null : comment,
    "is_deleted": isDeleted == null ? null : isDeleted,
    "created_by": createdBy == null ? null : createdBy,
    "post": post == null ? null : post,
  };
}
