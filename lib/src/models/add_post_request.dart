// To parse this JSON data, do
//
//     final addPostRequest = addPostRequestFromJson(jsonString);

import 'dart:convert';

AddPostRequest addPostRequestFromJson(String str) => AddPostRequest.fromJson(json.decode(str));

String addPostRequestToJson(AddPostRequest data) => json.encode(data.toJson());

class AddPostRequest {
  AddPostRequest({
    this.title,
    this.description,
    this.lat,
    this.lon,
    this.image1,
    this.hashTags,
    this.isDeleted,
    this.createdBy,
    this.likedBy,
  });

  final String title;
  final String description;
  final String lat;
  final String lon;
  final dynamic image1;
  final List<String> hashTags;
  final bool isDeleted;
  final int createdBy;
  final List<int> likedBy;

  factory AddPostRequest.fromJson(Map<String, dynamic> json) => AddPostRequest(
    title: json["title"] == null ? null : json["title"],
    description: json["description"] == null ? null : json["description"],
    lat: json["lat"] == null ? null : json["lat"],
    lon: json["lon"] == null ? null : json["lon"],
    image1: json["image1"],
    hashTags: json["hash_tags"] == null ? null : List<String>.from(json["hash_tags"].map((x) => x)),
    isDeleted: json["is_deleted"] == null ? null : json["is_deleted"],
    createdBy: json["created_by"] == null ? null : json["created_by"],
    likedBy: json["liked_by"] == null ? null : List<int>.from(json["liked_by"].map((x) => x)),
  );

  Map<String, dynamic> toJson() => {
    "title": title == null ? null : title,
    "description": description == null ? null : description,
    "lat": lat == null ? null : lat,
    "lon": lon == null ? null : lon,
    "image1": image1,
    "hash_tags": hashTags == null ? null : List<dynamic>.from(hashTags.map((x) => x)),
    "is_deleted": isDeleted == null ? null : isDeleted,
    "created_by": createdBy == null ? null : createdBy,
    "liked_by": likedBy == null ? null : List<dynamic>.from(likedBy.map((x) => x)),
  };
}
