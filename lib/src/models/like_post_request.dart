// To parse this JSON data, do
//
//     final likePostRequest = likePostRequestFromJson(jsonString);

import 'dart:convert';

LikePostRequest likePostRequestFromJson(String str) => LikePostRequest.fromJson(json.decode(str));

String likePostRequestToJson(LikePostRequest data) => json.encode(data.toJson());

class LikePostRequest {
  LikePostRequest({
    this.createdBy,
    this.post,
    this.isDeleted,
  });

  final String createdBy;
  final String post;
  final bool isDeleted;

  factory LikePostRequest.fromJson(Map<String, dynamic> json) => LikePostRequest(
    createdBy: json["created_by"] == null ? null : json["created_by"],
    post: json["post"] == null ? null : json["post"],
    isDeleted: json["is_deleted"] == null ? null : json["is_deleted"],
  );

  Map<String, dynamic> toJson() => {
    "created_by": createdBy == null ? null : createdBy,
    "post": post == null ? null : post,
    "is_deleted": isDeleted == null ? null : isDeleted,
  };
}
