

// To parse this JSON data, do
//
//     final userUpdateResponseModel = userUpdateResponseModelFromJson(jsonString);

// To parse this JSON data, do
//
//     final userUpdateResponseModel = userUpdateResponseModelFromJson(jsonString);

// import 'dart:convert';
//
// UserUpdateResponseModel userUpdateResponseModelFromJson(String str) => UserUpdateResponseModel.fromJson(json.decode(str));
//
// String userUpdateResponseModelToJson(UserUpdateResponseModel data) => json.encode(data.toJson());
//
// class UserUpdateResponseModel {
//   UserUpdateResponseModel({
//     this.id,
//     this.lastLogin,
//     this.isSuperuser,
//     this.firstName,
//     this.lastName,
//     this.isStaff,
//     this.dateJoined,
//     this.username,
//     this.uid,
//     this.email,
//     this.name,
//     this.phoneNumber,
//     this.lat,
//     this.lon,
//     this.profileImage,
//     this.description,
//     this.isActive,
//     this.isUserHasDog,
//     this.age,
//     this.gender,
//     this.dateOfBirth,
//     this.breedType,
//     this.groups,
//     this.userPermissions,
//     this.status,
//     this.message,
//   });
//
//   int id;
//   DateTime lastLogin;
//   bool isSuperuser;
//   String firstName;
//   String lastName;
//   bool isStaff;
//   DateTime dateJoined;
//   String username;
//   String uid;
//   String email;
//   String name;
//   String phoneNumber;
//   String lat;
//   String lon;
//   dynamic profileImage;
//   String description;
//   bool isActive;
//   bool isUserHasDog;
//   String age;
//   int gender;
//   DateTime dateOfBirth;
//   int breedType;
//   List<dynamic> groups;
//   List<dynamic> userPermissions;
//   int status;
//   String message;
//
//   factory UserUpdateResponseModel.fromJson(Map<String, dynamic> json) => UserUpdateResponseModel(
//     id: json["id"],
//     lastLogin: DateTime.parse(json["last_login"]),
//     isSuperuser: json["is_superuser"],
//     firstName: json["first_name"],
//     lastName: json["last_name"],
//     isStaff: json["is_staff"],
//     dateJoined: DateTime.parse(json["date_joined"]),
//     username: json["username"],
//     uid: json["uid"],
//     email: json["email"],
//     name: json["name"],
//     phoneNumber: json["phone_number"],
//     lat: json["lat"],
//     lon: json["lon"],
//     profileImage: json["profile_image"],
//     description: json["description"],
//     isActive: json["is_active"],
//     isUserHasDog: json["is_user_has_dog"],
//     age: json["age"],
//     gender: json["gender"],
//     dateOfBirth: DateTime.parse(json["date_of_birth"]),
//     breedType: json["breed_type"],
//     groups: List<dynamic>.from(json["groups"].map((x) => x)),
//     userPermissions: List<dynamic>.from(json["user_permissions"].map((x) => x)),
//     status: json["status"],
//     message: json["message"],
//   );
//
//   Map<String, dynamic> toJson() => {
//     "id": id,
//     "last_login": lastLogin.toIso8601String(),
//     "is_superuser": isSuperuser,
//     "first_name": firstName,
//     "last_name": lastName,
//     "is_staff": isStaff,
//     "date_joined": dateJoined.toIso8601String(),
//     "username": username,
//     "uid": uid,
//     "email": email,
//     "name": name,
//     "phone_number": phoneNumber,
//     "lat": lat,
//     "lon": lon,
//     "profile_image": profileImage,
//     "description": description,
//     "is_active": isActive,
//     "is_user_has_dog": isUserHasDog,
//     "age": age,
//     "gender": gender,
//     "date_of_birth": dateOfBirth.toIso8601String(),
//     "breed_type": breedType,
//     "groups": List<dynamic>.from(groups.map((x) => x)),
//     "user_permissions": List<dynamic>.from(userPermissions.map((x) => x)),
//     "status": status,
//     "message": message,
//   };
// }

// To parse this JSON data, do
//
//     final userUpdateResponseModel = userUpdateResponseModelFromJson(jsonString);

import 'dart:convert';

UserUpdateResponseModel userUpdateResponseModelFromJson(String str) => UserUpdateResponseModel.fromJson(json.decode(str));

String userUpdateResponseModelToJson(UserUpdateResponseModel data) => json.encode(data.toJson());

class UserUpdateResponseModel {
  UserUpdateResponseModel({
    this.id,
    this.lastLogin,
    this.isSuperuser,
    this.firstName,
    this.lastName,
    this.isStaff,
    this.dateJoined,
    this.username,
    this.uid,
    this.email,
    this.name,
    this.phoneNumber,
    this.lat,
    this.lon,
    this.profileImage,
    this.description,
    this.isActive,
    this.isUserHasDog,
    this.age,
    this.gender,
    this.dateOfBirth,
    this.breedType,
    this.groups,
    this.userPermissions,
    this.status,
    this.message,
  });

  final int id;
  final dynamic lastLogin;
  final bool isSuperuser;
  final String firstName;
  final String lastName;
  final bool isStaff;
  final DateTime dateJoined;
  final dynamic username;
  final String uid;
  final dynamic email;
  final String name;
  final dynamic phoneNumber;
  final dynamic lat;
  final dynamic lon;
  final dynamic profileImage;
  final dynamic description;
  final bool isActive;
  final bool isUserHasDog;
  final dynamic age;
  final int gender;
  final dynamic dateOfBirth;
  final dynamic breedType;
  final List<dynamic> groups;
  final List<dynamic> userPermissions;
  final int status;
  final String message;

  factory UserUpdateResponseModel.fromJson(Map<String, dynamic> json) => UserUpdateResponseModel(
    id: json["id"] == null ? null : json["id"],
    lastLogin: json["last_login"],
    isSuperuser: json["is_superuser"] == null ? null : json["is_superuser"],
    firstName: json["first_name"] == null ? null : json["first_name"],
    lastName: json["last_name"] == null ? null : json["last_name"],
    isStaff: json["is_staff"] == null ? null : json["is_staff"],
    dateJoined: json["date_joined"] == null ? null : DateTime.parse(json["date_joined"]),
    username: json["username"],
    uid: json["uid"] == null ? null : json["uid"],
    email: json["email"],
    name: json["name"] == null ? null : json["name"],
    phoneNumber: json["phone_number"],
    lat: json["lat"],
    lon: json["lon"],
    profileImage: json["profile_image"],
    description: json["description"],
    isActive: json["is_active"] == null ? null : json["is_active"],
    isUserHasDog: json["is_user_has_dog"] == null ? null : json["is_user_has_dog"],
    age: json["age"],
    gender: json["gender"] == null ? null : json["gender"],
    dateOfBirth: json["date_of_birth"],
    breedType: json["breed_type"],
    groups: json["groups"] == null ? null : List<dynamic>.from(json["groups"].map((x) => x)),
    userPermissions: json["user_permissions"] == null ? null : List<dynamic>.from(json["user_permissions"].map((x) => x)),
    status: json["status"] == null ? null : json["status"],
    message: json["message"] == null ? null : json["message"],
  );

  Map<String, dynamic> toJson() => {
    "id": id == null ? null : id,
    "last_login": lastLogin,
    "is_superuser": isSuperuser == null ? null : isSuperuser,
    "first_name": firstName == null ? null : firstName,
    "last_name": lastName == null ? null : lastName,
    "is_staff": isStaff == null ? null : isStaff,
    "date_joined": dateJoined == null ? null : dateJoined.toIso8601String(),
    "username": username,
    "uid": uid == null ? null : uid,
    "email": email,
    "name": name == null ? null : name,
    "phone_number": phoneNumber,
    "lat": lat,
    "lon": lon,
    "profile_image": profileImage,
    "description": description,
    "is_active": isActive == null ? null : isActive,
    "is_user_has_dog": isUserHasDog == null ? null : isUserHasDog,
    "age": age,
    "gender": gender == null ? null : gender,
    "date_of_birth": dateOfBirth,
    "breed_type": breedType,
    "groups": groups == null ? null : List<dynamic>.from(groups.map((x) => x)),
    "user_permissions": userPermissions == null ? null : List<dynamic>.from(userPermissions.map((x) => x)),
    "status": status == null ? null : status,
    "message": message == null ? null : message,
  };
}
