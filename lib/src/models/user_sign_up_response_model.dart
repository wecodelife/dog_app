// To parse this JSON data, do
//
//     final userSignUpResponseModel = userSignUpResponseModelFromJson(jsonString);

import 'dart:convert';

UserSignUpResponseModel userSignUpResponseModelFromJson(String str) =>
    UserSignUpResponseModel.fromJson(json.decode(str));

String userSignUpResponseModelToJson(UserSignUpResponseModel data) =>
    json.encode(data.toJson());

class UserSignUpResponseModel {
  UserSignUpResponseModel({
    this.url,
    this.id,
    this.email,
    this.firstName,
    this.lastName,
    this.dateJoined,
    this.isActive,
    this.isVerified,
  });

  final String url;
  final int id;
  final String email;
  final String firstName;
  final String lastName;
  final DateTime dateJoined;
  final bool isActive;
  final bool isVerified;

  factory UserSignUpResponseModel.fromJson(Map<String, dynamic> json) =>
      UserSignUpResponseModel(
        url: json["url"] == null ? null : json["url"],
        id: json["id"] == null ? null : json["id"],
        email: json["email"] == null ? null : json["email"],
        firstName: json["first_name"] == null ? null : json["first_name"],
        lastName: json["last_name"] == null ? null : json["last_name"],
        dateJoined: json["date_joined"] == null
            ? null
            : DateTime.parse(json["date_joined"]),
        isActive: json["is_active"] == null ? null : json["is_active"],
        isVerified: json["is_verified"] == null ? null : json["is_verified"],
      );

  Map<String, dynamic> toJson() => {
        "url": url == null ? null : url,
        "id": id == null ? null : id,
        "email": email == null ? null : email,
        "first_name": firstName == null ? null : firstName,
        "last_name": lastName == null ? null : lastName,
        "date_joined": dateJoined == null ? null : dateJoined.toIso8601String(),
        "is_active": isActive == null ? null : isActive,
        "is_verified": isVerified == null ? null : isVerified,
      };
}
