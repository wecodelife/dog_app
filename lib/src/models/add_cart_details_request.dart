// To parse this JSON data, do
//
//     final addCartDetailsRequest = addCartDetailsRequestFromJson(jsonString);

import 'dart:convert';

AddCartDetailsRequest addCartDetailsRequestFromJson(String str) => AddCartDetailsRequest.fromJson(json.decode(str));

String addCartDetailsRequestToJson(AddCartDetailsRequest data) => json.encode(data.toJson());

class AddCartDetailsRequest {
  AddCartDetailsRequest({
    this.quantity,
    this.isDeleted,
    this.product,
    this.user,
  });

  final int quantity;
  final bool isDeleted;
  final int product;
  final int user;

  factory AddCartDetailsRequest.fromJson(Map<String, dynamic> json) => AddCartDetailsRequest(
    quantity: json["quantity"] == null ? null : json["quantity"],
    isDeleted: json["is_deleted"] == null ? null : json["is_deleted"],
    product: json["product"] == null ? null : json["product"],
    user: json["user"] == null ? null : json["user"],
  );

  Map<String, dynamic> toJson() => {
    "quantity": quantity == null ? null : quantity,
    "is_deleted": isDeleted == null ? null : isDeleted,
    "product": product == null ? null : product,
    "user": user == null ? null : user,
  };
}
