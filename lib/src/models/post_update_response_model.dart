// To parse this JSON data, do
//
//     final updatePostResponse = updatePostResponseFromJson(jsonString);

import 'dart:convert';

UpdatePostResponse updatePostResponseFromJson(String str) => UpdatePostResponse.fromJson(json.decode(str));

String updatePostResponseToJson(UpdatePostResponse data) => json.encode(data.toJson());

class UpdatePostResponse {
  UpdatePostResponse({
    this.id,
    this.userProfileUrl,
    this.userName,
    this.tags,
    this.title,
    this.description,
    this.lat,
    this.lon,
    this.hashTags,
    this.image1,
    this.image2,
    this.image3,
    this.image4,
    this.createdAt,
    this.isDeleted,
    this.createdBy,
    this.likedBy,
    this.status,
    this.message,
  });

  final int id;
  final String userProfileUrl;
  final String userName;
  final String tags;
  final String title;
  final String description;
  final String lat;
  final String lon;
  final List<String> hashTags;
  final String image1;
  final dynamic image2;
  final dynamic image3;
  final dynamic image4;
  final DateTime createdAt;
  final bool isDeleted;
  final int createdBy;
  final List<int> likedBy;
  final int status;
  final String message;

  factory UpdatePostResponse.fromJson(Map<String, dynamic> json) => UpdatePostResponse(
    id: json["id"] == null ? null : json["id"],
    userProfileUrl: json["user_profile_url"] == null ? null : json["user_profile_url"],
    userName: json["user_name"] == null ? null : json["user_name"],
    tags: json["tags"] == null ? null : json["tags"],
    title: json["title"] == null ? null : json["title"],
    description: json["description"] == null ? null : json["description"],
    lat: json["lat"] == null ? null : json["lat"],
    lon: json["lon"] == null ? null : json["lon"],
    hashTags: json["hash_tags"] == null ? null : List<String>.from(json["hash_tags"].map((x) => x)),
    image1: json["image1"] == null ? null : json["image1"],
    image2: json["image2"],
    image3: json["image3"],
    image4: json["image4"],
    createdAt: json["created_at"] == null ? null : DateTime.parse(json["created_at"]),
    isDeleted: json["is_deleted"] == null ? null : json["is_deleted"],
    createdBy: json["created_by"] == null ? null : json["created_by"],
    likedBy: json["liked_by"] == null ? null : List<int>.from(json["liked_by"].map((x) => x)),
    status: json["status"] == null ? null : json["status"],
    message: json["message"] == null ? null : json["message"],
  );

  Map<String, dynamic> toJson() => {
    "id": id == null ? null : id,
    "user_profile_url": userProfileUrl == null ? null : userProfileUrl,
    "user_name": userName == null ? null : userName,
    "tags": tags == null ? null : tags,
    "title": title == null ? null : title,
    "description": description == null ? null : description,
    "lat": lat == null ? null : lat,
    "lon": lon == null ? null : lon,
    "hash_tags": hashTags == null ? null : List<dynamic>.from(hashTags.map((x) => x)),
    "image1": image1 == null ? null : image1,
    "image2": image2,
    "image3": image3,
    "image4": image4,
    "created_at": createdAt == null ? null : createdAt.toIso8601String(),
    "is_deleted": isDeleted == null ? null : isDeleted,
    "created_by": createdBy == null ? null : createdBy,
    "liked_by": likedBy == null ? null : List<dynamic>.from(likedBy.map((x) => x)),
    "status": status == null ? null : status,
    "message": message == null ? null : message,
  };
}
