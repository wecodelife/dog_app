// To parse this JSON data, do
//
//     final addCartDetailsResponse = addCartDetailsResponseFromJson(jsonString);

import 'dart:convert';

AddCartDetailsResponse addCartDetailsResponseFromJson(String str) => AddCartDetailsResponse.fromJson(json.decode(str));

String addCartDetailsResponseToJson(AddCartDetailsResponse data) => json.encode(data.toJson());

class AddCartDetailsResponse {
  AddCartDetailsResponse({
    this.id,
    this.quantity,
    this.createdAt,
    this.isDeleted,
    this.product,
    this.user,
    this.status,
    this.message,
  });

  final int id;
  final int quantity;
  final DateTime createdAt;
  final bool isDeleted;
  final int product;
  final int user;
  final int status;
  final String message;

  factory AddCartDetailsResponse.fromJson(Map<String, dynamic> json) => AddCartDetailsResponse(
    id: json["id"] == null ? null : json["id"],
    quantity: json["quantity"] == null ? null : json["quantity"],
    createdAt: json["created_at"] == null ? null : DateTime.parse(json["created_at"]),
    isDeleted: json["is_deleted"] == null ? null : json["is_deleted"],
    product: json["product"] == null ? null : json["product"],
    user: json["user"] == null ? null : json["user"],
    status: json["status"] == null ? null : json["status"],
    message: json["message"] == null ? null : json["message"],
  );

  Map<String, dynamic> toJson() => {
    "id": id == null ? null : id,
    "quantity": quantity == null ? null : quantity,
    "created_at": createdAt == null ? null : createdAt.toIso8601String(),
    "is_deleted": isDeleted == null ? null : isDeleted,
    "product": product == null ? null : product,
    "user": user == null ? null : user,
    "status": status == null ? null : status,
    "message": message == null ? null : message,
  };
}
