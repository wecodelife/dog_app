// To parse this JSON data, do
//
//     final userNameRequest = userNameRequestFromJson(jsonString);

import 'dart:convert';

UserNameRequest userNameRequestFromJson(String str) => UserNameRequest.fromJson(json.decode(str));

String userNameRequestToJson(UserNameRequest data) => json.encode(data.toJson());

class UserNameRequest {
  UserNameRequest({
    this.username,
  });

  final String username;

  factory UserNameRequest.fromJson(Map<String, dynamic> json) => UserNameRequest(
    username: json["username"] == null ? null : json["username"],
  );

  Map<String, dynamic> toJson() => {
    "username": username == null ? null : username,
  };
}
