// To parse this JSON data, do
//
//     final getLogoutResponse = getLogoutResponseFromJson(jsonString);

import 'dart:convert';

GetLogoutResponse getLogoutResponseFromJson(String str) => GetLogoutResponse.fromJson(json.decode(str));

String getLogoutResponseToJson(GetLogoutResponse data) => json.encode(data.toJson());

class GetLogoutResponse {
  GetLogoutResponse({
    this.status,
    this.message,
  });

  final int status;
  final String message;

  factory GetLogoutResponse.fromJson(Map<String, dynamic> json) => GetLogoutResponse(
    status: json["status"] == null ? null : json["status"],
    message: json["message"] == null ? null : json["message"],
  );

  Map<String, dynamic> toJson() => {
    "status": status == null ? null : status,
    "message": message == null ? null : message,
  };
}
