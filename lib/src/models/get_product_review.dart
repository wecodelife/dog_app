// To parse this JSON data, do
//
//     final getProductReview = getProductReviewFromJson(jsonString);

import 'dart:convert';

GetProductReview getProductReviewFromJson(String str) => GetProductReview.fromJson(json.decode(str));

String getProductReviewToJson(GetProductReview data) => json.encode(data.toJson());

class GetProductReview {
  GetProductReview({
    this.count,
    this.next,
    this.previous,
    this.results,
    this.status,
    this.message,
  });

  final int count;
  final dynamic next;
  final dynamic previous;
  final List<Result> results;
  final int status;
  final String message;

  factory GetProductReview.fromJson(Map<String, dynamic> json) => GetProductReview(
    count: json["count"] == null ? null : json["count"],
    next: json["next"],
    previous: json["previous"],
    results: json["results"] == null ? null : List<Result>.from(json["results"].map((x) => Result.fromJson(x))),
    status: json["status"] == null ? null : json["status"],
    message: json["message"] == null ? null : json["message"],
  );

  Map<String, dynamic> toJson() => {
    "count": count == null ? null : count,
    "next": next,
    "previous": previous,
    "results": results == null ? null : List<dynamic>.from(results.map((x) => x.toJson())),
    "status": status == null ? null : status,
    "message": message == null ? null : message,
  };
}

class Result {
  Result({
    this.id,
    this.review,
    this.rating,
    this.createdAt,
    this.isDeleted,
    this.product,
    this.createdBy,
  });

  final int id;
  final String review;
  final double rating;
  final DateTime createdAt;
  final bool isDeleted;
  final int product;
  final int createdBy;

  factory Result.fromJson(Map<String, dynamic> json) => Result(
    id: json["id"] == null ? null : json["id"],
    review: json["review"] == null ? null : json["review"],
    rating: json["rating"] == null ? null : json["rating"].toDouble(),
    createdAt: json["created_at"] == null ? null : DateTime.parse(json["created_at"]),
    isDeleted: json["is_deleted"] == null ? null : json["is_deleted"],
    product: json["product"] == null ? null : json["product"],
    createdBy: json["created_by"] == null ? null : json["created_by"],
  );

  Map<String, dynamic> toJson() => {
    "id": id == null ? null : id,
    "review": review == null ? null : review,
    "rating": rating == null ? null : rating,
    "created_at": createdAt == null ? null : createdAt.toIso8601String(),
    "is_deleted": isDeleted == null ? null : isDeleted,
    "product": product == null ? null : product,
    "created_by": createdBy == null ? null : createdBy,
  };
}
