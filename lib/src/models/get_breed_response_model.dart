// To parse this JSON data, do
//
//     final getBreedResponseModel = getBreedResponseModelFromJson(jsonString);

import 'dart:convert';

GetBreedResponseModel getBreedResponseModelFromJson(String str) => GetBreedResponseModel.fromJson(json.decode(str));

String getBreedResponseModelToJson(GetBreedResponseModel data) => json.encode(data.toJson());

class GetBreedResponseModel {
  GetBreedResponseModel({
    this.count,
    this.next,
    this.previous,
    this.results,
    this.status,
    this.message,
  });

  final int count;
  final dynamic next;
  final dynamic previous;
  final List<Result> results;
  final int status;
  final String message;

  factory GetBreedResponseModel.fromJson(Map<String, dynamic> json) => GetBreedResponseModel(
    count: json["count"] == null ? null : json["count"],
    next: json["next"],
    previous: json["previous"],
    results: json["results"] == null ? null : List<Result>.from(json["results"].map((x) => Result.fromJson(x))),
    status: json["status"] == null ? null : json["status"],
    message: json["message"] == null ? null : json["message"],
  );

  Map<String, dynamic> toJson() => {
    "count": count == null ? null : count,
    "next": next,
    "previous": previous,
    "results": results == null ? null : List<dynamic>.from(results.map((x) => x.toJson())),
    "status": status == null ? null : status,
    "message": message == null ? null : message,
  };
}

class Result {
  Result({
    this.id,
    this.name,
    this.isDeleted,
  });

  final int id;
  final String name;
  final bool isDeleted;

  factory Result.fromJson(Map<String, dynamic> json) => Result(
    id: json["id"] == null ? null : json["id"],
    name: json["name"] == null ? null : json["name"],
    isDeleted: json["is_deleted"] == null ? null : json["is_deleted"],
  );

  Map<String, dynamic> toJson() => {
    "id": id == null ? null : id,
    "name": name == null ? null : name,
    "is_deleted": isDeleted == null ? null : isDeleted,
  };
}
