// To parse this JSON data, do
//
//     final updatePostRequest = updatePostRequestFromJson(jsonString);

import 'dart:convert';

UpdatePostRequest updatePostRequestFromJson(String str) => UpdatePostRequest.fromJson(json.decode(str));

String updatePostRequestToJson(UpdatePostRequest data) => json.encode(data.toJson());

class UpdatePostRequest {
  UpdatePostRequest({
    this.title,
    this.description,
    this.lat,
    this.lon,
    this.hashTags,
    this.image1,
    this.isDeleted,
    this.createdBy,
    this.likedBy,
  });

  final String title;
  final String description;
  final String lat;
  final String lon;
  final List<String> hashTags;
  final dynamic image1;
  final bool isDeleted;
  final int createdBy;
  final List<int> likedBy;

  factory UpdatePostRequest.fromJson(Map<String, dynamic> json) => UpdatePostRequest(
    title: json["title"] == null ? null : json["title"],
    description: json["description"] == null ? null : json["description"],
    lat: json["lat"] == null ? null : json["lat"],
    lon: json["lon"] == null ? null : json["lon"],
    hashTags: json["hash_tags"] == null ? null : List<String>.from(json["hash_tags"].map((x) => x)),
    image1: json["image1"] == null ? null : json["image1"],
    isDeleted: json["is_deleted"] == null ? null : json["is_deleted"],
    createdBy: json["created_by"] == null ? null : json["created_by"],
    likedBy: json["liked_by"] == null ? null : List<int>.from(json["liked_by"].map((x) => x)),
  );

  Map<String, dynamic> toJson() => {
    "title": title == null ? null : title,
    "description": description == null ? null : description,
    "lat": lat == null ? null : lat,
    "lon": lon == null ? null : lon,
    "hash_tags": hashTags == null ? null : List<dynamic>.from(hashTags.map((x) => x)),
    "image1": image1 == null ? null : image1,
    "is_deleted": isDeleted == null ? null : isDeleted,
    "created_by": createdBy == null ? null : createdBy,
    "liked_by": likedBy == null ? null : List<dynamic>.from(likedBy.map((x) => x)),
  };
}
