// To parse this JSON data, do
//
//     final getPostResponse = getPostResponseFromJson(jsonString);

import 'dart:convert';

GetPostResponse getPostResponseFromJson(String str) => GetPostResponse.fromJson(json.decode(str));

String getPostResponseToJson(GetPostResponse data) => json.encode(data.toJson());

class GetPostResponse {
  GetPostResponse({
    this.count,
    this.next,
    this.previous,
    this.results,
    this.status,
    this.message,
  });

  final int count;
  final dynamic next;
  final dynamic previous;
  final List<Result> results;
  final int status;
  final String message;

  factory GetPostResponse.fromJson(Map<String, dynamic> json) => GetPostResponse(
    count: json["count"] == null ? null : json["count"],
    next: json["next"],
    previous: json["previous"],
    results: json["results"] == null ? null : List<Result>.from(json["results"].map((x) => Result.fromJson(x))),
    status: json["status"] == null ? null : json["status"],
    message: json["message"] == null ? null : json["message"],
  );

  Map<String, dynamic> toJson() => {
    "count": count == null ? null : count,
    "next": next,
    "previous": previous,
    "results": results == null ? null : List<dynamic>.from(results.map((x) => x.toJson())),
    "status": status == null ? null : status,
    "message": message == null ? null : message,
  };
}

class Result {
  Result({
    this.id,
    this.userProfileUrl,
    this.userName,
    this.likedBy,
    this.likeCount,
    this.tags,
    this.title,
    this.description,
    this.lat,
    this.lon,
    this.hashTags,
    this.image1,
    this.image2,
    this.image3,
    this.image4,
    this.createdAt,
    this.isDeleted,
    this.createdBy,
  });

  final int id;
  final String userProfileUrl;
  final String userName;
  final List<LikedBy> likedBy;
  final int likeCount;
  final dynamic tags;
  final dynamic title;
  final String description;
  final String lat;
  final String lon;
  final dynamic hashTags;
  final String image1;
  final dynamic image2;
  final dynamic image3;
  final dynamic image4;
  final DateTime createdAt;
  final bool isDeleted;
  final int createdBy;

  factory Result.fromJson(Map<String, dynamic> json) => Result(
    id: json["id"] == null ? null : json["id"],
    userProfileUrl: json["user_profile_url"] == null ? null : json["user_profile_url"],
    userName: json["user_name"] == null ? null : json["user_name"],
    likedBy: json["liked_by"] == null ? null : List<LikedBy>.from(json["liked_by"].map((x) => LikedBy.fromJson(x))),
    likeCount: json["like_count"] == null ? null : json["like_count"],
    tags: json["tags"],
    title: json["title"],
    description: json["description"] == null ? null : json["description"],
    lat: json["lat"] == null ? null : json["lat"],
    lon: json["lon"] == null ? null : json["lon"],
    hashTags: json["hash_tags"],
    image1: json["image1"] == null ? null : json["image1"],
    image2: json["image2"],
    image3: json["image3"],
    image4: json["image4"],
    createdAt: json["created_at"] == null ? null : DateTime.parse(json["created_at"]),
    isDeleted: json["is_deleted"] == null ? null : json["is_deleted"],
    createdBy: json["created_by"] == null ? null : json["created_by"],
  );

  Map<String, dynamic> toJson() => {
    "id": id == null ? null : id,
    "user_profile_url": userProfileUrl == null ? null : userProfileUrl,
    "user_name": userName == null ? null : userName,
    "liked_by": likedBy == null ? null : List<dynamic>.from(likedBy.map((x) => x.toJson())),
    "like_count": likeCount == null ? null : likeCount,
    "tags": tags,
    "title": title,
    "description": description == null ? null : description,
    "lat": lat == null ? null : lat,
    "lon": lon == null ? null : lon,
    "hash_tags": hashTags,
    "image1": image1 == null ? null : image1,
    "image2": image2,
    "image3": image3,
    "image4": image4,
    "created_at": createdAt == null ? null : createdAt.toIso8601String(),
    "is_deleted": isDeleted == null ? null : isDeleted,
    "created_by": createdBy == null ? null : createdBy,
  };
}

class LikedBy {
  LikedBy({
    this.id,
    this.username,
    this.createdAt,
    this.isDeleted,
    this.createdBy,
    this.post,
  });

  final int id;
  final String username;
  final DateTime createdAt;
  final bool isDeleted;
  final int createdBy;
  final int post;

  factory LikedBy.fromJson(Map<String, dynamic> json) => LikedBy(
    id: json["id"] == null ? null : json["id"],
    username: json["username"] == null ? null : json["username"],
    createdAt: json["created_at"] == null ? null : DateTime.parse(json["created_at"]),
    isDeleted: json["is_deleted"] == null ? null : json["is_deleted"],
    createdBy: json["created_by"] == null ? null : json["created_by"],
    post: json["post"] == null ? null : json["post"],
  );

  Map<String, dynamic> toJson() => {
    "id": id == null ? null : id,
    "username": username == null ? null : username,
    "created_at": createdAt == null ? null : createdAt.toIso8601String(),
    "is_deleted": isDeleted == null ? null : isDeleted,
    "created_by": createdBy == null ? null : createdBy,
    "post": post == null ? null : post,
  };
}
