// To parse this JSON data, do
//
//     final getProductDetails = getProductDetailsFromJson(jsonString);

import 'dart:convert';

GetProductDetails getProductDetailsFromJson(String str) => GetProductDetails.fromJson(json.decode(str));

String getProductDetailsToJson(GetProductDetails data) => json.encode(data.toJson());

class GetProductDetails {
  GetProductDetails({
    this.count,
    this.next,
    this.previous,
    this.results,
    this.status,
    this.message,
  });

  final int count;
  final dynamic next;
  final dynamic previous;
  final List<Result> results;
  final int status;
  final String message;

  factory GetProductDetails.fromJson(Map<String, dynamic> json) => GetProductDetails(
    count: json["count"] == null ? null : json["count"],
    next: json["next"],
    previous: json["previous"],
    results: json["results"] == null ? null : List<Result>.from(json["results"].map((x) => Result.fromJson(x))),
    status: json["status"] == null ? null : json["status"],
    message: json["message"] == null ? null : json["message"],
  );

  Map<String, dynamic> toJson() => {
    "count": count == null ? null : count,
    "next": next,
    "previous": previous,
    "results": results == null ? null : List<dynamic>.from(results.map((x) => x.toJson())),
    "status": status == null ? null : status,
    "message": message == null ? null : message,
  };
}

class Result {
  Result({
    this.id,
    this.categoryName,
    this.rating,
    this.name,
    this.highlights,
    this.description,
    this.image1,
    this.image2,
    this.image3,
    this.image4,
    this.image5,
    this.price,
    this.sellingPrice,
    this.createdAt,
    this.isDeleted,
    this.category,
    this.createdBy,
  });

  final int id;
  final String categoryName;
  final String rating;
  final String name;
  final List<String> highlights;
  final String description;
  final String image1;
  final String image2;
  final String image3;
  final String image4;
  final String image5;
  final String price;
  final String sellingPrice;
  final DateTime createdAt;
  final bool isDeleted;
  final int category;
  final int createdBy;

  factory Result.fromJson(Map<String, dynamic> json) => Result(
    id: json["id"] == null ? null : json["id"],
    categoryName: json["category_name"] == null ? null : json["category_name"],
    rating: json["rating"] == null ? null : json["rating"],
    name: json["name"] == null ? null : json["name"],
    highlights: json["highlights"] == null ? null : List<String>.from(json["highlights"].map((x) => x)),
    description: json["description"] == null ? null : json["description"],
    image1: json["image1"] == null ? null : json["image1"],
    image2: json["image2"] == null ? null : json["image2"],
    image3: json["image3"] == null ? null : json["image3"],
    image4: json["image4"] == null ? null : json["image4"],
    image5: json["image5"] == null ? null : json["image5"],
    price: json["price"] == null ? null : json["price"],
    sellingPrice: json["selling_price"] == null ? null : json["selling_price"],
    createdAt: json["created_at"] == null ? null : DateTime.parse(json["created_at"]),
    isDeleted: json["is_deleted"] == null ? null : json["is_deleted"],
    category: json["category"] == null ? null : json["category"],
    createdBy: json["created_by"] == null ? null : json["created_by"],
  );

  Map<String, dynamic> toJson() => {
    "id": id == null ? null : id,
    "category_name": categoryName == null ? null : categoryName,
    "rating": rating == null ? null : rating,
    "name": name == null ? null : name,
    "highlights": highlights == null ? null : List<dynamic>.from(highlights.map((x) => x)),
    "description": description == null ? null : description,
    "image1": image1 == null ? null : image1,
    "image2": image2 == null ? null : image2,
    "image3": image3 == null ? null : image3,
    "image4": image4 == null ? null : image4,
    "image5": image5 == null ? null : image5,
    "price": price == null ? null : price,
    "selling_price": sellingPrice == null ? null : sellingPrice,
    "created_at": createdAt == null ? null : createdAt.toIso8601String(),
    "is_deleted": isDeleted == null ? null : isDeleted,
    "category": category == null ? null : category,
    "created_by": createdBy == null ? null : createdBy,
  };
}

// enum Highlight { LOREM_IPSUM_DOLOR_SIT_AMET_CONSECTETUR_ADIPISCING_ELIT, LOREM_IPSUM_DOLOR_SIT_AMET_CONSECTETURADIPISCING_ELIT }
//
// final highlightValues = EnumValues({
//   "Lorem ipsum dolor sit amet consecteturadipiscing elit": Highlight.LOREM_IPSUM_DOLOR_SIT_AMET_CONSECTETURADIPISCING_ELIT,
//   "Lorem ipsum dolor sit amet consectetur adipiscing elit": Highlight.LOREM_IPSUM_DOLOR_SIT_AMET_CONSECTETUR_ADIPISCING_ELIT
// });

// class EnumValues<T> {
//   Map<String, T> map;
//   Map<T, String> reverseMap;
//
//   EnumValues(this.map);
//
//   Map<T, String> get reverse {
//     if (reverseMap == null) {
//       reverseMap = map.map((k, v) => new MapEntry(v, k));
//     }
//     return reverseMap;
//   }
// }
