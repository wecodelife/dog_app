// To parse this JSON data, do
//
//     final getUserProfileResponse = getUserProfileResponseFromJson(jsonString);

import 'dart:convert';

GetUserProfileResponse getUserProfileResponseFromJson(String str) => GetUserProfileResponse.fromJson(json.decode(str));

String getUserProfileResponseToJson(GetUserProfileResponse data) => json.encode(data.toJson());

class GetUserProfileResponse {
  GetUserProfileResponse({
    this.id,
    this.lastLogin,
    this.isSuperuser,
    this.firstName,
    this.lastName,
    this.isStaff,
    this.dateJoined,
    this.username,
    this.uid,
    this.email,
    this.name,
    this.phoneNumber,
    this.lat,
    this.lon,
    this.profileImage,
    this.description,
    this.isActive,
    this.isUserHasDog,
    this.age,
    this.gender,
    this.dateOfBirth,
    this.breedType,
    this.groups,
    this.userPermissions,
  });

  final int id;
  final DateTime lastLogin;
  final bool isSuperuser;
  final String firstName;
  final String lastName;
  final bool isStaff;
  final DateTime dateJoined;
  final String username;
  final String uid;
  final String email;
  final String name;
  final String phoneNumber;
  final String lat;
  final String lon;
  final dynamic profileImage;
  final String description;
  final bool isActive;
  final bool isUserHasDog;
  final String age;
  final int gender;
  final DateTime dateOfBirth;
  final int breedType;
  final List<dynamic> groups;
  final List<dynamic> userPermissions;

  factory GetUserProfileResponse.fromJson(Map<String, dynamic> json) => GetUserProfileResponse(
    id: json["id"] == null ? null : json["id"],
    lastLogin: json["last_login"] == null ? null : DateTime.parse(json["last_login"]),
    isSuperuser: json["is_superuser"] == null ? null : json["is_superuser"],
    firstName: json["first_name"] == null ? null : json["first_name"],
    lastName: json["last_name"] == null ? null : json["last_name"],
    isStaff: json["is_staff"] == null ? null : json["is_staff"],
    dateJoined: json["date_joined"] == null ? null : DateTime.parse(json["date_joined"]),
    username: json["username"],
    uid: json["uid"] == null ? null : json["uid"],
    email: json["email"] == null ? null : json["email"],
    name: json["name"] == null ? null : json["name"],
    phoneNumber: json["phone_number"] == null ? null : json["phone_number"],
    lat: json["lat"] == null ? null : json["lat"],
    lon: json["lon"] == null ? null : json["lon"],
    profileImage: json["profile_image"] == null ? null : json["profile_image"],
    description: json["description"] == null ? null : json["description"],
    isActive: json["is_active"] == null ? null : json["is_active"],
    isUserHasDog: json["is_user_has_dog"] == null ? null : json["is_user_has_dog"],
    age: json["age"] == null ? null : json["age"],
    gender: json["gender"] == null ? null : json["gender"],
    dateOfBirth: json["date_of_birth"] == null ? null : DateTime.parse(json["date_of_birth"]),
    breedType: json["breed_type"] == null ? null : json["breed_type"],
    groups: json["groups"] == null ? null : List<dynamic>.from(json["groups"].map((x) => x)),
    userPermissions: json["user_permissions"] == null ? null : List<dynamic>.from(json["user_permissions"].map((x) => x)),
  );

  Map<String, dynamic> toJson() => {
    "id": id == null ? null : id,
    "last_login": lastLogin == null ? null : lastLogin.toIso8601String(),
    "is_superuser": isSuperuser == null ? null : isSuperuser,
    "first_name": firstName == null ? null : firstName,
    "last_name": lastName == null ? null : lastName,
    "is_staff": isStaff == null ? null : isStaff,
    "date_joined": dateJoined == null ? null : dateJoined.toIso8601String(),
    "username": username,
    "uid": uid == null ? null : uid,
    "email": email == null ? null : email,
    "name": name == null ? null : name,
    "phone_number": phoneNumber == null ? null : phoneNumber,
    "lat": lat == null ? null : lat,
    "lon": lon == null ? null : lon,
    "profile_image": profileImage == null ? null : profileImage,
    "description": description == null ? null : description,
    "is_active": isActive == null ? null : isActive,
    "is_user_has_dog": isUserHasDog == null ? null : isUserHasDog,
    "age": age == null ? null : age,
    "gender": gender == null ? null : gender,
    "date_of_birth": dateOfBirth == null ? null : dateOfBirth.toIso8601String(),
    "breed_type": breedType == null ? null : breedType,
    "groups": groups == null ? null : List<dynamic>.from(groups.map((x) => x)),
    "user_permissions": userPermissions == null ? null : List<dynamic>.from(userPermissions.map((x) => x)),
  };
}
