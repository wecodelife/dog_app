// To parse this JSON data, do
//
//     final productPaymentRequest = productPaymentRequestFromJson(jsonString);

import 'dart:convert';

ProductPaymentRequest productPaymentRequestFromJson(String str) => ProductPaymentRequest.fromJson(json.decode(str));

String productPaymentRequestToJson(ProductPaymentRequest data) => json.encode(data.toJson());

class ProductPaymentRequest {
  ProductPaymentRequest({
    this.orderAmount,
    this.orderCurrency,
    this.orderNote,
    this.customerName,
    this.customerPhone,
    this.customerEmail,
  });

  final String orderAmount;
  final String orderCurrency;
  final String orderNote;
  final String customerName;
  final String customerPhone;
  final String customerEmail;

  factory ProductPaymentRequest.fromJson(Map<String, dynamic> json) => ProductPaymentRequest(
    orderAmount: json["orderAmount"] == null ? null : json["orderAmount"],
    orderCurrency: json["orderCurrency"] == null ? null : json["orderCurrency"],
    orderNote: json["orderNote"] == null ? null : json["orderNote"],
    customerName: json["customerName"] == null ? null : json["customerName"],
    customerPhone: json["customerPhone"] == null ? null : json["customerPhone"],
    customerEmail: json["customerEmail"] == null ? null : json["customerEmail"],
  );

  Map<String, dynamic> toJson() => {
    "orderAmount": orderAmount == null ? null : orderAmount,
    "orderCurrency": orderCurrency == null ? null : orderCurrency,
    "orderNote": orderNote == null ? null : orderNote,
    "customerName": customerName == null ? null : customerName,
    "customerPhone": customerPhone == null ? null : customerPhone,
    "customerEmail": customerEmail == null ? null : customerEmail,
  };
}
