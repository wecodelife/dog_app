import 'dart:io';
import 'dart:typed_data';
import 'package:app_template/src/utils/utils.dart';
import 'package:app_template/src/widgets/home_page_cards.dart';
import 'package:firebase_dynamic_links/firebase_dynamic_links.dart';
import 'package:flutter/foundation.dart';
import 'package:esys_flutter_share/esys_flutter_share.dart';
import 'package:app_template/src/screens/home_page.dart';
// import 'package:share/share.dart';

class DynamicLinkService {
  // final NavigationService _navigationService = locator<NavigationService>();
  bool loading = true;
  String title;
  Future handleDynamicLinks(context) async {
    // Get the initial dynamic link if the app is opened with a dynamic link
    final PendingDynamicLinkData data =
        await FirebaseDynamicLinks.instance.getInitialLink();

    // handle link that has been retrieved
    _handleDeepLink(data, context);

    // Register a link callback to fire if the app is opened up from the background
    // using a dynamic link.
    FirebaseDynamicLinks.instance.onLink(
        onSuccess: (PendingDynamicLinkData dynamicLink) async {
      // handle link that has been retrieved
      _handleDeepLink(dynamicLink, context);
    }, onError: (OnLinkErrorException e) async {
      print('Link Failed: ${e.message}');
    });
  }

  void _handleDeepLink(PendingDynamicLinkData data, context) {
    final Uri deepLink = data?.link;
    if (deepLink != null) {
      print('_handleDeepLink | deeplink: $deepLink');
      print("hhgh" + deepLink.path.toString());
      var isPost = deepLink.pathSegments.contains('helloworld');
      if (isPost) {
        title = deepLink.queryParameters['user'];
        if (title != null) {
          // Navigator.pushNamed(context, deepLink.path);
          pushAndRemoveUntil(
              context,
              HomePageCard(
                //postId: int.parse(title),
              ),
              false);
          //   // _navigationService.navigateTo(CreatePostViewRoute, arguments: title);
        }
      }
    }
  }

  Future<bool> shareImageFromUrl(String id, context,
      {String imgUrl, String description}) async {
    try {
      var request = await HttpClient().getUrl(Uri.parse(imgUrl));
      var response = await request.close();
      Uint8List bytes = await consolidateHttpClientResponseBytes(response);
      await Share.file('WowBow', 'wowbow.jpg', bytes, 'image/jpg',
          text: description);
      // loading = false;
      return false;
    } catch (e) {
      print('error: $e');
      return false;
    }
  }

  Future<bool> createFirstPostLink(String id, context,
      {String imgUrl, String description, ValueChanged<bool> loadMore}) async {
    // showLoadingModal(context);
    // loading = true;
    // loadMore(true);
    final DynamicLinkParameters parameters = DynamicLinkParameters(
      uriPrefix: 'https://wowbow.page.link',
      link: Uri.parse('https://wecodelife/welcome/helloworld?user=$id'),
      androidParameters: AndroidParameters(
        packageName: 'com.wecodelife.dog',
      ),

      // Other things to add as an example. We don't need it now
      iosParameters: IosParameters(
        bundleId: 'com.example.ios',
        minimumVersion: '1.0.1',
        appStoreId: '123456789',
      ),
      googleAnalyticsParameters: GoogleAnalyticsParameters(
        campaign: 'example-promo',
        medium: 'social',
        source: 'orkut',
      ),
      itunesConnectAnalyticsParameters: ItunesConnectAnalyticsParameters(
        providerToken: '123456',
        campaignToken: 'example-promo',
      ),
      socialMetaTagParameters: SocialMetaTagParameters(
        title: 'Example of a Dynamic Link',
        description: 'This link works whether app is installed or not!',
      ),
    );
    final ShortDynamicLink shortLink = await parameters.buildShortLink();
    final Uri dynamicUrl = shortLink.shortUrl;
    loading = await shareImageFromUrl(id, context,
        imgUrl: imgUrl, description: dynamicUrl.toString());
    loadMore(loading);
    // final RenderBox box = context.findRenderObject();
    // Share.share(
    //     // [Urls.imageBaseUrl + "/medias/images/post/IMG-20210628-WA0022.jpg"],
    //     dynamicUrl.toString(),
    //     subject: "Test",
    //     sharePositionOrigin: Rect.fromLTWH(
    //         10,
    //         10,
    //         screenWidth(context, dividedBy: 1),
    //         screenHeight(context, dividedBy: 2.1)));
    // var request = await HttpClient().getUrl(Uri.parse(imgUrl));
    // var response = await request.close();
    // Uint8List bytes = await consolidateHttpClientResponseBytes(response);
    // await Share.file(description, 'amlog.jpg', bytes, 'image/jpg',
    //     text: dynamicUrl.toString());
    // print(dynamicUrl.toString());
    // return dynamicUrl.toString();
    return false;
  }
}
