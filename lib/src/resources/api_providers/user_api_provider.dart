import 'package:app_template/src/models/add_post_request.dart';
import 'package:app_template/src/models/add_post_response.dart';
import 'package:app_template/src/models/get_breed_response_model.dart';
import 'package:app_template/src/models/get_post_comment_response.dart';
import 'package:app_template/src/models/get_user_profile_response.dart';
import 'package:app_template/src/models/login_request_model.dart';
import 'package:app_template/src/models/login_response_model.dart';
import 'package:app_template/src/models/state.dart';
import 'package:app_template/src/models/user_sign_up_request_model.dart';
import 'package:app_template/src/models/user_sign_up_response_model.dart';
import 'package:app_template/src/utils/object_factory.dart';
import 'package:app_template/src/models/user_update_request_model.dart';
import 'package:dio/dio.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:app_template/src/models/user_update_response_model.dart';
import 'package:app_template/src/models/get_post_response.dart';
import 'package:app_template/src/models/add_post_comment_request.dart';
import 'package:app_template/src/models/add_post_comment_response.dart';
import 'package:app_template/src/models/post_update_request_model.dart';
import 'package:app_template/src/models/post_update_response_model.dart';
import 'package:app_template/src/models/get_logout_response.dart';
import 'package:app_template/src/models/username_request_model.dart';
import 'package:app_template/src/models/username_response_model.dart';
import 'package:app_template/src/models/like_post_request.dart';
import 'package:app_template/src/models/like_post_response.dart';
import 'package:app_template/src/models/get_image_slider.dart';
import 'package:app_template/src/models/get_product_category.dart';
import 'package:app_template/src/models/get_product_details.dart';
import 'package:app_template/src/models/get_cart_details.dart';
import 'package:app_template/src/models/get_wishlist_details.dart';
import 'package:app_template/src/models/add_cart_details_response.dart';
import 'package:app_template/src/models/add_cart_details_request.dart';
import 'package:app_template/src/models/add_wishlist_request.dart';
import 'package:app_template/src/models/add_wishlist_response.dart';
import 'package:app_template/src/models/get_product_review.dart';
import 'package:app_template/src/models/update_cart_count_response.dart';
import 'package:app_template/src/models/update_cart_count_request.dart';





class UserApiProvider {
  Future<State> loginCall(LoginRequest loginRequest) async {
    final response = await ObjectFactory().apiClient.loginRequest(loginRequest);
    print(response.toString());
    print("API provider OK");
    if (response.statusCode == 200) {
      return State<LoginResponse>.success(
          LoginResponse.fromJson(response.data));
    } else
      print("API provider not OK");
    return null;
  }

  Future<State> userSignUpCall(UserSignUpRequest userSignUpRequest) async {
    final response = await ObjectFactory().apiClient.userSignUp(
        userSignUpRequest: userSignUpRequest);
    print(response.toString());
    if (response.statusCode == 200 || response.statusCode == 201) {
      return State<UserSignUpResponseModel>.success(
          UserSignUpResponseModel.fromJson(response.data));
    } else
      return null;
  }

  Future<State> getBreed() async {
    final response = await ObjectFactory().apiClient.getBreed();
    print(response.toString());
    if (response.statusCode == 200 || response.statusCode == 201) {
      return State<GetBreedResponseModel>.success(
          GetBreedResponseModel.fromJson(response.data));
    } else
      return null;
  }

  Future<State> userUpdateRequest(UserUpdateRequestModel userUpdateRequest,
      String id) async {
    try {
      final response = await ObjectFactory().apiClient.userUpdate(
          id: id,
          userUpdateRequest: userUpdateRequest);
      print(response.toString());
      if (response.statusCode == 200 || response.statusCode == 201) {
        return State<UserUpdateResponseModel>.success(
            UserUpdateResponseModel.fromJson(response.data));
      } else
        return null;
    } on DioError catch (e) {
      print(e.message.toString() + "gfgfhg");
      if (e.type == DioErrorType.RESPONSE) {
        //This is the custom message coming from the backend
        print("Error " + e.response.data['error']);
        return State<LoginResponse>.success(
            LoginResponse.fromJson(e.response.data));
      } else {
        showToast(e.message);
        return null;
      }
    }
  }

  /// get posts
  Future<State> getPosts({String filter}) async {
    print("api provider ok");
    final response = await ObjectFactory().apiClient.getPosts(filter: filter);
    print(response.toString());
    if (response.statusCode == 200 || response.statusCode == 201) {
      return State<GetPostResponse>.success(
          GetPostResponse.fromJson(response.data));
    } else
      return null;
  }
  /// get userposts
  Future<State> getUserPosts({String id}) async {
    print("api provider ok");
    final response = await ObjectFactory().apiClient.getUserPosts(id: id);
    print(response.toString());
    if (response.statusCode == 200 || response.statusCode == 201) {
      return State<GetPostResponse>.success(
          GetPostResponse.fromJson(response.data));
    } else
      return null;
  }


  /// add posts
  Future<State> addPost(AddPostRequest addPostRequest) async {
    final response = await ObjectFactory().apiClient.addPost(addPostRequest);
    print(response.toString());
    if (response.statusCode == 200 || response.statusCode == 201) {
      return State<AddPostResponse>.success(
          AddPostResponse.fromJson(response.data));
    } else
      return null;
  }

  /// get post comments
  Future<State> getPostComments({String id}) async {
    print("post comment api provider ok");
    final response = await ObjectFactory().apiClient.getPostComments(id: id);
    print(response.toString());
    if (response.statusCode == 200 || response.statusCode == 201) {
      return State<GetPostCommentResponse>.success(
          GetPostCommentResponse.fromJson(response.data));
    } else
      return null;
  }

  /// add posts comments
  Future<State> addPostComments(
      AddPostCommentRequest addPostCommentRequest) async {
    final response = await ObjectFactory().apiClient.addPostComments(
        addPostCommentRequest);
    print(response.toString());
    if (response.statusCode == 200 || response.statusCode == 201) {
      print(" post comments api provider ok");
      return State<AddPostCommentResponse>.success(
          AddPostCommentResponse.fromJson(response.data));
    } else
      return null;
  }

  /// logout
  Future<State> logout() async {
    print("logout api provider ok");
    final response = await ObjectFactory().apiClient.logout();
    print(response.toString());
    if (response.statusCode == 200 || response.statusCode == 201) {
      return State<GetLogoutResponse>.success(
          GetLogoutResponse.fromJson(response.data));
    } else
      return null;
  }

  /// update post
  Future<State> postUpdate(UpdatePostRequest updatePostRequest,
      String id) async {
    try {
      final response = await ObjectFactory().apiClient.postUpdate(
          id: id,
          updatePostRequest: updatePostRequest);
      print(response.toString());
      if (response.statusCode == 200 || response.statusCode == 201) {
        return State<UpdatePostResponse>.success(
            UpdatePostResponse.fromJson(response.data));
      } else
        return null;
    }
    on DioError catch (e) {
      print(e.message.toString() + "gfgfhg");
      if (e.type == DioErrorType.RESPONSE) {
        //This is the custom message coming from the backend
        print("Error " + e.response.data['error']);
        return State<LoginResponse>.success(
            LoginResponse.fromJson(e.response.data));
      } else {
        showToast(e.message);
        return null;
        // }
      }
    }
  }

  /// delete post
  Future<State> deletePost(String id) async {
    try {
      final response = await ObjectFactory().apiClient.deletePosts(id);
      print("Post Deleted Successfully");
      if (response.statusCode == 204) {
        return State<String>.success("Post Deleted Successfully");
      } else
        return null;
    } on DioError catch (e) {
      // print("Error" + e.toString());
      if (e != null) {
        // print("username error      "+e.response.toString());
        //This is the custom message coming from the backend
        // showToast(e.response.data["status"].toString());
        // print("NetwrokError" + NetworkErrors.getDioException(e));
        // return State<String>.error(NetworkErrors.getDioException(e));
         // throw e.response.data.toString() + "ghhghgf";
      } else {
        throw Exception("Error");
      }
    }
  }

  /// get userProfile
  Future<State> getUserProfile({String id}) async {
    print("post comment api provider ok");
    final response = await ObjectFactory().apiClient.getUserProfile(id: id);
    print(response.toString());
    if (response.statusCode == 200 || response.statusCode == 201) {
      return State<GetUserProfileResponse>.success(
          GetUserProfileResponse.fromJson(response.data));
    } else
      return null;
  }

  /// username check
  Future<State> userCheck(UserNameRequest userNameRequest) async {
    try {
      print(" username check api provider ok");
      final response =
      await ObjectFactory().apiClient.userCheck(userNameRequest);
      print(response.toString());
      if (response.statusCode == 200 || response.statusCode == 201) {
        return State<UserNameResponse>.success(
            UserNameResponse.fromJson(response.data));
      } else
        return null;
    } on DioError catch (e) {
      print("Error   " + e.toString());
      if (e != null) {
      print("Errorrr     " +e.response.toString());
      //This is the custom message coming from the backend
      // showToast(e.response.data["status"].toString());
      //   print("NetwrokError" + NetworkErrors.getDioException(e));
      //   return State<String>.error(NetworkErrors.getDioException(e));
      // throw e.response.data.toString() + "ghhghgf";
      } else {
        throw Exception("Error");
      }
    }
  }

  /// add posts like
  Future<State> addPostLike(
      LikePostRequest likePostRequest) async {
    final response = await ObjectFactory().apiClient.addPostLike(
        likePostRequest);
    print(response.toString());
    if (response.statusCode == 200 || response.statusCode == 201) {
      print(" post like api provider ok");
      return State<LikePostResponse>.success(
          LikePostResponse.fromJson(response.data));
    } else
      return null;
  }

  /// dislike post
  Future<State> disLikePost({LikePostRequest likePostRequest, String id}) async {
    try {
      final response = await ObjectFactory().apiClient.disLikePost(
          likePostRequest: likePostRequest,
      id: id);
      print("Post Disliked Successfully");
      if (response.statusCode == 204) {
        return State<String>.success("Post Disliked Successfully");
      } else
        return null;
    } on DioError catch (e) {
      print("Error" + e.toString());
      if (e != null) {
        // print("username error      "+e.response.toString());
        //This is the custom message coming from the backend
        // showToast(e.response.data["status"].toString());
        // print("NetwrokError" + NetworkErrors.getDioException(e));
        // return State<String>.error(NetworkErrors.getDioException(e));
        // throw e.response.data.toString() + "ghhghgf";
      } else {
        throw Exception("Error");
      }
    }
  }

  ///get image slider
  Future<State> getImageSlider() async {
    print("api provider ok");
    final response = await ObjectFactory().apiClient.getImageSlider();
    print(response.toString());
    if (response.statusCode == 200 || response.statusCode == 201) {
      return State<GetImageSlider>.success(
          GetImageSlider.fromJson(response.data));
    } else
      return null;
  }

  ///get product category
  Future<State> getProductCategory() async {
    print("api provider ok");
    final response = await ObjectFactory().apiClient.getProductCategory();
    print(response.toString());
    if (response.statusCode == 200 || response.statusCode == 201) {
      return State<GetProductCategory>.success(
          GetProductCategory.fromJson(response.data));
    } else
      return null;
  }

  ///get product details
  Future<State> getProductDetails() async {
    print("api provider ok");
    final response = await ObjectFactory().apiClient.getProductDetails();
    print(response.toString());
    if (response.statusCode == 200 || response.statusCode == 201) {
      return State<GetProductDetails>.success(
          GetProductDetails.fromJson(response.data));
    } else
      return null;
  }

  ///get cart category
  Future<State> getCartDetails() async {
    print("api provider ok");
    final response = await ObjectFactory().apiClient.getCartDetails();
    print(response.toString());
    if (response.statusCode == 200 || response.statusCode == 201) {
      return State<GetCartDetails>.success(
          GetCartDetails.fromJson(response.data));
    } else
      return null;
  }

  ///get wishlist category
  Future<State> getWishlistDetails() async {
    print("api provider ok");
    final response = await ObjectFactory().apiClient.getWishlistDetails();
    print(response.toString());
    if (response.statusCode == 200 || response.statusCode == 201) {
      return State<GetWishlistDetails>.success(
          GetWishlistDetails.fromJson(response.data));
    } else
      return null;
  }

  /// add cart details
  Future<State> addCartDetails(
      AddCartDetailsRequest addCartDetailsRequest) async {
    final response = await ObjectFactory().apiClient.addCartDetails(
        addCartDetailsRequest);
    print(response.toString());
    if (response.statusCode == 200 || response.statusCode == 201) {
      print(" cart details api provider ok");
      return State<AddCartDetailsResponse>.success(
          AddCartDetailsResponse.fromJson(response.data));
    } else
      return null;
  }

  /// delete cart item
  Future<State> deleteCartItem(String id) async {
    try {
      final response = await ObjectFactory().apiClient.deleteCartItem(id);
      print("Cart Item Deleted Successfully");
      if (response.statusCode == 204) {
        return State<String>.success("Cart Item Deleted Successfully");
      } else
        return null;
    } on DioError catch (e) {
      // print("Error" + e.toString());
      if (e != null) {
        // print("username error      "+e.response.toString());
        //This is the custom message coming from the backend
        // showToast(e.response.data["status"].toString());
        // print("NetwrokError" + NetworkErrors.getDioException(e));
        // return State<String>.error(NetworkErrors.getDioException(e));
        // throw e.response.data.toString() + "ghhghgf";
      } else {
        throw Exception("Error");
      }
    }
  }

  /// add wishlist details
  Future<State> addWishlistDetails(
      AddWishlistRequest addWishlistRequest) async {
    final response = await ObjectFactory().apiClient.addWishlistDetails(
        addWishlistRequest);
    print(response.toString());
    if (response.statusCode == 200 || response.statusCode == 201) {
      print(" wishlist details api provider ok");
      return State<AddWishlistResponse>.success(
          AddWishlistResponse.fromJson(response.data));
    } else
      return null;
  }

  /// delete wishlist item
  Future<State>deleteWishlistItem(String id) async {
    try {
      final response = await ObjectFactory().apiClient.deleteWishlistItem(id);
      print("Wishlist Item Deleted Successfully");
      if (response.statusCode == 204) {
        return State<String>.success("Wishlist Item Deleted Successfully");
      } else
        return null;
    } on DioError catch (e) {
      // print("Error" + e.toString());
      if (e != null) {
        // print("username error      "+e.response.toString());
        //This is the custom message coming from the backend
        // showToast(e.response.data["status"].toString());
        // print("NetwrokError" + NetworkErrors.getDioException(e));
        // return State<String>.error(NetworkErrors.getDioException(e));
        // throw e.response.data.toString() + "ghhghgf";
      } else {
        throw Exception("Error");
      }
    }
  }

  ///get cart category
  Future<State> getProductReview() async {
    print("api provider ok");
    final response = await ObjectFactory().apiClient.getProductReview();
    print(response.toString());
    if (response.statusCode == 200 || response.statusCode == 201) {
      return State<GetProductReview>.success(
          GetProductReview.fromJson(response.data));
    } else
      return null;
  }

  ///update cart count
  Future<State> updateCartCount(String id,UpdateCartCountRequest updateCartCountRequest) async {
    print("api provider ok");
    final response = await ObjectFactory().apiClient.updateCartCount(id,updateCartCountRequest);
    print(response.toString());
    if (response.statusCode == 200 || response.statusCode == 201) {
      return State<UpdateCartCountResponse>.success(
          UpdateCartCountResponse.fromJson(response.data));
    } else
      return null;
  }
}