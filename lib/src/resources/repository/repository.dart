import 'package:app_template/src/models/add_post_request.dart';
import 'package:app_template/src/models/login_request_model.dart';
import 'package:app_template/src/models/state.dart';
import 'package:app_template/src/models/user_sign_up_request_model.dart';
import 'package:app_template/src/models/user_update_request_model.dart';
import 'package:app_template/src/resources/api_providers/user_api_provider.dart';
import 'package:app_template/src/models/add_post_comment_request.dart';
import 'package:app_template/src/models/post_update_request_model.dart';
import 'package:app_template/src/models/username_request_model.dart';
import 'package:app_template/src/models/like_post_request.dart';
import 'package:app_template/src/models/add_cart_details_request.dart';
import 'package:app_template/src/models/add_wishlist_request.dart';
import 'package:app_template/src/models/update_cart_count_request.dart';

/// Repository is an intermediary class between network and data
class Repository {
  final userApiProvider = UserApiProvider();

  Future<State> login({LoginRequest loginRequest}) =>
      UserApiProvider().loginCall(loginRequest);

  Future<State> userSignUp ({UserSignUpRequest userSignUpRequest}) =>
      UserApiProvider().userSignUpCall(userSignUpRequest);
  Future<State> getBreed () =>
      UserApiProvider().getBreed();
  Future<State> userUpdate ({UserUpdateRequestModel userUpdateRequest, String id}) =>
      UserApiProvider().userUpdateRequest(userUpdateRequest,id);

  Future<State> getPosts({String filter}) => UserApiProvider().getPosts(filter: filter);
  Future<State> getUserPosts({String id}) => UserApiProvider().getUserPosts(id: id);
  Future<State> addPost(AddPostRequest addPostRequest) =>
      UserApiProvider().addPost(addPostRequest);

  Future<State> getPostComments({String id}) => UserApiProvider().getPostComments(id: id);
  Future<State> addPostComments(AddPostCommentRequest addPostCommentRequest) =>
      UserApiProvider().addPostComments(addPostCommentRequest);

  Future<State> logout() => UserApiProvider().logout();

  Future<State> postUpdate ({UpdatePostRequest updatePostRequest, String id}) =>
      UserApiProvider().postUpdate(updatePostRequest, id);

  Future<State> deletePost(String id) => UserApiProvider().deletePost(id);

  Future<State> getUserProfile({String id}) => UserApiProvider().getUserProfile(id: id);

  Future<State> userCheck(UserNameRequest userNameRequest) =>
      UserApiProvider().userCheck(userNameRequest);

  Future<State> addPostLike(LikePostRequest likePostRequest) =>
      UserApiProvider().addPostLike(likePostRequest);

  Future<State> dislikePost({LikePostRequest likePostRequest, String id}) =>
      UserApiProvider().disLikePost(likePostRequest:likePostRequest, id:id);

  Future<State> getImageSlider() =>
      UserApiProvider().getImageSlider();

  Future<State> getProductCategory() =>
      UserApiProvider().getProductCategory();

  Future<State> getProductDetails() =>
      UserApiProvider().getProductDetails();

  Future<State> getCartDetails() =>
      UserApiProvider().getCartDetails();

  Future<State> getWishlistDetails() =>
      UserApiProvider().getWishlistDetails();

  Future<State> addCartDetails(AddCartDetailsRequest addCartDetailsRequest) =>
      UserApiProvider().addCartDetails(addCartDetailsRequest);

  Future<State> deleteCartItem(String id) => UserApiProvider().deleteCartItem(id);

  Future<State> addWishlistDetails(AddWishlistRequest addWishlistRequest) =>
      UserApiProvider().addWishlistDetails(addWishlistRequest);

  Future<State> deleteWishlistItem(String id) => UserApiProvider().deleteWishlistItem(id);

  Future<State> getProductReview() =>
      UserApiProvider().getProductReview();


  Future<State> updateCartCount(String id,UpdateCartCountRequest updateCartCountRequest) =>
      UserApiProvider().updateCartCount(id,updateCartCountRequest);

}
